
QWT=qwt-6.1.0


all: $(QWT)/lib/libqwt.a build/CMakeCache.txt
	(cd build; $(MAKE) all)

# all: build/CMakeCache.txt
# 	(cd build; $(MAKE) all)
# 
build: 
	mkdir -p build

build/CMakeCache.txt: build $(QWT)/lib/libqwt.a
	(cd build; cmake ..)

$(QWT)/lib/libqwt.a : $(QWT)/Makefile
	(cd $(QWT); $(MAKE)) 

$(QWT): $(QWT).tar.bz2
	tar jxf $<
	(cd $(QWT); rm -f Makefile */Makefile)
	touch $(QWT)/qwt.pro

$(QWT)/Makefile : $(QWT)
	(cd $(QWT); qmake)

clean:
	(cd build; $(MAKE) clean)
	(cd $(QWT); $(MAKE) clean)
	rm -rf build

qwt-clean:
	rm -rf $(QWT)

apt-get:
	sudo apt-get install libcgal-dev libboost-dev qtbase5-dev libqt5svg5-dev 

