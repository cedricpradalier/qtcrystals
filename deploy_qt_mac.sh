#!/bin/bash

set -x -e

QT_MAJOR=5
QT_MINOR=5
QWT_MAJOR=6
QWT_ALL=${QWT_MAJOR}.1.0
RELEASE="$1"
if test x$RELEASE = x
then
    RELEASE=build-qtcrystals-Desktop_Qt_${QT_MAJOR}_${QT_MINOR}_0_clang_64bit-Release/qtcrystals.app
else
    RELEASE="$RELEASE"/qtcrystals.app
fi

cd $RELEASE
mkdir -p Contents/lib
BINARY=Contents/MacOs/qtcrystals
#QT=/Applications/QT/${QT_MAJOR}.${QT_MINOR}/clang_64
QT=/opt/Qt/${QT_MAJOR}.${QT_MINOR}/clang_64

QWT=qwt.framework/Versions/$QWT_MAJOR/qwt

mkdir -p Contents/Frameworks
rsync -vaP /usr/local/qwt-6.1.0/lib/qwt.framework Contents/Frameworks

QT_COMPONENTS="QtCore QtConcurrent QtSvg QtWidgets QtGui QtPrintSupport QtOpenGL"
for p in $QT_COMPONENTS
do
    rsync -vaP $QT/lib/$p.framework Contents/Frameworks
done

PLUGINS="libqcocoa_debug.dylib libqcocoa.dylib libqminimal_debug.dylib libqminimal.dylib libqoffscreen_debug.dylib libqoffscreen.dylib"
mkdir -p Contents/plugins
rsync -vaP $QT/plugins/platforms Contents/plugins

rsync -vaP /usr/lib/libsqlite3*.dylib Contents/lib
rsync -vaP /opt/local/lib/libCGAL*.dylib Contents/lib
rsync -vaP /opt/local/lib/libmpfr*.dylib Contents/lib
rsync -vaP /opt/local/lib/libgmp*.dylib Contents/lib
rsync -vaP /opt/local/lib/libboost_thread*.dylib Contents/lib
rsync -vaP /opt/local/lib/libboost_system-mt*.dylib Contents/lib
rsync -vaP /opt/local/lib/libboost_filesystem-mt*.dylib Contents/lib
rsync -vaP /opt/local/lib/libboost_date_time-mt*.dylib Contents/lib

LIBS="/usr/lib/libsqlite3.dylib /opt/local/lib/libCGAL.11.dylib /opt/local/lib/libmpfr.4.dylib /opt/local/lib/libgmp.10.dylib /opt/local/lib/libboost_thread-mt.dylib /opt/local/lib/libboost_system-mt.dylib /opt/local/lib/libboost_filesystem-mt.dylib /opt/local/lib/libboost_date_time-mt.dylib"


install_name_tool -id @executable_path/../Frameworks/$QWT \
            Contents/Frameworks/$QWT
install_name_tool -change \
            /urs/local/qwt-${QWT_ALL}/lib/$QWT \
            @executable_path/../Frameworks/$QWT \
            $BINARY

install_name_tool -change qwt.framework/Versions/${QWT_MAJOR}/qwt \
            @executable_path/../Frameworks/qwt.framework/Versions/${QWT_MAJOR}/qwt \
            $BINARY


install_name_tool -id @executable_path/../plugins/platforms/libqcocoa.dylib \
            Contents/plugins/platforms/libqcocoa.dylib
#install_name_tool -change \
#            $QT/plugins/platforms/libqcocoa.dylib \
#            @executable_path/../plugins/libqcocoa.dylib \
#            $BINARY

#for p in $PLUGINS
#do
#    install_name_tool -id @executable_path/../plugins/$p \
#            Contents/plugins/$p

#    install_name_tool -change \
#                        $QT/plugins/platforms/$p \
#                        @executable_path/../plugins/$p \
#                        $BINARY
#done


for f in $QT_COMPONENTS
do
    echo $f
    #ls -l Contents/Frameworks/$f.framework/Versions/5/$f
    install_name_tool -id @executable_path/../Frameworks/$f.framework/Versions/5/$f \
            Contents/Frameworks/$f.framework/Versions/5/$f
    install_name_tool -change \
            $QT/lib/$f.framework/Versions/5/$f \
            @executable_path/../Frameworks/$f.framework/Versions/5/$f \
            $BINARY

    for g in $QT_COMPONENTS
    do
        install_name_tool -change $QT/lib/$g.framework/Versions/5/$g \
                     @executable_path/../Frameworks/$g.framework/Versions/5/$g \
                     Contents/Frameworks/$f.framework/Versions/5/$f

    done

#    for h in $PLUGINS
#    do
        install_name_tool -change $QT/lib/$f.framework/Versions/5/$f \
                     @executable_path/../Frameworks/$f.framework/Versions/5/$f \
                     Contents/plugins/platforms/libqcocoa.dylib

#    done

    install_name_tool -change $QT/lib/$f.framework/Versions/5/$f \
                     @executable_path/../Frameworks/$f.framework/Versions/5/$f \
                     Contents/Frameworks/$QWT

    install_name_tool -change $QT/lib/$f.framework/Versions/5/$f \
                    @executable_path/../Frameworks/$f.framework/Versions/5/$f \
                    Contents/plugins/platforms/libqcocoa.dylib


done

for f in $LIBS
do
	install_name_tool -id @executable_path/../lib/`basename $f` Contents/lib/`basename $f`
        install_name_tool -change $f @executable_path/../lib/`basename $f` $BINARY

        for g in $LIBS
        do
            install_name_tool -change $f @executable_path/../lib/`basename $f` Contents/lib/`basename $g`
        done
done



otool -L $BINARY
