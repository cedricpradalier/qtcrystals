#!/bin/sh

source="$1"
target=`basename "$source" .png`.iconset
mkdir -p $target

convert "$source" -geometry 16x16! $target/icon_16x16.png
convert "$source" -geometry 16x16! $target/icon_16x16@2x.png
convert "$source" -geometry 32x32! $target/icon_32x32.png
convert "$source" -geometry 32x32! $target/icon_32x32@2x.png
convert "$source" -geometry 128x128! $target/icon_128x128.png
convert "$source" -geometry 128x128! $target/icon_128x128@2x.png
convert "$source" -geometry 256x256! $target/icon_256x256.png
convert "$source" -geometry 256x256! $target/icon_256x256@2x.png
convert "$source" -geometry 512x512! $target/icon_512x512.png
convert "$source" -geometry 512x512! $target/icon_512x512@2x.png


