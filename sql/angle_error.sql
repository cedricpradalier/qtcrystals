SELECT load_extension('./lib/libsqlitefunctions.so');

drop view if exists JointOrientation;
create temp view JointOrientation as 
    select jo.hist_id,angle_deg
    from JointOrientationHistogram jo
    inner join(
        select hist_id, max(occurence) as occ
        from JointOrientationHistogram
        group by hist_id
    ) ss on jo.hist_id = ss.hist_id and jo.occurence = ss.occ;

.sep ,
.output "joint.csv"
select E.i,E.j,E.twinning,
    K1_X*cos(radians(O.angle_deg))+K1_Y*sin(radians(O.angle_deg))
from ConnectedEdges as E 
left join Connected as C1 on C1.id=E.i
left join Connected as C2 on C2.id=E.j
left join JointOrientation O on E.Hist_ID=O.Hist_ID
where E.active and C1.is_parent and C1.grain=C2.grain and not C2.is_parent 
and E.twinning>0 and C2.twinstrip>=0 and not C1.map_edge and not C2.map_edge; 


.sep ,
.output "nojoint.csv"
select E.i,E.j,E.twinning,
    K1_X*cos(radians(O.angle_deg))+K1_Y*sin(radians(O.angle_deg))
from ConnectedEdges as E 
left join Connected as C1 on C1.id=E.i
left join Connected as C2 on C2.id=E.j
left join JointOrientation O on E.Hist_ID=O.Hist_ID
where not E.active and E.i<E.j and C1.twinstrip>=0
and C2.twinstrip>=0 and not C1.map_edge and not C2.map_edge; 

-- .sep ,
-- .output "nojoint.csv"
-- select E.i,E.j,E.twinning,abs(remainder(angle_deg-degrees(atan2(QY,QX)),180))
-- from ConnectedEdges as E 
-- left join Connected as C1 on C1.id=E.i
-- left join Connected as C2 on C2.id=E.j
-- left join JointOrientation O on E.Hist_ID=O.Hist_ID
-- where not E.active and C1.is_parent and C1.grain=C2.grain and not C2.is_parent 
--  and C2.twinstrip<=0 and not C1.map_edge and not C2.map_edge; 
