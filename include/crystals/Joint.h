#ifndef CRYSTALS_JOINT_H
#define CRYSTALS_JOINT_H

#include <stdint.h>
#include <vector>
#include "crystals/Vertex.h"
#include "crystals/Edge.h"
#include "crystals/Face.h"

namespace crystals {

    struct Contour;

    struct Joint {
        uint32_t ref_edge; // First edge, used to define who is left and right
        int32_t grain_1, grain_2;
        int32_t connected_1, connected_2;
        bool grain_joint;
        bool connected_joint;
        std::vector<uint32_t> face_ids;

        Joint();

        uint32_t first_face() const;
        uint32_t last_face() const;

        bool buildFromBranch(std::vector<Face> & faces, 
                const std::vector<Vertex> & vertices, 
                const std::vector<Edge> & edges, 
                uint32_t id, uint8_t direction);

        static bool orderJointVector(std::vector<Contour> & contours,
                const std::vector<int32_t> & joint_ids,
                const std::vector<Joint> & joints,
                const std::vector<Face> & faces);

        void updateGrains(const std::vector<Vertex> & vertices, 
                const std::vector<Edge> & edges); 
    };

    struct Contour {
        bool hole;
        std::vector<uint32_t> face_ids;
        std::map<uint32_t,uint32_t> face_idx; // mapping between face id and index
        std::vector<float> orientation; // In degrees in ]-90,90]
        // Careful, joint_ids must be the actual joint_ids+1, with a sign
        // telling which direction to consider the joint. This should only be
        // used by orderJointVector
        Contour(bool is_hole, const std::vector<int32_t> & joint_ids,
                const std::vector<Joint> & joints,
                const std::vector<Face> & faces);
    };

};

#endif // CRYSTALS_JOINT_H
