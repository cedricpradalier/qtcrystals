#ifndef CRYSTALS_CONNECTED_CLUSTER_H
#define CRYSTALS_CONNECTED_CLUSTER_H

#include "crystals/quaternion_tools.h"

namespace crystals {
    struct ConnectedCluster {
        bool parent;
        int twinning_order;
        double x, y;
        size_t size;
        size_t group;
        tf::Quaternion q_mean;
        std::vector<uint32_t> parts;

        ConnectedCluster() : parent(false), twinning_order(-1) , size(0){}
    };

};




#endif // CRYSTALS_CONNECTED_CLUSTER_H
