#ifndef CRYSTALS_CONNECTED_NEIGHBOUR_H
#define CRYSTALS_CONNECTED_NEIGHBOUR_H

#include <vector>
#include <map>
#include <LinearMath/Quaternion.h>
#include "crystals/quaternion_tools.h"
#include "crystals/constants.h"
#include "crystals/EBSDSurface.h"

namespace crystals {

    struct ConnectedNeighbour : public EBSDNeighbour {
        std::map<TwinningType,uint32_t> twinning_weight;
        // edges involved in the this neighbour
        std::vector<uint32_t> edges;
        bool embedded;
        bool active;
        bool manual;
        bool ignore_for_phases;
        // twinning parameters, according to P.A. Juan
        bool twinning; 
        float alpha; 
        float beta;  
        TwinningType twinning_type;
        unsigned int twinning_variant;
        float twinning_error;
        tf::Quaternion twinning_error_q;
        ConnectedNeighbour();
        ConnectedNeighbour(int _i, int _j, bool sg=false);
        // assumption, id has not been inserted before
        void addEdge(size_t id, TwinningType type, size_t weight);
        void updateTwinning(const EBSDSurface<ConnectedNeighbour> & parent);
        void updateTwinning(const tf::Quaternion & qmean);
        void updateTwinning();
    };


};


#endif // CRYSTALS_CONNECTED_NEIGHBOUR_H
