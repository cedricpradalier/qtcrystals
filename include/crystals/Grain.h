#ifndef CRYSTALS_GRAIN_H
#define CRYSTALS_GRAIN_H

#include "crystals/quaternion_tools.h"
#include "crystals/convex_hull.h"
#include "crystals/Face.h"
#include "crystals/EBSDSurface.h"

namespace crystals {

        struct GrainNeighbour : public EBSDNeighbour {
            GrainNeighbour():EBSDNeighbour() {}
            GrainNeighbour(int _i, int _j): EBSDNeighbour(_i,_j) {}
            GrainNeighbour(int _i, int _j, size_t w, const tf::Quaternion & q)
                : EBSDNeighbour(_i,_j,w,q) {}
        };

        struct Grain : public EBSDSurface<GrainNeighbour> {
            double disp_x, disp_y; // for display only
            size_t size, border_size;
            size_t parent_cluster;
            bool active;
            bool refined;
            double convex_area, area, border_length;
            // convexity?
            size_t n_in, n_out;
            double convexity_ratio;
            tf::Quaternion qmean;
            std::vector<uint32_t> parts;
            std::vector<uint32_t> border_vertices;
            std::vector<uint32_t> twin_border_vertices;
            CrystalPolygon border_hull;

            Grain() ;
            Grain(size_t id) ;

            void add_neighbour(int id, size_t weight,const tf::Quaternion & q);
            void add_joint(size_t grain_id, size_t joint_id);
        };

};



#endif // CRYSTALS_GRAIN_H
