#ifndef CRYSTALS_FACE_H
#define CRYSTALS_FACE_H

#include <stdint.h>
#include <vector>
#include "crystals/Vertex.h"
#include "crystals/Edge.h"

namespace crystals {

    typedef enum {
        UnclassifiedFace,
        InteriorFace,
        JointFace,
        JointBranchFace,
        EndOfJointFace
    } FaceType;

    struct Face {
        // Static variable as I don't see how it could vary within one map
        static uint8_t FaceSize;
        int32_t i,j;
        FaceType type;
        float x,y;
        // 4 elements, to keep the same structure for square meshes and triangular
        // meshes.
        uint32_t vertices[4];
        uint32_t edges[4];
        // Order of neighbour must verify that going to neighbour i requires
        // crossing edge i
        int32_t neighbours[4];
        bool border;
        bool grain_border;
        bool map_edge;
        bool up;
        bool visited;

        static void resetFaceSize() {FaceSize=0;}

        Face(int32_t a, int32_t b, bool point_up,
                const std::vector<Vertex> & v, const std::vector<Edge> & e, 
                uint32_t e1, uint32_t e2, uint32_t e3);
        Face(int32_t a, int32_t b, 
                const std::vector<Vertex> & v, const std::vector<Edge> & e, 
                uint32_t e1, uint32_t e2, uint32_t e3, uint32_t e4);

        void updateFaceType(const std::vector<Vertex> & v, 
                const std::vector<Edge> & e);
        void updateBorders(const std::vector<Vertex> & v);
    };

};

#endif // CRYSTALS_FACE_H
