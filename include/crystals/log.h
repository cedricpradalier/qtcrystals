#ifndef CRYSTALS_LOG_H
#define CRYSTALS_LOG_H
#include <stdarg.h>

namespace crystals {
    typedef enum {LogInfo,LogError} LogType;
    typedef void (*LoggerFunction)(LogType t, const char*format,va_list ap);

    void setLogger(LoggerFunction f);

    void LOG_INFO(const char * format, ...);
    void LOG_ERROR(const char * format, ...);

}

#endif // CRYSTALS_LOG_H
