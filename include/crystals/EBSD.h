#ifndef EBSD_ANALIZER_H
#define EBSD_ANALIZER_H

#include "crystals/quaternion_tools.h"
#include <QObject>
#include <QImage>
#include <QColor>
#include <QFont>
#include <QRect>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
// #include <unordered_map>
#include <map>

#include "crystals/Vertex.h"
#include "crystals/Edge.h"
#include "crystals/Face.h"
#include "crystals/Connected.h"
#include "crystals/ConnectedNeighbour.h"
#include "crystals/Grain.h"
#include "crystals/Phase.h"
#include "crystals/ConnectedCluster.h"
#include "crystals/ConnectedTwins.h"
#include "crystals/convex_hull.h"
#include "crystals/Histogram.h"

struct sqlite3;

namespace crystals {

    typedef std::pair<int32_t,int32_t> ICoord;
    typedef std::map<ICoord,uint32_t> CoordMap;

    typedef enum {
        FileTypeStandard,
        FileTypeCTF,
        FileTypeCPR
    } EBSDFileType ;

    const char * EBSDFileTypeToString(EBSDFileType ft);

    class EBSDAnalyzer {
        public:
            typedef enum {NO_GRAPH = 0, 
                GRAPH_CONNECTED, 
                GRAPH_GRAINS, 
                GRAPH_CLUSTERS, 
                GRAPH_TWINNING, 
                GRAPH_HULLS, 
                GRAPH_ELLIPSES, 
                GRAPH_CONNECTED_TWINS, 
                GRAPH_GRAIN_JOINTS, 
                NUM_GRAPH_MODES
            } GraphModes;
        protected:

            bool square_mesh;
            std::vector<Vertex> vertices;
            std::vector< Edge > edges;
            std::vector< Face > faces;
            std::vector< Joint > joints;
            std::vector< Connected > connected;
            std::vector< Grain > grains;
            std::vector< Phase > phases;
            std::vector< ConnectedCluster > clusters;
            std::vector< TwinStrip > twinstrips;
            ConnectedTwins ctwin_in_progress;
            std::vector< ConnectedTwins > ctwins;
            std::vector<double> tensorInput;
            std::vector<double> loadingDir;
            CoordMap coords;

            // In theory we need one per grain, but that could be too many of them.
            size_t num_threads;
            mutable QMutex connected_mutex;
            void lock_connected() const {connected_mutex.lock();}
            void unlock_connected() const {connected_mutex.unlock();}

            GraphModes graph_mode;
            float x_min, y_min;
            float x_max, y_max;
            float min_grain_size;
            bool replace_twinstrips;
            bool displayTwinJoint;
            bool displayGrainJoint;
            bool colorTwinningLinks;


            // For display
            int dx, dy;
            float x_org, y_org;
            float x_scale;
            float y_scale;
            uint32_t width, height;
            bool change_zoom;
            QFont idFontFg,idFontBg;
            QImage mesh,display;
            bool isInFrame(const QPoint & P,bool strict=true) const;

            bool find_grain_clusters();
            bool update_twinning_order();

            ConnectedCluster& get_vertex_cluster(int vertex_id);
            ConnectedCluster& get_grain_cluster(int grain_id);
            Grain& get_vertex_group(int vertex_id);
            Grain& get_grain_group(int grain_id);

            bool build_faces();
            bool build_face_and_joints();
            bool update_face_and_joints();

            void update_group_ids(Grain & gg) ;
            void update_group_properties(Grain & gout) ;
            void update_group_properties(Grain & gout, 
                    const std::map<size_t,size_t> & cgroup) const ;
            bool do_split_group(size_t g, size_t edge_i, size_t edge_j, Grain gsplit[2]) ;
            void update_grain_groups();

#ifdef USE_CGAL
            CrystalPolygon ebsd_footprint;
            K2Kf to_inexact;
#endif
            void drawId(QPainter & painter,const QPoint & P, int id,bool shift=true) ;
            void drawLine(QPainter & painter,const QPoint & P1, const QPoint & P2, 
                    const QColor & color, int width=1,bool dashed=false);
            void drawCircle(QPainter & painter,const QPoint & P1, int radius, const QColor & color, int width);
            void drawEllipse(QPainter & painter,const QPoint & C, const QSize & S, 
                    int angle_deg, const QColor & color, int width);

            sqlite3 *db;
            std::string db_filename;
            bool connect_db(const std::string & dbname);
            bool close_db();

        public:
            EBSDAnalyzer(size_t threads, size_t w, size_t h): tensorInput(9,0.0),loadingDir(3,0.0),num_threads(threads), 
            width((uint32_t)w), height((uint32_t)h), mesh((uint32_t)width,(uint32_t)height,QImage::Format_RGB32),
            display((uint32_t)width,(uint32_t)height,QImage::Format_RGB32) {
                if (num_threads<1) num_threads = 1;
                min_grain_size = 0.0;
                mesh.fill(QColor(0,0,0));
                display.fill(QColor(0,0,0));
                change_zoom = true; dx = dy = 0;
                replace_twinstrips = false;
                displayTwinJoint = true;
                displayGrainJoint = true;
                colorTwinningLinks = true;
                db = NULL;
                tensorInput[0] = 1.0;
                loadingDir[0] = 1.0;
            }

            virtual ~EBSDAnalyzer();

            bool parse_file(const std::string & sourcename,EBSDFileType filetype,bool radian);

            const QImage & get_image() const {
                return display;
            }

            QRect get_view() const {
                double x_ratio = width / (x_max - x_min);
                double y_ratio = height / (y_max - y_min);
                return QRect(QPoint((x_org-x_min) * x_ratio, (y_org-y_min) * y_ratio),
                        QSize(width*x_scale*x_ratio, height*y_scale*y_ratio));
            }

            QRectF get_view_ratio() const {
                double x_ratio = 1. / (x_max - x_min);
                double y_ratio = 1. / (y_max - y_min);
                return QRectF(QPointF((x_org-x_min) * x_ratio, (y_org-y_min) * y_ratio),
                        QSizeF(width*x_scale*x_ratio, height*y_scale*y_ratio));
            }

            void set_mesh_size(size_t width,size_t height,bool maximize=false);
            size_t get_mesh_width() const {return width;}
            size_t get_mesh_height() const {return height;}

            void force_repaint();
            void set_min_grain_size(float size) {
                min_grain_size = size;
                force_repaint();
            }
            void set_twinstrip_mode(bool tmode) {
                replace_twinstrips = tmode;
                force_repaint();
            }
            void toggle_twinstrip_mode() {
                replace_twinstrips = !replace_twinstrips;
                force_repaint();
            }

            void highlightTwinJoint(bool b) {
                displayTwinJoint = b;
            }

            void highlightGrainJoint(bool b) {
                displayGrainJoint = b;
            }

            void alwaysColorTwinningLinks(bool b) {
                colorTwinningLinks = b;
            }

            void reset_zoom();
            void zoom_to(int x, int y, double ratio);
            void zoom(double ratio);
            void translate(int x, int y);
            void pan_to(int x, int y);
            bool pan_to_object();
            bool pan_to_object(int id);
            QPoint toImage(double x, double y) {
                return QPoint((x - x_org) / x_scale, (y - y_org) / y_scale);
            }
            template <class C>
                QPoint toImage(const C & c) {
                    return QPoint((c.x - x_org) / x_scale, (c.y - y_org) / y_scale);
                }
            template <class C>
                QPoint toImageF(const C & c) {
                    return QPoint((c.x() - x_org) / x_scale, (c.y() - y_org) / y_scale);
                }

            bool handle_edge_click(int x, int y,std::string & output);
            bool handle_edge_click(int x, int y);

            bool update_image(GraphModes with_graph, bool ignore_map_edge, bool draw_ids);

            bool build_connected_parts();

            bool build_grain_groups(bool refine);


            bool prepare_edge_and_color(std::vector<Edge> & edges, size_t v_start, size_t v_end);
            void update_group_properties(size_t g) {
                update_group_properties(grains[g]);
            }

            bool update_grain_properties(QWaitCondition * cond, 
                    QMutex * sync1, QMutex * sync2,
                    size_t v_start, size_t v_end, 
                    size_t e_start, size_t e_end, 
                    size_t c_start, size_t c_end);
            bool test_split_group(size_t g, size_t edge_i, size_t edge_j, Grain split[2]) const;
            bool update_grain_properties();
            size_t update_edge_type(size_t start, size_t end);

            bool save_to_db(const std::string & sourcename, bool measurements=false, const std::string & filename = "grains.sql", bool update = false);
            bool load_settings_from_db(const std::string & sourcename,
                    const std::string & dbname);
            bool load_connected_settings_from_db(const std::string & sourcename, 
                    const std::string & dbname);
            bool update_settings(const std::string & name, const std::string & value);
            bool update_connected_settings(size_t id, bool is_parent, 
                    bool is_manual_parent, bool is_manual_twin);
            bool update_connected_edge_settings(size_t i,size_t j, bool is_active, bool is_manual);
            bool add_connected_twins(size_t i, size_t j, bool is_merged);


            // Histogram collection
            CountHistogram getGrainAreaHistogram() const;


            const std::vector<double> & getTensorInput() const {
                return tensorInput;
            }
            void setTensorInput(const std::vector<double> & ti) {
                tensorInput = ti;
            }
            const std::vector<double> & getLoadingDir() const {
                return loadingDir;
            }
            void setLoadingDir(const std::vector<double> & ld) {
                loadingDir = ld;
            }
    };

};

#endif // EBSD_ANALIZER_H
