#ifndef CRYSTALS_HISTOGRAM_H
#define CRYSTALS_HISTOGRAM_H

#include <cmath>

#include <vector>
#include <algorithm>

namespace crystals {

    typedef std::pair<double,double> Interval;
    typedef std::pair<Interval,size_t> CountSample;
    typedef std::pair<Interval,double> FreqSample;
    typedef std::vector<CountSample> CountHistogram;
    typedef std::vector<FreqSample> FreqHistogram;


    template <class iterator>
    CountHistogram buildHistogram(iterator start, iterator end, size_t num_intervals=10,double min=NAN,double max=NAN) {
        if (std::isnan(min)) {
            for (iterator it=start;it!=end;it++) {
                if (std::isnan(min)) {
                    min = *it;
                } else {
                    min = std::min(min,(double)*it);
                }
            }
        }
        if (std::isnan(max)) {
            for (iterator it=start;it!=end;it++) {
                if (std::isnan(max)) {
                    max = *it;
                } else {
                    max = std::max(max,(double)*it);
                }
            }
        }
        CountHistogram h;
        double delta = (max-min)/num_intervals;
        for (size_t i=0;i<num_intervals;i++) {
            h.push_back(CountSample(Interval(min+i*delta,min+(i+1)*delta),0));
        }
        for (iterator it=start;it!=end;it++) {
            size_t i = std::max(0,std::min((int)(h.size()-1),(int)floor(((double)*it-min)/delta)));
            h[i].second++;
        }
        return h;
    }

    FreqHistogram countToFrequency(const CountHistogram & h);

    template <class iterator>
    FreqHistogram buildFrequency(iterator start, iterator end, size_t num_intervals=10,double min=NAN,double max=NAN) {
        CountHistogram h = buildHistogram(start,end,num_intervals,min,max);
        return countToFrequency(h);
    }
};

#endif // CRYSTALS_HISTOGRAM_H
