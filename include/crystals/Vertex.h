#ifndef CRYSTALS_VERTEX_H
#define CRYSTALS_VERTEX_H

#include <map>
#include <string>
#include <LinearMath/Quaternion.h>
#include "crystals/constants.h"
#include "crystals/color_tools.h"

namespace crystals {

    struct SimpleVertex {
        float x,y;
        int32_t i,j;
    };

    struct Vertex : public SimpleVertex {
        // Stores the mapping between phase name and id
        float phi1, PHI, phi2;
        float iq, ci, fit;
        tf::Quaternion q;
        // Good measurement, based on iq, ci and membership into a large enough
        // connected component
        bool good;
        bool border;
        bool grain_border;
        int32_t membership;
        int32_t grain;
        MaterialEnum mat_type;
        bool map_edge;

        // Up to 6 edges in trigonometric order. Edge index + 1 is 
        // negative if the edge starts from this node, positive otherwise
        // zero means that the edge is unaffected
        int32_t edges[6];
        EBSDColorSet color;

        Vertex() ;

        bool loadFromString(const std::string & s, bool radian) ;

        bool loadFromCPR(const std::string & s, bool radian) ;
        bool loadFromCTF(const std::string & s, bool radian) ;

        // To be called once DeltaX,DeltaY have been computed
        void update_indices() ;

        void update_quaternion(const tf::Quaternion & qref);

        std::string phase_name() const;

    };

};



#endif // CRYSTALS_VERTEX_H
