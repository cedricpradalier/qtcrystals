#ifndef CRYSTALS_CONNECTED_H
#define CRYSTALS_CONNECTED_H

#include <vector>

#include <LinearMath/Quaternion.h>
#include "crystals/quaternion_tools.h"
#include "crystals/ConnectedNeighbour.h"
#include "crystals/EBSDSurface.h"
#include "crystals/Face.h"

namespace crystals {

    struct Connected : public EBSDSurface<ConnectedNeighbour> {
        bool valid;
        EBSDColorSet color;
        size_t num_connections;
        int group; // -1 when no group
        int cluster;
        int phase;
        int twinstrip; // 0 when N/A, -1 when it is a twinstrip, id of the twinstrip otherwise
        bool connected_twin;
        bool embedded;
        bool manual_parent;
        bool manual_twin;
        bool has_weird_parents;
        bool parent;
        int twinning_order;
        double cov_xx, cov_xy, cov_yy;
        double length, thickness, angle;
        double area, border_length, schmidt_factor;
        double ellipsicity;
        unsigned int subgrain_type;

        std::vector<uint32_t> vertices;
        // Expanded vertices are the "bad" vertices we merged into the
        // connected part in the gap-filling process
        std::vector<uint32_t> expanded_vertices;
        std::vector<uint32_t> border_vertices;
        std::vector<uint32_t> grain_border_vertices;

        struct ClosestPoint {
            float distance;
            float x,y;
            ClosestPoint(): distance(0),x(0),y(0) {}
            ClosestPoint(float d, float x, float y):distance(d),x(x),y(y){}
        };
        std::map<uint32_t,ClosestPoint> ingrain_distance;

        Connected() ;
        ~Connected() { }
        void updateStatistics(const std::vector<Vertex> & vertices);

        void add_neighbour(int id, size_t edge_id, TwinningType type, size_t weight=1) ;
        void add_joint(size_t conn_id, size_t joint_id);

        size_t count_twinning_type(TwinningType t) const ;

        size_t size() const { 
            return vertices.size() + expanded_vertices.size();
        }
    };

    struct TwinStrip : public Connected {
        std::vector<uint32_t> parts;
        // We also store the inverted relations. This will help later when
        // storing the DB and displaying the values
        NeighbourMap rev_neighbours;
        TwinStrip();

        void assemble(std::vector<Connected> & connected, 
                const std::vector<Vertex> & vertices); 

        void buildOrientationHistograms(const std::vector<Joint> & joints);
    };
};


#endif // CRYSTALS_CONNECTED_H
