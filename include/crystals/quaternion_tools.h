#ifndef CRYSTAL_QUATERNION_TOOLS_H
#define CRYSTAL_QUATERNION_TOOLS_H

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <LinearMath/Quaternion.h>
#include <LinearMath/Vector3.h>
#include <LinearMath/Matrix3x3.h>
#include "crystals/constants.h"
#include "crystals/Vertex.h"
#include "crystals/color_tools.h"

namespace crystals {

    template <typename T>
    inline T SQR(T x) {return x * x;}

    void initialise_quaternion_symmetries();

    extern tf::Quaternion QRef;

    struct DisorientationClass {
        double beta, alpha;
        TwinningType type;
        unsigned int variant;
        double error;
        tf::Quaternion error_q;
        std::map<unsigned int,double> probas;
        DisorientationClass() {}

        std::string toString() ; // can't be const because we're using the [] operator of probas
    };

    DisorientationClass classify_disorientation(const tf::Quaternion & q,bool with_proba=false);

    tf::Quaternion QFondamental(const tf::Quaternion & q, MaterialEnum mat_type);
    std::pair<double,tf::Quaternion> QDisorientation(const tf::Quaternion & q1, const tf::Quaternion & q2);

    tf::Vector3 QLog(const tf::Quaternion & q);

    tf::Quaternion QExp(tf::Vector3 v);

    bool QIsNan(const tf::Quaternion & q) ;

    tf::Quaternion QMeanWithSymmetries(const std::vector<tf::Quaternion> & v, 
            double tol=5e-4, size_t max_iter=1000);

    tf::Quaternion QMeanWithSymmetries(const std::vector<tf::Quaternion> & v, 
            const std::vector<double> & weights, double tol=5e-4, size_t max_iter=1000);

    tf::Quaternion QMeanWithSymmetries(const std::vector<Vertex> & v, 
            double tol=5e-4, size_t max_iter=1000);

    tf::Quaternion QMeanWithSymmetries(const std::vector<Vertex> & v, 
            const std::vector<uint32_t> & cluster, 
            double tol=5e-4, size_t max_iter=1000);

    // From Rod McCabe
    tf::Matrix3x3 matrixFromBungeEuler(double phi1,double PHI,double phi2);
    tf::Quaternion quaternionFromBungeEuler(double phi1,double PHI,double phi2);
    void matrixToBungeEuler(const tf::Matrix3x3 & R,double &phi1,double &PHI,double &phi2,bool degrees=false);
    void quaternionToBungeEuler(const tf::Quaternion & q,double &phi1,double &PHI,double &phi2,bool degrees=false);
};

#endif // CRYSTAL_TOOLS_H
