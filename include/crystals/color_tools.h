#ifndef CRYSTAL_COLOR_TOOLS_H
#define CRYSTAL_COLOR_TOOLS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <LinearMath/Quaternion.h>
#include <LinearMath/Vector3.h>
#include <LinearMath/Matrix3x3.h>
#include <QString>
#include <QColor>

namespace crystals {

//#define STORE_COLOR_NAME

    extern bool bgr_mode;
    QColor Color(int a, int b, int c);
    std::string QuatColor(const QColor & zxz);
    std::string QuatColor(const tf::Quaternion & q);
    QColor cvColor(const tf::Quaternion & q);

    struct EBSDColor {
        QColor scalar;
#ifdef STORE_COLOR_NAME
        QString name;
#else
        QString name() const {
            return scalar.name();
        }
#endif
        EBSDColor(); 
        EBSDColor(const QColor & s); 
        EBSDColor(uint8_t red, uint8_t green, uint8_t blue); 
        EBSDColor(uint8_t gray); 
    };

    typedef enum {
        EBSDCM_EULER=0,
        EBSDCM_FULL,
        EBSDCM_STEREO_001,
        EBSDCM_STEREO_010,
        EBSDCM_STEREO_100,
        EBSDCM_FIT,
        EBSDCM_IMAGE_QUALITY,
        EBSDCM_CONFIDENCE,
        EBSDCM_NUM_MODES
    } EBSDColorMode;


    struct EBSDColorSet {
        static float iq_min;
        static float iq_max;
        static float fit_max;
        static void resetEBSDStats();
        static void updateEBSDStats(float iq, float fit);
        static EBSDColorMode colorMode;
        static void setMode(EBSDColorMode m);
        static void nextColorMode();
        EBSDColor euler;
        EBSDColor full;
        EBSDColor stereo001;
        EBSDColor stereo010;
        EBSDColor stereo100;
        EBSDColor fit;
        EBSDColor iq;
        EBSDColor ci;
        EBSDColorSet() {}
        EBSDColorSet(const tf::Quaternion & q,float iq=0.0,float ci=0.0,float fit=0.0);
        // Default conversion
#ifdef STORE_COLOR_NAME
        operator const std::string&() const;
#else
        operator std::string() const;
#endif
        operator const QColor&() const;
    };


};

#endif // CRYSTAL_TOOLS_H
