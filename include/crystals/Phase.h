#ifndef CRYSTAL_PHASE_H
#define CRYSTAL_PHASE_H

#include <map>
#include <vector>
#include "crystals/EBSDSurface.h"
#include "crystals/Grain.h"
#include "crystals/Connected.h"
#include "crystals/constants.h"

namespace crystals {

        struct Phase  {
            // index to subphases;
            size_t id;
            double x, y;
            uint32_t size;
            tf::Quaternion qmean;
            uint32_t grain;
            int twinning_order;
            // The following 3 variables only make sense for twinning order > 1
            tf::Quaternion qdiso;
            unsigned short twinning_variant;
            TwinningType twinning_type;
            std::vector<uint32_t> parts;
            std::vector<int> is_parent;
            int parent;
            size_t cluster;
            std::multimap<TwinningType,size_t> subphases;
            std::vector<int32_t> joints;
            bool updated;

            Phase() : qmean(0,0,0,1),qdiso(0,0,0,1),twinning_variant(0),parent(-1),updated(false) {}
            Phase(const Grain & G,const std::vector<Connected> & connected,
                    const std::vector<Joint> & joints,
                    std::vector<Phase> & phases);

            // WARNING: This cannot be parallelised because it relies on
            // sequential access to phases.
            bool refine(const std::vector<Connected> & connected,
                    const std::vector<Joint> & conn_joints,
                    std::vector<Phase> & phases);
            bool update_statistics(const std::vector<Connected> & connected,
                    std::vector<Phase> & phases);

        };


};


#endif // CRYSTAL_PHASE_H
