#ifndef CRYSTAL_GRAPH_H
#define CRYSTAL_GRAPH_H

#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <algorithm>
#include <vector>
#include <list>

namespace crystals {

    // A set could be enough, but for connected component, we don't really
    // care.
    class Graph {
        protected:
            typedef std::vector<unsigned int> GraphVertexNeighbours;

            struct GraphVertex {
                int membership;
                GraphVertexNeighbours neighbours;
                GraphVertex() : membership(-1) {};
            };

            bool fixed_size;
            size_t connected_components;
            std::vector<GraphVertex> vertices;
            std::vector<uint32_t> component_size;

        public:
            Graph() : fixed_size(false), connected_components(0) {}
            Graph(unsigned int num_vertices) : fixed_size(true), connected_components(0), vertices(num_vertices) {}
            ~Graph() {}

            void add_connection(size_t i, size_t j,bool bidirectional=true) {
                if (fixed_size) {
                    assert(i<vertices.size());
                    assert(j<vertices.size());
                } else {
                    size_t n = std::max(i,j);
                    if (n > vertices.size()) {
                        vertices.resize(n+1);
                    }
                }
                vertices[i].neighbours.push_back((unsigned int)j);
                if (bidirectional) {
                    vertices[j].neighbours.push_back((unsigned int)i);
                }
            }

            size_t size() const {
                return vertices.size();
            }

            int membership(size_t i) const {
                assert(i<vertices.size());
                return vertices[i].membership;
            }

            int csize(size_t i) const {
                assert(i<component_size.size());
                return component_size[i];
            }

            size_t num_connected_components() {
                return connected_components;
            }

            size_t find_connected_components() ;

            void print() const;

            class TrivialActionCondition {
                bool operator()(unsigned int) const {
                    return true;
                }
            };

            template <class Condition,class Action> 
                size_t floodfill(size_t start, const Condition & C, Action & A) {
                    size_t count = 0;
                    std::vector<bool> visited(vertices.size(),false);
                    std::list<unsigned int> to_visit;
                    to_visit.push_back((unsigned int)start);
                    while (!to_visit.empty()) {
                        size_t v = to_visit.front();
                        to_visit.pop_front();
                        if (!C((unsigned int)v)) continue;
                        count += 1;
                        A((unsigned int)v);
                        visited[v] = true;
                        GraphVertexNeighbours::const_iterator j;
                        for (j=vertices[v].neighbours.begin();
                                j!=vertices[v].neighbours.end();j++) {
                            if (!visited[*j]) {
                                to_visit.push_back(*j);
                            }
                        }
                    }
                    return count;
                }
    };


};

#endif // CRYSTAL_GRAPH_H
