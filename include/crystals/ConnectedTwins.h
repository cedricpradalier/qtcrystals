#ifndef CRYSTALS_CONNECTED_TWINS_H
#define CRYSTALS_CONNECTED_TWINS_H

#include "crystals/quaternion_tools.h"

namespace crystals {
        struct ConnectedTwins {
            typedef enum {
                UNINITIALIZED,
                UNCONNECTED,
                CONNECTED,
                SINGLE
            } ConnectedTwinsType;
            ConnectedTwinsType type;
            // Index in the connected list
            size_t i,j;
            ConnectedTwins() : type(UNINITIALIZED) {}
        };

};




#endif // CRYSTALS_CONNECTED_TWINS_H
