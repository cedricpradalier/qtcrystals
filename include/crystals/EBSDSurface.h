#ifndef EBSD_SURFACE_H
#define EBSD_SURFACE_H

#include <cmath>
#include "crystals/quaternion_tools.h"
#include "crystals/Face.h"
#include "crystals/Joint.h"

struct sqlite3;

namespace crystals {

    struct EBSDNeighbour {
        int i, j;
        uint32_t weight;
        tf::Quaternion q_disorientation;
        std::vector<uint32_t> joints;
        std::map<int16_t,uint32_t> orientation_histogram;
        EBSDNeighbour():i(-1), j(-1), weight(0), q_disorientation(0,0,0,1)  {}
        EBSDNeighbour(int _i, int _j): i(_i), j(_j), weight(0), q_disorientation(0,0,0,1) {}
        EBSDNeighbour(int _i, int _j, size_t w, const tf::Quaternion & q)
            : i(_i), j(_j), weight((uint32_t)w), q_disorientation(q) {}

        // Defined in EBSD_db.cpp
        bool storeOrientationHistogram(sqlite3 *db,size_t hist_id) const;
    };

    template <class NeighbourClass>
        struct EBSDSurface {
            typedef std::map<uint32_t,NeighbourClass> NeighbourMap;
            typedef EBSDSurface<NeighbourClass> Surface;
            int i;
            bool map_edge;
            double x, y;
            tf::Quaternion qmean;
            // Neighbouring members, the second member of the map is the number of
            // border cells pointing to these neighbour; 
            NeighbourMap neighbours;
            std::vector<int32_t> joints;
            // Ordered version of the above. The first contour is the outside, the
            // others are holes. 
            std::vector<Contour> contours;

            EBSDSurface() : i(-1), map_edge(false), x(0), y(0) {}

            typename NeighbourMap::iterator add_neighbour(int id) {
                typename NeighbourMap::iterator nit;
                assert(id != i);
                nit = neighbours.find(id);
                if (nit == neighbours.end()) {
                    nit = neighbours.insert(typename NeighbourMap::value_type(id,NeighbourClass(i,id))).first;
                } else {
                    assert(nit->first == (uint32_t)id);
                    assert(nit->second.i == i);
                    assert(nit->second.j == id);
                }
                return nit;
            }

            float get_orientation_at_face(uint32_t face) 
            {
                for (size_t contour_number=0;contour_number<contours.size();contour_number++) {
                    std::map<uint32_t,uint32_t>::const_iterator it = 
                        contours[contour_number].face_idx.find(face);
                    if (it == contours[contour_number].face_idx.end()) {
                        continue;
                    }
                    return contours[contour_number].orientation[it->second];
                }
                return NAN;
            }


            void buildOrientationHistograms(const std::vector<Joint> & joints) {
                typename NeighbourMap::iterator it;
                for (it=neighbours.begin();it != neighbours.end();it++) {
                    it->second.orientation_histogram.clear();
                    for (int alpha = 0; alpha * OrientationResolutionDeg <= 180 ; alpha ++) {
                        it->second.orientation_histogram[(short)(alpha*OrientationResolutionDeg)] = 0;
                        it->second.orientation_histogram[(short)(-alpha*OrientationResolutionDeg)] = 0;
                    }
                    for (size_t j=0;j<it->second.joints.size();j++) {
                        const Joint & J = joints[it->second.joints[j]];
                        for (size_t k=0;k<J.face_ids.size();k++) {
                            float angle = get_orientation_at_face(J.face_ids[k])*180/M_PI;
                            if (std::isnan(angle)) continue;
                            float angle_discretized = round(round(angle/OrientationResolutionDeg)*OrientationResolutionDeg);
                            // Relying on the default value of integers to be zero
                            it->second.orientation_histogram[angle_discretized] += 1;
                        }
                    }
                }
            }
        };


};


#endif // EBSD_SURFACE_H
