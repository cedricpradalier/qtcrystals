#ifndef CRYSTALS_CONVEX_HULL_H
#define CRYSTALS_CONVEX_HULL_H

#ifdef USE_PCL
#include <pcl/point_types.h>
#include <pcl/surface/convex_hull.h>
#endif
#ifdef USE_CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/Point_set_2.h>
#include <CGAL/minkowski_sum_2.h>
#include <CGAL/exceptions.h>
#endif
#include <vector>

namespace crystals {

#ifdef USE_PCL
    #define CRYSTAL_POLYGON_DEFINED
    typedef pcl::PointCloud<pcl::PointXYZ> CrystalPolygon;
#endif

#ifdef USE_CGAL
    #define CRYSTAL_POLYGON_DEFINED
    typedef CGAL::Exact_predicates_exact_constructions_kernel K;
    typedef CGAL::Exact_predicates_inexact_constructions_kernel Kf;
    typedef K::Point_2 cgal_point;
    typedef Kf::Point_2 cgal_pointf;
    typedef CGAL::Polygon_2<K> CrystalPolygon;
    typedef CGAL::Polygon_with_holes_2<K> CrystalPolygonWithHoles;
    typedef CGAL::Cartesian_converter<K,Kf> K2Kf;
    typedef CGAL::Point_set_2<Kf> PointSet;

#endif

#ifndef CRYSTAL_POLYGON_DEFINED
    // dummy type
    typedef std::vector<int> CrystalPolygon;
#endif
};


#endif // CRYSTALS_CONVEX_HULL_H
