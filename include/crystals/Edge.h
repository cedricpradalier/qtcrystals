#ifndef CRYSTALS_EDGE_H
#define CRYSTALS_EDGE_H

#include <vector>
#include "crystals/Vertex.h"
#include "crystals/quaternion_tools.h"

namespace crystals {
    struct Edge {
        bool marked;
        // Vertex index
        int32_t i, j;
        // Angle
        uint8_t slot_i, slot_j;
        tf::Quaternion q_disorientation;
        float disorientation;
        float weight;
        bool different_phase;
        TwinningType twinning_type;
        float twinning_error;
        Edge(const std::vector<Vertex> & v, int a, int slot_a, int b, int slot_b) ;

        bool isSolid(const std::vector<Vertex> & v) const ;
        bool isConnected(const std::vector<Vertex> & v) const ;

        bool isBorder(const std::vector<Vertex> & v) const ;
        bool isGrainBorder(const std::vector<Vertex> & v) const ;

        // In a separate function because the material type might not be set
        // when we're loading the edge.
        void updateType();

        std::vector<uint32_t> getNeighbours(const std::vector<Vertex> & v, bool square_mesh) const;
    };

};




#endif // CRYSTALS_EDGE_H
