#ifndef CRYSTAL_CONSTANTS_H
#define CRYSTAL_CONSTANTS_H

#include <map>
#include <LinearMath/Quaternion.h>


#ifndef M_PI
#define M_PI (3.141592653589793238462643383279502884e+00)
#endif


namespace crystals {

    extern double MeshSize;
    extern double MeshArea;
    extern double XOffset;
    extern double YOffset;
    extern double DeltaX;
    extern double DeltaY;
    extern double EdgeTolerance;
    extern double SigmaSqr;
    extern double MinWeight;
    extern size_t MinJointWeight;
    extern double MinComponentSize;
    extern double SubGrainMinEnclosure;
    extern double TwinningTypeTolerance;
    extern double TwinningGamma;
    extern double TwinningGammaMg;
    extern double TwinningGammaZr;
    extern double TwinningGammaMr;
    extern bool TrustExternalPhaseData;
    // Number of points to consider on each side when computing linear
    // approximation of a joint
    extern size_t JointNeighbourhood;
    // Resolution to use when building the orientation histogram for a contour
    extern size_t OrientationResolutionDeg;

    typedef enum {
        MAT_UNDEF, 
        MAT_MAGNESIUM, 
        MAT_ZIRCONIUM,
        MAT_MARTENSITE,
        MAT_AUSTENITE
    } MaterialEnum;

    extern MaterialEnum MaterialType;
    const char * material_type_str(MaterialEnum type);

    typedef enum {
        NO_TWINNING=0, 
        TWINNING_ZERO, 
        Mg_TWINNING_TENSILE, 
        Mg_TWINNING_COMPRESSIVE, 
        Zr_TWINNING_TENSILE_1, 
        Zr_TWINNING_TENSILE_2, 
        Zr_TWINNING_COMPRESSIVE_1, 
        Zr_TWINNING_COMPRESSIVE_2, 
        Mr_TWINNING_TYPE, 
        NUM_TWINNING_TYPES,
    } TwinningType; 

    const char * twinning_type_str(TwinningType type);

    typedef std::map<unsigned int, tf::Quaternion> TwinningDisorientationsMap;
    extern TwinningDisorientationsMap twinning_disorientations;
    void initialise_twinning_disorientations();


    const unsigned int MAX_VARIANT_PER_TYPE = 100;
};


#endif // CRYSTAL_CONSTANTS_H
