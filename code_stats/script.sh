#!/bin/bash

#set -e -x
set -e

DB_FILE=grainmap.sqlite

if test "x$CRYSTALS_HOME" == "x"
then
    if test -r scripts/sql/request_integrated.sql -a "x$1" != "x"
    then
        DB_FILE="$1"
    else 
        echo "This script can only be called with the variable \$CRYSTALS_HOME defined."
        echo "Alternatively, it can be launched from the code_stats direcotry"
        echo "but it then requires the database as an argument."
        exit -1
    fi
else 
    cd $CRYSTALS_HOME/code_stats
fi

echo "Extracting data tables"
mkdir -p tmp
rm -f tmp/*.txt tmp/*.csv
cat scripts/sql/request_integrated.sql | \
    sqlite3 "$DB_FILE"


echo "Post-processing data tables"
make -C scripts/fortran
if test -r tmp/data_proc.in
then
    echo "Using input in tmp/data_proc.in"
else
    echo "Using input in scripts/fortran/data_proc.in"
    cp scripts/fortran/data_proc.in tmp
fi
scripts/fortran/data_proc

echo "Generating plots"
mkdir -p plots
rm -f plots/*.eps
gnuplot -e "load 'scripts/gnuplot/gnucall3.s'"
