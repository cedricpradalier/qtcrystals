      program data_proc
       
      implicit none
      
      include 'data_proc.dim'

ccc Initilization of variables present in the input 'data_proc.in' file
ccc ---- Loading parameters ccc

      do i=1,3; do j=1,3
        sigma(i,j)=0.
      enddo; enddo

      do i=1,3
        load_dir(i)=0.
      enddo
ccc NB: in the case of LN-TT Zr maps, the 3rd direction corresponds to the through thickness direction 

ccc Reading of the input file

   1  format(a)

      open(unit=1,file="tmp/data_proc.in",status='old')     

      read(1,1) prosa
      read(1,1) prosa
      read(1,*) ratio
      read(1,1) prosa
      read(1,*) sigma(1,1)
      read(1,*) sigma(2,2)
      read(1,*) sigma(3,3)
      read(1,1) prosa
      read(1,*) load_dir(1)
      read(1,*) load_dir(2)
      read(1,*) load_dir(3)
      read(1,1) prosa
      read(1,*) area_min
      read(1,*) area_max
      read(1,*) diam_min
      read(1,*) diam_max
      read(1,1) prosa

      close(unit=1)

ccc Initialization and definition of variables used in the following

      delta_area=(area_max-area_min)/nbr
      delta_diam=(diam_max-diam_min)/nbr

      do nr=1,nmax
        map_id_gr(nr)=0
        map_id_par(nr)=0
        map_id_tw(nr)=0
        map_id_twinter(nr)=0
      enddo
    
      do i=1,100; do j=1,50      
        cp(i,j)=0
      enddo; enddo

      do i=1,4; do j=1,nmax      
       cp_fracmod(i,j)=0.
       cp_twnbmod(i,j)=0.
       iflag_fracmod(i,j)=0
       cp_twnbmod_gr(i,j)=0.
      enddo; enddo

ccc Bin parameters for histogram plots ccc

      do i=1,50; do j=1,nbr
        max_local(i,j)=0.
        min_local(i,j)=0.
      enddo; enddo

      do j=1,nbr+1
        bar_area(j)=area_min+(j-1.)*delta_area
        bar_diam(j)=diam_min+(j-1.)*delta_diam
      enddo

      delta_sch=0.5/nbrs
      delta_sf=1./nbrs2

      do j=1,nbrs+1
        bar_sch(j)=(j-1.)*delta_sch
      enddo

      do j=1,nbrs2+1
        bar_sf(j)=-0.5+(j-1.)*delta_sf
      enddo

      tot_area_gr=0.
      tot_area_tw=0.
      tot_area_par=0.


ccc SF calc Method
ccc SF calc Method

      iMethod=1

ccc Twinning parameters
ccc Twinning parameters
ccc Twinning parameters


cc 1 - Tensile I
cc 2 - Tensile II
cc 3 - Compressive I
cc 4 - Compressive II

ccc
ccc angles alpha, beta and delta are used to compute normal and burger vectors corresponding to the 4 previous modes and their respective 6 variants
ccc

cc 1 - Tensile I

      if(iMethod.eq.1)then

      alpha(1,1)=2.*pi/3.
      alpha(1,2)=pi
      alpha(1,3)=-2.*pi/3.
      alpha(1,4)=-pi/3.
      alpha(1,5)=0.
      alpha(1,6)=pi/3.

      beta(1)=atan(ratio/sqrt(3.))

      delta(1,1)=-5.*pi/6.
      delta(1,2)=-pi/2.
      delta(1,3)=-pi/6.
      delta(1,4)=pi/6.
      delta(1,5)=pi/2.
      delta(1,6)=5.*pi/6.

      do i=1,6
        bv(1,i,1)=sqrt(3.)*cos(delta(1,i))/
     #             sqrt(3.+ratio**2.)
        bv(1,i,2)=sqrt(3.)*sin(delta(1,i))/
     #             sqrt(3.+ratio**2.)
        bv(1,i,3)=ratio/
     #             sqrt(3.+ratio**2.)
      enddo

cc 2 - Tensile II

      alpha(2,1)=-pi/6.
      alpha(2,2)=pi/6.
      alpha(2,3)=pi/2.
      alpha(2,4)=5.*pi/6.
      alpha(2,5)=-5.*pi/6.
      alpha(2,6)=-pi/2.

      beta(2)=atan(2.*ratio)

      delta(2,1)=-2.*pi/3.
      delta(2,2)=-pi/3.
      delta(2,3)=0.
      delta(2,4)=pi/3.
      delta(2,5)=2.*pi/3.
      delta(2,6)=pi

      do i=1,6
        bv(2,i,1)=cos(delta(2,i))/sqrt(1.+4.*ratio**2.)
        bv(2,i,2)=sin(delta(2,i))/sqrt(1.+4.*ratio**2.)
        bv(2,i,3)=2.*ratio/sqrt(1.+4.*ratio**2.)
      enddo

cc 3 - Compressive I

      alpha(3,1)=5.*pi/6.
      alpha(3,2)=-5.*pi/6.
      alpha(3,3)=-pi/2.
      alpha(3,4)=-pi/6.
      alpha(3,5)=pi/6.
      alpha(3,6)=pi/2.

      beta(3)=atan(ratio)

      delta(3,1)=pi/3.
      delta(3,2)=2.*pi/3.
      delta(3,3)=pi
      delta(3,4)=-2.*pi/3.
      delta(3,5)=-pi/3.
      delta(3,6)=0.

      do i=1,6
        bv(3,i,1)=cos(delta(3,i))/sqrt(1.+ratio**2.)
        bv(3,i,2)=sin(delta(3,i))/sqrt(1.+ratio**2.)
        bv(3,i,3)=-ratio/sqrt(1.+ratio**2.)
      enddo

cc 4 - Compressive II

      alpha(4,1)=2.*pi/3.
      alpha(4,2)=pi 
      alpha(4,3)=-2.*pi/3.
      alpha(4,4)=-pi/3.
      alpha(4,5)=0.
      alpha(4,6)=pi/3.

      beta(4)=atan(2.*ratio/sqrt(3.))

      delta(4,1)=pi/6.
      delta(4,2)=pi/2.
      delta(4,3)=5.*pi/6.
      delta(4,4)=-5.*pi/6.
      delta(4,5)=-pi/2.
      delta(4,6)=-pi/6.

      do i=1,6
        bv(4,i,1)=-sqrt(3.)*cos(delta(4,i))/      ! not yet checked with epsc 
     #             sqrt(3.+4.*ratio**2.)
        bv(4,i,2)=-sqrt(3.)*sin(delta(4,i))/    
     #             sqrt(3.+4.*ratio**2.)
        bv(4,i,3)=-2.*ratio/
     #             sqrt(3.+4.*ratio**2.)

      enddo

cc For all twin systems     

      do j=1,6
        nv(1,j,1)=bv(1,j,3)*sin(alpha(1,j))
        nv(1,j,2)=-bv(1,j,3)*cos(alpha(1,j))
        nv(1,j,3)=bv(1,j,2)*cos(alpha(1,j))-
     #              bv(1,j,1)*sin(alpha(1,j))
      enddo

      do j=1,6
        nv(2,j,1)=-bv(2,j,3)*sin(alpha(2,j))
        nv(2,j,2)=bv(2,j,3)*cos(alpha(2,j))
        nv(2,j,3)=-bv(2,j,2)*cos(alpha(2,j))+
     #              bv(2,j,1)*sin(alpha(2,j))
      enddo

      do i=3,4; do j=1,6                         
        nv(i,j,1)=-bv(i,j,3)*sin(alpha(i,j))
        nv(i,j,2)=bv(i,j,3)*cos(alpha(i,j))
        nv(i,j,3)=-bv(i,j,2)*cos(alpha(i,j))+
     #              bv(i,j,1)*sin(alpha(i,j))
      enddo; enddo

      do j=1,6
        nv(4,j,1)=bv(4,j,3)*sin(alpha(4,j))    
        nv(4,j,2)=-bv(4,j,3)*cos(alpha(4,j))   
        nv(4,j,3)=-bv(4,j,2)*cos(alpha(4,j))+
     #              bv(4,j,1)*sin(alpha(4,j))
      enddo

      else    ! else iMethod=2

ccc    iMethod=2 would contain read statements able to read any material file

      endif   ! endif iMethod

ccc Reading csv files
ccc Reading csv files
ccc Reading csv files

      open(unit=10,file="tmp/table_grains.csv"
     #         ,access='sequential'
     #         ,form="formatted")
      open(unit=11,file="tmp/table_grains.txt"
     #         ,status='unknown')
      rewind(10)

       nr_10=1
       do while (.true.)
         read(10,*,end=101) map_id_gr(nr_10),id(nr_10),area(nr_10)
     #              ,qx(nr_10),qy(nr_10),qz(nr_10),qw(nr_10)
         write(11,*) map_id_gr(nr_10),nr_10,id(nr_10),area(nr_10)
     #              ,qx(nr_10),qy(nr_10),qz(nr_10),qw(nr_10)
         nr_10=nr_10+1
       enddo 
 101    continue
       rewind(10)
       close(10)

       nr_10=nr_10-1

c Meaning of variables used for the read statement:
c         read(10,*,end=101) EBSD map id (ie index),grain id, grain area in mum²
c     #              ,and grain quaternion components:(imiginary part) qx,qy,qz and qw(real part)
c ------------------------------------------------------------------


c NB: a twinned grain is referred in the following to as a "parent grain";
c     the suffix "p" is generally used to refer to a parent grain variable
      
      open(unit=20,file="tmp/table_parents.csv"
     #         ,access='sequential'
     #         ,form="formatted")
      open(unit=21,file="tmp/table_parents.txt"
     #         ,status='unknown')
      rewind(20)

       nr_20=1
       do while (.true.)
         read(20,*,end=102) map_id_par(nr_20), idp(nr_20)
     #     ,areap(nr_20),blp(nr_20)
     #     ,avgp(nr_20),sump(nr_20),countp(nr_20),avgtwthp(nr_20)
         write(21,*) map_id_par(nr_20), nr_20,idp(nr_20)
     #      ,areap(nr_20),blp(nr_20)
     #     ,avgp(nr_20),sump(nr_20),countp(nr_20),avgtwthp(nr_20)
         nr_20=nr_20+1
       enddo 
 102    continue
       rewind(20)
       close(20)

       nr_20=nr_20-1

c         read(20,*,end=102) map id, parent id,
c     #     ,parent area, parent border length,
c     #     ,average twin area in the parent,total area occupied by twins in the parent, number of twins in the parent, average "non-corrected" thickness of twins in the parent 
c ----------------------------------------------------------------------

      open(unit=30,file="tmp/table_twins.csv"
     #         ,access='sequential'
     #         ,form="formatted")
      open(unit=31,file="tmp/table_twins.txt"
     #         ,status='unknown')
      rewind(30)

       nr_30=1
       do while (.true.)
         read(30,*,end=103) map_id_tw(nr_30),idt(nr_30)
     #    ,gt(nr_30),areat(nr_30)
     #      ,sizet(nr_30),maxqwt(nr_30),lt(nr_30),thickt(nr_30)
     #        ,covxxt(nr_30),covxyt(nr_30),covyyt(nr_30)
     #          ,xt(nr_30),yt(nr_30),twtypet(nr_30),twvart(nr_30)
     #            ,qxt(nr_30),qyt(nr_30),qzt(nr_30),qwt(nr_30)
     #              ,alphat(nr_30),betat(nr_30)
         write(31,*)map_id_tw(nr_30),idt(nr_30),gt(nr_30),areat(nr_30)
     #      ,sizet(nr_30),maxqwt(nr_30),lt(nr_30),thickt(nr_30)
     #        ,covxxt(nr_30),covxyt(nr_30),covyyt(nr_30)
     #          ,xt(nr_30),yt(nr_30),twtypet(nr_30),twvart(nr_30)
     #            ,qxt(nr_30),qyt(nr_30),qzt(nr_30),qwt(nr_30)
     #              ,alphat(nr_30),betat(nr_30)
         nr_30=nr_30+1
       enddo 
 103    continue
       rewind(30)
       close(30)

       nr_30=nr_30-1

c NB: twins are assumed to have an elliptical shape; so, length and thickness correspond to the long and short axis of the ellipse fitted to the twin
c         read(30,*,end=103) map id, twin id
c     #    ,parent grain id (same as in the grain table), twin area
c     #      ,twin size in terms of pixels, maxqwt(nr_30),twin length,non corrected twin thickness
c     #        ,covxxt(nr_30),covxyt(nr_30),covyyt(nr_30)
c     #          ,position of the center of the twin along the x-axis, position of the center of the twin along the y-axis,twin mode also called twin type,twin variant
c     #            ,quaternion components: qx,qy,qz,qw
c     #              ,angle alpha,angle beta
c ----------------------------------------------------------------------

      open(unit=40
     #         ,file="tmp/table_twins_inter.csv"
     #         ,access='sequential'
     #         ,form="formatted")
      open(unit=41
     #         ,file="tmp/table_twins_inter.txt"
     #         ,status='unknown')
      rewind(40)

       nr_40=1
       do while (.true.)
         read(40,*,end=104) map_id_twinter(nr_40),idgr(nr_40)
     #      ,tw1id(nr_40),tw2id(nr_40)
     #      ,tw1alpha(nr_40),tw1beta(nr_40),tw1twin(nr_40)
     #      ,tw1var(nr_40)
     #      ,tw2alpha(nr_40),tw2beta(nr_40),tw2twin(nr_40)
     #      ,tw2var(nr_40)
c     #      ,num_connect(nr_40)
         write(41,*) map_id_twinter(nr_40),idgr(nr_40)
     #      ,tw1id(nr_40),tw2id(nr_40)
     #      ,tw1alpha(nr_40),tw1beta(nr_40),tw1twin(nr_40)
     #      ,tw1var(nr_40)
     #      ,tw2alpha(nr_40),tw2beta(nr_40),tw2twin(nr_40)
     #      ,tw2var(nr_40)
c     #      ,num_connect(nr_40)
         nr_40=nr_40+1
       enddo 
 104    continue
       rewind(40)
       close(40)

       nr_40=nr_40-1

c NB: twin junctions are described by a twin #1 intersecting a twin #2
c         read(40,*,end=104) map id,grain id (same as in the grain table)
c     #      ,id of twin #1, id of twin #2
c     #      ,alpha angle of twin #1,beta angle of twin #1, twinning mode of twin #1
c     #      ,variant of twin #1
c     #      ,alpha angle of twin #2,beta angle of twin #2, twinning mode of twin #2
c     #      ,variant of twin #2
c     #      ,number of pixels connecting the two twins


cccc Twin counters for grain table ( nr_10 )
cccc Twin counters for grain table ( nr_10 ) 
cccc Twin counters for grain table ( nr_10 )

      do nr=1,nmax
        counttw_gr(nr)=0.
        do nr2=1,25
          counttw_idgr(nr2,nr)=0.
        enddo
      enddo

      do nr=1,nr_10
        ntwin=0.
        do nr2=1,nr_30
          if(map_id_gr(nr).eq.map_id_tw(nr2))then
            if(gt(nr2).eq.id(nr))then
              ntwin=ntwin+1
            endif
          endif
        enddo
        counttw_gr(nr)=ntwin
        counttw_idgr(map_id_gr(nr),id(nr))=ntwin 
      enddo


ccc Data post-processing - Parent grain property calculations 

       do nr=1,nr_30
         do nr2=1,nr_20
           if(map_id_tw(nr).eq.map_id_par(nr2))then
             if(idp(nr2).eq.gt(nr))then
               parent_area(nr)=areap(nr2)
             endif
           endif
         enddo
       enddo


ccc ccc Output files creation ccc ccc 

c NB: comments explain how data of text files are used for their corresponding plots 

       open(unit=1001,file="tmp/gnu_area_freq.txt"
     #                                            ,status='unknown')     
       open(unit=1002,file="tmp/gnu_diam_freq.txt"
     #                                            ,status='unknown')     
       open(unit=1003,file="tmp/gnu_area_twnb.txt"
     #                                            ,status='unknown')  
   
       open(unit=1004,file="tmp/gnu_area_twareafrac.txt"
     #                                            ,status='unknown')     

       open(unit=1005,file="tmp/gnu_area_twthck.txt"
     #                                            ,status='unknown')    
       open(unit=1006,file="tmp/gnu_area_frac.txt"
     #                                            ,status='unknown')
     
       open(unit=1007,file="tmp/gnu_diam_frac.txt"
     #                                            ,status='unknown')
       open(unit=1008,file="tmp/gnu_schm_freq.txt"
     #                                            ,status='unknown')
       open(unit=1009,file="tmp/gnu_schm_twthck.txt"
     #                                            ,status='unknown')

       open(unit=1010,file="tmp/gnu_schm_twareafrac.txt"
     #                                            ,status='unknown')   
       
       open(unit=1011,file="tmp/gnu_schm_freq_debug.txt"
     #                                            ,status='unknown')
c       open(unit=1012,file="tmp/gnu_tw_inter_qstats.txt"
c     #                                            ,status='unknown')
       open(unit=1013,file="tmp/gnu_tw_inter_TT1TT1.txt"
     #                                            ,status='unknown')
       open(unit=1014,file="tmp/gnu_tw_inter_TT1TT2.txt"
     #                                            ,status='unknown')
       open(unit=1015,file="tmp/gnu_tw_inter_TT1CT1.txt"
     #                                            ,status='unknown')
       open(unit=1016,file="tmp/gnu_tw_inter_TT1CT2.txt"
     #                                            ,status='unknown')
       open(unit=1017,file="tmp/gnu_tw_inter_TT2TT2.txt"
     #                                            ,status='unknown')
       open(unit=1018,file="tmp/gnu_tw_inter_TT2CT1.txt"
     #                                            ,status='unknown')
       open(unit=1019,file="tmp/gnu_tw_inter_TT2CT2.txt"
     #                                            ,status='unknown')
       open(unit=1020,file="tmp/gnu_tw_inter_CT1CT1.txt"
     #                                            ,status='unknown')
       open(unit=1021,file="tmp/gnu_tw_inter_CT1CT2.txt"
     #                                            ,status='unknown')
       open(unit=1022,file="tmp/gnu_tw_inter_CT2CT2.txt"
     #                                            ,status='unknown')
ccc
       open(unit=1025,file="tmp/gnu_area_twthckcor.txt"
     #                                            ,status='unknown')

       open(unit=1026,file="tmp/gnu_schm_freq_TT1.txt"
     #                                            ,status='unknown')
       open(unit=1027,file="tmp/gnu_schm_freq_TT2.txt"
     #                                            ,status='unknown')    
       open(unit=1028,file="tmp/gnu_schm_freq_CT1.txt"
     #                                            ,status='unknown')    
       open(unit=1029,file="tmp/gnu_schm_freq_CT2.txt"
     #                                            ,status='unknown')
c       open(unit=1030,file="tmp/gnu_schm_freq_type.txt"
c     #                                            ,status='unknown')  

       open(unit=1031,file="tmp/gnu_schm_freq_TT1_debug.txt"
     #                                            ,status='unknown') 
       open(unit=1032,file="tmp/gnu_schm_freq_TT2_debug.txt"
     #                                            ,status='unknown')      
       open(unit=1033,file="tmp/gnu_schm_freq_CT1_debug.txt"
     #                                            ,status='unknown')      
       open(unit=1034,file="tmp/gnu_schm_freq_CT2_debug.txt"
     #                                            ,status='unknown') 

c       open(unit=1035,file="tmp/gnu_schm_freq_rk1.txt"
c     #                                            ,status='unknown')    
c       open(unit=1036,file="tmp/gnu_schm_freq_rk2.txt"
c     #                                            ,status='unknown')  
c       open(unit=1037,file="tmp/gnu_schm_freq_rk3plus.txt"
c     #                                            ,status='unknown')      
c       open(unit=1038,file="tmp/gnu_schm_freq_rk_all.txt"                   
c     #                                            ,status='unknown')            
      open(unit=1039,file="tmp/gnu_schm_freq_rk1_debug.txt"
     #                                            ,status='unknown')  
       open(unit=1040,file="tmp/gnu_schm_freq_rk2_debug.txt"
     #                                            ,status='unknown')    
       open(unit=1041,file="tmp/gnu_schm_freq_rk3_debug.txt"
     #                                            ,status='unknown')  
       open(unit=1043,file="tmp/gnu_schm_freq_rk4plus_debug.txt"
     #                                            ,status='unknown') 
       open(unit=1042,file="tmp/gnu_schm_twthckcor_debug.txt"
     #                                            ,status='unknown') 
       open(unit=1044
     #        ,file="tmp/gnu_schm_freq_rk_clustered_debug.txt"
     #        ,status='unknown') 

       open(unit=1045
     #        ,file="tmp/gnu_schm_freq_type_clustered_debug.txt"
     #        ,status='unknown')  
       open(unit=1046,file="tmp/gnu_schm_twinnedarea_debug.txt"
     #                                            ,status='unknown')  
       open(unit=1047
     #        ,file="tmp/gnu_schm_freq_hSF_clustered_debug.txt"
     #        ,status='unknown')  

ccc Mono
       open(unit=1048,file="tmp/gnu_area_freq_mono.txt"
     #                                            ,status='unknown')
       open(unit=1049,file="tmp/gnu_twareafrac_mono.txt"
     #                                            ,status='unknown')
       open(unit=1050,file="tmp/gnu_area_twthckcor_mono.txt"
     #                                            ,status='unknown')
       open(unit=1051,file="tmp/gnu_schm_freq_mono_debug.txt"
     #                                            ,status='unknown')
       open(unit=1052,file="tmp/gnu_schm_freq_type_mono_debug.txt"
     #                                            ,status='unknown')
ccc End Mono

       open(unit=1053,file="tmp/gnu_sf_freq_rkall_rel_TT1_dbg.txt"          
     #                                            ,status='unknown') 
       open(unit=1054,file="tmp/gnu_sf_freq_rkall_rel_TT2_dbg.txt"
     #                                            ,status='unknown')
       open(unit=1055,file="tmp/gnu_sf_freq_rkall_rel_CT1_dbg.txt"
     #                                            ,status='unknown')
       open(unit=1056,file="tmp/gnu_sf_freq_rkall_rel_CT2_dbg.txt"
     #                                            ,status='unknown')
       open(unit=1057,file="tmp/gnu_sf_freq_rkall_abs_TT1_dbg.txt"
     #                                            ,status='unknown') 
       open(unit=1058,file="tmp/gnu_sf_freq_rkall_abs_TT2_dbg.txt"
     #                                            ,status='unknown')
       open(unit=1059,file="tmp/gnu_sf_freq_rkall_abs_CT1_dbg.txt"
     #                                            ,status='unknown')
       open(unit=1060,file="tmp/gnu_sf_freq_rkall_abs_CT2_dbg.txt"
     #                                            ,status='unknown')

       open(unit=1061
     #      ,file="tmp/gnu_schm_twthckcor_type_cluster_debug.txt"                    
     #      ,status='unknown')
       open(unit=1062,file="tmp/gnu_area_twthckcor_type.txt"                         
     #                                            ,status='unknown')
       open(unit=1063,file="tmp/gnu_area_tw_proportion_type.txt"                     
     #                                            ,status='unknown')

       open(unit=1064,file="tmp/gnu_sf_twthckcor_mono_multi.txt"                     
     #                                            ,status='unknown')
       open(unit=1065,file="tmp/gnu_sf_freq_mono_multi.txt"                          
     #                                            ,status='unknown')
       open(unit=1066,file="tmp/gnu_area_twthckcor_mono_multi.txt"                   
     #                                            ,status='unknown')
       open(unit=1067,file="tmp/gnu_area_freq_mono_multi.txt"                        
     #                                            ,status='unknown')

       open(unit=1068,file="tmp/gnu_twnb_area.txt"
     #                                            ,status='unknown')
       open(unit=1069,file="tmp/gnu_twnb_sf.txt"
     #                                            ,status='unknown')

       open(unit=1070
     #          ,file="tmp/gnu_sf_twthckcor_monovar_multivar.txt"                   
     #          ,status='unknown')
       open(unit=1071,file="tmp/gnu_sf_freq_monovar_multivar.txt"                    
     #                                            ,status='unknown')
       open(unit=1072
     #        ,file="tmp/gnu_area_twthckcor_monovar_multivar.txt"                    
     #        ,status='unknown')
       open(unit=1073
     #        ,file="tmp/gnu_area_freq_monovar_multivar.txt"                         
     #        ,status='unknown')


       open(unit=1074,file="tmp/gnu_area_frac_modes.txt"
     #                                            ,status='unknown')                     
       open(unit=1075,file="tmp/gnu_area_twnb_modes.txt"
     #                                            ,status='unknown')                     
       open(unit=1076
     #        ,file="tmp/gnu_area_twnb_modes_scatter_T1.txt"
     #        ,status='unknown')                                                       
       open(unit=1077
     #        ,file="tmp/gnu_area_twnb_modes_scatter_T2.txt"
     #        ,status='unknown')                                                         
       open(unit=1078
     #        ,file="tmp/gnu_area_twnb_modes_scatter_C1.txt"
     #        ,status='unknown')                                                        
       open(unit=1079
     #        ,file="tmp/gnu_area_twnb_modes_scatter_C2.txt"
     #        ,status='unknown')                                                        
       open(unit=1080,file="tmp/gnu_area_twthckcor_modes_T1.txt"
     #                                              ,status='unknown')           
       open(unit=1081,file="tmp/gnu_area_twthckcor_modes_T2.txt"
     #                                              ,status='unknown')         
       open(unit=1082,file="tmp/gnu_area_twthckcor_modes_C1.txt"
     #                                              ,status='unknown')          
       open(unit=1083,file="tmp/gnu_area_twthckcor_modes_C2.txt"
     #                                              ,status='unknown')          
       open(unit=1084
     #        ,file="tmp/gnu_area_twthckcor_modes_scatter_T1.txt"
     #        ,status='unknown')                                                
       open(unit=1085
     #        ,file="tmp/gnu_area_twthckcor_modes_scatter_T2.txt"
     #        ,status='unknown')                                                
       open(unit=1086
     #        ,file="tmp/gnu_area_twthckcor_modes_scatter_C1.txt"
     #        ,status='unknown')                                                 
       open(unit=1087
     #        ,file="tmp/gnu_area_twthckcor_modes_scatter_C2.txt"
     #        ,status='unknown')           ! Feb 22, 2015

       open(unit=1088,file="tmp/gnu_area_freq_type.txt"
     #                                            ,status='unknown')            

ccc ccc Output files creation - End ccc ccc


       do i=1,50; do j=1,nbr
         cp(i,j)=0
       enddo; enddo
     
       do nr=1,nr_10        ! GRAINS
          do j=1,nbr
             if(area(nr).ge.bar_area(j) .and. 
     #           area(nr).lt.bar_area(j+1))then
               cp(1,j)=cp(1,j)+1
             endif
             if(sqrt(4.*area(nr)/pi).ge.bar_diam(j) .and. 
     #           sqrt(4.*area(nr)/pi).lt.bar_diam(j+1))then
               cp(2,j)=cp(2,j)+1
             endif
          enddo
       enddo

       do j=1,nbr
          write(1001,*)j,float(cp(1,j))/float(nr_10)                      ! 1001: grain area vs number of grains
     #                   ,bar_area(j),bar_area(j+1),cp(1,j)
          write(1002,*)j,float(cp(2,j))/float(nr_10)                      ! 1002: grain diameter vs number of grains
     #                   ,bar_diam(j),bar_diam(j+1),cp(2,j)               
       enddo

ccc         
       do j=1,nbr
         twnb(j)=0
         twarea(j)=0.
         av_twnb(j)=0.
         av_twarea(j)=0.
         twfr(j)=0.
         av_twfr(j)=0.
         grarea(j)=0.
         twthck(j)=0.
         av_twthck(j)=0.
       enddo 

       do j=1,nbr
         min_local(1,j)=100.
         max_local(1,j)=1.     ! nb of twins per grain
         min_local(2,j)=1.     ! ration tw area / parent area
         min_local(3,j)=1000.
       enddo

       do nr=1,nr_20       ! PARENTS --> deal with twin thickness and max-min
          do j=1,nbr
             if(areap(nr).ge.bar_area(j) .and. 
     #           areap(nr).lt.bar_area(j+1))then
               cp(3,j)=cp(3,j)+1 
               twnb(j)=twnb(j)+countp(nr)
               twarea(j)=twarea(j)+sump(nr)
               twfr(j)=twfr(j)+sump(nr)/areap(nr)
               grarea(j)=grarea(j)+areap(nr)
               twthck(j)=twthck(j)+avgtwthp(nr)           ! twthck (for the sum),av_twthck (final avg) ,avgtwthp (from the table) TO PERF AVG
               if(countp(nr).gt.max_local(1,j))then
                 max_local(1,j)=countp(nr)
               endif
               if(countp(nr).lt.min_local(1,j))then
                 min_local(1,j)=countp(nr)
               endif
               if(sump(nr)/areap(nr).gt.max_local(2,j))then
                   max_local(2,j)=sump(nr)/areap(nr)
               endif
                 if(sump(nr)/areap(nr).lt.min_local(2,j))then
                   min_local(2,j)=sump(nr)/areap(nr)
                 endif
               if(avgtwthp(nr).gt.max_local(3,j))then
                 max_local(3,j)=avgtwthp(nr)
               endif
               if(avgtwthp(nr).lt.min_local(3,j))then
                 min_local(3,j)=avgtwthp(nr)
               endif 
             endif
             if(sqrt(4.*areap(nr)/pi).ge.bar_diam(j) .and. 
     #           sqrt(4.*areap(nr)/pi).lt.bar_diam(j+1))then
               cp(4,j)=cp(4,j)+1 
             endif
          enddo
       enddo

       do j=1,nbr
         av_twnb(j)=float(twnb(j))/float(cp(3,j))
         av_twarea(j)=twarea(j)/grarea(j)        
         av_twfr(j)=twfr(j)/float(cp(3,j))
         av_twthck(j)=twthck(j)/float(cp(3,j))
       enddo

       do j=1,nbr
          if(cp(3,j).ne.0)then
            write(1003,*)j,av_twnb(j),min_local(1,j),max_local(1,j)       ! 1003: twinned grain area vs min of twins contained, max of twins contained by twinned grains belonging to the considered area range and number of twinned grains 
     #                   ,bar_area(j),bar_area(j+1),cp(3,j)
            write(1004,*)j,av_twfr(j),min_local(2,j),max_local(2,j)       ! 1004: twinned grain area vs average twinned area fraction, min and max of twinned grain fraction observed in a twinned grain belonging to the considered area range, average twinnned area fraction (computed in another way; redundant)
     #                   ,av_twarea(j)
     #                   ,bar_area(j),bar_area(j+1)
            write(1005,*)j,av_twthck(j),min_local(3,j),max_local(3,j)     ! 1005: grain area diameter vs average non corrected twin thickness of grains belonging to this area bracket as well as the smallest and largest twin thicknesses observed in these grains
     #                   ,bar_area(j),bar_area(j+1)
          else    ! to avoid 'Nan' statement
            write(*,*)'Warning NAN-parents area'
     #                ,bar_area(j),bar_area(j+1)
            write(1003,*)j,0.,0.,0.
     #                   ,bar_area(j),bar_area(j+1),cp(3,j)
            write(1004,*)j,0.,0.,0.,0.
     #                   ,bar_area(j),bar_area(j+1)     
            write(1005,*)j,0.,0.,0.
     #                   ,bar_area(j),bar_area(j+1)      
          endif
          if(cp(1,j).ne.0)then
            write(1006,*)j,float(cp(3,j))/float(cp(1,j))                  ! 1006: grain area vs fraction of twinned grains (nb parents/nb grains)
     #                   ,bar_area(j),bar_area(j+1),cp(3,j)
          else
            write(*,*)'Warning NAN-grains area',' '
     #                ,'BIN SIZE TO CHANGE'   
     #                ,bar_area(j),bar_area(j+1)         
          endif
          if(cp(2,j).ne.0)then
            write(1007,*)j,float(cp(4,j))/float(cp(2,j))                  ! 1007: same as 1007 but with grain diameters as x-axis
     #                   ,bar_diam(j),bar_diam(j+1),cp(4,j)
          else
            write(*,*)'Warning NAN-grains diam',' '
     #                ,'BIN SIZE TO CHANGE'      
     #                ,bar_diam(j),bar_diam(j+1) 
          endif
       enddo



       do nr=1,nr_10
         do j=1,nbr
           if(area(nr).ge.bar_area(j).and.
     #         area(nr).lt.bar_area(j+1))then
             do nr2=1,nr_30
               if(map_id_tw(nr2).eq.map_id_gr(nr))then
                 if(gt(nr2).eq.id(nr))then
                   if(twtypet(nr2).eq.4)then
                     if(iflag_fracmod(1,nr).eq.0)then
                       cp_fracmod(1,j)=cp_fracmod(1,j)+1.
                       iflag_fracmod(1,nr)=1
                     endif
                   endif
                   if(twtypet(nr2).eq.5)then
                     if(iflag_fracmod(2,nr).eq.0)then
                       cp_fracmod(2,j)=cp_fracmod(2,j)+1.
                       iflag_fracmod(2,nr)=1
                     endif
                   endif
                   if(twtypet(nr2).eq.6)then
                     if(iflag_fracmod(3,nr).eq.0)then
                       cp_fracmod(3,j)=cp_fracmod(3,j)+1.
                       iflag_fracmod(3,nr)=1
                     endif
                   endif
                   if(twtypet(nr2).eq.7)then
                     if(iflag_fracmod(4,nr).eq.0)then
                       cp_fracmod(4,j)=cp_fracmod(4,j)+1.
                       iflag_fracmod(4,nr)=1
                     endif
                   endif
                 endif
               endif
             enddo
           endif
         enddo
       enddo

c nb: iflag_fracmod is useless

       do j=1,nbr
         if(cp(1,j).ne.0)then
           write(1074,*)j,cp_fracmod(1,j)/float(cp(1,j))                  ! 1074: (same principle as 1006) grain diameter vs average number of T1, T2, C1 and C2 twins per grain
     #              ,cp_fracmod(2,j)/float(cp(1,j))
     #              ,cp_fracmod(3,j)/float(cp(1,j))
     #              ,cp_fracmod(4,j)/float(cp(1,j))  
     #              ,bar_area(j),bar_area(j+1)
         else
           write(1074,*)j,0.,0.,0.,0.
     #             ,bar_area(j),bar_area(j+1)       
         endif
       enddo


       do nr=1,nr_10
         do j=1,nbr
           if(area(nr).ge.bar_area(j).and.
     #        area(nr).lt.bar_area(j+1))then
             do nr2=1,nr_30
               if(map_id_tw(nr2).eq.map_id_gr(nr))then
                 if(gt(nr2).eq.id(nr))then
                   if(twtypet(nr2).eq.4)then
                     cp_twnbmod(1,j)=cp_twnbmod(1,j)+1.
                   endif
                   if(twtypet(nr2).eq.5)then
                     cp_twnbmod(2,j)=cp_twnbmod(2,j)+1.
                   endif
                   if(twtypet(nr2).eq.6)then
                     cp_twnbmod(3,j)=cp_twnbmod(3,j)+1.
                   endif
                   if(twtypet(nr2).eq.7)then
                     cp_twnbmod(4,j)=cp_twnbmod(4,j)+1.
                   endif
                 endif           
               endif
             enddo
           endif
         enddo
       enddo

       do j=1,nbr
         if(cp(3,j).ne.0)then
           write(1075,*)j,cp_twnbmod(1,j)/float(cp(3,j))                  ! 1075: grain diameter vs average number of T1, T2, C1 and C2 twins per twinned grain (nb: cp_fracmod counters could have been used)
     #              ,cp_twnbmod(2,j)/float(cp(3,j))
     #              ,cp_twnbmod(3,j)/float(cp(3,j))
     #              ,cp_twnbmod(4,j)/float(cp(3,j))  
     #              ,bar_area(j),bar_area(j+1)
     #              ,cp(3,j)
         else
           write(1075,*)j,0.,0.,0.,0.
     #             ,bar_area(j),bar_area(j+1)       
         endif
       enddo

       do nr=1,nr_10
         tmp1=0.
         tmp2=0.
         tmp3=0.
         tmp4=0.
         do nr2=1,nr_30
           if(map_id_tw(nr2).eq.map_id_gr(nr))then
             if(gt(nr2).eq.id(nr))then
               if(twtypet(nr2).eq.4) tmp1=tmp1+1.
               if(twtypet(nr2).eq.5) tmp2=tmp2+1.
               if(twtypet(nr2).eq.6) tmp3=tmp3+1.
               if(twtypet(nr2).eq.7) tmp4=tmp4+1.
             endif           
           endif
         enddo
         cp_twnbmod_gr(1,nr)=tmp1
         cp_twnbmod_gr(2,nr)=tmp2
         cp_twnbmod_gr(3,nr)=tmp3
         cp_twnbmod_gr(4,nr)=tmp4
       enddo

       do j=1,nbr
         do nr=1,nr_10
           if(area(nr).ge.bar_area(j).and.
     #        area(nr).lt.bar_area(j+1))then
              if(cp_twnbmod_gr(1,nr).gt.0.)then
                write(1076,*)j                                            ! 1076: grain area vs number of T1 twins per grain belonging to the considered area range; useful for scatter plots
     #                 ,(bar_area(j)+(bar_area(j+1)-bar_area(j))/4.)
     #                 ,cp_twnbmod_gr(1,nr)
     #                 ,bar_area(j),bar_area(j+1)
              endif 
              if(cp_twnbmod_gr(2,nr).gt.0.)then
                write(1077,*)j                                            ! 1077: same as 1076 but for T2
     #               ,(bar_area(j)+2.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,cp_twnbmod_gr(2,nr)
     #               ,bar_area(j),bar_area(j+1)
              endif  
              if(cp_twnbmod_gr(3,nr).gt.0.)then                           ! 1078: same as 1076 but for C1
                write(1078,*)j
     #               ,(bar_area(j)+3.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,cp_twnbmod_gr(3,nr)
     #               ,bar_area(j),bar_area(j+1)
              endif 
              if(cp_twnbmod_gr(4,nr).gt.0.)then                           ! 1079: same as 1076 but for C2
                write(1079,*)j
     #               ,(bar_area(j)+4.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,cp_twnbmod_gr(4,nr)
     #               ,bar_area(j),bar_area(j+1)
              endif   
           endif
         enddo
       enddo

ccc Indexing twin variants

      do nr=1,nr_30
        twvar(nr)=twvart(nr)+1
        twtype(nr)=twtypet(nr)-3
      enddo

ccc SF of all twins

       do nr=1,nr_30                     ! loop over all twins

         do i=1,3; do j=1,3       
           matrot(i,j)=0.
           matrotinv(i,j)=0.
         enddo; enddo    

         do nr2=1,nr_10
           if(map_id_tw(nr).eq.map_id_gr(nr2))then
             if(id(nr2).eq.gt(nr))then
               call q2m(qx(nr2),qy(nr2),qz(nr2),qw(nr2),matrot)
               call inversion(1,3,matrot,matrotinv)                           ! check matrices 
             endif
           endif
         enddo        
     

         do i=1,4
           do j=1,6

              do k=1,3
                nvs(i,j,k)=0.
                bvs(i,j,k)=0.
                do l=1,3
                  nvs(i,j,k)=nvs(i,j,k)+matrot(k,l)*nv(i,j,l)    
                  bvs(i,j,k)=bvs(i,j,k)+matrot(k,l)*bv(i,j,l)   
                enddo  ! do loop l
              enddo    ! do loop k

              do k=1,3
                do l=1,3
                  schmid(i,j,k,l)=0.5*(nvs(i,j,k)*bvs(i,j,l)
     #                                  +nvs(i,j,l)*bvs(i,j,k))
                enddo  ! do loop l
              enddo    ! do loop k

              SF(i,j)=0.

              do k=1,3
                do l=1,3
                  SF(i,j)=SF(i,j)+schmid(i,j,k,l)*sigma(k,l)
                enddo             
              enddo 
           
           enddo       ! do loop j (twin variants)
         enddo         ! do loop i (twin types)


c         if(iwarning.eq.0)then
           SFo(nr)=SF(twtype(nr),twvar(nr))
c         endif

         rk=1
         do i=1,6
           if(SFo(nr).lt.SF(twtype(nr),i))then
             rk=rk+1
           endif
         enddo
         rkSFo(nr)=rk

c         if(rkSFo(nr).ge.7)then
c           write(*,*) 'WARNING: variant rank greater or equal to 7'
c           write(*,*) 'WARNING: variant rank greater or equal to 7'
c           write(*,*) 'WARNING: variant rank greater or equal to 7'
c           write(*,*) rkSFo(nr),'rk',nr,'twin number'
c     #                  ,gt(nr),'parent id'
c         endif

       enddo           ! do loop nr (nr_30)
 
ccc ccc ccc ccc End twin SF calculation ccc ccc ccc ccc
ccc ccc ccc ccc End twin SF calculation ccc ccc ccc ccc 


ccc Final check before submission
       write(*,*)'  '
       write(*,*)' * WARNING REGARDING TWIN SCHMID VALUES '    
       write(*,*)'  '
       do nr=1,nr_30
         if(twtypet(nr).eq.6)then
           if(SFo(nr).lt.-0.45)then
             write(*,*)map_id_tw(nr),'map_id',nr,'nr',idt(nr),'id'
     #                 ,sizet(nr),'size',gt(nr),'gr_id',SFo(nr),'SF'
           endif
         endif
       enddo
       write(*,*)'  '
c       write(*,*)'  '


ccc ccc ccc ccc Thickness correction ccc ccc ccc
ccc ccc ccc ccc Thickness correction ccc ccc ccc

       do nr=1,nr_30

         do i=1,3; do j=1,3
           matrot(i,j)=0.
           matrotinv(i,j)=0.
         enddo; enddo

         do nr2=1,nr_10
           if(map_id_tw(nr).eq.map_id_gr(nr2))then
             if(id(nr2).eq.gt(nr))then
               call q2m(qx(nr2),qy(nr2),qz(nr2),qw(nr2),matrot)
               call inversion(1,3,matrot,matrotinv)               
             endif
           endif
         enddo


         do i=1,3
           nvr(i)=0.
         enddo

         do i=1,3; do j=1,3
           nvr(i)=nvr(i)+matrot(i,j)*nv(twtype(nr),twvar(nr),j)          
         enddo; enddo

         facthck=0.
         do i=1,3
           facthck=facthck+nvr(i)*load_dir(i)
         enddo
         temp_twthckcor=0.
         temp_twthckcor=thickt(nr)*facthck
         twthckcor(nr)=abs(temp_twthckcor)        

       enddo    ! end do nr (nr_30)

ccc ccc ccc ccc End thickness correction ccc ccc ccc
ccc ccc ccc ccc End thickness correction ccc ccc ccc

ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -


      do i=1,4; do j=1,nmax
        cp_twnbmod(i,j)=0.
        sum_twthckcor_mod(i,j)=0.
      enddo; enddo

       do nr=1,nr_10
         do j=1,nbr
           if(area(nr).ge.bar_area(j).and.
     #        area(nr).lt.bar_area(j+1))then
             do nr2=1,nr_30
               if(map_id_tw(nr2).eq.map_id_gr(nr))then
                 if(gt(nr2).eq.id(nr))then
                   if(twtypet(nr2).eq.4)then
                     cp_twnbmod(1,j)=cp_twnbmod(1,j)+1.
                     sum_twthckcor_mod(1,j)=sum_twthckcor_mod(1,j)
     #                                        +twthckcor(nr2)
                   endif
                   if(twtypet(nr2).eq.5)then
                     cp_twnbmod(2,j)=cp_twnbmod(2,j)+1.
                     sum_twthckcor_mod(2,j)=sum_twthckcor_mod(2,j)
     #                                        +twthckcor(nr2)
                   endif
                   if(twtypet(nr2).eq.6)then
                     cp_twnbmod(3,j)=cp_twnbmod(3,j)+1.
                     sum_twthckcor_mod(3,j)=sum_twthckcor_mod(3,j)
     #                                        +twthckcor(nr2)
                   endif
                   if(twtypet(nr2).eq.7)then
                     cp_twnbmod(4,j)=cp_twnbmod(4,j)+1.
                     sum_twthckcor_mod(4,j)=sum_twthckcor_mod(4,j)
     #                                        +twthckcor(nr2)
                   endif
                 endif           
               endif
             enddo
           endif
         enddo
       enddo

       do j=1,nbr
         if(cp_twnbmod(1,j).gt.2.)then
           write(1080,*)j,sum_twthckcor_mod(1,j)/cp_twnbmod(1,j)          ! 1080: grain area diameter vs average corrected T1 twin thickness
     #              ,bar_area(j),bar_area(j+1)
         else
           write(1080,*)j,0.
     #             ,bar_area(j),bar_area(j+1)       
         endif
         if(cp_twnbmod(2,j).gt.2.)then
           write(1081,*)j,sum_twthckcor_mod(2,j)/cp_twnbmod(2,j)          ! 1081: same as 1080 but for T2
     #              ,bar_area(j),bar_area(j+1)
         else
           write(1081,*)j,0.                                              
     #             ,bar_area(j),bar_area(j+1)       
         endif
         if(cp_twnbmod(3,j).gt.2.)then
           write(1082,*)j,sum_twthckcor_mod(3,j)/cp_twnbmod(3,j)          ! 1082: same as 1080 but for C1
     #              ,bar_area(j),bar_area(j+1)
         else
           write(1082,*)j,0.
     #             ,bar_area(j),bar_area(j+1)       
         endif
         if(cp_twnbmod(4,j).gt.0.)then                                    ! 1083: same as 1080 but for C2
           write(1083,*)j,sum_twthckcor_mod(4,j)/cp_twnbmod(4,j) 
     #              ,bar_area(j),bar_area(j+1)
         else
           write(1083,*)j,0.
     #             ,bar_area(j),bar_area(j+1)       
         endif
       enddo


cccc   Other plotting way: scatter plot

       do j=1,nbr
         do nr=1,nr_10
           if(area(nr).ge.bar_area(j).and.
     #        area(nr).lt.bar_area(j+1))then
             do nr2=1,nr_30
               if(map_id_tw(nr2).eq.map_id_gr(nr))then
                 if(gt(nr2).eq.id(nr))then
                   if(twtypet(nr2).eq.4)then
                     write(1084,*)j                                       ! 1084: grain area vs corrected twin thickness of T1 twins embedded in grains belonging to this area range
     #                 ,(bar_area(j)+(bar_area(j+1)-bar_area(j))/4.)
     #                 ,twthckcor(nr2)
     #                 ,bar_area(j),bar_area(j+1)                     
                   endif
                   if(twtypet(nr2).eq.5)then
                     write(1085,*)j                                       ! 1085: same as 1084 but for T2
     #               ,(bar_area(j)+2.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,twthckcor(nr2)
     #               ,bar_area(j),bar_area(j+1)                     
                   endif
                   if(twtypet(nr2).eq.6)then
                     write(1086,*)j                                       ! 1086: same as 1084 but for C1
     #               ,(bar_area(j)+3.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,twthckcor(nr2)
     #               ,bar_area(j),bar_area(j+1)                     
                   endif
                   if(twtypet(nr2).eq.7)then
                     write(1087,*)j                                       ! 1087: same as 1084 but for C2
     #               ,(bar_area(j)+4.*(bar_area(j+1)-bar_area(j))/4.)
     #               ,twthckcor(nr2)
     #               ,bar_area(j),bar_area(j+1)                     
                   endif
                 endif
               endif
             enddo
           endif           
         enddo
       enddo
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -
ccc - - - - - - - - - - - - - - - - - - - - - - - - - - -

ccc ccc ccc ccc NEW OUTPUT RESULTS ccc ccc ccc ccc
ccc ccc ccc ccc NEW OUTPUT RESULTS ccc ccc ccc ccc


ccc NB: for plots, use cp from cp(5,j) 

      do j=1,nbrs
        temp_twthck(j)=0.
        avg_twthck_sch(j)=0.
        temp_twarea(j)=0.
      enddo

ccc ccc Counting twins ccc ccc
ccc ccc Counting twins ccc ccc
ccc ccc Counting twins ccc ccc

       do i=1,4
         count_twtype(i)=0
       enddo
       
       do nr=1,nr_30
         if(twtype(nr).eq.1)then
           count_twtype(1)=count_twtype(1)+1
         endif
         if(twtype(nr).eq.2)then
           count_twtype(2)=count_twtype(2)+1
         endif
         if(twtype(nr).eq.3)then
           count_twtype(3)=count_twtype(3)+1
         endif
         if(twtype(nr).eq.4)then
           count_twtype(4)=count_twtype(4)+1
         endif
       enddo

      do j=1,nbrs
        cp(5,j)=0
        cp(9,j)=0
        cp(10,j)=0
        cp(11,j)=0
        cp(12,j)=0
      enddo

      do nr=1,nr_30
c        if(iw(nr).ne.1)then       
          do j=1,nbrs
            if(SFo(nr).gt.bar_sch(j).and.
     #           SFo(nr).lt.bar_sch(j+1))then
              cp(5,j)=cp(5,j)+1
              temp_twthck(j)=temp_twthck(j)+twthckcor(nr)            
              temp_twarea(j)=temp_twarea(j)+grain_twarea(gt(nr))      
              temp_twarea(j)=temp_twarea(j)+parent_area(nr)           
                if(twtype(nr).eq.1)then
                  cp(9,j)=cp(9,j)+1
                endif
                if(twtype(nr).eq.2)then
                  cp(10,j)=cp(10,j)+1
                endif
                if(twtype(nr).eq.3)then
                  cp(11,j)=cp(11,j)+1
                endif
                if(twtype(nr).eq.4)then
                  cp(12,j)=cp(12,j)+1
                endif
            endif
          enddo
c        endif
      enddo

      do j=1,nbrs
        if(cp(5,j).ne.0)then
          avg_twthck_sch(j)=temp_twthck(j)/cp(5,j)
          avg_twarea_sch(j)=temp_twarea(j)/cp(5,j)              
        else
          avg_twthck_sch(j)=0.
          avg_twarea_sch(j)=0.
        endif
      enddo

      do j=1,nbrs

        write(1008,*)j,bar_sch(j),bar_sch(j+1)                            ! 1008: Schmid factor vs number of twins having their SF included in the considered SF bracket
     #                ,float(cp(5,j))/float(nr_30),cp(5,j)
        write(1009,*)j,bar_sch(j),bar_sch(j+1)                            ! 1009: SF vs average  corrected twin thickness
     #                ,avg_twthck_sch(j),cp(5,j)
        write(1010,*)j,bar_sch(j),bar_sch(j+1)                            ! 1010: SF vs average twin area -> CANNOT BE USED! 
     #                ,avg_twarea_sch(j),cp(5,j)

        if(count_twtype(1).ne.0)then
        write(1026,*)j,bar_sch(j),bar_sch(j+1)                            ! 1026: SF vs number of T1 twins                  
     #                ,float(cp(9,j))/float(count_twtype(1)),cp(9,j)
        else
        write(1026,*)j,bar_sch(j),bar_sch(j+1)                    
     #                ,0.,cp(9,j)
        endif   

        if(count_twtype(2).ne.0)then     
        write(1027,*)j,bar_sch(j),bar_sch(j+1)                            ! 1027: same as 1026 but for T2 twins
     #                ,float(cp(10,j))/float(count_twtype(2)),cp(10,j)
        else
        write(1027,*)j,bar_sch(j),bar_sch(j+1)
     #                ,0.,cp(10,j)
        endif

        if(count_twtype(3).ne.0)then     
        write(1028,*)j,bar_sch(j),bar_sch(j+1)                            ! 1028: same as 1026 but for C1 twins
     #                ,float(cp(11,j))/float(count_twtype(3)),cp(11,j)
        else
        write(1028,*)j,bar_sch(j),bar_sch(j+1)
     #                ,0.,cp(11,j)
        endif

        if(count_twtype(4).ne.0)then     
        write(1029,*)j,bar_sch(j),bar_sch(j+1)
     #                ,float(cp(12,j))/float(count_twtype(4)),cp(12,j)    ! 1029: same as 1026 but for C2 twins
        else
        write(1029,*)j,bar_sch(j),bar_sch(j+1)
     #                ,0.,cp(12,j)
        endif
c        write(1030,*)j,bar_sch(j),bar_sch(j+1)                            
c     #                ,float(cp(5,j))/float(nr_30),cp(5,j)
      enddo


      do j=1,nbrs2
        cp(6,j)=0
        cp(13,j)=0  ! tt1
        cp(14,j)=0  ! tt2
        cp(15,j)=0  ! ct1
        cp(16,j)=0  ! ct2
        cp(17,j)=0  ! rk1
        cp(18,j)=0  ! rk2
        cp(19,j)=0  ! rk3
        cp(20,j)=0  ! rk4 and plus  -> laisser 20,21,22 for the remaining variants
        temp_twthck_sf(j)=0.
        avg_twthck_sf(j)=0.
      enddo

      do i=1,4
        do j=1,nbrs2   
          temp_twthck_type_sf(i,j)=0.
          avg_twthck_type_sf(i,j)=0.
        enddo
      enddo

      do nr=1,nr_30
c        if(iw(nr).ne.1)then
          do j=1,nbrs2
            if(SFo(nr).ge.bar_sf(j).and.
     #           SFo(nr).lt.bar_sf(j+1))then
              cp(6,j)=cp(6,j)+1
              temp_twthck_sf(j)=temp_twthck_sf(j)+twthckcor(nr)
              temp_twarea_sf(j)=temp_twarea_sf(j)+
     #                            areat(nr)/parent_area(nr)        

                if(twtype(nr).eq.1)then
                  cp(13,j)=cp(13,j)+1
                  temp_twthck_type_sf(1,j)=temp_twthck_type_sf(1,j)+  
     #                                                twthckcor(nr)
                  if(rkSFo(nr).eq.1) cp(29,j)=cp(29,j)+1
                  if(rkSFo(nr).eq.2) cp(30,j)=cp(30,j)+1
                  if(rkSFo(nr).eq.3) cp(31,j)=cp(31,j)+1
                  if(rkSFo(nr).gt.3) cp(32,j)=cp(32,j)+1
                endif
                if(twtype(nr).eq.2)then
                  cp(14,j)=cp(14,j)+1
                  temp_twthck_type_sf(2,j)=temp_twthck_type_sf(2,j)+
     #                                                twthckcor(nr)
                  if(rkSFo(nr).eq.1) cp(33,j)=cp(33,j)+1
                  if(rkSFo(nr).eq.2) cp(34,j)=cp(34,j)+1
                  if(rkSFo(nr).eq.3) cp(35,j)=cp(35,j)+1
                  if(rkSFo(nr).gt.3) cp(36,j)=cp(36,j)+1
                endif
                if(twtype(nr).eq.3)then
                  cp(15,j)=cp(15,j)+1
                  temp_twthck_type_sf(3,j)=temp_twthck_type_sf(3,j)+
     #                                                twthckcor(nr)
                  if(rkSFo(nr).eq.1) cp(37,j)=cp(37,j)+1
                  if(rkSFo(nr).eq.2) cp(38,j)=cp(38,j)+1
                  if(rkSFo(nr).eq.3) cp(39,j)=cp(39,j)+1
                  if(rkSFo(nr).gt.3) cp(40,j)=cp(40,j)+1
                endif
                if(twtype(nr).eq.4)then
                  cp(16,j)=cp(16,j)+1
                  temp_twthck_type_sf(4,j)=temp_twthck_type_sf(4,j)+
     #                                                twthckcor(nr)
                  if(rkSFo(nr).eq.1) cp(41,j)=cp(41,j)+1
                  if(rkSFo(nr).eq.2) cp(42,j)=cp(42,j)+1
                  if(rkSFo(nr).eq.3) cp(43,j)=cp(43,j)+1
                  if(rkSFo(nr).gt.3) cp(44,j)=cp(44,j)+1
                endif

                if(rkSFo(nr).eq.1) cp(17,j)=cp(17,j)+1
                if(rkSFo(nr).eq.2) cp(18,j)=cp(18,j)+1
                if(rkSFo(nr).eq.3) cp(19,j)=cp(19,j)+1
                if(rkSFo(nr).gt.3) cp(20,j)=cp(20,j)+1

            endif
          enddo
c        endif
      enddo

      do i=1,4                                                  
        do j=1,nbrs2 
          if(cp(12+i,j).ne.0)then
            avg_twthck_type_sf(i,j)=temp_twthck_type_sf(i,j)
     #                                      /float(cp(12+i,j))
          else
            avg_twthck_type_sf(i,j)=0.
          endif
        enddo
      enddo

      do j=1,nbrs2
        if(cp(6,j).ne.0)then
          avg_twthck_sf(j)=temp_twthck_sf(j)/float(cp(6,j))
          avg_twarea_sf(j)=temp_twarea_sf(j)/float(cp(6,j))
        else
          avg_twthck_sf(j)=0.
          avg_twarea_sf(j)=0.
        endif
      enddo

ccc warning

      do nr=1,nr_30
        if(rkSFo(nr).ge.7)then
          write(*,*)rkSFo(nr),nr,'warning rk'
        endif
      enddo


      do j=1,nbrs2
        write(1042,*)j,bar_sf(j),bar_sf(j+1)                              ! 1042: SF vs average corrected twin thickness
     #                ,avg_twthck_sf(j),cp(6,j)
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))                   

        write(1061,*)j,bar_sf(j),bar_sf(j+1)                              ! 1061: SF vs average corrected thickness of T1, T2, C1 and C2 twins                       
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))
     #                ,avg_twthck_type_sf(1,j),avg_twthck_type_sf(2,j)
     #                ,avg_twthck_type_sf(3,j),avg_twthck_type_sf(4,j)
     #                ,cp(13,j),cp(14,j),cp(15,j),cp(16,j)

        write(1046,*)j,bar_sf(j),bar_sf(j+1)                              ! 1046: SF vs average twin area
     #                ,avg_twarea_sf(j),cp(6,j)
      
        write(1011,*)j,bar_sf(j),bar_sf(j+1)                              ! 1011: SF vs number of twins (same as 1008 but including negative SF)
     #                ,float(cp(6,j))/float(nr_30),cp(6,j)

        write(1031,*)j,bar_sf(j),bar_sf(j+1)                              ! 1031: SF vs fraction of T1 twins ( = number of T1 twins with SF included in the considered SF bracket / total number of twins)
     #                ,float(cp(13,j))/float(nr_30),cp(13,j)
        write(1032,*)j,bar_sf(j),bar_sf(j+1)                              ! 1032: same as 1031 but for T2 twins
     #                ,float(cp(14,j))/float(nr_30),cp(14,j)
        write(1033,*)j,bar_sf(j),bar_sf(j+1)                              ! 1033: same as 1031 but for C1 twins
     #                ,float(cp(15,j))/float(nr_30),cp(15,j)
        write(1034,*)j,bar_sf(j),bar_sf(j+1)                              ! 1034: same as 1031 but for C2 twins
     #                ,float(cp(16,j))/float(nr_30),cp(16,j)

        write(1045,*)j,bar_sf(j),bar_sf(j+1)                              ! 1045: combination of 1031, 1032, 1033 and 1035 files
     #                ,float(cp(13,j))/float(nr_30)
     #                ,float(cp(14,j))/float(nr_30)
     #                ,float(cp(15,j))/float(nr_30)
     #                ,float(cp(16,j))/float(nr_30)

        write(1039,*)j,bar_sf(j),bar_sf(j+1)                              ! 1039: SF vs fraction of 1st rank SF twin variants 
     #                ,float(cp(17,j))/float(nr_30),cp(17,j)
        write(1040,*)j,bar_sf(j),bar_sf(j+1)                              ! 1040: same as 1039 but for 2nd rank twin variants
     #                ,float(cp(18,j))/float(nr_30),cp(18,j)
        write(1041,*)j,bar_sf(j),bar_sf(j+1)                              ! 1041: same as 1039 but for 3rd rank twin variants
     #                ,float(cp(19,j))/float(nr_30),cp(19,j)
        write(1043,*)j,bar_sf(j),bar_sf(j+1)                              ! 1042: same as 1039 but for 4th, 5th and 6th rank twin variants
     #                ,float(cp(20,j))/float(nr_30),cp(20,j)
        write(1044,*)j,bar_sf(j),bar_sf(j+1)                              ! 1044: combination of 1039, 1040, 1041 and 1043 files
     #                ,float(cp(17,j))/float(nr_30)
     #                ,float(cp(18,j))/float(nr_30)
     #                ,float(cp(19,j))/float(nr_30)
     #                ,float(cp(20,j))/float(nr_30)

        write(1057,*)j,bar_sf(j),bar_sf(j+1)                              ! 1057: SF vs fraction of 1st, 2nd, 3rd and 3rd+ rank SF T1 twin variants - fraction computed with respect to the total number of twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))            
     #                ,float(cp(29,j))/float(nr_30)
     #                ,float(cp(30,j))/float(nr_30)
     #                ,float(cp(31,j))/float(nr_30)
     #                ,float(cp(32,j))/float(nr_30)
     #                ,cp(29,j),cp(30,j),cp(31,j),cp(32,j)
        write(1058,*)j,bar_sf(j),bar_sf(j+1)                              ! 1058: same as 1057 but for T2 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(33,j))/float(nr_30)
     #                ,float(cp(34,j))/float(nr_30)
     #                ,float(cp(35,j))/float(nr_30)
     #                ,float(cp(36,j))/float(nr_30)
     #                ,cp(33,j),cp(34,j),cp(35,j),cp(36,j)
        write(1059,*)j,bar_sf(j),bar_sf(j+1)                              ! 1059: same as 1057 but for C1 twins 
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(37,j))/float(nr_30)
     #                ,float(cp(38,j))/float(nr_30)
     #                ,float(cp(39,j))/float(nr_30)
     #                ,float(cp(40,j))/float(nr_30)
     #                ,cp(37,j),cp(38,j),cp(39,j),cp(40,j)
        write(1060,*)j,bar_sf(j),bar_sf(j+1)                              ! 1060: same as 1057 but for C2 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(41,j))/float(nr_30)
     #                ,float(cp(42,j))/float(nr_30)
     #                ,float(cp(43,j))/float(nr_30)
     #                ,float(cp(44,j))/float(nr_30)
     #                ,cp(41,j),cp(42,j),cp(43,j),cp(44,j)

        write(1053,*)j,bar_sf(j),bar_sf(j+1)                              ! 1053: same as 1057 except that the fraction is computed using the total number of T1 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(29,j))/float(count_twtype(1))
     #                ,float(cp(30,j))/float(count_twtype(1))
     #                ,float(cp(31,j))/float(count_twtype(1))
     #                ,float(cp(32,j))/float(count_twtype(1))
     #                ,cp(29,j),cp(30,j),cp(31,j),cp(32,j)
        write(1054,*)j,bar_sf(j),bar_sf(j+1)                              ! 1054: same as 1053 but for T2 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(33,j))/float(count_twtype(2))
     #                ,float(cp(34,j))/float(count_twtype(2))
     #                ,float(cp(35,j))/float(count_twtype(2))
     #                ,float(cp(36,j))/float(count_twtype(2))
     #                ,cp(33,j),cp(34,j),cp(35,j),cp(36,j)
        write(1055,*)j,bar_sf(j),bar_sf(j+1)                              ! 1055: same as 1053 but for C1 twins 
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(37,j))/float(count_twtype(3))
     #                ,float(cp(38,j))/float(count_twtype(3))
     #                ,float(cp(39,j))/float(count_twtype(3))
     #                ,float(cp(40,j))/float(count_twtype(3))
     #                ,cp(37,j),cp(38,j),cp(39,j),cp(40,j)
        write(1056,*)j,bar_sf(j),bar_sf(j+1)                              ! 1056: same as 1053 but for C2 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))             
     #                ,float(cp(41,j))/float(count_twtype(4))
     #                ,float(cp(42,j))/float(count_twtype(4))
     #                ,float(cp(43,j))/float(count_twtype(4))
     #                ,float(cp(44,j))/float(count_twtype(4))
     #                ,cp(41,j),cp(42,j),cp(43,j),cp(44,j)
     
       enddo


ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc  
ccc ccc ccc ccc ccc ccc  TWIN IFO STATISTICS ccc ccc ccc ccc ccc ccc ccc ccc ccc    
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc  

      do j=1,nbrs2
        counterSF(j)=0.
        counterSF_T1(j)=0.
        counterSF_T2(j)=0.
        counterSF_C1(j)=0.
        counterSF_C2(j)=0.
        counterArea(j)=0.
        counterArea_T1(j)=0.
        counterArea_T2(j)=0.
        counterArea_C1(j)=0.
        counterArea_C2(j)=0.
      enddo 

      do nr=1,nr_30
       do j=1,nbr
        do nr2=1,nr_10
         if(map_id_tw(nr).eq.map_id_gr(nr2))then
          if(gt(nr).eq.id(nr2))then
           if(area(nr2).ge.bar_area(j).and.
     #       area(nr2).lt.bar_area(j+1))then
            counterArea(j)=counterArea(j)+1.
            if(twtypet(nr).eq.4) counterArea_T1(j)=counterArea_T1(j)+1
            if(twtypet(nr).eq.5) counterArea_T2(j)=counterArea_T2(j)+1
            if(twtypet(nr).eq.6) counterArea_C1(j)=counterArea_C1(j)+1
            if(twtypet(nr).eq.7) counterArea_C2(j)=counterArea_C2(j)+1
           endif
          endif 
         endif
        enddo
       enddo
      enddo    

      do nr=1,nr_30
        do j=1,nbrs2
          if(SFo(nr).ge.bar_sf(j).and.
     #      SFo(nr).lt.bar_sf(j+1))then
            counterSF(j)=counterSF(j)+1.
            if(twtypet(nr).eq.4) counterSF_T1(j)=counterSF_T1(j)+1.
            if(twtypet(nr).eq.5) counterSF_T2(j)=counterSF_T2(j)+1.
            if(twtypet(nr).eq.6) counterSF_C1(j)=counterSF_C1(j)+1.
            if(twtypet(nr).eq.7) counterSF_C2(j)=counterSF_C2(j)+1.
          endif
        enddo
      enddo


      do j=1,nbrs2
        write(1068,*)j,bar_area(j),bar_area(j+1),counterArea(j)           ! 1068: grain area vs number of T1, T2, C1 and C2 twins embedded in grains with an area included in the considered area range
     #               ,counterArea_T1(j),counterArea_T2(j)
     #               ,counterArea_C1(j),counterArea_C2(j)            
      enddo

      do j=1,nbrs2
        write(1069,*)j,bar_sf(j),bar_sf(j+1)                              ! 1069: SF vs number of T1, T2, C1 and C2 twins embedded in grains with an area included in the considered area range
     #                ,counterSF(j),counterSF_T1(j),counterSF_T2(j)
     #                ,counterSF_C1(j),counterSF_C2(j)            
      enddo

      do j=1,nbrs2                                                       
        write(1088,*)j,bar_area(j),bar_area(j+1),counterArea(j)/nr_30     ! 1088: grain area vs fraction of T1, T2, C1 and C2 twins embedded in grains with an area included in the considered area range (nb: fraction computed using the total number of twins) 
     #               ,counterArea_T1(j)/nr_30,counterArea_T2(j)/nr_30
     #               ,counterArea_C1(j)/nr_30,counterArea_C2(j)/nr_30            
      enddo



ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc  
ccc ccc ccc ccc ccc ccc FOCUS ON NUCLEATION ccc ccc ccc ccc ccc ccc ccc ccc ccc    
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc and Verification of previous stats

       do nr=1,nr_30       
         do nr2=1,nr_10
           if(map_id_tw(nr).eq.map_id_gr(nr2))then
             if(id(nr2).eq.gt(nr))then
               do j=1,nbr
                 if(area(nr2).ge.bar_area(j) .and. 
     #           area(nr2).lt.bar_area(j+1))then
                   cp(50,j)=cp(50,j)+1
                   if(twtype(nr).eq.1)then
                     cp(51,j)=cp(51,j)+1
                   endif
                   if(twtype(nr).eq.2)then
                     cp(52,j)=cp(52,j)+1
                   endif
                   if(twtype(nr).eq.3)then
                     cp(53,j)=cp(53,j)+1
                   endif
                   if(twtype(nr).eq.4)then
                     cp(54,j)=cp(54,j)+1
                   endif
                 endif
               enddo
             endif
           endif
         enddo
       enddo
       
       do j=1,nbr
        if(cp(50,j).ne.0)then
        write(1063,*)j,bar_area(j),bar_area(j+1)                          ! 1063: grain area vs fraction of T1, T2, C1 and C2 twins (nb: fraction computed using the total number of twins embedded in grains with area belonging to the area range)
     #                ,0.5*(bar_area(j)+bar_area(j+1))             
     #                ,float(cp(51,j))/float(cp(50,j))
     #                ,float(cp(52,j))/float(cp(50,j))
     #                ,float(cp(53,j))/float(cp(50,j))
     #                ,float(cp(54,j))/float(cp(50,j))
     #                ,cp(50,j),cp(51,j),cp(52,j),cp(53,j),cp(54,j)
        else
        write(1063,*)j,bar_area(j),bar_area(j+1)
     #                ,0.5*(bar_area(j)+bar_area(j+1))             
     #                ,0.,0.,0.,0.,0.,0.,0.,0.,0.
        endif
       enddo

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc  
ccc ccc ccc ccc ccc ccc END SPECIAL NUCLEATION ccc ccc ccc ccc ccc ccc ccc ccc ccc    
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc  

ccc -------------------------------------------------------------------------------

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc 
ccc ccc ccc ccc ccc ccc  HIGHEST SCHMID FACTORS ccc ccc ccc ccc ccc ccc ccc ccc ccc 
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc 

ccc For all grains

ccc loops aimed at determining the highest SF that can have a twin in a given grain

        do nr=1,nr_10

          hallSF(nr)=-1.

          call q2m(qx(nr),qy(nr),qz(nr),qw(nr),matrot)
          call inversion(1,3,matrot,matrotinv)                           


           do i=1,4
             do j=1,6

                SF(i,j)=0.

                do k=1,3
                  nvs(i,j,k)=0.
                  bvs(i,j,k)=0.
                  do l=1,3   
                    nvs(i,j,k)=nvs(i,j,k)+matrot(k,l)*nv(i,j,l)            
                    bvs(i,j,k)=bvs(i,j,k)+matrot(k,l)*bv(i,j,l)         
                  enddo  ! do loop l
                enddo    ! do loop k

                do k=1,3
                  do l=1,3
                    schmid(i,j,k,l)=0.5*(nvs(i,j,k)*bvs(i,j,l)
     #                                  +nvs(i,j,l)*bvs(i,j,k))
                  enddo  ! do loop l
                enddo    ! do loop k

                do k=1,3
                  do l=1,3
                    SF(i,j)=SF(i,j)+schmid(i,j,k,l)*sigma(k,l)
                  enddo             
                enddo

             enddo       ! do loop j (twin variants)
           enddo         ! do loop i (twin types)

           do i=1,4
             do j=1,6
               if(SF(i,j).ge.hallSF(nr))then
                 hallSF(nr)=SF(i,j)
               endif
             enddo
           enddo

         enddo  ! nr_10 (all grains)

ccc For twinned grains

        test2=0.

        do nr=1,nr_20

           hpSF(nr)=-1.

           do nr2=1,nr_10
            if(map_id_par(nr).eq.map_id_gr(nr2))then
              if(id(nr2).eq.idp(nr))then
                call q2m(qx(nr2),qy(nr2),qz(nr2),qw(nr2),matrot)
                call inversion(1,3,matrot,matrotinv)                           ! check matrices 
              endif
            endif
           enddo

           do i=1,4
             do j=1,6

                SF(i,j)=0.

                do k=1,3
                  nvs(i,j,k)=0.
                  bvs(i,j,k)=0.
                  do l=1,3  
                     nvs(i,j,k)=nvs(i,j,k)+matrot(k,l)*nv(i,j,l)    
                     bvs(i,j,k)=bvs(i,j,k)+matrot(k,l)*bv(i,j,l)  
                  enddo  ! do loop l
                enddo    ! do loop k

                do k=1,3
                  do l=1,3
                    schmid(i,j,k,l)=0.5*(nvs(i,j,k)*bvs(i,j,l)
     #                                  +nvs(i,j,l)*bvs(i,j,k))
                  enddo  ! do loop l
                enddo    ! do loop k

                do k=1,3
                  do l=1,3
                    SF(i,j)=SF(i,j)+schmid(i,j,k,l)*sigma(k,l)
                  enddo             
                enddo

             enddo       ! do loop j (twin variants)
           enddo         ! do loop i (twin types)

           do i=1,4
             do j=1,6
               if(SF(i,j).ge.hpSF(nr))then
                 hpSF(nr)=SF(i,j)
               endif
             enddo
           enddo

         enddo  ! nr_20 (parent grains)

ccc NB: hallSF, hpSF denote the highest SF value that can have a twin in a given grain and parent, respectively - note they denote a potential value.

ccc Counting

         do nr=1,nr_10
           do j=1,nbrs2
             if(hallSF(nr).ge.bar_sf(j).and.
     #           hallSF(nr).lt.bar_sf(j+1))then
               cp(21,j)=cp(21,j)+1
             endif
           enddo
         enddo

         do nr=1,nr_20
           do j=1,nbrs2
             if(hpSF(nr).ge.bar_sf(j).and.
     #           hpSF(nr).lt.bar_sf(j+1))then
               cp(22,j)=cp(22,j)+1
            endif
           enddo
         enddo

       do j=1,nbrs2
         write(1047,*)j,bar_sf(j),bar_sf(j+1)                             ! 1047: distribution of highest potential SF values for grains and parents     
     #                ,float(cp(21,j))/float(nr_10)
     #                ,float(cp(22,j))/float(nr_10)
       enddo

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc 
ccc ccc ccc ccc ccc ccc END HIGHEST SCHMID FACTORS ccc ccc ccc ccc ccc ccc ccc ccc 
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc 

ccc
ccc
ccc

       do j=1,nbr
         twthck(j)=0.
         av_twthckcor(j)=0.
       enddo 

       do j=1,nbr
         max_local(4,j)=1.     ! nb of twins per grain
         min_local(4,j)=1000.
       enddo

       do nr=1,nr_30       !  --> deal with twin thickness and max-min
       do nr2=1,nr_10

          if(map_id_tw(nr).eq.map_id_gr(nr2))then
          if(id(nr2).eq.gt(nr))then

          do j=1,nbr
             if(area(nr2).ge.bar_area(j) .and. 
     #           area(nr2).lt.bar_area(j+1))then
               cp(7,j)=cp(7,j)+1 
               twthck(j)=twthck(j)+twthckcor(nr)           ! twthck (for the sum),av_twthck (final avg) ,avgtwthp (from the table) TO PERF AVG
               if(twthckcor(nr).gt.max_local(4,j))then
                 max_local(4,j)=twthckcor(nr)
               endif
               if(twthckcor(nr).lt.min_local(4,j))then
                 min_local(4,j)=twthckcor(nr)
               endif 

               if(twtype(nr).eq.1)then                                  
                 cp(46,j)=cp(46,j)+1
                 temp_twthck_type_area(1,j)=temp_twthck_type_area(1,j)+
     #                                                twthckcor(nr)
               endif
               if(twtype(nr).eq.2)then
                 cp(47,j)=cp(47,j)+1
                 temp_twthck_type_area(2,j)=temp_twthck_type_area(2,j)+
     #                                                twthckcor(nr)
               endif
               if(twtype(nr).eq.3)then
                 cp(48,j)=cp(48,j)+1
                 temp_twthck_type_area(3,j)=temp_twthck_type_area(3,j)+
     #                                                twthckcor(nr)
               endif
               if(twtype(nr).eq.4)then
                 cp(49,j)=cp(49,j)+1
                 temp_twthck_type_area(4,j)=temp_twthck_type_area(4,j)+
     #                                                twthckcor(nr)
               endif

               if(sqrt(4.*area(nr2)/pi).ge.bar_diam(j) .and. 
     #           sqrt(4.*area(nr2)/pi).lt.bar_diam(j+1))then
                 cp(8,j)=cp(8,j)+1 
               endif

             endif
          enddo

          endif
          endif
  
       enddo
       enddo

       do j=1,nbr                                                     
         av_twthckcor(j)=twthck(j)/float(cp(7,j))
         do i=1,4
           if(cp(45+i,j).ne.0)then
             twthck_type_area(i,j)=temp_twthck_type_area(i,j)/
     #                              float(cp(45+i,j)) 
           else
             twthck_type_area(i,j)=0.
           endif
         enddo
       enddo

       do j=1,nbr                                                 
         write(1062,*)j,bar_area(j),bar_area(j+1),                        ! 1062: grain area vs average correctd T1, T2, C1 and C2 twin thicknesses
     #                 0.5*(bar_area(j)+bar_area(j+1)),
     #                 twthck_type_area(1,j),twthck_type_area(2,j),
     #                 twthck_type_area(3,j),twthck_type_area(4,j),
     #                 cp(46,j),cp(47,j),cp(48,j),cp(49,j)     
       enddo


       do j=1,nbr
          if(cp(7,j).ne.0)then
            write(1025,*)j,av_twthckcor(j),min_local(4,j),max_local(4,j)  ! 1025: grain area vs average corrected thickness, min and max thicknesses of twins embedded in grains with area belonging to the considered area range
     #                   ,bar_area(j),bar_area(j+1)
          else    ! to avoid 'Nan' statement
c29may            write(*,*)'Warning NAN-parents area for twin thickness'
c29may     #                ,bar_area(j),bar_area(j+1)
            write(1025,*)j,0.,0.,0.
     #                   ,bar_area(j),bar_area(j+1)      
          endif
       enddo



cccccccccccccccccccccccccccccccc
ccc --- Twin-twin junctions ---
cccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccc
ccc --- Twin-twin junctions ---
cccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccc
ccc --- Twin-twin junctions ---
cccccccccccccccccccccccccccccccc


       do nr=1,nr_40

         tw1var_pa(nr)=tw1var(nr)+1      
         tw2var_pa(nr)=tw2var(nr)+1

       enddo	 ! enddo nr_40

      do i=1,110
        vctw2(i)=0
      enddo

      do nr=1,nr_40

        if(tw1twin(nr).eq.4 .and. tw2twin(nr).eq.4) then  
        vctw2(10)=vctw2(10)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(14)=vctw2(14)+1    
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(12)=vctw2(12)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(12)=vctw2(12)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(13)=vctw2(13)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(13)=vctw2(13)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(11)=vctw2(11)+1
        endif

        if(tw1twin(nr).eq.4 .and. tw2twin(nr).eq.5) then  
         vctw2(20)=vctw2(20)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.0)vctw2(21)=vctw2(21)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-5)vctw2(21)=vctw2(21)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.1)vctw2(21)=vctw2(21)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-4)vctw2(22)=vctw2(22)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-1)vctw2(22)=vctw2(22)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.2)vctw2(22)=vctw2(22)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.5)vctw2(22)=vctw2(22)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-3)vctw2(23)=vctw2(23)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-2)vctw2(23)=vctw2(23)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.3)vctw2(23)=vctw2(23)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.4)vctw2(23)=vctw2(23)+1
        endif

        if(tw1twin(nr).eq.5 .and. tw2twin(nr).eq.4) then  
         vctw2(20)=vctw2(20)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.0)vctw2(21)=vctw2(21)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-5)vctw2(21)=vctw2(21)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.1)vctw2(21)=vctw2(21)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-4)vctw2(22)=vctw2(22)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-1)vctw2(22)=vctw2(22)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.2)vctw2(22)=vctw2(22)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.5)vctw2(22)=vctw2(22)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-3)vctw2(23)=vctw2(23)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-2)vctw2(23)=vctw2(23)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.3)vctw2(23)=vctw2(23)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.4)vctw2(23)=vctw2(23)+1
        endif

        if(tw1twin(nr).eq.4 .and. tw2twin(nr).eq.6) then  
         vctw2(30)=vctw2(30)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.0)vctw2(31)=vctw2(31)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-5)vctw2(31)=vctw2(31)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.1)vctw2(31)=vctw2(31)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-4)vctw2(32)=vctw2(32)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-1)vctw2(32)=vctw2(32)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.2)vctw2(32)=vctw2(32)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.5)vctw2(32)=vctw2(32)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-3)vctw2(33)=vctw2(33)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-2)vctw2(33)=vctw2(33)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.3)vctw2(33)=vctw2(33)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.4)vctw2(33)=vctw2(33)+1
        endif

        if(tw1twin(nr).eq.6 .and. tw2twin(nr).eq.4) then  
         vctw2(30)=vctw2(30)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.0)vctw2(31)=vctw2(31)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-5)vctw2(31)=vctw2(31)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.1)vctw2(31)=vctw2(31)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-4)vctw2(32)=vctw2(32)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-1)vctw2(32)=vctw2(32)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.2)vctw2(32)=vctw2(32)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.5)vctw2(32)=vctw2(32)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-3)vctw2(33)=vctw2(33)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-2)vctw2(33)=vctw2(33)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.3)vctw2(33)=vctw2(33)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.4)vctw2(33)=vctw2(33)+1
        endif

        if(tw1twin(nr).eq.4 .and. tw2twin(nr).eq.7) then  
        vctw2(40)=vctw2(40)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(44)=vctw2(44)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(42)=vctw2(42)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(42)=vctw2(42)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(43)=vctw2(43)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(43)=vctw2(43)+1
	if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(41)=vctw2(41)+1
        endif

        if(tw2twin(nr).eq.4 .and. tw1twin(nr).eq.7) then  
        vctw2(40)=vctw2(40)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(44)=vctw2(44)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(42)=vctw2(42)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(42)=vctw2(42)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(43)=vctw2(43)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(43)=vctw2(43)+1
	if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(41)=vctw2(41)+1
        endif

        if(tw1twin(nr).eq.5 .and. tw2twin(nr).eq.5) then  
        vctw2(50)=vctw2(50)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(54)=vctw2(54)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(52)=vctw2(52)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(52)=vctw2(52)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(53)=vctw2(53)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(53)=vctw2(53)+1
	if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(51)=vctw2(51)+1
        endif

        if(tw1twin(nr).eq.5 .and. tw2twin(nr).eq.6) then  
        vctw2(60)=vctw2(60)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(64)=vctw2(64)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(62)=vctw2(62)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(62)=vctw2(62)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(63)=vctw2(63)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(63)=vctw2(63)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(61)=vctw2(61)+1
        endif

        if(tw1twin(nr).eq.6 .and. tw2twin(nr).eq.5) then  
        vctw2(60)=vctw2(60)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(64)=vctw2(64)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(62)=vctw2(62)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(62)=vctw2(62)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(63)=vctw2(63)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(63)=vctw2(63)+1
        if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(61)=vctw2(61)+1
        endif

        if(tw1twin(nr).eq.5 .and. tw2twin(nr).eq.7) then  
         vctw2(70)=vctw2(70)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.0)vctw2(71)=vctw2(71)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-5)vctw2(71)=vctw2(71)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.1)vctw2(71)=vctw2(71)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-4)vctw2(72)=vctw2(72)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-1)vctw2(72)=vctw2(72)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.2)vctw2(72)=vctw2(72)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.5)vctw2(72)=vctw2(72)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-3)vctw2(73)=vctw2(73)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-2)vctw2(73)=vctw2(73)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.3)vctw2(73)=vctw2(73)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.4)vctw2(73)=vctw2(73)+1
        endif

        if(tw1twin(nr).eq.7 .and. tw2twin(nr).eq.5) then  
         vctw2(70)=vctw2(70)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.0)vctw2(1)=vctw2(1)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-5)vctw2(1)=vctw2(1)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.1)vctw2(1)=vctw2(1)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-4)vctw2(2)=vctw2(2)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-1)vctw2(2)=vctw2(2)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.2)vctw2(2)=vctw2(2)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.5)vctw2(2)=vctw2(2)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-3)vctw2(3)=vctw2(3)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-2)vctw2(3)=vctw2(3)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.3)vctw2(3)=vctw2(3)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.4)vctw2(3)=vctw2(3)+1
        endif

        if(tw1twin(nr).eq.6 .and. tw2twin(nr).eq.6) then  
         vctw2(80)=vctw2(80)+1
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)vctw2(84)=vctw2(84)+1        
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)vctw2(82)=vctw2(82)+1        
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)vctw2(82)=vctw2(82)+1        
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)vctw2(83)=vctw2(83)+1         
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)vctw2(83)=vctw2(83)+1       
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)vctw2(81)=vctw2(81)+1
        endif

        if(tw1twin(nr).eq.6 .and. tw2twin(nr).eq.7) then  
         vctw2(90)=vctw2(90)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.0)vctw2(91)=vctw2(91)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-5)vctw2(91)=vctw2(91)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.1)vctw2(91)=vctw2(91)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-4)vctw2(92)=vctw2(92)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-1)vctw2(92)=vctw2(92)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.2)vctw2(92)=vctw2(92)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.5)vctw2(92)=vctw2(92)+1

         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-3)vctw2(93)=vctw2(93)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.-2)vctw2(93)=vctw2(93)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.3)vctw2(93)=vctw2(93)+1
         if(tw1var_pa(nr)-tw2var_pa(nr).eq.4)vctw2(93)=vctw2(93)+1
        endif

        if(tw1twin(nr).eq.7 .and. tw2twin(nr).eq.6) then  
         vctw2(90)=vctw2(90)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.0)vctw2(91)=vctw2(91)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-5)vctw2(91)=vctw2(91)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.1)vctw2(91)=vctw2(91)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-4)vctw2(92)=vctw2(92)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-1)vctw2(92)=vctw2(92)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.2)vctw2(92)=vctw2(92)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.5)vctw2(92)=vctw2(92)+1

         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-3)vctw2(93)=vctw2(93)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.-2)vctw2(93)=vctw2(93)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.3)vctw2(93)=vctw2(93)+1
         if(tw2var_pa(nr)-tw1var_pa(nr).eq.4)vctw2(93)=vctw2(93)+1
        endif

        if(tw1twin(nr).eq.7 .and. tw2twin(nr).eq.7) then  
         vctw2(100)=vctw2(100)+1    
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.0)then
          vctw2(104)=vctw2(104)+1
         endif
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.1)then
          vctw2(102)=vctw2(102)+1
         endif
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.5)then
          vctw2(102)=vctw2(102)+1
         endif
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.2)then
          vctw2(103)=vctw2(103)+1
         endif
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.4)then
          vctw2(103)=vctw2(103)+1
         endif
         if(abs(tw1var_pa(nr)-tw2var_pa(nr)).eq.3)then
          vctw2(101)=vctw2(101)+1
         endif
        endif

      enddo


ccc --- Results Plots tw-tw interactions --- ccc
ccc --- Results Plots tw-tw interactions --- ccc
ccc --- Results Plots tw-tw interactions --- ccc


       if((vctw2(11)+vctw2(12)+vctw2(13)+vctw2(14)+vctw2(15)).gt.0)then
       write(1013,*)1,float(vctw2(11))/float(vctw2(11)+vctw2(12)          ! 1013: fraction of the different sub-types of T1-T1 junctions computed with respect to the total number of T1-T1 junctions
     #                               +vctw2(13)+vctw2(14)+vctw2(15))
       write(1013,*)2,float(vctw2(12))/float(vctw2(11)+vctw2(12)
     #                               +vctw2(13)+vctw2(14)+vctw2(15))
       write(1013,*)3,float(vctw2(13))/float(vctw2(11)+vctw2(12)
     #                               +vctw2(13)+vctw2(14)+vctw2(15))
       write(1013,*)4,float(vctw2(14))/float(vctw2(11)+vctw2(12)
     #                               +vctw2(13)+vctw2(14)+vctw2(15))
       write(1013,*)5,float(vctw2(15))/float(vctw2(11)+vctw2(12)
     #                               +vctw2(13)+vctw2(14)+vctw2(15))
       else
       write(1013,*)1,0.
       write(1013,*)2,0.
       write(1013,*)3,0.
       write(1013,*)4,0.
       write(1013,*)5,0.
       endif

       if((vctw2(21)+vctw2(22)+vctw2(23)+vctw2(25)).gt.0)then             ! 1014: same as 1013 but for T1-T2 junctions
       write(1014,*)1,float(vctw2(21))/float(vctw2(21)+vctw2(22)
     #                                     +vctw2(23)+vctw2(25))
       write(1014,*)2,float(vctw2(22))/float(vctw2(21)+vctw2(22)
     #                                     +vctw2(23)+vctw2(25))
       write(1014,*)3,float(vctw2(23))/float(vctw2(21)+vctw2(22)
     #                                     +vctw2(23)+vctw2(25))
       write(1014,*)5,float(vctw2(25))/float(vctw2(21)+vctw2(22)
     #                                     +vctw2(23)+vctw2(25))
       else
       write(1014,*)1,0.
       write(1014,*)2,0.
       write(1014,*)3,0.
       write(1014,*)5,0.
       endif

       if((vctw2(31)+vctw2(32)+vctw2(33)+vctw2(35)).gt.0)then             
       write(1015,*)1,float(vctw2(31))/float(vctw2(31)+vctw2(32)          ! 1015: same as 1013 but for T1-C1 junctions
     #                                      +vctw2(33)+vctw2(35))
       write(1015,*)2,float(vctw2(32))/float(vctw2(31)+vctw2(32)
     #                                      +vctw2(33)+vctw2(35))
       write(1015,*)3,float(vctw2(33))/float(vctw2(31)+vctw2(32)
     #                                      +vctw2(33)+vctw2(35))
       write(1015,*)5,float(vctw2(35))/float(vctw2(31)+vctw2(32)
     #                                      +vctw2(33)+vctw2(35))
       else
       write(1015,*)1,0.
       write(1015,*)2,0.
       write(1015,*)3,0.
       write(1015,*)5,0.
       endif

       if((vctw2(41)+vctw2(42)+vctw2(43)+vctw2(44)+vctw2(45)).gt.0)then
       write(1016,*)1,float(vctw2(41))/float(vctw2(41)+vctw2(42)          ! 1016: same as 1013 but for T1-C1 junctions
     #                             +vctw2(43)+vctw2(44)+vctw2(45))
       write(1016,*)2,float(vctw2(42))/float(vctw2(41)+vctw2(42)
     #                             +vctw2(43)+vctw2(44)+vctw2(45))
       write(1016,*)3,float(vctw2(43))/float(vctw2(41)+vctw2(42)
     #                             +vctw2(43)+vctw2(44)+vctw2(45))
       write(1016,*)4,float(vctw2(44))/float(vctw2(41)+vctw2(42)
     #                             +vctw2(43)+vctw2(44)+vctw2(45))
       write(1016,*)5,float(vctw2(45))/float(vctw2(41)+vctw2(42)
     #                             +vctw2(43)+vctw2(44)+vctw2(45))
       else
       write(1016,*)1,0.
       write(1016,*)2,0.
       write(1016,*)3,0.
       write(1016,*)4,0.
       write(1016,*)5,0.
       endif

       if((vctw2(51)+vctw2(52)+vctw2(53)+vctw2(54)+vctw2(55)).gt.0)then
       write(1017,*)1,float(vctw2(51))/float(vctw2(51)+vctw2(52)          ! 1017: same as 1013 but for T2-T2 junctions
     #                             +vctw2(53)+vctw2(54)+vctw2(55))
       write(1017,*)2,float(vctw2(52))/float(vctw2(51)+vctw2(52)
     #                             +vctw2(53)+vctw2(54)+vctw2(55))
       write(1017,*)3,float(vctw2(53))/float(vctw2(51)+vctw2(52)
     #                             +vctw2(53)+vctw2(54)+vctw2(55))
       write(1017,*)4,float(vctw2(54))/float(vctw2(51)+vctw2(52)
     #                             +vctw2(53)+vctw2(54)+vctw2(55))
       write(1017,*)5,float(vctw2(55))/float(vctw2(51)+vctw2(52)
     #                             +vctw2(53)+vctw2(54)+vctw2(55))
       else
       write(1017,*)1,0.
       write(1017,*)2,0.
       write(1017,*)3,0.
       write(1017,*)4,0.
       write(1017,*)5,0.
       endif

       if((vctw2(61)+vctw2(62)+vctw2(63)+vctw2(64)+vctw2(65)).gt.0)then
       write(1018,*)1,float(vctw2(61))/float(vctw2(61)+vctw2(62)          ! 1018: same as 1013 but for T2-C1 junctions
     #                                +vctw2(63)+vctw2(64)+vctw2(65))
       write(1018,*)2,float(vctw2(62))/float(vctw2(61)+vctw2(62)
     #                                +vctw2(63)+vctw2(64)+vctw2(65))
       write(1018,*)3,float(vctw2(63))/float(vctw2(61)+vctw2(62)
     #                                +vctw2(63)+vctw2(64)+vctw2(65))
       write(1018,*)4,float(vctw2(64))/float(vctw2(61)+vctw2(62)
     #                                +vctw2(63)+vctw2(64)+vctw2(65))
       write(1018,*)5,float(vctw2(65))/float(vctw2(61)+vctw2(62)
     #                                +vctw2(63)+vctw2(64)+vctw2(65))
       else
       write(1018,*)1,0.
       write(1018,*)2,0.
       write(1018,*)3,0.
       write(1018,*)4,0.
       write(1018,*)5,0.
       endif

       if((vctw2(71)+vctw2(72)+vctw2(73)+vctw2(75)).gt.0)then
       write(1019,*)1,float(vctw2(71))/float(vctw2(71)+vctw2(72)          ! 1019: same as 1013 but for T2-C2 junctions
     #                                      +vctw2(73)+vctw2(75))
       write(1019,*)2,float(vctw2(72))/float(vctw2(71)+vctw2(72)
     #                                      +vctw2(73)+vctw2(75))
       write(1019,*)3,float(vctw2(73))/float(vctw2(71)+vctw2(72)
     #                                      +vctw2(73)+vctw2(75))
       write(1019,*)5,float(vctw2(75))/float(vctw2(71)+vctw2(72)
     #                                      +vctw2(73)+vctw2(75))
       else
       write(1019,*)1,0.
       write(1019,*)2,0.
       write(1019,*)3,0.
       write(1019,*)5,0.
       endif

       if((vctw2(81)+vctw2(82)+vctw2(83)+vctw2(84)+vctw2(85)).gt.0)then
       write(1020,*)1,float(vctw2(81))/float(vctw2(81)+vctw2(82)          ! 1020: same as 1013 but for C1-C1 junctions
     #                                 +vctw2(83)+vctw2(84)+vctw2(85))
       write(1020,*)2,float(vctw2(82))/float(vctw2(81)+vctw2(82)
     #                                 +vctw2(83)+vctw2(84)+vctw2(85))
       write(1020,*)3,float(vctw2(83))/float(vctw2(81)+vctw2(82)
     #                                 +vctw2(83)+vctw2(84)+vctw2(85))
       write(1020,*)4,float(vctw2(84))/float(vctw2(81)+vctw2(82)
     #                                 +vctw2(83)+vctw2(84)+vctw2(85))
       write(1020,*)5,float(vctw2(85))/float(vctw2(81)+vctw2(82)
     #                                 +vctw2(83)+vctw2(84)+vctw2(85))
       else
       write(1020,*)1,0.
       write(1020,*)2,0.
       write(1020,*)3,0.
       write(1020,*)4,0.
       write(1020,*)5,0.
       endif

       if((vctw2(91)+vctw2(92)+vctw2(93)+vctw2(95)).gt.0)then
       write(1021,*)1,float(vctw2(91))/float(vctw2(91)+vctw2(92)          ! 1021: same as 1013 but for C1-C2 junctions
     #                                      +vctw2(93)+vctw2(95))
       write(1021,*)2,float(vctw2(92))/float(vctw2(91)+vctw2(92)
     #                                      +vctw2(93)+vctw2(95))
       write(1021,*)3,float(vctw2(93))/float(vctw2(91)+vctw2(92)
     #                                      +vctw2(93)+vctw2(95))
       write(1021,*)5,float(vctw2(95))/float(vctw2(91)+vctw2(92)
     #                                     +vctw2(93)+vctw2(95))
       else
       write(1021,*)1,0.
       write(1021,*)2,0.
       write(1021,*)3,0.
       write(1021,*)5,0.
       endif

       if((vctw2(101)+vctw2(102)+vctw2(103)
     #       +vctw2(104)+vctw2(105)).gt.0)then
       write(1022,*)1,float(vctw2(101))/                                  ! 1022: same as 1013 but for C2-C2 junctions
     #                     float(vctw2(101)+vctw2(102)+vctw2(103)
     #                          +vctw2(104)+vctw2(105))
       write(1022,*)2,float(vctw2(102))/
     #                     float(vctw2(101)+vctw2(102)+vctw2(103)
     #                          +vctw2(104)+vctw2(105))
       write(1022,*)3,float(vctw2(103))/
     #                     float(vctw2(101)+vctw2(102)+vctw2(103)
     #                          +vctw2(104)+vctw2(105))
       write(1022,*)4,float(vctw2(104))/
     #                     float(vctw2(101)+vctw2(102)+vctw2(103)
     #                          +vctw2(104)+vctw2(105))
       write(1022,*)5,float(vctw2(105))/
     #                     float(vctw2(101)+vctw2(102)+vctw2(103)
     #                          +vctw2(104)+vctw2(105))
       else
       write(1022,*)1,0.
       write(1022,*)2,0.
       write(1022,*)3,0.
       write(1022,*)4,0.
       write(1022,*)5,0.
       endif

       do i=1,110
         freqtw(i)=0.
       enddo

       do i=1,110
         j=i*10
         freqtw(i)=float(vctw2(i))/float(nr_40)
       enddo


ccc ccc Counting twins ccc ccc
ccc ccc Counting twins ccc ccc
ccc ccc Counting twins ccc ccc

       do i=1,4
         count_twtype(i)=0
       enddo
       
       do nr=1,nr_30
         if(twtype(nr).eq.1)then
           count_twtype(1)=count_twtype(1)+1
         endif
         if(twtype(nr).eq.2)then
           count_twtype(2)=count_twtype(2)+1
         endif
         if(twtype(nr).eq.3)then
           count_twtype(3)=count_twtype(3)+1
         endif
         if(twtype(nr).eq.4)then
           count_twtype(4)=count_twtype(4)+1
         endif
       enddo

 
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccccc ABOUT MONOTWINS ccccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccccc ABOUT MONOTWINS ccccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccccc ABOUT MONOTWINS ccccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

       cmono=0
       do nr=1,nr_20
         if(countp(nr).eq.1.)then
           cmono=cmono+1
         endif      
       enddo     

       cmonoTT1=0
       cmonoTT2=0
       cmonoCT1=0
       cmonoCT2=0

       do j=1,nbrs2
         cp(23,j)=0
         cp(24,j)=0  ! all
         cp(25,j)=0  ! TT1
         cp(26,j)=0  ! TT2
         cp(27,j)=0  ! CT1
         cp(28,j)=0  ! CT2
       enddo

       do nr=1,nr_30
         imonotwin(nr)=0
         do nr2=1,nr_20
           if(map_id_tw(nr).eq.map_id_par(nr2))then
             if(gt(nr).eq.idp(nr2))then
               if(countp(nr2).eq.1)then
                 imonotwin(nr)=1
               endif
             endif
           endif
         enddo 
         if(imonotwin(nr).eq.1)then
           if(twtype(nr).eq.1)then
             cmonoTT1=cmonoTT1+1
           endif
           if(twtype(nr).eq.2)then
             cmonoTT2=cmonoTT2+1
           endif
           if(twtype(nr).eq.3)then
             cmonoCT1=cmonoCT1+1
           endif
           if(twtype(nr).eq.4)then
             cmonoCT2=cmonoCT2+1
           endif
           do j=1,nbrs2
             if(SFo(nr).ge.bar_sf(j).and.SFo(nr).lt.bar_sf(j+1))then
               cp(24,j)=cp(24,j)+1
               if(twtype(nr).eq.1)then
                 cp(25,j)=cp(25,j)+1
               endif
               if(twtype(nr).eq.2)then
                 cp(26,j)=cp(26,j)+1
               endif
               if(twtype(nr).eq.3)then
                 cp(27,j)=cp(27,j)+1
               endif
               if(twtype(nr).eq.4)then
                 cp(28,j)=cp(28,j)+1
               endif
             endif
           enddo
           do j=1,nbr
             if(parent_area(nr).ge.bar_area(j).and.              
     #             parent_area(nr).lt.bar_area(j+1))then
               cp(23,j)=cp(23,j)+1
             endif
           enddo    ! nbr
         endif      ! imonotwin
       enddo        ! nr_30

ccc
ccc

       do j=1,nbr
         twthck_mono(j)=0.
         twfr_mono(j)=0.
         av_twfr_mono(j)=0.    ! useless
       enddo
    
       do nr=1,nr_30 
         if(imonotwin(nr).eq.1)then      
           do j=1,nbr
             if(parent_area(nr).ge.bar_area(j).and.               
     #             parent_area(nr).lt.bar_area(j+1))then
                 twthck_mono(j)=twthck_mono(j)+twthckcor(nr)
                 twfr_mono(j)=twfr_mono(j)+areat(nr)/parent_area(nr)
             endif
           enddo
         endif
       enddo
ccc
ccc

ccc ccc ccc ccc
ccc june 26, 2014 - particular case: only CT1 are studies bc of TT03
ccc ccc ccc ccc

      do j=1,nbrs2
        temp_twthck_multi_sf(j)=0.
        avg_twthck_multi_sf(j)=0.
        temp_twthck_mono_sf(j)=0.
        avg_twthck_mono_sf(j)=0.
      enddo

      do nr=1,nr_30
          if(twtype(nr).eq.3)then
            do j=1,nbrs2
              if(SFo(nr).ge.bar_sf(j).and.
     #          SFo(nr).lt.bar_sf(j+1))then
                cp(55,j)=cp(55,j)+1       ! all CT1s
                if(imonotwin(nr).eq.0)then
                  cp(56,j)=cp(56,j)+1     ! multiple
                  temp_twthck_multi_sf(j)=temp_twthck_multi_sf(j)+
     #                                                 twthckcor(nr)
                else
                  cp(57,j)=cp(57,j)+1     ! mono
                  temp_twthck_mono_sf(j)=temp_twthck_mono_sf(j)+
     #                                               twthckcor(nr)
                endif
              endif
            enddo
          endif
      enddo    

      do j=1,nbrs2
        if(cp(56,j).ne.0)then
          avg_twthck_multi_sf(j)=temp_twthck_multi_sf(j)/float(cp(56,j))
        else
          avg_twthck_multi_sf(j)=0.
        endif
        if(cp(57,j).ne.0)then
          avg_twthck_mono_sf(j)=temp_twthck_mono_sf(j)/float(cp(57,j))
        else
          avg_twthck_mono_sf(j)=0.
        endif
      enddo

ccc Areas

       do j=1,nbr
         temp_twthck_multi_area(j)=0.
         temp_twthck_mono_area(j)=0.
         avg_twthck_multi_area(j)=0.
         avg_twthck_mono_area(j)=0.
       enddo 

       do nr=1,nr_30       
       if(twtype(nr).eq.3)then
       do nr2=1,nr_10

          if(map_id_tw(nr).eq.map_id_gr(nr2))then
          if(id(nr2).eq.gt(nr))then

          do j=1,nbr
            if(areap(nr2).ge.bar_area(j) .and. 
     #           areap(nr2).lt.bar_area(j+1))then
              cp(60,j)=cp(60,j)+1
              if(imonotwin(nr).eq.0)then
                cp(58,j)=cp(58,j)+1
                temp_twthck_multi_area(j)=temp_twthck_multi_area(j)
     #                                                 +twthckcor(nr)
              else
                cp(59,j)=cp(59,j)+1
                temp_twthck_mono_area(j)=temp_twthck_mono_area(j)
     #                                                +twthckcor(nr)
              endif
            endif
          enddo

          endif
          endif
  
       enddo
       endif
       enddo

      do j=1,nbr
        if(cp(58,j).ne.0)then
          avg_twthck_multi_area(j)=temp_twthck_multi_area(j)/
     #                                         float(cp(58,j))
        else
          avg_twthck_multi_area(j)=0.
        endif
        if(cp(59,j).ne.0)then
          avg_twthck_mono_area(j)=temp_twthck_mono_area(j)/
     #                                       float(cp(59,j))
        else
          avg_twthck_mono_area(j)=0.
        endif
      enddo

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc Monovariant twins & grains ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc
ccc About the influence of mono and multivariant non monotwinned grains
ccc

ccc NB: Notation: - a monotwin is a twin embedded in a grain containing only one twin
ccc               - a multitwin is a twin embedded in a grain containing several twins
ccc               - a monovariant is a twin embedded in a grain containing only one type of twin variant
ccc               - a multivariant is a twin embedded in a grain containing different twin variants but these variants all belong to the same twinning mode

ccc ccc ccc ccc
ccc february 08, 2015 - particular case: only CT1 are studies bc of TT03
ccc ccc ccc ccc

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

       nr1=0
       ctwchecked=0       
       ctottype=0
       ctotvar=0
       ntottype=0          
       ntotvar=0
       ntottype_predom=0
       ntotvar_predom=0

       do nr=1,nr_30
         itwchecked(nr)=0
       enddo

       do nr=1,nr_30
         singlevar_flag(nr)=0
         singlevar_predom_flag(nr)=0
       enddo
    
       do while(ctwchecked.lt.nr_30)
         nr1=nr1+1
         if(itwchecked(nr1).eq.0)then
           itwchecked(nr1)=1
         else 
           go to 1000
         endif
         ctwchecked=ctwchecked+1
         itwtype=0
         itwvar=0
         itwmul=0
         itottype=0
         itotvar=0
         itottype_predom=0
         itotvar_predom=0
         itwvar_predom=0
         itwtype_predom=0
    
         if(nr1.lt.nr_30)then
           do nr2=nr1+1,nr_30
            if(map_id_tw(nr1).eq.map_id_tw(nr2))then 
             if(gt(nr1).eq.gt(nr2))then
               itwmul=1
               ctwchecked=ctwchecked+1
               itwchecked(nr2)=1
               if(twtypet(nr1).eq.twtypet(nr2))then
                 itwtype=1 
                 itottype=itottype+1
                 if(twtypet(nr2).eq.6)then
                   itottype_predom=itottype_predom+1
                 endif
                 if(twvart(nr1).eq.twvart(nr2))then
                   itwvar=1
                   itotvar=itotvar+1
                   if(twtypet(nr2).eq.6)then
                     itotvar_predom=itotvar_predom+1
                   endif                   
                 endif   ! twvar
               endif     ! twtype
             endif       ! gt
            endif        ! map_id_tw      
           enddo         ! nr2


           if(itwmul.eq.1)then                     

             if(itottype.eq.
     #           (counttw_idgr(map_id_tw(nr1),gt(nr1))-1.))then
               ctottype=ctottype+1
               ntottype=ntottype+(itottype+1)
             endif
             if(itotvar.eq.
     #           (counttw_idgr(map_id_tw(nr1),gt(nr1))-1.))then
               ctotvar=ctotvar+1 
               ntotvar=ntotvar+(itotvar+1)
             endif

             if(itottype_predom.eq.
     #           (counttw_idgr(map_id_tw(nr1),gt(nr1))-1.))then
               ctottype_predom=ctottype_predom+1
               ntottype_predom=ntottype_predom+(itottype_predom+1)
             endif
             if(itotvar_predom.eq.
     #           (counttw_idgr(map_id_tw(nr1),gt(nr1))-1.))then
               ctotvar_predom=ctotvar_predom+1 
               ntotvar_predom=ntotvar_predom+(itotvar_predom+1)
               do nr2=nr1+1,nr_30
                if(map_id_tw(nr1).eq.map_id_tw(nr2))then  
                  if(gt(nr1).eq.gt(nr2))then              
                    if(twtypet(nr1).eq.twtypet(nr2))then
                      if(twvart(nr1).eq.twvart(nr2))then
                        singlevar_flag(nr1)=1
                        singlevar_flag(nr2)=1
                        if(twtypet(nr1).eq.6)then
                        singlevar_predom_flag(nr1)=1
                        singlevar_predom_flag(nr2)=1
                        endif
                      endif
                    endif
                  endif
                endif   
               enddo
             endif

           endif  ! itwmul                   
cccccccccccccccccccccccccccccccccccccccccccccc


         else
           write(*,*) 'Warning: no twin multiplicity'
         endif           ! nr1
 1000    continue
       enddo             ! while nr1

      do j=1,nbrs2
        temp_twthck_multivar_sf(j)=0.
        avg_twthck_multivar_sf(j)=0.
        temp_twthck_monovar_sf(j)=0.
        avg_twthck_monovar_sf(j)=0.
      enddo

      do nr=1,nr_30
c        if(iw(nr).ne.1)then                     ! to check the meaning of "iw"
          if(twtype(nr).eq.3)then
            do j=1,nbrs2
              if(SFo(nr).ge.bar_sf(j).and.
     #          SFo(nr).lt.bar_sf(j+1))then
                cp(55,j)=cp(55,j)+1       ! all CT1s
                if(singlevar_predom_flag(nr).eq.0)then
                  cp(61,j)=cp(61,j)+1     ! multiple
                  temp_twthck_multivar_sf(j)=
     #                           temp_twthck_multivar_sf(j)+
     #                                               twthckcor(nr)
                else
                  cp(62,j)=cp(62,j)+1     ! mono
                  temp_twthck_monovar_sf(j)=
     #                            temp_twthck_monovar_sf(j)+
     #                                               twthckcor(nr)
                endif
              endif
            enddo
          endif
c        endif
      enddo    

      do j=1,nbrs2
        if(cp(61,j).ne.0)then
          avg_twthck_multivar_sf(j)=temp_twthck_multivar_sf(j)
     #                                          /float(cp(61,j))
        else
          avg_twthck_multivar_sf(j)=0.
        endif
        if(cp(62,j).ne.0)then
          avg_twthck_monovar_sf(j)=temp_twthck_monovar_sf(j)
     #                                          /float(cp(62,j))
        else
          avg_twthck_monovar_sf(j)=0.
        endif
      enddo

ccc Areas

       do j=1,nbr
         temp_twthck_multivar_area(j)=0.
         temp_twthck_monovar_area(j)=0.
         avg_twthck_multivar_area(j)=0.
         avg_twthck_monovar_area(j)=0.
       enddo 

       do nr=1,nr_30       
       if(twtype(nr).eq.3)then
       do nr2=1,nr_10

          if(map_id_tw(nr).eq.map_id_gr(nr2))then
          if(id(nr2).eq.gt(nr))then

          do j=1,nbr
            if(areap(nr2).ge.bar_area(j) .and. 
     #           areap(nr2).lt.bar_area(j+1))then
              cp(60,j)=cp(60,j)+1
              if(singlevar_predom_flag(nr).eq.0)then
                cp(63,j)=cp(63,j)+1
                temp_twthck_multivar_area(j)=
     #                             temp_twthck_multivar_area(j)
     #                                                 +twthckcor(nr)
              else
                cp(64,j)=cp(64,j)+1
                temp_twthck_monovar_area(j)=
     #                             temp_twthck_monovar_area(j)
     #                                                +twthckcor(nr)
              endif
            endif
          enddo

          endif
          endif
  
       enddo
       endif
       enddo

      do j=1,nbr
        if(cp(63,j).ne.0)then
          avg_twthck_multivar_area(j)=temp_twthck_multivar_area(j)/
     #                                               float(cp(63,j))
        else
          avg_twthck_multivar_area(j)=0.
        endif
        if(cp(64,j).ne.0)then
          avg_twthck_monovar_area(j)=temp_twthck_monovar_area(j)/
     #                                               float(cp(64,j))
        else
          avg_twthck_monovar_area(j)=0.
        endif
      enddo
 

ccc ccc Plotting ccc ccc
 
       do j=1,nbr
         write(1048,*)j,bar_area(j),bar_area(j+1)                         ! 1048: grain area vs fraction of monotwins embedded in grains of a given size computed with respect to the total number of monotwins and twins
     #                ,float(cp(23,j))/float(cmono)
     #                ,float(cp(23,j))/float(nr_10)
         write(1049,*)j,bar_area(j),bar_area(j+1)                         ! 1049: grain area vs average fraction area occupied by monotwins in twinned grains
     #                ,twfr_mono(j)/float(cp(23,j))
         write(1050,*)j,bar_area(j),bar_area(j+1)                         ! 1050: grain area vs average corrected thickness of monotwins
     #                ,twthck_mono(j)/float(cp(23,j))

         write(1066,*)j,bar_sf(j),bar_sf(j+1)                             ! 1066: grain area vs average correct thicknesses of monotwins and "multi-twins"
     #                ,0.5*(bar_area(j)+bar_area(j+1))
     #                ,avg_twthck_mono_area(j)
     #                ,avg_twthck_multi_area(j)
     #                ,cp(59,j),cp(58,j)
         write(1067,*)j,bar_sf(j),bar_sf(j+1)                             ! 1067: grain area vs fraction of C1 monotwins and multitwins computed with respect to the total number of C1 twins (= mono + multi)
     #                ,0.5*(bar_area(j)+bar_area(j+1))
     #                ,cp(59,j)/float(count_twtype(3))
     #                ,cp(58,j)/float(count_twtype(3))
     #                ,cp(59,j),cp(58,j)
         write(1072,*)j,bar_area(j),bar_area(j+1)                         ! 1072: grain area vs average of corrected thickness of monovariant and multivariant twins 
     #                ,0.5*(bar_area(j)+bar_area(j+1))
     #                ,avg_twthck_monovar_area(j)
     #                ,avg_twthck_multivar_area(j)
     #                ,cp(64,j),cp(63,j)
         write(1073,*)j,bar_area(j),bar_area(j+1)                         ! 1073: grain area vs fraction of monovariant and multivariant twins computed with respect to the total number of C1 twins
     #                ,0.5*(bar_area(j)+bar_area(j+1))
     #                ,cp(64,j)/float(count_twtype(3))
     #                ,cp(63,j)/float(count_twtype(3))
     #                ,float(count_twtype(3)),count_twtype(3)
       enddo

       do j=1,nbrs2
         write(1051,*)j,bar_sf(j),bar_sf(j+1)                             ! 1051: SF vs fractions of monotwins computed with respect to the total number of monotwins and twins
     #                ,float(cp(24,j))/float(cmono)
     #                ,float(cp(24,j))/float(nr_30)
         write(1052,*)j,bar_sf(j),bar_sf(j+1)                             ! 1052: SF vs fractions of T1, T2, C1, C2 monotwins computed with respect to the total number of monotwins 
     #                ,float(cp(25,j))/float(cmono)
     #                ,float(cp(26,j))/float(cmono)
     #                ,float(cp(27,j))/float(cmono)
     #                ,float(cp(28,j))/float(cmono)

         write(1064,*)j,bar_sf(j),bar_sf(j+1)                             ! 1064: SF vs average corrected twin thicknesses of C1 monotwins and multitwins           
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))
     #                ,avg_twthck_mono_sf(j),avg_twthck_multi_sf(j)
     #                ,cp(57,j),cp(56,j)
         write(1065,*)j,bar_sf(j),bar_sf(j+1)                             ! 1065: SF vs fractions of C1 mono and multitwins computed wih respect to the total number of C1 twins
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))
     #                ,cp(57,j)/float(count_twtype(3))
     #                ,cp(56,j)/float(count_twtype(3))
     #                ,cp(57,j),cp(56,j)

         write(1070,*)j,bar_sf(j),bar_sf(j+1)           
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))
     #                ,avg_twthck_monovar_sf(j)
     #                ,avg_twthck_multivar_sf(j)
     #                ,cp(62,j),cp(61,j)
         write(1071,*)j,bar_sf(j),bar_sf(j+1)
     #                ,0.5*(bar_sf(j)+bar_sf(j+1))
     #                ,cp(62,j)/float(count_twtype(3))
     #                ,cp(61,j)/float(count_twtype(3))
     #                ,float(count_twtype(3)),count_twtype(3)
       enddo



ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc WRITTEN IN THE TERMINAL ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc WRITTEN IN THE TERMINAL ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc WRITTEN IN THE TERMINAL ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc

ccc About twin types
ccc About twin types
ccc About twin types

       do i=1,4
         count_twtype(i)=0
       enddo
       
       do nr=1,nr_30
         if(twtype(nr).eq.1)then
           count_twtype(1)=count_twtype(1)+1
         endif
         if(twtype(nr).eq.2)then
           count_twtype(2)=count_twtype(2)+1
         endif
         if(twtype(nr).eq.3)then
           count_twtype(3)=count_twtype(3)+1
         endif
         if(twtype(nr).eq.4)then
           count_twtype(4)=count_twtype(4)+1
         endif
       enddo

ccc ccc ccc ccc Bin size optim ccc ccc ccc ccc
ccc ccc ccc ccc Bin size optim ccc ccc ccc ccc

      sd_sf=0.
      sdcor_sf=0.
      esp_sf=0.

      sd_area=0.
      sdcor_area=0.
      esp_area=0.

      sd_diam=0.
      sdcor_diam=0.
      esp_diam=0.

ccc area
      dummy1=0.
      do nr=1,nr_10
        dummy1=dummy1+area(nr)
      enddo
      esp_area=dummy1/float(nr_10)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_10
        dummy1=dummy1+area(nr)*area(nr)
      enddo  
      dummy2=dummy1/float(nr_10)

      sd_area=sqrt(dummy2-esp_area**2.)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_10
        dummy1=dummy1+(area(nr)-esp_area)**2.
      enddo  
      dummy2=dummy1/float(nr_10-1)
    
      sdcor_area=sqrt(dummy2)

ccc diam
      dummy1=0.
      do nr=1,nr_10
        dummy1=dummy1+sqrt(4.*area(nr)/pi)
      enddo
      esp_diam=dummy1/float(nr_10)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_10
        dummy1=dummy1+4.*area(nr)/pi
      enddo  
      dummy2=dummy1/float(nr_10)

      sd_diam=sqrt(dummy2-esp_diam**2.)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_10
        dummy1=dummy1+(sqrt(4.*area(nr)/pi)-esp_diam)**2.
      enddo  
      dummy2=dummy1/float(nr_10-1)
    
      sdcor_diam=sqrt(dummy2)

ccc sf
      dummy1=0.
      do nr=1,nr_30
        dummy1=dummy1+sfo(nr)
      enddo
      esp_sf=dummy1/float(nr_30)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_30
        dummy1=dummy1+sfo(nr)*sfo(nr)
      enddo  
      dummy2=dummy1/float(nr_30)

      sd_sf=sqrt(dummy2-esp_sf**2.)

      dummy1=0.
      dummy2=0.
      do nr=1,nr_30
        dummy1=dummy1+(sfo(nr)-esp_sf)**2.
      enddo  
      dummy2=dummy1/float(nr_30-1)
    
      sdcor_sf=sqrt(dummy2)

      write(*,*) '    '
      write(*,*) ' * USEFUL DATA FOR GRAPHICAL REPRESENTATION '
      write(*,*)'  '
      write(*,*) ' --- Bin Size Optimization '
c      write(*,*) '    '
      write(*,*) esp_area,' area esperance', sd_area, 'area SD', 
     #          sdcor_area, 'area unbiased SD'
      write(*,*) 3.49*sd_area*(float(nr_10))**(-1./3.), 'bin width',
     #       3.49*sdcor_area*(float(nr_10))**(-1./3.), 'cor bin width'
     #          ,nr_10,'nr_10'
      write(*,*) esp_diam,' diam esperance', sd_diam, 'diam SD', 
     #          sdcor_diam, 'diam unbiased SD'
      write(*,*) 3.49*sd_diam*(float(nr_10))**(-1./3.), 'width',
     #          3.49*sdcor_diam*(float(nr_10))**(-1./3.), 'cor width'
      write(*,*) esp_sf,' sf esperance', sd_sf, 'sf SD', 
     #          sdcor_sf, 'sf unbiased SD'
      write(*,*) 3.49*sd_sf*(float(nr_30))**(-1./3.), 'bin width',
     #       3.49*sdcor_sf*(float(nr_30))**(-1./3.), 'cor bin width'
c      write(*,*) '    '
c      write(*,*) ' ******** End Bin Size Optimization ***********'
      write(*,*) '    '
      
ccc ccc ccc ccc End bin size optim ccc ccc ccc ccc 
ccc ccc ccc ccc End bin size optim ccc ccc ccc ccc

ccc ccc ccc ccc Area Statistics - Representativity ccc ccc ccc ccc
ccc ccc ccc ccc Area Statistics - Representativity ccc ccc ccc ccc


      area_T1=0.
      area_T2=0.
      area_C1=0.
      area_C2=0.                          
      area_T1_SFneg=0.
      area_T2_SFneg=0.
      area_C1_SFneg=0.
      area_C2_SFneg=0.        
      tot_area_tw=0.

      do nr=1,nr_30
        tot_area_tw=tot_area_tw+areat(nr)
        if(twtypet(nr).eq.4)then
          area_T1=area_T1+areat(nr)
          if(SFo(nr).lt.0.) area_T1_SFneg=area_T1_SFneg+areat(nr)
        endif
        if(twtypet(nr).eq.5)then
          area_T2=area_T2+areat(nr)
          if(SFo(nr).lt.0.) area_T2_SFneg=area_T2_SFneg+areat(nr)
        endif
        if(twtypet(nr).eq.6)then
          area_C1=area_C1+areat(nr)
          if(SFo(nr).lt.0.) area_C1_SFneg=area_C1_SFneg+areat(nr)
        endif
      enddo

c      write(*,*) ' '
c      write(*,*) '** ** ** ** ** ** ** ** ** ** ** ** ** ** **'
c      write(*,*) '** ** ** Twin Area Statistics ** ** ** ** **'
c      write(*,*) '** ** ** ** ** ** ** ** ** ** ** ** ** ** **'
c      write(*,*) ' '
c      write(*,*)area_T1,tot_area_tw,area_T1/tot_area_tw
c      write(*,*)area_T2,tot_area_tw,area_T2/tot_area_tw
c      write(*,*)area_C1,tot_area_tw,area_C1/tot_area_tw
c      write(*,*) ' '
c      write(*,*)area_T1_SFneg,area_T1,area_T1_SFneg/area_T1
c      write(*,*)area_T2_SFneg,area_T2,area_T2_SFneg/area_T2
c      write(*,*)area_C1_SFneg,area_C1,area_C1_SFneg/area_C1
c      write(*,*) ' '
c      write(*,*) '** ** ** ** ** ** ** ** ** ** ** ** ** ** **'
c      write(*,*) '** ** ** End Twin Area Statistics ** ** ** ** **'
c      write(*,*) '** ** ** ** ** ** ** ** ** ** ** ** ** ** **'
c      write(*,*) ' '

ccc About grains to exclude or consider bc of your size

       taille_min=4.
       c10=0.
       do nr=1,nr_10
        if(area(nr).lt.taille_min)then
        c10=c10+1.
        endif
       enddo
       write(*,*)' --- Area boundaries ' 
       write(*,*)c10,'over',nr_10,'grains are smaller than',taille_min 
       c20=0.
       do nr=1,nr_20
        if(areap(nr).lt.taille_min)then
        c20=c20+1.
        endif
       enddo
       write(*,*)c20,'over',nr_20,'parents are smaller than',taille_min 
c16apr14       write(*,*) ' '
       
       taille_max1=654.
       taille_max2=1450.
       c101=0.
       c102=0.
       c1001=0.
       c1002=0.
       do nr=1,nr_10
        if(area(nr).gt.taille_max1)then
        c101=c101+1.
          do nr2=1,nr_30
            if(map_id_gr(nr).eq.map_id_tw(nr2))then
              if(id(nr).eq.gt(nr2))then
                c1001=c1001+1.
              endif
            endif
          enddo
        endif    
       enddo
       do nr=1,nr_10
        if(area(nr).gt.taille_max2)then
        c102=c102+1.
          do nr2=1,nr_30
            if(map_id_gr(nr).eq.map_id_tw(nr2))then
              if(id(nr).eq.gt(nr2))then
                c1002=c1002+1.
              endif
            endif
          enddo
        endif    
       enddo
       write(*,*)c101,'over',nr_10,'grains are bigger than',taille_max1
       write(*,*)c102,'over',nr_10,'grains are bigger than',taille_max2
       write(*,*)c1001,'over',nr_30,'twins in grains bigger than',taille_max1
       write(*,*)c1002,'over',nr_30,'twins in grains bigger than',taille_max2
       write(*,*) ' '     

ccc
ccc
ccc       
c       write(*,*)' '
c       write(*,*)' *** *** *** *** *** *** *** *** *** *** '
c       write(*,*)' *** *** *** *** *** *** *** *** *** *** '
       write(*,*)' ' 
       write(*,*)' * GENERAL INFO ABOUT GRAIN AND TWIN POPULATIONS ' 
       write(*,*)'  '
       write(*,*) nr_10,'# grains'
       write(*,*) nr_20,'# twinned grains'
       write(*,*) nr_30,'# twins'
       write(*,*)' with, '
       write(*,*) count_twtype(1),'# T1, i.e. ',
     #      float(count_twtype(1))/float(nr_30)*100.,'% of all twins'
       write(*,*) count_twtype(2),'# T2, i.e. ',
     #      float(count_twtype(2))/float(nr_30)*100.,'% of all twins'
       write(*,*) count_twtype(3),'# C1, i.e. ',
     #      float(count_twtype(3))/float(nr_30)*100.,'% of all twins'
       write(*,*) count_twtype(4),'# C2, i.e. ',
     #      float(count_twtype(4))/float(nr_30)*100.,'% of all twins'
       write(*,*) ' '
       write(*,*)cmono,'# single twinned grains, i.e.',
     #      float(cmono)/float(nr_20),'% of all twinned grains'
       write(*,*)' with, '
       write(*,*)cmonoTT1,'# single T1, i.e.',
     #      float(cmonoTT1)/float(cmono),'% of all single twins'
       write(*,*)cmonoTT2,'# single T2, i.e.',
     #      float(cmonoTT2)/float(cmono),'% of all single twins'
       write(*,*)cmonoCT1,'# single C1, i.e.',
     #      float(cmonoCT1)/float(cmono),'% of all single twins'
       write(*,*)cmonoCT2,'# single C2, i.e.',
     #      float(cmonoCT2)/float(cmono),'% of all single twins'
       write(*,*) ' '

       do i=1,4
         count_negSF(i)=0
       enddo

       do nr=1,nr_30
         if(SFo(nr).le.0.)then
           if(twtype(nr).eq.1) count_negSF(1)=count_negSF(1)+1
           if(twtype(nr).eq.2) count_negSF(2)=count_negSF(2)+1
           if(twtype(nr).eq.3) count_negSF(3)=count_negSF(3)+1
           if(twtype(nr).eq.4) count_negSF(4)=count_negSF(4)+1
         endif
       enddo

       write(*,*) count_negSF(1)+count_negSF(2)+count_negSF(3)
     #   +count_negSF(4),'# twins with SF<0 i.e.',
     #   float(count_negSF(1)+count_negSF(2)+count_negSF(3)
     #   +count_negSF(4))/float(nr_30),'% of all twins'
       write(*,*)' with, '
       write(*,*)count_negSF(1),'# T1 with SF<0, i.e.',
     #   float(count_negSF(1))/float(count_negSF(1)+count_negSF(2)
     #   +count_negSF(3)+count_negSF(4)),
     #   '% of all twins with SF<0'
       write(*,*)count_negSF(2),'# T2 with SF<0, i.e.',
     #   float(count_negSF(2))/float(count_negSF(1)+count_negSF(2)
     #   +count_negSF(3)+count_negSF(4)),
     #   '% of all twins with SF<0'
       write(*,*)count_negSF(3),'# C1 with SF<0, i.e.',
     #   float(count_negSF(3))/float(count_negSF(1)+count_negSF(2)
     #   +count_negSF(3)+count_negSF(4)),
     #   '% of all twins with SF<0'
       write(*,*)count_negSF(4),'# C2 with SF<0, i.e.',
     #   float(count_negSF(4))/float(count_negSF(1)+count_negSF(2)
     #   +count_negSF(3)+count_negSF(4)),
     #   '% of all twins with SF<0'

       write(*,*) ' '
       write(*,*) nr_40,'# twin-twin junctions'
       write(*,*) ' with, '
       write(*,*) vctw2(10),'# T1-T1, i.e.',freqtw(10)*100.,
     #           '% of all junctions'
       write(*,*) vctw2(20),'# T1-T2, i.e.',freqtw(20)*100.,   
     #           '% of all junctions'
       write(*,*) vctw2(30),'# T1-C1, i.e.',freqtw(30)*100., 
     #           '% of all junctions'    
       write(*,*) vctw2(40),'# T1-C2, i.e.',freqtw(40)*100., 
     #           '% of all junctions'     
       write(*,*) vctw2(50),'# T2-T2, i.e.',freqtw(50)*100., 
     #           '% of all junctions'      
       write(*,*) vctw2(60),'# T2-C1, i.e.',freqtw(60)*100.,
     #           '% of all junctions'      
       write(*,*) vctw2(70),'# T2-C2, i.e.',freqtw(70)*100.,
     #           '% of all junctions'       
       write(*,*) vctw2(80),'# C1-C1, i.e.',freqtw(80)*100., 
     #           '% of all junctions'     
       write(*,*) vctw2(90),'# C1-C2, i.e.',freqtw(90)*100.,
     #           '% of all junctions'       
       write(*,*) vctw2(100),'# C2-C2, i.e.',freqtw(100)*100.,
     #           '% of all junctions'
       write(*,*) ' '

ccc About areas 

       c201=0.
       do nr=1,nr_10
        c201=c201+area(nr) 
       enddo
c       write(*,*) c201,'Total map area'

       c202=0.
       do nr=1,nr_20
        c202=c202+areap(nr) 
       enddo

       c203=0.
       do nr=1,nr_30
        c203=c203+areat(nr) 
       enddo

       tot_area_gr=c201
       tot_area_par=c202
       tot_area_tw=c203

       twfrac=tot_area_tw/tot_area_gr
       twfrac_par=tot_area_tw/tot_area_par

       tot_area_T1=0.
       tot_area_T2=0.
       tot_area_C1=0.
       tot_area_C2=0.
       do nr=1,nr_30
         if(twtypet(nr).eq.4)then
           tot_area_T1=tot_area_T1+areat(nr)
         endif
         if(twtypet(nr).eq.5)then
           tot_area_T2=tot_area_T2+areat(nr)
         endif
         if(twtypet(nr).eq.6)then
           tot_area_C1=tot_area_C1+areat(nr)
         endif
         if(twtypet(nr).eq.7)then
           tot_area_C2=tot_area_C2+areat(nr)
         endif
       enddo

       write(*,*)' ' 
       write(*,*)' * IN TERMS OF AREA, ' 
       write(*,*)' ' 
       write(*,*) tot_area_gr,'mum^2',' Total map area'
       write(*,*) tot_area_par,'mum^2',
     #            ' Total area occupied by twinned grains'
       write(*,*) tot_area_tw,'mum^2',
     #            ' Total area occupied by twins'
       write(*,*)' i.e.,'
       write(*,*)' twins occupy',twfrac*100.,
     #           '% of the total map area'
       write(*,*)' and',twfrac_par*100.,
     #           '% of the area occupied by twinned grains'           
       write(*,*)' with,'
       write(*,*)'       T1 occupying',tot_area_T1/tot_area_tw*100.,
     #           '% of the total twinned area'
       write(*,*)'       T2 occupying',tot_area_T2/tot_area_tw*100.,
     #           '% of the total twinned area'
       write(*,*)'       C1 occupying',tot_area_C1/tot_area_tw*100.,
     #           '% of the total twinned area'
       write(*,*)'       C2 occupying',tot_area_C2/tot_area_tw*100.,
     #           '% of the total twinned area'
       write(*,*) ' '



ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc DONE WITH WRITE STATEMENTS IN THE TERMINAL ccc ccc 
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
  
      end program data_proc


ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc
ccc ccc ccc ccc ccc ccc ccc SUBROUTINES ccc ccc ccc ccc ccc ccc ccc ccc
ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc 
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc


      subroutine q2m(qx,qy,qz,qw,rot)

      real:: qx,qy,qz,qw,qnorm
      real,dimension(3,3)::rot

      qnorm=sqrt(qx**2.+qy**2.+qz**2.+qw**2.)

      if(abs(qnorm-1.).gt.0.05)then
         write(*,*)'WARNING: non unit quaternion'
      endif

      rot(1,1)=qw**2.+qx**2.-qy**2.-qz**2.
      rot(1,2)=2.*(qx*qy+qw*qz)
      rot(1,3)=2.*(qx*qz-qw*qy)

      rot(2,1)=2.*(qx*qy-qw*qz)
      rot(2,2)=qw**2.-qx**2.+qy**2.-qz**2.
      rot(2,3)=2.*(qy*qz+qw*qx)

      rot(3,1)=2.*(qx*qz+qw*qy)
      rot(3,2)=2.*(qy*qz-qw*qx)
      rot(3,3)=qw**2.+qz**2.-qx**2.-qy**2.
      
      end subroutine q2m

ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc ccc
c     Kronecker's function

      real function kronecker(i,j)
	
      integer i,j

      if (i .eq. j) then
	kronecker=1.
      else
	kronecker=0.
      end if

      return

      end function

ccc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine inversion(iMethod,n,matrix,matrixinv)
    
      integer n
      real, dimension(n,n):: matrix, matrixinv, ll, uu, t,tt  ! t for debug
      real, dimension(n):: xx,b
      integer iMethod
      real kronecker

      do i=1,n
        do j=1,n
            matrixinv(i,j)=0.
        enddo
      enddo
    
      if(iMethod.eq.1)then
        
      call ludecomposition(matrix,n,ll,uu)
    
      do j=1,n
         do i=1,n
            b(i)=kronecker(i,j)
         enddo
             call lubacksub(n,ll,uu,b,xx)
             do i=1,n
                matrixinv(i,j)=xx(i)
             enddo
       enddo
    
      endif
    
      if(iMethod.eq.2)then
        do j=1,n
          do i=1,n
            b(i)=kronecker(i,j)
          enddo
          call gaussseidel(matrix,n,b,xx)
          do i=1,n
            matrixinv(i,j)=xx(i)
          enddo
        enddo
      endif
        
      end subroutine inversion

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine ludecomposition(a,n,ll,uu)
  
      integer n
      real, dimension(n,n) :: a,ll,uu,mattemp2,mattemp3
      real kronecker
      real, dimension(n-1,n,n):: mk, llk, llki
      real, dimension(n-1,n):: lk,ek
      real, dimension (n,n):: temp, dum
      integer i,j,k,l,p
    
 

        do i=1,n; do j=1,n  !=======>initiation
            uu(i,j)=a(i,j)
            mattemp2(i,j)=0
            ll(i,j)=0
            mattemp3(i,j)=kronecker(i,j)
        enddo; enddo

      do k=1,n-1


c !===============================================def des vecteurs |k
          
        do i=1,n
            ek(k,i)=kronecker(k,i)
            if(i.le.k)then                   !OK mais seulement pour L1
                lk(k,i)=0.
            else
                lk(k,i)=uu(i,k)/uu(k,k)     
            endif
        enddo
    
c !==================def des vecteurs |k*ek t  -->il y en a n a chaque step
    
        do i=1,n; do j=1,n
           mk(k,i,j)=lk(k,i)*ek(k,j)               
        enddo; enddo
    
c !===================================def de Lk (k-ieme step) et son inverse
       
        do i=1,n; do j=1,n
           llk(k,i,j)=kronecker(i,j)-mk(k,i,j)           
           llki(k,i,j)=kronecker(i,j)+mk(k,i,j)      
        enddo; enddo
   
c !===================================================

        do i=1,n; do j=1,n
            do p=1,n
            mattemp2(i,j)=mattemp2(i,j)+llk(k,i,p)*uu(p,j)
            enddo
        enddo; enddo

        do i=1,n; do j=1,n  !========> stockage L tild
            do p=1,n
            ll(i,j)=ll(i,j)+llk(k,i,p)*mattemp3(p,j)
            enddo
        enddo; enddo

        do i=1,n; do j=1,n
            uu(i,j)=mattemp2(i,j)
            mattemp2(i,j)=0.0
            mattemp3(i,j)=ll(i,j)
        enddo; enddo

      if (uu(k,k).eq.0) then 
        write (*,*) 'no solution to the system ==> det(A)=0'
        stop
      end if 

      end do  ! do loop k   !====> a ce stade [ll]=[mattemp3]= L(tild) et [uu]= U

      do i=1,n; do j=1,n
        ll(i,j)=kronecker(i,j)
      enddo ; enddo

      do k=1,n-1
        do i=1,n; do j=1,n
          ll(i,j)=ll(i,j)+mk(k,i,j)
        enddo ; enddo
      end do

      do k=1,n 
        if (ll(k,k).eq.0) then 
          write (*,*) 'no solution to the system ==> det(A)=0'
          stop
        end if  
      end do
    
      end subroutine ludecomposition

ccc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine lubacksub(n,ll,uu,b,xx)

      integer n
      real, dimension(n,n) :: ll,uu, aa   
      real, dimension (n):: b, xx, yy, bp
      real temp, aux 
      integer i,j,k,l
    
c    ! A=LU with LU=A
c    ! LUx=b -> solve Ly=b -> solve Ux=y

c      if((b(1)+b(2)).lt.0.01.and.b(6).gt.0.99)then
c        write(20,*)(b(i),i=1,n)
c      endif
            
      yy(1)=b(1)
      do k=2,n
         temp=0.
         do j=1,k-1
            temp=temp+ll(k,j)*yy(j)
         enddo
         yy(k)=(b(k)-temp)/ll(k,k)
      enddo
    
      xx(n)=yy(n)/uu(n,n)
      do j=1,n-1
        k=n-j
        aux=0.
        do l=k+1,n
            aux=aux+uu(k,l)*xx(l)
        enddo
        xx(k)=(1./uu(k,k))*(yy(k)-aux)
      enddo
     
      do i=1,n; do j=1,n
        aa(i,j)=0.
        do l=1,n
        aa(i,j)=aa(i,j)+ll(i,l)*uu(l,j)
        enddo
      enddo; enddo
      do i=1,n
        bp(i)=0.
        do j=1,n
        bp(i)=bp(i)+aa(i,j)*bp(j)
        enddo
      enddo
      

      end subroutine lubacksub

ccc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ccc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ccc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine gaussseidel(a,n,b,x)
    
      implicit none
    
      integer n, nmax
      !real, dimension(nmax):: norm
      real, dimension(1000)::norm
      real,dimension(n,n)::a
      real,dimension(n)::b,x,ax,diff
      integer i,j,k,icounter,iconv
      real toler
      real sum1,sum2,normb
    
      toler=0.000001
    
      icounter=0
      iconv=0
    
      do i=1,n
        x(i)=1.
      enddo
    
      do while (iconv.eq.0)
        
        icounter=icounter+1
        do i=1,n
            sum1=0.
            sum2=0.
            if(i.gt.1)then
                do j=1,i-1
                    sum1=sum1+a(i,j)*x(j)
                enddo
            endif
            !write(*,*)sum1,'sum1',i,'i',k
            if(i.lt.n)then
                do j=i+1,n
                    sum2=sum2+a(i,j)*x(j)
                enddo
            endif
            !write(*,*)sum2,'sum2',i,'i',k
            x(i)=(b(i)-sum1-sum2)/a(i,i)
         enddo
         !write(*,*)(x(i),i=1,n),'x(i)'
         
         do i=1,n
             ax(i)=0.
             do j=1,n
                 ax(i)=ax(i)+a(i,j)*x(j)
             enddo
         enddo
         
         do i=1,n
             diff(i)=b(i)-ax(i)
         enddo
         !write(*,*)(diff(i),i=1,n),'diff(i)'
 
         
         norm(icounter)=0.
         do i=1,n
            norm(icounter)=norm(icounter)+diff(i)**2
         enddo
         norm(icounter)=norm(icounter)**(1./2.)

         normb=0.
         do i=1,n
            normb=normb+b(i)**2
         enddo 
         normb=normb**(1./2.)        
                                    
         if (icounter.gt.1) then
             if((norm(icounter)/normb).lt.toler)then
                iconv=1
             else 
                 iconv=0
             endif
         else
             iconv=0
         endif
         
        !write(*,*)iconv,'iconv'
        !write(*,*)''
        
         
         if(icounter.eq.1000)then
             if(iconv.eq.0)then
                 write(*,*)'No CV with Gauss-Seidel'
                 stop
             endif
         endif

      enddo
     
c     !write(*,*)icounter

      end subroutine gaussseidel

	subroutine q2eulB(p1,p,p2,qq)

!  converts quaternion to Bunge Euler angles in radians
!  based on Altmann's solution for Euler->quat

	real qq(4),p1,p,p2
	real sum,diff,tmp
        real pi
        parameter ( pi = 3.14159265 )

!  CODE::

	if((abs(qq(2)).lt.1e-35).and.(abs(qq(1)).lt.1e-35)) then
!		diff=pi/4.
		diff = 0.  !  different choice!!!
	else
		diff = atan2(qq(2),qq(1))
	endif
!  ATAN2 is unhappy is both arguments are exactly zero!

	if((abs(qq(3)).lt.1e-35).and.(abs(qq(4)).lt.1e-35)) then
!		sum=pi/4.
		sum = 0.  !  different choice!!!
	else
		sum = atan2(qq(3),qq(4))
	endif

	p1 = diff + sum
	p2 = sum - diff
	tmp = sqrt( qq(3)**2 + qq(4)**2 )
	if(tmp.gt.1.0) tmp = 1.0
	if(tmp.lt.-1.0) tmp = -1.0
	p = 2.*acos(tmp)


	return
	end
!
