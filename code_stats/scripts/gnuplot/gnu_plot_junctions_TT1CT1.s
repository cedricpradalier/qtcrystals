reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_tw_inter_TT1CT1_TT03.eps"
set key at graph 0.24, 0.85 horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.
set grid
#set xtic # rotate by 90 scale 0
#unset ytics
set ytic # rotate by 90
set yrange [0:1.]
set xrange [-0.5:2.5]
set xtics ("1" 0,"2" 1,"3" 2) 
set ylabel 'Frequency' # offset -2.5
set xlabel 'T_{1}-C_{1} twin junctions'
set size 0.6, 0.95
plot 'tmp/gnu_tw_inter_TT1CT1.txt' using 2 title '' linecolor 3 # , '' using 0:(0):xticlabel(1) w l title ''

