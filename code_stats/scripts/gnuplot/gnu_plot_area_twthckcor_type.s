reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_area_twthckcor_type_cluster.eps"
set key at graph 0.27, 0.98 # horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.25
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:2.1]
set xrange [-0.5:11]
set xtics ("0" -0.1,"300" 2,"600" 4.2, "900" 6.4, "1200" 8.6, "1500" 10.8)  
set ylabel 'Average thickness of T_{1}, T_{2} and C_{1} twins in {/Symbol m}m' # offset -2.5
set xlabel ' Grain area in {/Symbol m}m^2'
set size 0.8, 0.95
#set key at 0.5,0.5 
#set key left top Right
#plot for [COL=4:5] 'gnu_sf_twthckcor_type_cluster_dbg.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_area_twthckcor_type.txt' using 5 title 'T_1', \
 '' using 6 title 'T_2' ,\
 '' using 7 title 'C_1' #,\
# '' using 8 title 'C2'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
