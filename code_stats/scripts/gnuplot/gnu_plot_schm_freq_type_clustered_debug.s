reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_schm_freq_type_clustered_debug_TT03.eps"
set key at graph 0.27, 0.97 # horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.5
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:0.3]
set xrange [-0.5:20]
#set xtics ("-0.5" -0.1, "-0.25" 12., "0" 25, "0.25" 37.5, "0.5" 50 )
set xtics ("-0.5" -0.15, "-0.25" 4.5, "0" 9.5, "0.25" 14.5, "0.5" 19.5 )
set ylabel 'Twinning mode frequency' # offset -2.5
set xlabel ' Schmid factor '
set size 0.8, 0.95
#set key left top
#plot for [COL=4:5] 'gnu_schm_freq_rk_clustered_debug.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_schm_freq_type_clustered_debug.txt' using 4 title 'T_1', \
 '' using 5 title 'T_2' ,\
 '' using 6 title 'C_1'#,\
# '' using 7 title 'CT2'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
