reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_sf_twthckcor_monovar_multivar_debug.eps"
set key at graph 0.24, 0.85 horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:1.15]
set xrange [-0.5:20]
#set xtics ("-0.5" 0, "-0.25" 12.5, "0" 25, "0.25" 37.5, "0.5" 50 )
set xtics ("-0.5" -0.1, "-0.25" 4.5, "0" 9.5, "0.25" 14.5, "0.5" 19.5 )
set ylabel 'Twin thickness in {/Symbol m}m' # offset -2.5
set xlabel ' Schmid factor '
#set size 0.8, 0.95
set key at 4,1. 
set key left top Right
#plot for [COL=4:5] 'tmp/gnu_sf_twthckcor_mono_multi.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_sf_twthckcor_monovar_multivar.txt' using 5 title 'Monovariant', \
 '' using 6 title 'Multivariant'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
