reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_area_frac_TT03.eps"
set key at graph 0.24, 0.85 horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.5
#set xtic # rotate by 90 scale 0
#unset ytics
set ytic # rotate by 90
set yrange [0:1.05]
set xrange [-0.5:11]
set xtics ("0" -0.1,"300" 2,"600" 4.2, "900" 6.4, "1200" 8.6, "1500" 10.8) 
set ylabel 'Fraction of twinned grains' # offset -2.5
set xlabel ' Grain area in {/Symbol m}m^2'
set size 0.6, 0.95
plot 'tmp/gnu_area_frac.txt' using 2 title '' linecolor 3 # , '' using 0:(0):xticlabel(1) w l title ''

