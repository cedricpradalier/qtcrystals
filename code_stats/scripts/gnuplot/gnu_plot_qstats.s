reset
set terminal postscript eps enhanced color "Times-Bold" 16
set output "plots/plot_tw_inter_qstats_TT03.eps"
#set key at graph 0.24, 0.85 horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.
#set xtic rotate by 90 scale 0
#unset ytics
#set ytic rotate by 90
set yrange [0:0.9]
set xrange [-0.5:10.5]
set xtics ("1" 0, "2" 1, "3" 2,"4" 3, "5" 4, "6" 5,"7" 6, "8" 7, "9" 8,"10" 9,"U" 10) 
set ylabel 'Frequency' # offset -2.5
set xlabel 'Twin-twin interactions'
set size 0.6, 0.95
plot 'tmp/gnu_tw_inter_qstats.txt' using 2 title '' linecolor 3 # rotate by 90 # , '' using 0:(0):xticlabel(1) w l title ''
