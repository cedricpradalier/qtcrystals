reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_area_freq_type_TT03.eps"
set key at graph 0.98, 0.97 # horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
set style fill solid 0.5 border -1
set boxwidth 1.35
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:0.225]
set xrange [-0.5:11]
set xtics ("0" -0.1,"300" 2,"600" 4.2, "900" 6.4, "1200" 8.6, "1500" 10.8) 
set ylabel 'Twinning mode frequency' # offset -2.5
set xlabel ' Grain area in {/Symbol m}m^2'
set size 0.8, 0.95
#set key at 0.5,0.5 
#set key left top Right
plot 'tmp/gnu_area_freq_type.txt' using 5 title 'T_{1}',\
 '' using 6 title 'T_{2}' ,\
 '' using 7 title 'C_{1}'#,\
# '' using 8 title 'C_{2}'
