reset
set terminal postscript eps enhanced color "Times-Bold" 19
set output "plots/plot_area_twnb_type_scatter.eps"
set clip points                                                             
#set xtic auto
set xrange [0:1485]
set ytic auto                                                                # set ytics automatically
set mytics                                                                   # write minor scale tics              
set grid
#set ylabel 'Number of T_{1}, T_{2} and C_{1} twins per grain' # offset -2.5
set ylabel 'Number of T_{1}, T_{2} and C_{1} twins per grain'
set xlabel ' Grain area in {/Symbol m}m^2'
set xtics ("0" 0.,"300" 300,"600" 600, "900" 900, "1200" 1200, "1500" 1485)  
set size 0.8, 0.95
set key at graph 0.3, 0.96
plot "tmp/gnu_area_twnb_modes_scatter_T1.txt" using (1.*$2):(1.*$3) title 'T_{1}',\
       "tmp/gnu_area_twnb_modes_scatter_T2.txt" using (1.*$2):(1.*$3) title 'T_{2}',\
       "tmp/gnu_area_twnb_modes_scatter_C1.txt" using (1.*$2):(1.*$3) title 'C_{1}'#,\
#       "tmp/gnu_area_twnb_modes_scatter_T1.txt" using (1.*$2):(1.*$3) title 'C_{2}'#,\
