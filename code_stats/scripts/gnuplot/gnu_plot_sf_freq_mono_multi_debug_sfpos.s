reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_sf_freq_mono_multi_sfpos.eps"
set key at graph 0.37, 0.97 # horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.15
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:0.4]
set xrange [-0.25:10]
#set xtics ("-0.5" 0, "-0.25" 12.5, "0" 25, "0.25" 37.5, "0.5" 50 )
set xtics ("0" -0.1, "0.1" 1.5, "0.2" 3.5, "0.3" 5.5, "0.4" 7.5, "0.5" 9.5 )
set ylabel 'Frequency' # offset -2.5
set xlabel ' Schmid factor '
set size 0.8, 0.95
#set key at 0.5,0.5 
#set key left top Right
#plot for [COL=4:5] 'gnu_sf_twthckcor_mono_multi.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_sf_freq_mono_multi.txt' using 5 title 'Mono', \
 '' using 6 title 'Multi'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
