reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_diam_freq_TT03.eps"
set key at graph 0.24, 0.85 horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.5
#set xtic # rotate by 90 scale 0
#unset ytics
set ytic # rotate by 90
set yrange [0:0.225]
set xrange [-0.5:11]
set xtics ("2" 0, "8" 1.75, "16" 3.65, "24" 5.6, "32" 7.55, "40" 9.5)
set ylabel 'Frequency' # offset -2.5
set xlabel ' Grain diameter in {/Symbol m}m '
set size 0.6, 0.95
plot 'tmp/gnu_diam_freq.txt' using 2 title '' linecolor 3 # , '' using 0:(0):xticlabel(1) w l title ''
# ("2" 0, "5" 2.61, "10" 6.96, "15" 11.3, "20" 15.65,"25" 20)
