reset
set terminal postscript eps enhanced color "Times-Bold" 16
set output "plots/plot_area_twnb_type_TT03.eps"
set key at graph 0.1, 0.96 # horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
set style fill solid 0.5 border -1
set boxwidth 1.35
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:31]
set xrange [-0.5:11]
set xtics ("0" -0.1,"300" 2,"600" 4.2, "900" 6.4, "1200" 8.6, "1500" 10.8) 
set ylabel 'Average number of T_{1}, T_{2} and C_{1} twins per twinned grain' # offset -2.5
set xlabel ' Grain area in {/Symbol m}m^2'
set size 0.8, 0.95
#set key at 0.5,0.5 
set key left top Right
#plot for [COL=4:5] 'gnu_sf_twthckcor_type_cluster_dbg.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_area_twnb_modes.txt' using 2 title 'T_{1}',\
 '' using 3 title 'T_{2}' ,\
 '' using 4 title 'C_{1}'#,\
# '' using 8 title 'C_{2}'
