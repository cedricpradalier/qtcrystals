reset
set terminal postscript eps enhanced color "Times-Bold" 20
set output "plots/plot_schm_twthckcor_debug_TT03.eps"
set key at graph 0.24, 0.85 horizontal samplen 0.1
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.5
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:1.25]
set xrange [-0.5:20]
#set xtics ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
set xtics ("-0.5" -0.1, "-0.25" 4.5, "0" 9.5, "0.25" 14.5, "0.5" 19.5 )
set ylabel 'Twin thickness in {/Symbol m}m' # offset -2.5
set xlabel ' Schmid factor '
set size 0.6, 0.95
plot 'tmp/gnu_schm_twthckcor_debug.txt' using 4 title '' linecolor 3 # , '' using 0:(0):xticlabel(1) w l title ''
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
