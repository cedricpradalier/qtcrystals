reset
set terminal postscript eps enhanced color "Times-Bold" 23
set output "plots/plot_sf_freq_rkall_rel_TT2_dbg.eps"
set key at graph 0.47,0.97 # horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.5
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:0.265]
set xrange [-0.5:20]
#set xtics ("-0.5" 0, "-0.25" 12.5, "0" 25, "0.25" 37.5, "0.5" 50 )
set xtics ("-0.5" -0.1, "-0.25" 4.5, "0" 9.5, "0.25" 14.5, "0.5" 19.5 )
set ylabel 'T_{2} variant frequency' # offset -2.5
set xlabel ' Schmid factor - TT03 scans '
set size 0.8, 0.95
#set key at 0.5,0.5 
#set key center top Right
#plot for [COL=4:5] 'gnu_schm_freq_rk_clustered_debug.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_sf_freq_rkall_rel_TT2_dbg.txt' using 5 title 'v_1', \
 '' using 6 title 'v_2' ,\
 '' using 7 title 'v_3',\
 '' using 8 title 'v_4, v_5, v_6'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
