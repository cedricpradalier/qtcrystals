
load 'scripts/gnuplot/gnu_plot_diam_freq.s'
load 'scripts/gnuplot/gnu_plot_area_freq.s'

#load 'scripts/gnuplot/gnu_plot_qstats.s'

load 'scripts/gnuplot/gnu_plot_diam_frac.s'
load 'scripts/gnuplot/gnu_plot_area_frac.s'

#load 'scripts/gnuplot/gnu_plot_area_twnb.s'

load 'scripts/gnuplot/gnu_plot_sf_twthckcor_type_cluster_debug.s'
load 'scripts/gnuplot/gnu_plot_schm_twthckcor_debug.s'

load 'scripts/gnuplot/gnu_plot_area_twthckcor.s'

load 'scripts/gnuplot/gnu_plot_area_freq_mono_multi.s'
load 'scripts/gnuplot/gnu_plot_area_twthckcor_mono_multi.s'

load 'scripts/gnuplot/gnu_plot_sf_freq_mono_multi_debug.s'
load 'scripts/gnuplot/gnu_plot_sf_twthckcor_mono_multi_debug.s'

load 'scripts/gnuplot/gnu_plot_area_freq_monovar_multivar.s'
load 'scripts/gnuplot/gnu_plot_area_twthckcor_monovar_multivar.s'

load 'scripts/gnuplot/gnu_plot_sf_freq_monovar_multivar_debug.s'
load 'scripts/gnuplot/gnu_plot_sf_twthckcor_monovar_multivar_debug.s'

#load 'scripts/gnuplot/gnu_plot_area_tottwnb_type.s'
#load 'scripts/gnuplot/gnu_plot_twnb_sf.s'

#########################################################

#load 'scripts/gnuplot/gnu_plot_junctions_TT1TT1.s'
load 'scripts/gnuplot/gnu_plot_junctions_TT1TT2.s'
load 'scripts/gnuplot/gnu_plot_junctions_TT1CT1.s'
#load 'scripts/gnuplot/gnu_plot_junctions_TT1CT2.s'
#load 'scripts/gnuplot/gnu_plot_junctions_TT2TT2.s'
load 'scripts/gnuplot/gnu_plot_junctions_TT2CT1.s'
#load 'scripts/gnuplot/gnu_plot_junctions_TT2CT2.s'
load 'scripts/gnuplot/gnu_plot_junctions_CT1CT1.s'
#load 'scripts/gnuplot/gnu_plot_junctions_CT1CT2.s'
#load 'scripts/gnuplot/gnu_plot_junctions_CT2CT2.s'

load 'scripts/gnuplot/gnu_plot_area_twnb_type.s'
load 'scripts/gnuplot/gnu_plot_area_twnb_type_scatter.s'
load 'scripts/gnuplot/gnu_plot_area_frac_type.s'
load 'scripts/gnuplot/gnu_plot_area_twthckcor_type_scatter.s'

load 'scripts/gnuplot/gnu_plot_area_tw_proportion_type.s'

load 'scripts/gnuplot/gnu_plot_area_twthckcor_type.s'

load 'scripts/gnuplot/gnu_plot_schm_freq_type_clustered_debug.s'

load 'scripts/gnuplot/gnu_plot_sf_freq_rkall_rel_TT1_dbg.s'
load 'scripts/gnuplot/gnu_plot_sf_freq_rkall_rel_TT2_dbg.s'
load 'scripts/gnuplot/gnu_plot_sf_freq_rkall_rel_CT1_dbg.s'
#load 'scripts/gnuplot/gnu_plot_sf_freq_rkall_rel_CT2_dbg.s'

load 'scripts/gnuplot/gnu_plot_area_freq_type.s'
########################################################

load 'scripts/gnuplot/gnu_plot_area_twthckcor_modes.s'

load 'scripts/gnuplot/gnu_plot_sf_freq_mono_multi_debug_sfpos.s'
load 'scripts/gnuplot/gnu_plot_sf_twthckcor_mono_multi_debug_sfpos.s'

load 'scripts/gnuplot/gnu_plot_area_freq_mono_multi_sfpos.s'
load 'scripts/gnuplot/gnu_plot_area_twthckcor_mono_multi_sfpos.s'
