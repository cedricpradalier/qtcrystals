reset
set terminal postscript eps enhanced color "Times-Bold" 19
set output "plots/plot_sf_twthckcor_type_cluster_debug_3tw_rev_TT03.eps"
set key at graph 0.27, 0.97 # horizontal samplen 0.1
#set style histogram clustered
set style data histogram
set style histogram cluster gap 1
#set style histogram errorbars linewidth 2
set style fill solid 0.5 border -1
set boxwidth 1.35
#set xtic # rotate by 90 scale 0
#unset ytics
set grid
set ytic # rotate by 90
set yrange [0:1.6]
set xrange [-0.5:20]
#set xtics ("-0.5" 0, "-0.25" 12.5, "0" 25, "0.25" 37.5, "0.5" 50 )
set xtics ("-0.5" -0.1, "-0.25" 4.5, "0" 9.5, "0.25" 14.5, "0.5" 19.5 )
set ylabel 'Average thickness of T_{1}, T_{2} and C_{1} twins in {/Symbol m}m' # offset -2.5
set xlabel ' Schmid factor '
set size 0.8, 0.95
#set key at 12.5,2.75 
#set key left top Right
#plot for [COL=4:5] 'gnu_sf_twthckcor_type_cluster_dbg.txt' using COL:xticlabel(1) title ''
plot 'tmp/gnu_schm_twthckcor_type_cluster_debug.txt' using 5 title 'T_1', \
 '' using 6 title 'T_2' ,\
 '' using 7 title 'C_1'#,\
# '' using 8 title 'CT2'
# ("0" 0, "0.1" 5, "0.2" 10, "0.3" 15, "0.4" 20, "0.5" 25)
