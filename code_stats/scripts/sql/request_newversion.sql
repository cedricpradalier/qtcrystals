.separator ,
.output table_gavg.csv
select MAP_ID,g.id, g.area,g.border_length,count(g.id),g.border_length/count(g.id)
from Grains as g, GrainEdges as e
where e.i = g.id
group by g.id order by g.id;

DROP VIEW Twins;
CREATE VIEW Twins as
SELECT C1.id as P, C2.id,c2.grain,S2.area,C2.size,max(E.qw),S2.length,S2.thickness,S2.cov_xx,S2.cov_xy,S2.cov_yy,c2.x,c2.y,e.twinning,e.variant,s2.qx,s2.qy,s2.qz,s2.qw,e.alpha,e.beta
from ConnectedEdges as E
inner join connected as c1 on c1.id=e.i
inner join connected as c2 on c2.id=e.j 
inner join connectedStats as s1 on c1.id=s1.id
inner join connectedStats as s2 on c2.id=s2.id
inner join Grains as G on G.id = C2.grain
and C1.is_parent
and E.twinning>1
and C1.grain=C2.grain
and not G.map_edge
and c1.twinstrip<=0 and c2.twinstrip<=0
and c2.twinning_order=1
order by C2.id ;

.output table_twins.csv
select MAP_ID,* from Twins;

.output table_parents.csv
select MAP_ID,g.id, g.area,g.border_length,avg(t.area),sum(t.area),count(t.id),avg(t.thickness)
from grains as g,  Twins as t
where G.id = t.grain
group by g.id order by g.id;

.output table_grains.csv
select MAP_ID,g.id,g.area,g.qx,g.qy,g.qz,g.qw
from grains as g
where not g.map_edge
order by g.id; 

.output table_tw_inter.csv
select MAP_ID,c1.grain, c1.id, c2.id,t1.alpha,t1.beta,t1.twinning,t1.variant,t2.alpha,t2.beta,t2.twinning,t2.variant
from connectededges as e 
inner join connected as c1 on c1.id=e.i 
inner join connected as c2 on c2.id=e.j
inner join twins as t1 on c1.id = t1.id
inner join twins as t2 on c2.id = t2.id
where e.i>e.j 
and c1.is_parent<>1 and c2.is_parent<>1
and c1.is_valid=1 and c2.is_valid=1
and c1.grain=c2.grain
and c1.twinstrip<=0 and c2.twinstrip<=0
order by c1.grain;

.output table_tw_pairs.csv
select MAP_ID,t.grain,d.i,d.j,d.dist,d.xi,d.yi,d.xj,d.yj
from twins as t, IngrainDistances as d
where (t.id=d.i or t.id=d.j)
group by t.grain order by t.grain;

.output table_twins_with_problems.csv
select distinct MAP_ID,T.id,e1.variant,P1.id,e2.variant,P2.id
from Connected as T
left join ConnectedEdges as e1 on T.id=e1.j
left join Connected as P1 on P1.id=e1.i
left join ConnectedEdges as e2 on T.id=e2.j
left join Connected as P2 on P2.id=e2.i
where (P1.id < P2.id) and (e1.variant!=e2.variant)
and e1.active and e2.active
and P1.is_parent and P2.is_parent;
