#!/bin/bash
#set -e -x

i=1
d=../../bin/db/  
HERE=$PWD
# while [ $i -le 1 ]
#do 
      
    dn=`basename $d`

    echo "$i"

    echo "$dn"
    f=grains-Map$i.sql 
    pushd $d
    echo "$f"

if test -r $f
    then
        echo "Processing $f"
        cat $HERE/request_newversion.sql | sed -e "s/MAP_ID/$i/" | sqlite3 $f
    else
	echo "File $f does not exist"
    fi
    popd
 

(( i++ ))

#done


mv ../../bin/db/table_grains.csv ../batch/table_grains.csv
mv ../../bin/db/table_parents.csv ../batch/table_parents.csv
mv ../../bin/db/table_twins.csv ../batch/table_twins.csv
mv ../../bin/db/table_tw_inter.csv ../batch/table_tw_inter.csv
mv ../../bin/db/table_tw_pairs.csv ../batch/table_tw_pairs.csv

# exit



