#include <QPixmap>
#include <QImage>
#include <QRect>
#include <QSize>
#include <QPoint>
#include <QPen>
#include <QBrush>
#include <QPainter>
#include "keywindow.h"
#include "ui_keywindow.h"
#include "mainwindow.h"
#include "crystals/color_tools.h"

using namespace crystals;

KeyWindow::KeyWindow(MainWindow *main, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KeyWindow),
    main_window(main)
{
    ui->setupUi(this);
}

KeyWindow::~KeyWindow()
{
    delete ui;
}

void KeyWindow::closeEvent(QCloseEvent * event)
{
    this->QMainWindow::closeEvent(event);
    main_window->recordKeyClosure();
}

void KeyWindow::clearLayout()
{
    QLayoutItem *item;
    while(ui->formLayout->count()) {
        item = ui->formLayout->takeAt(0);
        if (item->layout()) {
            QWidget().setLayout(item->layout());
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
    ui->formLayout->update();
}

QLabel * KeyWindow::coloredLine(QColor c, unsigned int width, QColor bg) {
    QLabel * label = new QLabel();
    QImage Ilabel = QImage(50,20,QImage::Format_RGB32);
    Ilabel.fill(bg);
    QPainter painter(&Ilabel);
    QPen pen(c);
    pen.setWidth(width);
    painter.setPen(pen);
    painter.drawLine(QPoint(0,10),QPoint(50,10));
    painter.end();
    label->setPixmap(QPixmap::fromImage(Ilabel));
    return label;
}

QLabel * KeyWindow::coloredCircle(QColor c, unsigned int radius, int width, QColor bg) {
    QLabel * label = new QLabel();
    QImage Ilabel = QImage(50,20,QImage::Format_RGB32);
    Ilabel.fill(bg);
    QPainter painter(&Ilabel);
    if (width <= 0) {
        QPen pen(c);
        pen.setWidth(1);
        QBrush brush(c);
        painter.setPen(pen);
        painter.setBrush(brush);
        painter.drawEllipse(QPoint(25,10),radius,radius);
    } else {
        QPen pen(c);
        pen.setWidth(width);
        painter.setPen(pen);
        painter.drawEllipse(QPoint(25,10),radius,radius);
    }
    painter.end();
    label->setPixmap(QPixmap::fromImage(Ilabel));
    return label;
}

QLabel * KeyWindow::weirdParent() {
    QLabel * label = new QLabel();
    QImage Ilabel = QImage(50,20,QImage::Format_RGB32);
    Ilabel.fill(QColor(0,0,0));
    QPainter painter(&Ilabel);
    QColor C[3] = {
        Color(255,255,255),
        Color(0,0,0),
        Color(255,0,0)
    };
    for (int i=0;i<3;i++) {
        QPen pen(C[i]);
        pen.setWidth(1);
        QBrush brush(C[i]);
        painter.setPen(pen);
        painter.setBrush(brush);
        painter.drawEllipse(QPoint(25,10),8-3*i,8-3*i);
    }
    painter.end();
    label->setPixmap(QPixmap::fromImage(Ilabel));
    return label;
}

QLabel * KeyWindow::textSymbol(const std::string & txt) {
    QLabel * label = new QLabel();
    QImage Ilabel = QImage(50,20,QImage::Format_RGB32);
    Ilabel.fill(QColor(0,0,128));
    QPainter painter(&Ilabel);
    QPen bg(QColor(0xC0,0xC0,0xC0));
    QPen fg(QColor(0x00,0x00,0x00));
    painter.setPen(bg);
    QPoint Pg(20,15);
    QFont idFontFg;
    idFontFg.setFamily(QString("Courier"));
    idFontFg.setWeight(QFont::Normal);
    idFontFg.setPointSize(12);
    idFontFg.setStyleHint(QFont::Courier);
    idFontFg.setFixedPitch(true);
    idFontFg.setWeight(QFont::Bold);
    painter.setFont(idFontFg);
    painter.drawText(-QPoint(-1,-1),QString(txt.c_str()));
    painter.drawText(Pg-QPoint(-1,1),QString(txt.c_str()));
    painter.drawText(Pg-QPoint(1,-1),QString(txt.c_str()));
    painter.drawText(Pg-QPoint(1,1),QString(txt.c_str()));
    painter.setPen(fg);
    painter.drawText(Pg,QString(txt.c_str()));
    painter.end();
    label->setPixmap(QPixmap::fromImage(Ilabel));
    return label;
}

void KeyWindow::addPair(QWidget *w1, QWidget *w2, int row, int col) 
{
    ui->formLayout->addWidget(w1,row,col,Qt::AlignRight);
    ui->formLayout->addWidget(w2,row,col+1,Qt::AlignLeft);
}

void KeyWindow::updateKey(crystals::MaterialEnum material,
        crystals::EBSDAnalyzer::GraphModes mode) 
{
    clearLayout();
    switch (mode) {
        case EBSDAnalyzer::NO_GRAPH:
            ui->keyTitle->setText("Key for RAW graphic mode ");
            ui->commentBrowser->setText(
                   "<h2>Displaying raw data</h2>"
                   "Use the <em>Colors</em> menu to change colouring mode."
                   "<h3>Mouse actions</h3>"
                   "In this mode, clicking on pixel displays their value.");
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);
            break;
        case EBSDAnalyzer::GRAPH_CONNECTED: 
            ui->keyTitle->setText("Key for Twinning Edition");
            ui->commentBrowser->setText(
                    "<h2>Displaying connected fragments for edition</h2>"
                   "<h3>Mouse actions</h3>"
                   "In this mode, clicking on edges enable/disable them."
                    );

            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"), 0, 2);
            addPair(coloredCircle(Color(0,255,255),3,-1),
                    new QLabel("Center of parent fragment"), 1, 0);
            addPair(coloredCircle(Color(255,255,0),3,-1),
                    new QLabel("Center of primary twin fragment"), 1, 2);
            addPair(coloredCircle(Color(255,0,0),4,-1),
                    new QLabel("Center of secondary twin fragment"), 2, 0);
            addPair(coloredCircle(Color(0,0,255),5,-1),
                    new QLabel("Center of higher order twin"), 2, 2);
            addPair(coloredLine(Color(0,0,0),3,Color(128,128,128)),
                    new QLabel("Zero relation"), 3, 0);
            addPair(coloredLine(Color(0,0,0),1,Color(128,128,128)),
                    new QLabel("Unused zero relation"), 3, 2);
            switch (material) {
                case MAT_MAGNESIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Valid tensile relation"), 4, 0);
                    addPair(coloredLine(Color(0,255,0),1),
                            new QLabel("Unused tensile relation"), 4, 2);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Valid compressive relation"), 5, 0);
                    addPair(coloredLine(Color(0,0,255),1),
                            new QLabel("Unused compressive relation"), 5, 2);
                    break;
                case MAT_ZIRCONIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Valid tensile 1 relation"), 4, 0);
                    addPair(coloredLine(Color(0,255,0),1),
                            new QLabel("Unused tensile 1 relation"), 4, 2);
                    addPair(coloredLine(Color(255,0, 255),3),
                            new QLabel("Valid tensile 2 relation"), 5, 0);
                    addPair(coloredLine(Color(255, 0, 255),1),
                            new QLabel("Unused tensile 2 relation"), 5, 2);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Valid compressive 1 relation"), 6, 0);
                    addPair(coloredLine(Color(0,0,255),1),
                            new QLabel("Unused compressive 1 relation"), 6, 2);
                    break;
                default:
                    break;
            }
            break;
        case EBSDAnalyzer::GRAPH_GRAINS: 
            ui->keyTitle->setText("Key for GRAIN graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Displaying grain neihbouring relationship</h2>"
                   "<h3>Mouse actions</h3>"
                   "Clicking on a fragment force it to be/not be the parent phase of the grain."
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);
            addPair(coloredCircle(Color(0,0,0),3,-1, Color(192,192,192)),
                    new QLabel("Center of valid grain"), 1, 0);
            addPair(coloredCircle(Color(92,92,92),3,-1, Color(192,192,192)),
                    new QLabel("Center of grain on map edge"), 1, 2);
            addPair(coloredLine(Color(255,0,0),2),
                    new QLabel("Grain neighbours"), 2, 0);
            break;
        case EBSDAnalyzer::GRAPH_CLUSTERS: 
            ui->keyTitle->setText("Key for CLUSTER graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Displaying hierarchical relations between fragments</h2>"
                   "<h3>Mouse actions</h3>"
                   "Clicking on a fragment displays statistics on this fragment."
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredCircle(Color(0,255,255),3,-1),
                    new QLabel("Center of parent fragment"), 0, 2);

            addPair(coloredLine(Color(255,255,0),2),
                    new QLabel("Primary twin joint"),1,0);
            addPair(coloredCircle(Color(255,255,0),3,-1),
                    new QLabel("Center of primary twin fragment"), 1, 2);

            addPair(coloredLine(Color(255,0,0),2),
                    new QLabel("Secondary twin joint"),2,0);
            addPair(coloredCircle(Color(255,0,0),3,-1),
                    new QLabel("Center of secondary twin fragment"), 2, 2);

            addPair(coloredLine(Color(0,0,255),2),
                    new QLabel("Higher order twin joint"),3,0);
            addPair(coloredCircle(Color(0,0,255),3,-1),
                    new QLabel("Center of higher order twin fragment"), 3, 2);

            addPair(textSymbol("4"), new QLabel("Variant number"), 4, 0);

            switch (material) {
                case MAT_MAGNESIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Tensile relation"), 5, 0);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Compressive relation"), 5, 2);
                    break;
                case MAT_ZIRCONIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Tensile 1 relation"), 5, 0);
                    addPair(coloredLine(Color(255,0, 255),3),
                            new QLabel("Tensile 2 relation"), 5, 2);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Compressive 1 relation"), 6, 0);
                    break;
                default:
                    break;
            }
            break;
        case EBSDAnalyzer::GRAPH_TWINNING: 
            ui->keyTitle->setText("Key for Twinning Statistics");
            ui->commentBrowser->setText(
                    "<h2>Displaying connected graph for statistics</h2>"
                   "<h3>Mouse actions</h3>"
                   "Clicking on an edge provides statistical data on the twinning relation."
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"), 0, 2);
            addPair(coloredCircle(Color(0,255,255),3,-1),
                    new QLabel("Center of parent fragment"), 1, 0);
            addPair(coloredCircle(Color(255,255,0),3,-1),
                    new QLabel("Center of primary twin fragment"), 1, 2);
            addPair(coloredCircle(Color(255,0,0),3,-1),
                    new QLabel("Center of secondary twin fragment"), 2, 0);
            addPair(coloredCircle(Color(0,0,255),3,-1),
                    new QLabel("Center of higher order twin"), 2, 2);
            addPair(weirdParent(), new QLabel("Inconsistent parent"), 3, 0);
            addPair(textSymbol("4"), new QLabel("Variant number"), 3, 2);

            addPair(coloredLine(Color(0,0,0),3,Color(128,128,128)),
                    new QLabel("Zero relation"), 4, 0);

            switch (material) {
                case MAT_MAGNESIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Tensile relation"), 5, 0);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Compressive relation"), 5, 2);
                    break;
                case MAT_ZIRCONIUM:
                    addPair(coloredLine(Color(0,255,0),3),
                            new QLabel("Tensile 1 relation"), 5, 0);
                    addPair(coloredLine(Color(255,0, 255),3),
                            new QLabel("Tensile 2 relation"), 5, 2);
                    addPair(coloredLine(Color(0,0,255),3),
                            new QLabel("Compressive 1 relation"), 6, 0);
                    break;
                default:
                    break;
            }
            break;
        case EBSDAnalyzer::GRAPH_HULLS: 
            ui->keyTitle->setText("Key for Convex Hull graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Displaying convex hulls around grains</h2>"
                    "The purpose of this mode is to identify non-convex grains as requiring closer inspection. Convexity is measured as the ratio between the area of the grain and the area of its convex hull."
                   "<h3>Mouse actions</h3>"
                   "None"
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);
            addPair(coloredLine(Color(0,255,0),3),
                    new QLabel("Convex hull of convex grain"), 1, 0);
            addPair(coloredLine(Color(0,0,255),3),
                    new QLabel("Convex hull of non-convex grain"),1,2);
            addPair(coloredCircle(Color(0,0,0),3,-1, Color(192,192,192)),
                    new QLabel("Grain center"), 2, 0);
            break;
        case EBSDAnalyzer::GRAPH_ELLIPSES: 
            ui->keyTitle->setText("Key for Twinning Ellipsicity graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Displaying twin ellipsicity and statistics</h2>"
                   "<h3>Mouse actions</h3>"
                   "Clicking on a fragment displays statistics on itself and its grain." 
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);
            addPair(coloredCircle(Color(0,255,0),8,2),
                    new QLabel("Elliptical twin"), 1, 0);
            addPair(coloredCircle(Color(0,0,255),8,3),
                    new QLabel("Non-elliptical twin"),1,2);
            break;
        case EBSDAnalyzer::GRAPH_CONNECTED_TWINS: 
            ui->keyTitle->setText("Key for Twinning Connection graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Highlighting and editing touching twins</h2>"
                   "<h3>Mouse actions</h3>"
                   "Clicking on two twins allows to record a neighbouring relation, even if it is not directly apparent in the data (because of missing data for instance)."
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);
            addPair(coloredLine(Color(255,0,255),3),
                    new QLabel("Neighbour twins connection"), 1, 0);
            addPair(coloredCircle(Color(255,0,255),4,-1),
                    new QLabel("Center of neighbour twins"),1,2);
            addPair(coloredLine(Color(255,0,0),3),
                    new QLabel("Forced neighbouring connection"), 2, 0);
            addPair(coloredCircle(Color(255,0,0),4,-1),
                    new QLabel("Center of forced neighbours"),2,2);
            break;
        case EBSDAnalyzer::GRAPH_GRAIN_JOINTS:
            ui->keyTitle->setText("Key for Twinning Joint graphic mode");
            ui->commentBrowser->setText(
                    "<h2>Highlighting twinning relations on the joints</h2>"
                    "The purpose of this mode is to highlight the type of twinning observed on the fragment joint. The little wiggly segments corresponds to these measurements at the resolution of the EBSD data acquisition."
                   "<h3>Mouse actions</h3>"
                   "None"
                    );
            addPair(coloredLine(Color(0,255,255),2),
                    new QLabel("Grain joint"), 0, 0);
            addPair(coloredLine(Color(0,0,127),2),
                    new QLabel("Fragment joint"),0,2);

            addPair(coloredCircle(Color(0,255,255),3,-1),
                    new QLabel("Center of parent fragment"), 1, 0);
            addPair(coloredCircle(Color(255,255,0),3,-1),
                    new QLabel("Center of primary twin fragment"), 1, 2);
            addPair(coloredCircle(Color(255,0,0),3,-1),
                    new QLabel("Center of secondary twin fragment"), 2, 0);
            addPair(coloredCircle(Color(0,0,255),3,-1),
                    new QLabel("Center of higher order twin"), 2, 2);
            addPair(coloredLine(Color(128,128,128),2),
                    new QLabel("Unclassified relation"), 3, 0);
            switch (material) {
                case MAT_MAGNESIUM:
                    addPair(coloredLine(Color(0,255,0),2),
                            new QLabel("Tensile relation"), 4, 0);
                    addPair(coloredLine(Color(0,0,255),2),
                            new QLabel("Compressive relation"), 4, 2);
                    break;
                case MAT_ZIRCONIUM:
                    addPair(coloredLine(Color(0,255,0),2),
                            new QLabel("Tensile 1 relation"), 4, 0);
                    addPair(coloredLine(Color(255,0, 255),2),
                            new QLabel("Tensile 2 relation"), 4, 2);
                    addPair(coloredLine(Color(0,0,255),2),
                            new QLabel("Compressive 1 relation"), 5, 0);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

}

