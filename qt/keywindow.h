#ifndef KEYWINDOW_H
#define KEYWINDOW_H

#include <QMainWindow>
#include <QColor>
#include <QLabel>
#include <crystals/constants.h>
#include <crystals/EBSD.h>

namespace Ui {
class KeyWindow;
}

class MainWindow;

class KeyWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit KeyWindow(MainWindow *main, QWidget *parent = 0);
    ~KeyWindow();

    void updateKey(crystals::MaterialEnum material,
            crystals::EBSDAnalyzer::GraphModes mode);

private:
    Ui::KeyWindow *ui;
    MainWindow *main_window;
    void closeEvent(QCloseEvent * event);

    QLabel * coloredLine(QColor c, unsigned int width=1, QColor bg=QColor(0,0,0));
    QLabel * coloredCircle(QColor c, unsigned int radius, int width=1, QColor bg=QColor(0,0,0));
    QLabel * textSymbol(const std::string & txt);
    QLabel * weirdParent();
    void addPair(QWidget *w1, QWidget *w2, int row, int col);
    void clearLayout();
};

#endif // KEYWINDOW_H
