#include <QtGui>
#include <QPainter>
#include "overviewwindow.h"
#include "mainwindow.h"
#include "ui_overviewwindow.h"

OverviewWindow::OverviewWindow(MainWindow *main, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OverviewWindow),
    main_window(main)
{
    ui->setupUi(this);
}

OverviewWindow::~OverviewWindow()
{
    delete ui;
}

void OverviewWindow::setBackground(const QImage& bg)
{
    bg_mesh = bg.copy();
}

void OverviewWindow::setImage(const QRectF & rect)
{
    QRect view(rect.x()*bg_mesh.width(),rect.y()*bg_mesh.height(),
            rect.width()*bg_mesh.width(),rect.height()*bg_mesh.height());
    mesh = bg_mesh.copy();
    QPainter painter(&mesh);
    QPen pen(QColor(0,0,0));
    pen.setWidth(2);
    painter.setPen(pen);
    painter.drawRect(view);
    painter.end();
    ui->label->setPixmap(QPixmap::fromImage(mesh));
}


void OverviewWindow::on_actionPan_Left_triggered()
{
    main_window->on_actionPan_Left_triggered();
}

void OverviewWindow::on_actionPan_Right_triggered()
{
    main_window->on_actionPan_Right_triggered();
}

void OverviewWindow::on_actionPan_Up_triggered()
{
    main_window->on_actionPan_Up_triggered();
}

void OverviewWindow::on_actionPan_Down_triggered()
{
    main_window->on_actionPan_Down_triggered();
}

void OverviewWindow::on_actionClose_triggered()
{
    hide();
}

void OverviewWindow::mouseReleaseEvent(QMouseEvent *releaseEvent)
{
    QPoint pos = ui->label->mapFrom(this,releaseEvent->pos());
    // printf("Mouse event: (%d,%d) -> (%d,%d)\n",releaseEvent->x(),releaseEvent->y(), pos.x(),pos.y());
    if (releaseEvent->button() == Qt::LeftButton) {
        main_window->pan_to(pos.x()/((float)ui->label->width()),
                pos.y()/((float)ui->label->height()));
    }
}


void OverviewWindow::closeEvent(QCloseEvent * event)
{
    this->QMainWindow::closeEvent(event);
    main_window->recordOverviewClosure();
}

#if 0
void OverviewWindow::hideEvent(QHideEvent * event)
{
    this->QMainWindow::hideEvent(event);
    main_window->on_actionMap_overview_triggered(false);
}
#endif


