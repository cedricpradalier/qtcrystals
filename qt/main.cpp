#include <assert.h>
#include <locale.h>

#include "mainwindow.h"
#include <QApplication>
#include <QIcon>


#include <QDir>

#include "crystals/quaternion_tools.h"
#include "crystals/color_tools.h"
using namespace crystals;


int main(int argc, char *argv[])
{
#ifdef MACOS_APP
    QDir dir(argv[0]);  // e.g. appdir/Contents/MacOS/appname
      assert(dir.cdUp());
      assert(dir.cdUp());
      assert(dir.cd("Plugins"));  // e.g. appdir/Contents/PlugIns
      QCoreApplication::setLibraryPaths(QStringList(dir.absolutePath()));
      printf("after change, libraryPaths=(%s)\n", QCoreApplication::libraryPaths().join(",").toUtf8().data());
#endif
    QApplication a(argc, argv);
    setlocale(LC_ALL,"C");
    crystals::bgr_mode=false;

    initialise_quaternion_symmetries();

    MainWindow w(argc,argv);
    w.setWindowIcon(QIcon("/icon.png"));
    ///////////////////////////////////////////////////////////////////////

    w.showMaximized();
    w.load_map_bg();

    return a.exec();
}
