#include "console.h"
#include "ui_console.h"
#include "mainwindow.h"

Console::Console(MainWindow *main,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Console),
    main_window(main)
{
    ui->setupUi(this);
    ui->plainTextEdit->setReadOnly(true);
}

Console::~Console()
{
    delete ui;
}

void Console::setText(const QString & s)
{
    ui->plainTextEdit->setPlainText(s);
}

void Console::appendText(const QString & s)
{
    ui->plainTextEdit->appendPlainText(s);
}

void Console::on_closeButton_clicked()
{
    main_window->recordConsoleClosure();
    hide();
}

void Console::closeEvent(QCloseEvent * event)
{
    this->QDialog::closeEvent(event);
    main_window->recordConsoleClosure();
}

