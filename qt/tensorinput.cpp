#include <assert.h>
#include <stdio.h>
#include "tensorinput.h"
#include "ui_tensorinput.h"

TensorInput::TensorInput(const std::vector<double> & tensor, const std::vector<double> & loading, QWidget *parent) :
    QDialog(parent),
    data(tensor),
    dir(loading),
    ui(new Ui::TensorInput)
{
    assert(data.size() == 9);
    assert(dir.size() == 3);
    ui->setupUi(this);
    ui->T11->setText(toText(data[0*3+0]));
    ui->T11b->setText(toText(data[0*3+0]));
    ui->T11->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T12->setText(toText(data[0*3+1]));
    ui->T12->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T13->setText(toText(data[0*3+2]));
    ui->T13->setValidator( new QDoubleValidator(0, 100, 5, this) );

    ui->T21->setText(toText(data[1*3+0]));
    ui->T21->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T22->setText(toText(data[1*3+1]));
    ui->T22b->setText(toText(data[1*3+1]));
    ui->T22->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T23->setText(toText(data[1*3+2]));
    ui->T23->setValidator( new QDoubleValidator(0, 100, 5, this) );

    ui->T31->setText(toText(data[2*3+0]));
    ui->T31->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T32->setText(toText(data[2*3+1]));
    ui->T32->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->T33->setText(toText(data[2*3+2]));
    ui->T33b->setText(toText(data[2*3+2]));
    ui->T33->setValidator( new QDoubleValidator(0, 100, 5, this) );

    ui->T11b->hide();
    ui->T22b->hide();
    ui->T33b->hide();

    ui->D1->setText(toText(dir[0]));
    ui->D1->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->D2->setText(toText(dir[1]));
    ui->D2->setValidator( new QDoubleValidator(0, 100, 5, this) );
    ui->D3->setText(toText(dir[2]));
    ui->D3->setValidator( new QDoubleValidator(0, 100, 5, this) );

    tensorType = TensorFull;
    collectData();
}

TensorInput::~TensorInput()
{
    delete ui;
}

QString TensorInput::toText(double x) {
    char tmp[128];
    sprintf(tmp,"%.5f",x);
    return QString(tmp);
}

void TensorInput::on_tensorMinimal_clicked(bool checked)
{
    ui->tensorMinimal->setChecked(true);
    ui->tensorDiagonal->setChecked(false);
    ui->tensorFull->setChecked(false);
    tensorType = TensorMinimal;

    ui->T11->setText(QString("1.0"));
    ui->T11b->setText(QString("1.0"));
    ui->T12->setText(QString("0.0"));
    ui->T12->setEnabled(false);
    ui->T13->setText(QString("0.0"));
    ui->T13->setEnabled(false);

    ui->T21->setText(QString("0.0"));
    ui->T21->setEnabled(false);
    ui->T22->setText(QString("0.0"));
    ui->T22b->setText(QString("0.0"));
    ui->T23->setText(QString("0.0"));
    ui->T23->setEnabled(false);

    ui->T31->setText(QString("0.0"));
    ui->T31->setEnabled(false);
    ui->T32->setText(QString("0.0"));
    ui->T32->setEnabled(false);
    ui->T33->setText(QString("0.0"));
    ui->T33b->setText(QString("0.0"));

    ui->T11->setReadOnly(true);
    ui->T22->setReadOnly(true);
    ui->T33->setReadOnly(true);
    ui->T11b->show();
    ui->T22b->show();
    ui->T33b->show();
    ui->T11->hide();
    ui->T22->hide();
    ui->T33->hide();
}

void TensorInput::on_tensorDiagonal_clicked(bool checked)
{
    ui->tensorMinimal->setChecked(false);
    ui->tensorDiagonal->setChecked(true);
    ui->tensorFull->setChecked(false);
    tensorType = TensorDiagonal;

    //ui->T11->setText(QString("1.0"));
    ui->T12->setText(QString("0.0"));
    ui->T12->setEnabled(false);
    ui->T13->setText(QString("0.0"));
    ui->T13->setEnabled(false);

    ui->T21->setText(QString("0.0"));
    ui->T21->setEnabled(false);
    //ui->T22->setText(QString("0.0"));
    ui->T23->setText(QString("0.0"));
    ui->T23->setEnabled(false);

    ui->T31->setText(QString("0.0"));
    ui->T31->setEnabled(false);
    ui->T32->setText(QString("0.0"));
    ui->T32->setEnabled(false);
    //ui->T33->setText(QString("0.0"));

    ui->T11->setReadOnly(false);
    ui->T22->setReadOnly(false);
    ui->T33->setReadOnly(false);

    ui->T11->show();
    ui->T22->show();
    ui->T33->show();
    ui->T11b->hide();
    ui->T22b->hide();
    ui->T33b->hide();
}

void TensorInput::on_tensorFull_clicked(bool checked)
{
    ui->tensorMinimal->setChecked(false);
    ui->tensorDiagonal->setChecked(false);
    ui->tensorFull->setChecked(true);
    tensorType = TensorFull;

    ui->T12->setEnabled(true);
    ui->T13->setEnabled(true);

    ui->T21->setEnabled(true);
    ui->T23->setEnabled(true);

    ui->T31->setEnabled(true);
    ui->T32->setEnabled(true);


    ui->T11->setReadOnly(false);
    ui->T22->setReadOnly(false);
    ui->T33->setReadOnly(false);

    ui->T11->show();
    ui->T22->show();
    ui->T33->show();
    ui->T11b->hide();
    ui->T22b->hide();
    ui->T33b->hide();
}

void TensorInput::on_T11_cursorPositionChanged(int arg1, int arg2)
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("1.0"));
        ui->T22->setText(QString("0.0"));
        ui->T33->setText(QString("0.0"));
    }
}

void TensorInput::on_T11_editingFinished()
{

}

void TensorInput::on_T12_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T12_editingFinished()
{

}

void TensorInput::on_T13_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T13_editingFinished()
{

}

void TensorInput::on_T21_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T21_editingFinished()
{

}

void TensorInput::on_T22_cursorPositionChanged(int arg1, int arg2)
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("0.0"));
        ui->T22->setText(QString("1.0"));
        ui->T33->setText(QString("0.0"));
    }
}

void TensorInput::on_T22_editingFinished()
{

}

void TensorInput::on_T23_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T23_editingFinished()
{

}

void TensorInput::on_T31_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T31_editingFinished()
{

}

void TensorInput::on_T32_cursorPositionChanged(int arg1, int arg2)
{

}

void TensorInput::on_T32_editingFinished()
{

}

void TensorInput::on_T33_cursorPositionChanged(int arg1, int arg2)
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("0.0"));
        ui->T22->setText(QString("0.0"));
        ui->T33->setText(QString("1.0"));
    }
}

void TensorInput::on_T33_editingFinished()
{

}

void TensorInput::on_buttonBox_accepted()
{
    collectData();
}

void TensorInput::on_buttonBox_rejected()
{

}

void TensorInput::collectData() {
    data.resize(9);
    data[0*3+0] = ui->T11->text().toFloat();
    data[0*3+1] = ui->T12->text().toFloat();
    data[0*3+2] = ui->T13->text().toFloat();

    data[1*3+0] = ui->T21->text().toFloat();
    data[1*3+1] = ui->T22->text().toFloat();
    data[1*3+2] = ui->T23->text().toFloat();

    data[2*3+0] = ui->T31->text().toFloat();
    data[2*3+1] = ui->T32->text().toFloat();
    data[2*3+2] = ui->T33->text().toFloat();

    dir.resize(3);
    dir[0] = ui->D1->text().toFloat();
    dir[1] = ui->D1->text().toFloat();
    dir[2] = ui->D1->text().toFloat();
}


void TensorInput::on_T33b_clicked()
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("0.0"));
        ui->T22->setText(QString("0.0"));
        ui->T33->setText(QString("1.0"));
        ui->T11b->setText(QString("0.0"));
        ui->T22b->setText(QString("0.0"));
        ui->T33b->setText(QString("1.0"));
    }
}

void TensorInput::on_T22b_clicked()
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("0.0"));
        ui->T22->setText(QString("1.0"));
        ui->T33->setText(QString("0.0"));
        ui->T11b->setText(QString("0.0"));
        ui->T22b->setText(QString("1.0"));
        ui->T33b->setText(QString("0.0"));
    }
}

void TensorInput::on_T11b_clicked()
{
    if (tensorType == TensorMinimal) {
        ui->T11->setText(QString("1.0"));
        ui->T22->setText(QString("0.0"));
        ui->T33->setText(QString("0.0"));
        ui->T11b->setText(QString("1.0"));
        ui->T22b->setText(QString("0.0"));
        ui->T33b->setText(QString("0.0"));
    }
}
