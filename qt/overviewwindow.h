#ifndef OVERVIEWWINDOW_H
#define OVERVIEWWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QRect>

namespace Ui {
class OverviewWindow;
}

class MainWindow;

class OverviewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OverviewWindow(MainWindow *main, QWidget *parent = 0);
    ~OverviewWindow();

    void setBackground(const QImage &bg_mesh);
    void setImage(const QRectF & view);

private slots:
    void on_actionPan_Left_triggered();

    void on_actionPan_Right_triggered();

    void on_actionPan_Up_triggered();

    void on_actionPan_Down_triggered();

    void on_actionClose_triggered();

private:
    Ui::OverviewWindow *ui;
    MainWindow *main_window;
    QImage bg_mesh;
    QImage mesh;

    void mouseReleaseEvent(QMouseEvent *releaseEvent);
    void closeEvent(QCloseEvent * event);
    // void hideEvent(QHideEvent * event);
};

#endif // OVERVIEWWINDOW_H
