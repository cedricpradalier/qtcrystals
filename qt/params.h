#ifndef PARAMS_H
#define PARAMS_H

#include <QDialog>
#include "crystals/EBSD.h"

namespace Ui {
class Params;
}

class MainWindow;

class Params : public QDialog
{
    Q_OBJECT

public:
    explicit Params(crystals::EBSDFileType file_type,
            const std::string & db_src, QWidget *parent = 0);
    ~Params();

    bool hasBeenConfirmed() const {
        return applyConfirmed;
    }

    QString getSelectedMaterial() const; 
    std::string getDBFile() const; 
    bool isAngleUnitRadian() const;
    crystals::EBSDFileType getFileType() const;

private slots:

    void on_button_apply_clicked();

    void on_button_cancel_clicked();

    void on_materialCombo_currentTextChanged(const QString &arg1);

    void on_databaseButton_clicked();

    void on_fileTypeCombo_currentTextChanged(const QString &arg1);

private:
    Ui::Params *ui;
    bool applyConfirmed;
    void closeEvent(QCloseEvent * event);
};

#endif // PARAMS_H
