#include <math.h>
#include <stdio.h>     /* for printf */
#include <stdlib.h>    /* for exit */
#ifdef USE_GETOPT
#include <getopt.h>
#endif
#include <time.h>
#include <string.h>
#include <ctype.h>

#include <QtGui>
#include <QApplication>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QByteArray>
#include <QtConcurrent>
#include <QMutex>

#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include "crystals/log.h"
#include "mainwindow.h"
#include "params.h"
#include "tensorinput.h"
#include "ui_mainwindow.h"

#ifdef TEST_QWT
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_interval.h>
#include <qwt_plot_histogram.h>
#include <qwt_plot_renderer.h>
#endif


using namespace crystals;

QMutex console_mutex;
bool verbose = false;
static std::list<QString> lines_to_display;
static QString console;
static Console *consolew = NULL;
static MainWindow *mainw = NULL;
void QtLoggerFunction(LogType t, const char*format,va_list ap) {
    QString out;
    switch (t) {
        case LogError: out += "E: "; break;
        case LogInfo: out += "I: "; break;
        default : break;
    }
    QString s;
    out += s.vsprintf(format,ap);
    if (verbose || (t == LogError)) {
        fprintf(stderr,"%s\n",out.toLocal8Bit().constData());
    }
    console_mutex.lock();
    console += out + "\n";
#if 0
    if (mainw) {
#if not defined(WIN32) and not defined(WIN64)
        mainw->set_status_bar(s);
#endif
    }
    if (consolew) {
        consolew->appendText(out);
    }
#else
    lines_to_display.push_back(out);
#endif
    console_mutex.unlock();
}

void MainWindow::update_console() 
{
    console_mutex.lock();
    while (!lines_to_display.empty()) {
        if (mainw) {
#if not defined(WIN32) and not defined(WIN64)
            mainw->set_status_bar(lines_to_display.front());
#endif
        }
        if (consolew) {
            consolew->appendText(lines_to_display.front());
        }
        lines_to_display.pop_front();
    }
    console_mutex.unlock();
}


void MainWindow::set_status_bar(const QString & s) {
    ui->statusBar->showMessage(s);
}


#ifdef TEST_QWT
class PlotWindow : public QwtPlot
{
    protected:
        MainWindow *win;
    public:
        PlotWindow(MainWindow *w) : win(w) {}
        void closeEvent(QCloseEvent * event);
};
#endif




MainWindow::MainWindow(int argc, char *argv[], QWidget *parent) :
    QMainWindow(parent),
    analyzer(NULL),
    ui(new Ui::MainWindow),
    refine_grains(false), with_graph(EBSDAnalyzer::NO_GRAPH), 
    ignore_map_edge(true), draw_ids(false),
    overview(new OverviewWindow(this)),
    key(new KeyWindow(this)),
    timer(new QTimer(this)),
    console(new Console(this))
{
    mainw = this;
    loading_completed = false;
    //source_name = "source.txt";
    image_output = "microstructure";
    graph_output = "cgraph.txt";
    fullgraph_output = "graph.txt"; 
    sql_output = "grains";



    ui->setupUi(this);
    ProgressDialog = new QProgressDialog(this);
    ProgressDialog->setLabelText("Loading...");

#ifdef TEST_QWT
    plot = NULL;
#endif

    setLogger(QtLoggerFunction);

    connect(&this->FutureWatcher, SIGNAL(finished()), this, SLOT(slot_finished()));
    connect(&this->FutureWatcher, SIGNAL(finished()), this->ProgressDialog , SLOT(cancel()));
    connect(this->ProgressDialog, SIGNAL(canceled()), &this->FutureWatcher , SLOT(cancel()));
    connect(timer, SIGNAL(timeout()), this, SLOT(update_console()));
    timer->start(200);

    QList<QKeySequence> list[12];
    for (size_t i=0;i<10;i++) {
        list[(int)i].append(QKeySequence(Qt::Key_0 + (int)i));
        list[(int)i].append(QKeySequence(Qt::Key_0 + (int)i + Qt::ShiftModifier));
        list[(int)i].append(QKeySequence(Qt::Key_0 + (int)i + Qt::KeypadModifier));
    }
    list[10].append(QKeySequence(Qt::Key_Z));
    list[10].append(QKeySequence(Qt::Key_Plus));
    list[10].append(QKeySequence(Qt::Key_Plus + Qt::ShiftModifier));
    list[10].append(QKeySequence(Qt::Key_Plus + Qt::KeypadModifier));
    list[11].append(QKeySequence(Qt::Key_Minus));
    list[11].append(QKeySequence(Qt::Key_Minus + Qt::ShiftModifier));
    list[11].append(QKeySequence(Qt::Key_Minus + Qt::KeypadModifier));

    ui->actionRaw_Grains->setShortcuts(list[1]);
    ui->actionTwinning_Editor->setShortcuts(list[2]);
    ui->actionGrain_neighbours->setShortcuts(list[3]);
    ui->actionClusters->setShortcuts(list[4]);
    ui->actionTwinning_statistics->setShortcuts(list[5]);
    ui->actionConvex_hulls->setShortcuts(list[6]);
    ui->actionEllipses->setShortcuts(list[7]);
    ui->actionConnected_twins->setShortcuts(list[8]);
    ui->actionTwin_joints->setShortcuts(list[9]);
    ui->actionReset_zoom->setShortcuts(list[0]);
    ui->actionZoom_in->setShortcuts(list[10]);
    ui->actionZoom_out->setShortcuts(list[11]);

    viewMode = new QActionGroup(this);
    viewMode->addAction(ui->actionRaw_Grains);
    viewMode->addAction(ui->actionTwinning_Editor);
    viewMode->addAction(ui->actionGrain_neighbours);
    viewMode->addAction(ui->actionClusters);
    viewMode->addAction(ui->actionTwinning_statistics);
    viewMode->addAction(ui->actionConvex_hulls);
    viewMode->addAction(ui->actionEllipses);
    viewMode->addAction(ui->actionConnected_twins);
    viewMode->addAction(ui->actionTwin_joints);

    colorMode = new QActionGroup(this);
    colorMode->addAction(ui->actionEuler);
    colorMode->addAction(ui->actionFull_Gamet);
    colorMode->addAction(ui->actionStereo_100);
    colorMode->addAction(ui->actionStereo_010);
    colorMode->addAction(ui->actionStereo_001);
    colorMode->addAction(ui->actionEBSD_Image_Quality);
    colorMode->addAction(ui->actionEBSD_Confidence);
    colorMode->addAction(ui->actionEBSD_Fitness);

    ui->actionLoad_SQLite_DB->setEnabled(false);
    ui->actionWrite_full_DB->setEnabled(false);
    ui->actionWrite_DB->setEnabled(false);
    ui->actionSave_image->setEnabled(false);

    ui->actionDraw_ids->setChecked(draw_ids);
    ui->actionAlways_color_twinning_links->setChecked(true);
    ui->actionView_border_grains->setChecked(ignore_map_edge);
    ui->actionTwinning_Editor->setChecked(true);
    ui->actionFull_Gamet->setChecked(true);
    ui->actionReplace_twin_strips->setChecked(false);
    ui->actionHighlight_twin_joints->setChecked(true);
    ui->actionHighlight_grain_joints->setChecked(true);

    file_type = crystals::FileTypeStandard;
    reload = false;
    num_threads = boost::thread::hardware_concurrency();
 #ifdef USE_GETOPT
    int c;
    static struct option long_options[] = {
        {"num-threads",  required_argument, 0,  'n' },
        {"help",  no_argument, 0,  'h' },
        {"verbose",  no_argument, 0,  'v' },
        {"sigma", required_argument, 0,  's' },
        {"min-weight", required_argument, 0,  'w' },
        {"material", required_argument, 0,  'M' },
        {"min-size", required_argument, 0,  'm' },
        {"min-embedding", required_argument, 0,  'e' },
        {"reload", required_argument, 0,  'R' },
        {"twinning-gamma", required_argument, 0,  'g' },
        {"twinning-tol", required_argument, 0,  't' },
        {"min-joint-length", required_argument, 0,  'j' },
        {0,         0,                 0,  0 }
    };
    while (1) {
        int option_index = 0;
        double tmp; int itmp;
        c = getopt_long(argc, argv, "R:cn:s:w:m:M:e:g:t:j:hv",
                long_options, &option_index);
        if (c == -1)
            break;
        switch (c) {
            case 'n':
                itmp = 0;
                sscanf(optarg,"%d",&itmp);
                if (itmp > 0) {
                    num_threads = itmp;
                }
                break;
            case 'm':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    MinComponentSize = (int)round(tmp);
                    LOG_INFO("Min component size: %.1f",MinComponentSize);
                }
                break;
            case 'M':
                if (strcmp(optarg,"Mg")==0) {
                    material = "Magnesium";
                    MaterialType = MAT_MAGNESIUM;
                    LOG_INFO("Selecting material: Magnesium");
                }
                if (strcmp(optarg,"Zr")==0) {
                    material = "Zirconium";
                    MaterialType = MAT_ZIRCONIUM;
                    LOG_INFO("Selecting material: Zirconium");
                }
                if (strcmp(optarg,"Mr")==0) {
                    material = "Martensite";
                    MaterialType = MAT_MARTENSITE;
                    LOG_INFO("Selecting material: Martensite");
                }
                break;
            case 'R':
                reload = true;
                sql_input = optarg;
                LOG_INFO("Reloading file %s",sql_input.c_str());
                break;
            case 'e':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    SubGrainMinEnclosure = tmp;
                    LOG_INFO("Min grain enclosure: %.2f",SubGrainMinEnclosure);
                }
                break;
            case 's':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    LOG_INFO("Sigma: %.2f deg",tmp);
                    tmp *= M_PI / 180.;
                    EdgeTolerance = tmp;
                    SigmaSqr = tmp*tmp;
                }
                break;
            case 'w':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    MinWeight = tmp;
                    LOG_INFO("Minimum Weight: %.2f",MinWeight);
                }
                break;
            case 'g':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    switch (MaterialType) {
                        case MAT_MAGNESIUM:
                            TwinningGammaMg = tmp;
                            LOG_INFO("Twinning gamma Mg: %.2f",TwinningGammaMg);
                            break;
                        case MAT_ZIRCONIUM:
                            TwinningGammaZr = tmp;
                            LOG_INFO("Twinning gamma Zr: %.2f",TwinningGammaZr);
                            break;
                        case MAT_MARTENSITE:
                            TwinningGammaMr = tmp;
                            LOG_INFO("Twinning gamma Mr: %.2f",TwinningGammaMr);
                            break;
                        default:
                            LOG_ERROR("Cannot set twinning gamma without specifying material");
                            break;
                    }
                }
                break;
            case 't':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    TwinningTypeTolerance = tmp * M_PI/180.;
                    LOG_INFO("Twinning type tolerance: %.2f deg",tmp);
                }
                break;
            case 'j':
                if (sscanf(optarg,"%le",&tmp)==1) {
                    MinJointWeight = (int)round(tmp);
                    LOG_INFO("Minimum Joint Weight: %d",(int)MinJointWeight);
                }
                break;
            case 'v':
                verbose = true;
                LOG_INFO("Activating verbose mode");
                break;
            case 'h':
            default:
                printf("Unknown option '%c'\n",c);
                print_help();
                break;
        }
    }
    source_name.clear();
    if (optind < argc) {
        source_name = argv[optind++];
        if (optind < argc) {
            printf("Too many arguments. Last argument should be the source file name\n");
            print_help();
        }
    }
#endif
    LOG_INFO("Num threads: %d",(int)num_threads);
    key->updateKey(MaterialType,with_graph);
}

MainWindow::~MainWindow()
{
    ::consolew = NULL;
    mainw = NULL;
    setLogger(NULL);
    delete ProgressDialog;
    delete timer;
    delete console;
    delete overview;
    delete key;
    delete ui;
    delete analyzer;
}


void MainWindow::print_help() {
    printf("EBSD Analyzer. Usage:\n"
            "\tloadgraph [options] source_file.txt\n\nOptions:\n"
        "\t-n,--num-threads <int>        : Number of threads to use.\n"
        "\t-R,--reload <filename>        : Reload settings from SQL db.\n"
        "\t-s,--sigma <float>            : Uncertainty (deg) on disorientations.\n"
        "\t-w,--min-weight <float>       : Cut-off value for the likelihood of disorientations.\n"
        "\t-m,--min-size <int>           : Minimum size of a connected component to be considered.\n"
        "\t-e,--min-embedding <float>    : Minimum percentage to detected an embedded component.\n"
        "\t-g,--twinning-gamma <float>   : Mesh gamma (material specific).\n"
        "\t-t,--twinning-tol <float>     : Tolerance (deg) for the identification of twinning types.\n"
        "\t-j,--min-joint-length <int>   : Minimum length (measure points) of a grain joint.\n"
        "\t-v,--verbose                  : Activate verbose output.\n"
        "\t-h,--help                     : Display this message.\n"
    );
    exit(0);
}

void MainWindow::load_map_bg()
{

    // Start the computation.
    loading_completed = false;
    ::console = "";
    if (!source_name.empty()) {
        bool load_confirmed = false;
        file_type = crystals::FileTypeStandard;
        if (source_name.substr(source_name.size()-4,4)==".ctf") {
            file_type = crystals::FileTypeCTF;
        }
        if (source_name.substr(source_name.size()-4,4)==".cpr") {
            file_type = crystals::FileTypeCPR;
        }

        Params * params = new Params(file_type,sql_input,this);
        params->raise();
        params->exec();
        load_confirmed = params->hasBeenConfirmed(); 
        sql_input = params->getDBFile();
        material = params->getSelectedMaterial();
        angles_in_radian = params->isAngleUnitRadian();
        file_type = params->getFileType();
        delete params;
        if (!load_confirmed) {
            LOG_ERROR("Load has been cancelled");
            source_name.clear();
            return;
        }
        reload = !sql_input.empty(); 
#if 0
        QStringList items;
        items << "Magnesium" << "Zirconium" << "Martensite";

        bool ok;
        QString item = QInputDialog::getItem(NULL, "Select Material",
                "Material:", items, 0, false, &ok);
        if (ok && !item.isEmpty()) {
            material = item;
            if (item == "Magnesium") {
                MaterialType = MAT_MAGNESIUM;
            } else if (item == "Zirconium") {
                MaterialType = MAT_ZIRCONIUM;
            } else if (item == "Martensite") {
                MaterialType = MAT_MARTENSITE;
            }
        } else {
            LOG_ERROR("Undefined material");
            return;
        }
#endif
    }
    QFuture<void> future_load = QtConcurrent::run(this, &MainWindow::load_map);
    this->FutureWatcher.setFuture(future_load);

    this->ProgressDialog->setLabelText(QString("Loading ") + source_name.c_str());
    this->ProgressDialog->setMinimum(0);
    this->ProgressDialog->setMaximum(0);
    this->ProgressDialog->setCancelButton(0);
    this->ProgressDialog->setWindowModality(Qt::WindowModal);
    this->ProgressDialog->exec();
    loading_completed = true;
    setWindowTitle(QString("QT EBSD Map Editor: ") + source_name.c_str() + " (" + material + ")");
    if (analyzer) {
        analyzer->set_mesh_size(ui->label->width()-2,ui->label->height()-2);
        analyzer->reset_zoom();
        analyzer->force_repaint();
        overview->setBackground(analyzer->get_image());
        setImage();
    }
    // qApp->processEvents();

    QFuture<void> future_process = QtConcurrent::run(this, &MainWindow::process_map);
    this->FutureWatcher.setFuture(future_process);
    this->ProgressDialog->exec();

    key->updateKey(MaterialType,with_graph);
    ui->actionLoad_SQLite_DB->setEnabled(true);
    ui->actionWrite_full_DB->setEnabled(true);
    ui->actionWrite_DB->setEnabled(true);
    ui->actionSave_image->setEnabled(true);
    ui->statusBar->showMessage(QString("Successfully loaded '")+source_name.c_str()+"'");
    if (analyzer) {
        analyzer->set_mesh_size(ui->label->width()-2,ui->label->height()-2);
        analyzer->reset_zoom();
        overview->setImage(analyzer->get_view_ratio());
    }
    setImage();
}


void MainWindow::slot_finished() 
{
    if (!analyzer || !loading_completed) return;
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

bool MainWindow::load_map()
{
    if (source_name.empty()) {
        return true;
    }
    // bool redraw = true;
    // double overview_ratio = 2.0;

    sql_output = "grains";
    QFileInfo fi(source_name.c_str());
    std::string bname = fi.baseName().toLocal8Bit().constData();
    for (size_t i=0;i<bname.size();++i) {
        if (!isalnum(bname[i])) {
            bname[i]='_';
        }
    }
    sql_output = sql_output+"-"+bname+".sql";

    delete analyzer;
    analyzer = new EBSDAnalyzer(num_threads, 500, 500);

#if 0
    // defined in load_map_bg
    file_type = crystals::FileTypeStandard;
    if (source_name.substr(source_name.size()-4,4)==".ctf") {
        file_type = crystals::FileTypeCTF;
    }
    if (source_name.substr(source_name.size()-4,4)==".cpr") {
        file_type = crystals::FileTypeCPR;
    }
#endif

    if (!analyzer->parse_file(source_name,file_type,angles_in_radian)) {
        LOG_ERROR("Can't open file '%s'",source_name.c_str());
        ui->statusBar->showMessage(QString("Can't open file '")+source_name.c_str()+"'");
        delete analyzer;
        analyzer = NULL;
        return false;
    }

    ignore_map_edge = true;
    with_graph = EBSDAnalyzer::NO_GRAPH;
    // analyzer->set_mesh_size(500,500,true);
    // analyzer->force_repaint();
    // analyzer->update_image(with_graph,ignore_map_edge,draw_ids);
    // overview->setBackground(analyzer->get_image());

    draw_ids = false;
    ignore_map_edge = false;
    with_graph = EBSDAnalyzer::GRAPH_CONNECTED;

    ui->actionDraw_ids->setChecked(draw_ids);
    ui->actionView_border_grains->setChecked(ignore_map_edge);
    ui->actionTwinning_Editor->setChecked(true);
    // ui->actionFull_Gamet->setChecked(true);
    // ui->actionReplace_twin_strips->setChecked(false);

    if (reload) {
        QFileInfo fi(source_name.c_str());
        std::string fname = fi.absoluteFilePath().toLocal8Bit().constData();
        analyzer->load_settings_from_db(fname,sql_input);
    }
    return true;
}

bool MainWindow::process_map()
{
    // assumes load_map has been run
    if (!analyzer) return false;

    // At this stage we know the material for sure, update the twinning diso.
    initialise_twinning_disorientations();
    analyzer->build_connected_parts();
    analyzer->update_grain_properties();
    if (reload) {
        QFileInfo fi(source_name.c_str());
        std::string fname = fi.absoluteFilePath().toLocal8Bit().constData();
        analyzer->load_connected_settings_from_db(fname,sql_input);
    }
    analyzer->build_grain_groups(false);
    // analyzer->set_mesh_size(ui->label->width()-2,ui->label->height()-2);
    // analyzer->reset_zoom();
    // analyzer->force_repaint();
    // analyzer->update_image(with_graph,ignore_map_edge,draw_ids);
    return true;
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    if (!analyzer || !loading_completed) return;
    analyzer->set_mesh_size(ui->label->width()-2,ui->label->height()-2);
    analyzer->reset_zoom();
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::setImage()
{
    if (!analyzer || !loading_completed) return;
    analyzer->update_image(with_graph,ignore_map_edge,draw_ids);
    ui->label->setPixmap(QPixmap::fromImage(analyzer->get_image()));
    // ui->label->repaint();
}

void MainWindow::pan_to(float x, float y)
{
    if (!analyzer || !loading_completed) return;
    analyzer->pan_to(x*analyzer->get_mesh_width(),y*analyzer->get_mesh_height());
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionRaw_Grains_triggered()
{
    with_graph = EBSDAnalyzer::NO_GRAPH;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying no graph. In this mode, clicking on pixel displays their value.");
    setImage();
}

void MainWindow::on_actionTwinning_Editor_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_CONNECTED;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying connected graph. In this mode, clicking on edges enable/disable them.");
    setImage();
}

void MainWindow::on_actionTwin_joints_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_GRAIN_JOINTS;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying grain and twin joints. No mouse effects.");
    setImage();
}

void MainWindow::on_actionGrain_neighbours_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_GRAINS;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying grain graph. In this mode, clicking on connected parts make them parent.");
    setImage();
}

void MainWindow::on_actionClusters_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_CLUSTERS;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying cluster graph. No mouse effects.");
    setImage();
}

void MainWindow::on_actionTwinning_statistics_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_TWINNING;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying twinning graph. In this mode, clicking on an edge display it's statistics.");
    setImage();
}

void MainWindow::on_actionConvex_hulls_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_HULLS;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying convex hulls. No mouse effects.");
    setImage();
}

void MainWindow::on_actionEllipses_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_ELLIPSES;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying ellipses. Click a grain/twin displays its statistics.");
    setImage();
}

void MainWindow::on_actionConnected_twins_triggered()
{
    with_graph = EBSDAnalyzer::GRAPH_CONNECTED_TWINS;
    key->updateKey(MaterialType,with_graph);
    ui->statusBar->showMessage("Displaying connected twins. Mouse marks the connections.");
    setImage();
}

void MainWindow::on_actionDraw_ids_triggered(bool checked)
{
    draw_ids = checked;
    setImage();
}

void MainWindow::on_actionView_border_grains_triggered(bool checked)
{
    ignore_map_edge = checked;
    setImage();
}


void MainWindow::on_actionEuler_triggered()
{
    EBSDColorSet::setMode(EBSDCM_EULER);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionFull_Gamet_triggered()
{
    EBSDColorSet::setMode(EBSDCM_FULL);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionStereo_001_triggered()
{
    EBSDColorSet::setMode(EBSDCM_STEREO_001);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionStereo_010_triggered()
{
    EBSDColorSet::setMode(EBSDCM_STEREO_010);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionStereo_100_triggered()
{
    EBSDColorSet::setMode(EBSDCM_STEREO_100);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionEBSD_Image_Quality_triggered()
{
    EBSDColorSet::setMode(EBSDCM_IMAGE_QUALITY);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionEBSD_Confidence_triggered()
{
    EBSDColorSet::setMode(EBSDCM_CONFIDENCE);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}

void MainWindow::on_actionEBSD_Fitness_triggered()
{
    EBSDColorSet::setMode(EBSDCM_FIT);
    if (!analyzer || !loading_completed) return;
    analyzer->force_repaint();
    setImage();
}
void MainWindow::on_actionReset_zoom_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->reset_zoom();
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionFind_object_triggered()
{
    if (!analyzer || !loading_completed) return;
    bool ok = false;
    int id = QInputDialog::getInt(this, tr( "Select object to find..." ),
            tr( "Object Id:" ), -1, -1, 1000000, 1, &ok);
    if ( !ok || (id<0) )
        return;
    if (analyzer->pan_to_object(id)) {
        overview->setImage(analyzer->get_view_ratio());
        setImage();
    }
}

void MainWindow::write_db()
{
    if (!analyzer || !loading_completed) return;
    QFileInfo fi(source_name.c_str());
    std::string fname = fi.absoluteFilePath().toLocal8Bit().constData();
    // analyzer->save_borders();
    // analyzer->save_fullgraph(fullgraph_output);
    // analyzer->save_grain_graph(graph_output);
    analyzer->save_to_db(fname,false,sql_output.c_str(),sql_update);
    LOG_INFO("Saved DB to '%s'",sql_output.c_str());
}

void MainWindow::write_full_db()
{
    if (!analyzer || !loading_completed) return;
    LOG_INFO("Saving with all the vertices. This may take some time...");
    QFileInfo fi(source_name.c_str());
    std::string fname = fi.absoluteFilePath().toLocal8Bit().constData();
    analyzer->save_to_db(fname,true,sql_output.c_str(),sql_update);
    LOG_INFO("Saved Full DB to '%s'",sql_output.c_str());
}

void MainWindow::on_actionWrite_DB_triggered()
{
    QString home(getenv("CRYSTALS_HOME"));
    QString fileName;
    if (!home.isEmpty()) {
        fileName = home + "/code_stats/grainmap.sqlite";
    } else {
        fileName = QFileDialog::getSaveFileName(this, tr("Save DB"),
                QString(sql_output.c_str()), tr("Sqlite DB (*.sql  *.sqlite)"),
                0, QFileDialog::DontConfirmOverwrite);
    }
    if (!fileName.isEmpty()) {
        sql_output = fileName.toLocal8Bit().constData();
        sql_update = false;
        if (boost::filesystem::exists( sql_output ) ) {
            QMessageBox msgBox;
            msgBox.setText("This file already exists.");
            msgBox.setInformativeText("Do you want to append the data or overwrite the file?");
            QPushButton * replace = msgBox.addButton("Overwrite", QMessageBox::ResetRole);
            QPushButton * update = msgBox.addButton("Append", QMessageBox::AcceptRole);
            msgBox.addButton(QMessageBox::Cancel);
            msgBox.setDefaultButton(update);
            msgBox.exec();
            if (msgBox.clickedButton() == (QAbstractButton*)replace) {
                sql_update = false;
            } else if (msgBox.clickedButton() ==(QAbstractButton*)update) {
                sql_update = true; 
            } else {
                return;
            }
        }

        QFuture<void> future = QtConcurrent::run(this, &MainWindow::write_db);
        this->FutureWatcher.setFuture(future);

        this->ProgressDialog->setLabelText(QString("Saving to DB ") + sql_output.c_str());
        this->ProgressDialog->setMinimum(0);
        this->ProgressDialog->setMaximum(0);
        this->ProgressDialog->setCancelButton(0);
        this->ProgressDialog->setWindowModality(Qt::WindowModal);
        this->ProgressDialog->exec();
    }
}

void MainWindow::on_actionWrite_full_DB_triggered()
{
    QString home(getenv("CRYSTALS_HOME"));
    QString fileName;
    if (!home.isEmpty()) {
        fileName = home + "/code_stats/grainmap.sqlite";
    } else {
        fileName = QFileDialog::getSaveFileName(this, tr("Save DB"),
                QString(sql_output.c_str()), tr("Sqlite DB (*.sql  *.sqlite)"));
    }
    if (!fileName.isEmpty()) {
        sql_output = fileName.toLocal8Bit().constData();

        QFuture<void> future = QtConcurrent::run(this, &MainWindow::write_full_db);
        this->FutureWatcher.setFuture(future);

        this->ProgressDialog->setLabelText(QString("Saving everything to DB ") + sql_output.c_str());
        this->ProgressDialog->setMinimum(0);
        this->ProgressDialog->setMaximum(0);
        this->ProgressDialog->setCancelButton(0);
        this->ProgressDialog->setWindowModality(Qt::WindowModal);
        this->ProgressDialog->exec();
    }
}

void MainWindow::on_actionSave_image_triggered()
{
    if (!analyzer || !loading_completed) return;
    QDateTime t = QDateTime::currentDateTime();
    std::string datestr = t.toString("yyyyMMddhhmmss").toLatin1().constData();
    std::string im_name = image_output+"-"+datestr+".png";
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image"),
            QString(im_name.c_str()), tr("Image (*.png)"));
    if (!fileName.isEmpty()) {
        analyzer->get_image().save(fileName,"PNG");
        LOG_INFO("Saved image file \"%s\"",fileName.toLocal8Bit().constData());
    }
}

void MainWindow::on_actionReplace_twin_strips_triggered(bool checked)
{
    if (!analyzer || !loading_completed) return;
    analyzer->set_twinstrip_mode(checked);
    setImage();
}

void MainWindow::on_actionRefine_using_convexity_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->build_grain_groups(true);
    setImage();
}

void MainWindow::on_actionZoom_in_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->zoom(3./2.);
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionZoom_out_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->zoom(2./3.);
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}


void MainWindow::on_actionPan_Left_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->translate(-0.20*width(),0);
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionPan_Right_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->translate(+0.20*width(),0);
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionPan_Up_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->translate(0,-0.20*width());
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::on_actionPan_Down_triggered()
{
    if (!analyzer || !loading_completed) return;
    analyzer->translate(0,+0.20*width());
    overview->setImage(analyzer->get_view_ratio());
    setImage();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *releaseEvent)
{
    if (!analyzer || !loading_completed) return;
    QPoint pos = ui->label->mapFrom(this,releaseEvent->pos());
    int x_org = (int)(ui->label->width()-analyzer->get_mesh_width())/2;
    int y_org = (int)(ui->label->height()-analyzer->get_mesh_height())/2;
    // printf("Mouse event: (%d,%d) -> (%d,%d)\n",releaseEvent->x(),releaseEvent->y(), pos.x(),pos.y());
    if (releaseEvent->button() == Qt::RightButton) {
        analyzer->zoom_to(pos.x()-x_org,pos.y()-y_org,1.5);
        overview->setImage(analyzer->get_view_ratio());
        setImage();
        return;
    }
    if (releaseEvent->button() == Qt::LeftButton) {
        // printf("Mouse up %d %d\n",x,y);
        std::string output;
        if (analyzer->handle_edge_click(pos.x()-x_org,pos.y()-y_org,output)) {
            analyzer->build_grain_groups(false);
            setImage();
            return;
        } else if (!output.empty()) {
            QMessageBox msgBox(this);
            msgBox.setWindowFlags(msgBox.windowFlags()|Qt::WindowStaysOnTopHint);
            msgBox.setWindowModality(Qt::ApplicationModal);
            msgBox.setModal(true);
            QString s(output.c_str());
            s.replace("\t","    ");
            msgBox.setText(QString("<b><pre>")+s+QString("</pre></b>"));
            msgBox.exec();
        }
    }
}


void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this,"About QTCrystals",
                       "EBSD Map Editor & Analyzer\n\n"
                       "Written by Cedric Pradalier\n"
                       "With contributions from:\n"
                       "    - Pierre-Alexandre Juan\n"
                       "    - Laurent Capolongo\n\n"
                       "Copyright GTL / CNRS UMI 2958 - 2014"
                       );
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QMessageBox::aboutQt(this,"About QTCrystals");
}

void MainWindow::on_actionMap_overview_triggered(bool checked)
{
    if (checked) {
        overview->show();
        overview->raise();
    } else {
        overview->hide();
    }
}

void MainWindow::on_actionQuit_triggered()
{
#if 0
    if (QMessageBox::No == QMessageBox::question(this, "Close Confirmation?",
                "Are you sure you want to quit?", 
                QMessageBox::Yes|QMessageBox::No))
    {
        return;
    }
#endif
#ifdef TEST_QWT
    if (plot) {
        recordPlotClosure();
    }
#endif
    overview->close();
    key->close();
    close();
}

void MainWindow::recordOverviewClosure()
{
    ui->actionMap_overview->setChecked(false);
}

void MainWindow::recordKeyClosure()
{
    ui->actionShow_key->setChecked(false);
}

void MainWindow::recordConsoleClosure()
{
    ::consolew = NULL;
    ui->actionShow_console->setChecked(false);
}

void MainWindow::closeEvent(QCloseEvent * event)
{

    event->ignore();	    
    if (QMessageBox::No == QMessageBox::question(this, "Close Confirmation?",
                "Are you sure you want to quit?", 
                QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes))
    {
        return;
    }

    event->accept();
    this->QMainWindow::closeEvent(event);
    overview->close();
    key->close();
    console->close();
}


void MainWindow::on_actionLoad_Map_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
            QString(), tr("EBSD Map (*.txt *.ctf *cpr)"));
    if (!fileName.isEmpty()) {
        QByteArray array = fileName.toLocal8Bit();
        source_name = array.constData();
        reload = false;
        loading_completed = false;
        MaterialType = MAT_UNDEF;
        load_map_bg();
    }
}

void MainWindow::on_actionLoad_SQLite_DB_triggered()
{
    // OBSOLETE
    if (!analyzer || !loading_completed) return;
    QString home(getenv("CRYSTALS_HOME"));
    QString fileName;
    if (!home.isEmpty()) {
        fileName = home + "/code_stats/grainmap.sqlite";
    } else {
        fileName = QFileDialog::getOpenFileName(this, tr("Open DB"),
                QString(), tr("SQLLite DB (*.sql *.sqlite)"));
    }
    if (!fileName.isEmpty()) {
        QByteArray array = fileName.toLocal8Bit();
        sql_input = array.constData();
        reload = true;
        ui->actionLoad_SQLite_DB->setEnabled(false);
        ui->actionWrite_full_DB->setEnabled(false);
        ui->actionWrite_DB->setEnabled(false);
        ui->actionSave_image->setEnabled(false);
        loading_completed = false;
        load_map_bg();
    }

}


void MainWindow::on_actionHighlight_grain_joints_triggered(bool checked)
{
    if (!analyzer || !loading_completed) return;
    analyzer->highlightGrainJoint(checked);
    setImage();
}

void MainWindow::on_actionHighlight_twin_joints_triggered(bool checked)
{
    if (!analyzer || !loading_completed) return;
    analyzer->highlightTwinJoint(checked);
    setImage();
}

void MainWindow::on_actionShow_console_triggered(bool checked)
{
    if (checked) {
        console_mutex.lock();
        console->setText(::console);
        console_mutex.unlock();
        console->setModal(true);
        ::consolew = console;
        console->exec();
    } else {
        console->hide();
        ::consolew = NULL;
    }
}

void MainWindow::on_actionArea_Histogram_triggered(bool checked)
{
    if (!analyzer || !loading_completed) return;
#ifdef TEST_QWT
    if (checked) {
        assert(!plot);
        plot = new PlotWindow(this);

        plot->setCanvasBackground(::QColor(Qt::white));
        plot->setTitle("Grain Area");

        QwtPlotGrid *grid = new QwtPlotGrid;
        grid->enableXMin(true);
        grid->enableYMin(true);
        grid->setMajorPen(QPen(Qt::black, 0, Qt::DotLine));
        grid->setMinorPen(QPen(Qt::gray, 0 , Qt::DotLine));
        grid->attach(plot);

        ::QColor color = Qt::cyan;
        color.setAlpha( 180 );
        QwtPlotHistogram *histogram = new QwtPlotHistogram();
        histogram->setPen( QPen( Qt::darkCyan ) );
        histogram->setBrush( QBrush( color ) );

        CountHistogram h = analyzer->getGrainAreaHistogram();
        QVector<QwtIntervalSample> samples((int)h.size());

        for ( size_t i = 0; i < h.size(); i++ )
        {
            samples[(int)i] = QwtIntervalSample(h[i].second,QwtInterval(h[i].first.first,h[i].first.second));
        }
        histogram->setData(new QwtIntervalSeriesData( samples ));
        histogram->attach(plot);

        plot->setAxisTitle(QwtPlot::yLeft, QString("Count"));
        plot->setAxisTitle(QwtPlot::xBottom, QString("Area (um^2)"));
        // plot->setAxisScale(QwtPlot::yLeft, 0.0, 100.0);
        // plot->setAxisScale(QwtPlot::xBottom, 0.0, pos);
        plot->replot();
        plot->resize(600,400);
        plot->show();
    } else {
        assert(plot);
        plot->hide();
        delete plot;
        plot = NULL;
    }
#endif
}

#ifdef TEST_QWT
void PlotWindow::closeEvent(QCloseEvent * event)
{
    this->QwtPlot::closeEvent(event);
    win->recordPlotClosure();
}
#endif

void MainWindow::recordPlotClosure() 
{
    ui->actionArea_Histogram->setChecked(false);
#ifdef TEST_QWT
    delete plot;
    plot = NULL;
#endif
}


void MainWindow::on_actionSave_Plot_triggered()
{
#ifdef TEST_QWT
    if (!plot) return;
    QwtPlotRenderer renderer;
    renderer.exportTo(plot,QString("graph.pdf"),QSizeF(300, 200),200);
#endif
}

void MainWindow::on_actionAlways_color_twinning_links_triggered(bool checked)
{
    if (!analyzer || !loading_completed) return;
    analyzer->alwaysColorTwinningLinks(checked);
    setImage();
}

void MainWindow::on_actionEnter_tensor_triggered()
{
    if (!analyzer || !loading_completed) return;
    TensorInput * ti = new TensorInput(analyzer->getTensorInput(),analyzer->getLoadingDir(),this);
    ti->raise();
    if (ti->exec()) {
        analyzer->setTensorInput(ti->data);
        analyzer->setLoadingDir(ti->dir);
        std::string home(getenv("CRYSTALS_HOME"));
        if (!home.empty()) {
            std::string filename = home+"/code_stats/tmp/data_proc.in";
            FILE *fp = fopen(filename.c_str(),"w");
            if (fp) {
                std::string mat_abbr("undef");
                switch (MaterialType) {
                    case MAT_MAGNESIUM: mat_abbr="Mg"; break;
                    case MAT_ZIRCONIUM: mat_abbr="Zr"; break;
                    case MAT_MARTENSITE: mat_abbr="Mr"; break;
                    case MAT_AUSTENITE: mat_abbr="Au"; break;
                    default: break;
                }
                fprintf(fp,"Material Information: %s\n"
                        "Loading paramaters:\n"
                        "%e   c/a gamma ratio\n"
                        "Stress tensor diagonal components used for Schmid factor (SF) computation\n"
                        "%e	sigma(1,1)\n"
                        "%e	sigma(2,2)\n"
                        "%e	sigma(3,3)\n"
                        "Loading direction vector also used for SF computation\n"
                        "%e	load_dir(1)\n"
                        "%e	load_dir(2)\n"
                        "%e	load_dir(3)\n"
                        "Plotting parameters:\n"
                        "4.	Min area x-value\n"
                        "1456	Max area x-value\n"
                        "2.26	Min diameter x-value\n"
                        "43.11	Max diameter x-value\n"
                        "The number of bars of histograms having area, diameter and SF as x-values have to be indicated in the 'data_proc.dim' dim file\n",
                        mat_abbr.c_str(),TwinningGamma,
                        ti->data[0],ti->data[4],ti->data[8],
                        ti->dir[0],ti->dir[1],ti->dir[2]);
                fclose(fp);
                int res = system((home+"/code_stats/script.sh").c_str());
                LOG_INFO("Executed post-processing script: %d",res);
            } else {
                LOG_ERROR("Could not write %s",filename.c_str());
            }
        }
    }
    delete ti;

}



void MainWindow::on_actionShow_key_triggered(bool checked)
{
    if (checked) {
        key->show();
        key->raise();
    } else {
        key->hide();
    }
}
