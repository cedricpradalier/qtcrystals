#include <stdio.h>
#include <QFileDialog>
#include "params.h"
#include "ui_params.h"
#include "crystals/constants.h"
#include "crystals/log.h"

using namespace crystals;

Params::Params(EBSDFileType file_type,
        const std::string & db_src, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Params)
{
    char tmp[1204];
    applyConfirmed = false;
    QStringList mt_items;
    // mt_items << "------" << "Magnesium" << "Zirconium" << "Martensite";
    mt_items << "------" << "Magnesium" << "Zirconium" << "HCP Generic";
    QStringList ft_items;
    ft_items << "Standard" << "CTF" << "CPR";
    ui->setupUi(this);
    ui->materialCombo->addItems(mt_items);
    ui->fileTypeCombo->addItems(ft_items);
    sprintf(tmp,"%.5f",TwinningGamma);
    ui->gammaEdit->setText(QString(tmp));
    ui->gammaEdit->setValidator( new QDoubleValidator(0, 100, 5, this) );
    sprintf(tmp,"%.2f",MinComponentSize);
    ui->minFragEdit->setText(QString(tmp));
    ui->minFragEdit->setValidator( new QDoubleValidator(0, 100, 2, this) );
    sprintf(tmp,"%.1f",EdgeTolerance*180./M_PI);
    ui->disoTolEdit->setText(QString(tmp));
    ui->disoTolEdit->setValidator( new QDoubleValidator(0, 100, 1, this) );
    sprintf(tmp,"%.1f",TwinningTypeTolerance*180./M_PI);
    ui->twinningTolEdit->setText(QString(tmp));
    ui->twinningTolEdit->setValidator( new QDoubleValidator(0, 100, 1, this) );
    switch (MaterialType) {
        case MAT_MAGNESIUM:
            ui->materialCombo->setCurrentIndex(1);
            break;
        case MAT_ZIRCONIUM:
            ui->materialCombo->setCurrentIndex(2);
            break;
        case MAT_MARTENSITE:
            ui->materialCombo->setCurrentIndex(3);
            break;
        default:
            ui->materialCombo->setCurrentIndex(0);
            break;
    };
    ui->databaseEdit->setText(db_src.c_str());
    switch (file_type) {
        case FileTypeCPR:
            ui->fileTypeCombo->setCurrentIndex(2);
            ui->angleUnit->setChecked(false);
            break;
        case FileTypeCTF:
            ui->fileTypeCombo->setCurrentIndex(1);
            ui->angleUnit->setChecked(false);
            break;
        case FileTypeStandard:
        default:
            ui->fileTypeCombo->setCurrentIndex(0);
            ui->angleUnit->setChecked(true);
            break;
    }
}

Params::~Params()
{
    delete ui;
}

std::string Params::getDBFile() const {
    QByteArray array = ui->databaseEdit->text().toLocal8Bit();
    return std::string(array.constData());
}

void Params::on_button_apply_clicked()
{
    QString selected = ui->materialCombo->currentText();
    if (selected == "Magnesium") {
        MaterialType = MAT_MAGNESIUM;
        TwinningGamma = TwinningGammaMg = ui->gammaEdit->text().toFloat();
        applyConfirmed = true;
    } else if ((selected == "Zirconium") || (selected == "HCP Generic")) {
        MaterialType = MAT_ZIRCONIUM;
        TwinningGamma = TwinningGammaZr = ui->gammaEdit->text().toFloat();
        applyConfirmed = true;
    } else if (selected == "Martensite") {
        MaterialType = MAT_MARTENSITE;
        TwinningGamma = TwinningGammaMr = ui->gammaEdit->text().toFloat();
        applyConfirmed = true;
    } else {
        applyConfirmed = false;
    }
    if (applyConfirmed) {
        MinComponentSize = ui->minFragEdit->text().toFloat();
        EdgeTolerance = ui->disoTolEdit->text().toFloat() * M_PI / 180.;
        TwinningTypeTolerance = ui->twinningTolEdit->text().toFloat() * M_PI / 180.;
        TrustExternalPhaseData = ui->trustPhaseData->isChecked();
    }
    hide();
}

void Params::on_button_cancel_clicked()
{
    applyConfirmed = false;
    hide();
}

void Params::on_materialCombo_currentTextChanged(const QString &selected)
{
    if (selected == "Magnesium") {
        MaterialType = MAT_MAGNESIUM;
        TwinningGamma = TwinningGammaMg;
        ui->button_apply->setEnabled(true);
        ui->button_apply->setDefault(true);
    } else if ((selected == "Zirconium") || (selected == "HCP Generic")) {
        MaterialType = MAT_ZIRCONIUM;
        TwinningGamma = TwinningGammaZr;
        ui->button_apply->setEnabled(true);
        ui->button_apply->setDefault(true);
    } else if (selected == "Martensite") {
        MaterialType = MAT_MARTENSITE;
        TwinningGamma = TwinningGammaMr;
        ui->button_apply->setEnabled(true);
        ui->button_apply->setDefault(true);
    } else {
        ui->button_apply->setEnabled(false);
        ui->button_cancel->setDefault(true);
    }
    char tmp[1024];
    sprintf(tmp,"%.5f",TwinningGamma);
    ui->gammaEdit->setText(QString(tmp));
}

QString Params::getSelectedMaterial() const {
    return ui->materialCombo->currentText();
}

bool Params::isAngleUnitRadian() const {
    return ui->angleUnit->isChecked();
}

void Params::closeEvent(QCloseEvent * event)
{
    this->QDialog::closeEvent(event);
    applyConfirmed = false;
}

crystals::EBSDFileType Params::getFileType() const
{
    QString selected = ui->fileTypeCombo->currentText();
    if (selected == "Standard") {
        return crystals::FileTypeStandard;
    } else if (selected == "CTF") {
        return crystals::FileTypeCTF;
    } else if (selected == "CPR") {
        return crystals::FileTypeCPR;
    }
    return crystals::FileTypeStandard;
}

void Params::on_databaseButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open DB"),
            QString(), tr("SQLLite DB (*.sql *.sqlite)"));
    if (!fileName.isEmpty()) {
        ui->databaseEdit->setText(fileName);
    }
}

void Params::on_fileTypeCombo_currentTextChanged(const QString &selected)
{
    if (selected == "Standard") {
        ui->angleUnit->setChecked(true);
    } else if (selected == "CTF") {
        ui->angleUnit->setChecked(false);
    } else if (selected == "CPR") {
        ui->angleUnit->setChecked(false);
    }

}
