#-------------------------------------------------
#
# Project created by QtCreator 2014-03-14T22:32:48
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtcrystals

win32|win64: SYSROOT="..\.."
win32|win64: CONFIG+=STATIC
win32|win64: INCLUDEPATH += ../include "$$SYSROOT\CGAL-4.5\include" "$$SYSROOT\boost_1_57_0" "$$SYSROOT\qwt-6.1.0\src" "$$SYSROOT\sqlite-3\include"
win32|win64:DEFINES += _CRT_SECURE_NO_WARNINGS
win32|win64:LFLAGS += /VERBOSE
win32|win64: ICON = icon.png

mac:TEMPLATE = app
mac:QMAKE_MAC_SDK = macosx10.9
mac|linux: INCLUDEPATH += ../include
mac|linux:DEFINES += USE_CGAL
mac:DEFINES += MACOS_APP
mac:CONFIG+=-lboost_system-mt=app_bundle
mac: ICON = icon.icns

linux:FLAGS += -frounding-math
#linux:FLAGS += -fp-model strict
CONFIG+=qwt
CONFIG+=release



#mac:include ( /usr/local/qwt-6.1.0/features/qwt.prf )
mac|linux:include (../qwt-6.1.0/qwt.prf)
mac|linux:INCLUDEPATH+=../qwt-6.1.0/src

#DESTDIR       = ../../qtcrystals/Plugins
SOURCES += main.cpp\
        mainwindow.cpp \
    overviewwindow.cpp \
    console.cpp \
    ../src/color_tools.cpp \
    ../src/Connected.cpp \
    ../src/ConnectedNeighbour.cpp \
    ../src/constants.cpp \
    ../src/EBSD_db.cpp \
    ../src/EBSD_display.cpp \
    ../src/EBSD.cpp \
    ../src/Edge.cpp \
    ../src/Face.cpp \
    ../src/Grain.cpp \
    ../src/Graph.cpp \
    ../src/Histogram.cpp \
    ../src/Joint.cpp \
    ../src/log.cpp \
    ../src/quaternion_tools.cpp \
    ../src/Vertex.cpp \
    ../src/Phase.cpp \
    params.cpp \
    tensorinput.cpp \
    keywindow.cpp


HEADERS  += mainwindow.h \
    overviewwindow.h \
    console.h \
    ../include/crystals/color_tools.h \
    ../include/crystals/Connected.h \
    ../include/crystals/ConnectedCluster.h \
    ../include/crystals/ConnectedNeighbour.h \
    ../include/crystals/ConnectedTwins.h \
    ../include/crystals/constants.h \
    ../include/crystals/convex_hull.h \
    ../include/crystals/EBSD.h \
    ../include/crystals/EBSDSurface.h \
    ../include/crystals/Edge.h \
    ../include/crystals/Face.h \
    ../include/crystals/Grain.h \
    ../include/crystals/Graph.h \
    ../include/crystals/Histogram.h \
    ../include/crystals/Joint.h \
    ../include/crystals/log.h \
    ../include/crystals/quaternion_tools.h \
    ../include/crystals/Vertex.h \
    ../include/LinearMath/Matrix3x3.h \
    ../include/LinearMath/MinMax.h \
    ../include/LinearMath/QuadWord.h \
    ../include/LinearMath/Quaternion.h \
    ../include/LinearMath/Scalar.h \
    ../include/LinearMath/Transform.h \
    ../include/LinearMath/Vector3.h \
    ../include/crystals/Phase.h \
    params.h \
    tensorinput.h \
    keywindow.h

FORMS    += mainwindow.ui overviewwindow.ui \
    console.ui \
    params.ui \
    tensorinput.ui \
    keywindow.ui

RESOURCES +=     qtcrystals.qrc
win32|win64: SOURCES += $$SYSROOT\sqlite-3\src/sqlite3.c
win32|win64: HEADERS += $$SYSROOT\sqlite-3\include/sqlite3.h
win32|win64: LIBPATH += $$SYSROOT\boost_1_57_0\lib64-msvc-12.0
win32|win64: LIBPATH += "$$SYSROOT\qwt-6.1.0\lib"
win32|win64: LIBS += "boost_thread-vc120-mt-1_57.lib"
win32|win64: LIBS += "boost_filesystem-vc120-mt-1_57.lib"
win32|win64: LIBS += "qwt.lib"
win32|win64: DEPENDPATH += "$$SYSROOT\boost_1_57_0"

unix: LIBS += -L/usr/local/lib  -L/usr/lib -L/opt/local/lib/ -lsqlite3 -lCGAL -lmpfr -lgmp -lboost_thread  -lboost_filesystem -lboost_system -lboost_date_time
unix: INCLUDEPATH += /usr/local/include
unix: DEPENDPATH += /usr/local/include
unix: INCLUDEPATH += /usr/include
unix: DEPENDPATH += /usr/include
unix: INCLUDEPATH += /opt/local/include
unix: DEPENDPATH += /opt/local/include

