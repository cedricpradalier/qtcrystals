#ifndef CONSOLE_H
#define CONSOLE_H

#include <QDialog>

namespace Ui {
class Console;
}

class MainWindow;

class Console : public QDialog
{
    Q_OBJECT

public:
    explicit Console(MainWindow *main, QWidget *parent = 0);
    ~Console();

    void setText(const QString & s);
    void appendText(const QString & s);
private slots:
    void on_closeButton_clicked();

private:
    Ui::Console *ui;
    MainWindow *main_window;
    void closeEvent(QCloseEvent * event);
};

#endif // CONSOLE_H
