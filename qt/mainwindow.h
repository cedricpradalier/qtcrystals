#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "crystals/quaternion_tools.h"
#include "crystals/color_tools.h"
#include "crystals/EBSD.h"
#include "overviewwindow.h"
#include "console.h"
#include "keywindow.h"
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QPlainTextEdit>
#include <QTimer>

//#define TEST_QWT
#ifdef TEST_QWT
#include <qwt_plot.h>
#endif

namespace Ui {
class MainWindow;
}

class QActionGroup;

#ifdef TEST_QWT
class PlotWindow;
#endif



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int argc, char *argv[], QWidget *parent = 0);
    ~MainWindow();

    void setImage();
    void pan_to(float x, float y);
    void recordOverviewClosure();
    void recordKeyClosure();
    void recordConsoleClosure();
    void recordPlotClosure();
    void prepare();

    void write_db();
    void write_full_db();
    void load_map_bg();
    bool process_map();
    bool load_map();
    void print_help();
    void set_status_bar(const QString & s);

private slots:
    void on_actionRaw_Grains_triggered();

    void on_actionTwinning_Editor_triggered();

    void on_actionDraw_ids_triggered(bool checked);

    void on_actionView_border_grains_triggered(bool checked);

    void on_actionTwin_joints_triggered();

    void on_actionEuler_triggered();

    void on_actionFull_Gamet_triggered();

    void on_actionStereo_001_triggered();

    void on_actionStereo_010_triggered();

    void on_actionStereo_100_triggered();

    void on_actionReset_zoom_triggered();

    void on_actionFind_object_triggered();

    void on_actionGrain_neighbours_triggered();

    void on_actionClusters_triggered();

    void on_actionTwinning_statistics_triggered();

    void on_actionConvex_hulls_triggered();

    void on_actionEllipses_triggered();

    void on_actionConnected_twins_triggered();

    void on_actionWrite_DB_triggered();

    void on_actionWrite_full_DB_triggered();

    void on_actionSave_image_triggered();

    void on_actionReplace_twin_strips_triggered(bool checked);

    void on_actionRefine_using_convexity_triggered();

    void on_actionZoom_in_triggered();

    void on_actionZoom_out_triggered();

    void on_actionAbout_triggered();

    void on_actionAbout_Qt_triggered();

    void on_actionQuit_triggered();

    void on_actionLoad_Map_triggered();

    void on_actionLoad_SQLite_DB_triggered();

    void on_actionEBSD_Image_Quality_triggered();

    void on_actionEBSD_Confidence_triggered();

    void on_actionEBSD_Fitness_triggered();

    void on_actionHighlight_grain_joints_triggered(bool checked);

    void on_actionHighlight_twin_joints_triggered(bool checked);

    void on_actionShow_console_triggered(bool checked);

    void on_actionArea_Histogram_triggered(bool checked);

    void on_actionSave_Plot_triggered();

    void on_actionAlways_color_twinning_links_triggered(bool checked);

    void on_actionEnter_tensor_triggered();

public slots:
    void on_actionMap_overview_triggered(bool checked);

    void on_actionShow_key_triggered(bool checked);

    void on_actionPan_Left_triggered();

    void on_actionPan_Right_triggered();

    void on_actionPan_Up_triggered();

    void on_actionPan_Down_triggered();

    void slot_finished();

    void update_console();


private:
    std::string source_name;
    bool sql_update;
    std::string sql_output;
    std::string sql_input;
    std::string fullgraph_output;
    std::string graph_output;
    std::string image_output;
    crystals::EBSDAnalyzer *analyzer;
    Ui::MainWindow *ui;
    QActionGroup *viewMode;
    QActionGroup *colorMode;
    bool refine_grains;
    crystals::EBSDAnalyzer::GraphModes with_graph;
    bool ignore_map_edge;
    bool draw_ids;
    crystals::EBSDFileType file_type;
    bool angles_in_radian;
    bool reload;
    bool loading_completed;
    size_t num_threads;
    QString material;
    OverviewWindow *overview;
    KeyWindow *key;

    QFutureWatcher<void> FutureWatcher;
    QProgressDialog* ProgressDialog;
    QTimer *timer;
    Console* console;
#ifdef TEST_QWT
    PlotWindow *plot;
#endif

    void resizeEvent(QResizeEvent* event);
    void mouseReleaseEvent(QMouseEvent *releaseEvent);
    void closeEvent(QCloseEvent * event);
};

#endif // MAINWINDOW_H
