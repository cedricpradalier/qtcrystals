#ifndef TENSORINPUT_H
#define TENSORINPUT_H

#include <QDialog>
#include <vector>

namespace Ui {
class TensorInput;
}

class TensorInput : public QDialog
{
    Q_OBJECT

public:
    explicit TensorInput(const std::vector<double> & tensor, const std::vector<double> & loading, QWidget *parent = 0);
    ~TensorInput();

    std::vector<double> data;
    std::vector<double> dir;


private slots:
    void on_tensorMinimal_clicked(bool checked);

    void on_tensorDiagonal_clicked(bool checked);

    void on_tensorFull_clicked(bool checked);

    void on_T11_cursorPositionChanged(int arg1, int arg2);

    void on_T11_editingFinished();

    void on_T12_cursorPositionChanged(int arg1, int arg2);

    void on_T12_editingFinished();

    void on_T13_cursorPositionChanged(int arg1, int arg2);

    void on_T13_editingFinished();

    void on_T21_cursorPositionChanged(int arg1, int arg2);

    void on_T21_editingFinished();

    void on_T22_cursorPositionChanged(int arg1, int arg2);

    void on_T22_editingFinished();

    void on_T23_cursorPositionChanged(int arg1, int arg2);

    void on_T23_editingFinished();

    void on_T31_cursorPositionChanged(int arg1, int arg2);

    void on_T31_editingFinished();

    void on_T32_cursorPositionChanged(int arg1, int arg2);

    void on_T32_editingFinished();

    void on_T33_cursorPositionChanged(int arg1, int arg2);

    void on_T33_editingFinished();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_T33b_clicked();

    void on_T22b_clicked();

    void on_T11b_clicked();

private:
    enum {
        TensorMinimal,
        TensorDiagonal,
        TensorFull
    } tensorType;

    void collectData();

    Ui::TensorInput *ui;
    QString toText(double x);
};

#endif // TENSORINPUT_H
