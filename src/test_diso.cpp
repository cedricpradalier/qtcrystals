#include <LinearMath/Quaternion.h>
#include <LinearMath/Vector3.h>
#include <LinearMath/Matrix3x3.h>
#include <math.h>

#include "crystals/constants.h"
#include "crystals/Vertex.h"
#include "crystals/quaternion_tools.h"

using namespace crystals;

void printQ(const tf::Quaternion & q) {
    printf("%f %f %f %f\n",q.x(),q.y(),q.z(),q.w());
    tf::Matrix3x3 R;
    R.setRotation(q);
    printf("------------------\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n------------------\n",
            R[0][0], R[0][1], R[0][2],
            R[1][0], R[1][1], R[1][2],
            R[2][0], R[2][1], R[2][2]);
}


int main(int argc, char * argv[])
{
    initialise_quaternion_symmetries();
    MaterialType = MAT_ZIRCONIUM;
    initialise_twinning_disorientations();

    
#if 1
    Vertex v1, v2;
    // assert(v1.loadFromString("4.46 0.37 1.646 0.0 0.0 0.0 0.0 0.0")); 
    // assert(v2.loadFromString("3.05 0.563 2.988 0.0 0.0 0.0 0.0 0.0")); 
    // assert(v1.loadFromString("0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0")); 
    // assert(v2.loadFromString("1.57 2.53 -1.57  0.0 0.0 0.0 0.0 0.0 0.0")); 
    char v1str[1024], v2str[1024];
    sprintf(v1str," %.6f %.6f %.6f 0.0 0.0 0.0 0.0 0.0 ", 136.94*M_PI/180.0, 152.18*M_PI/180.0,  73.18*M_PI/180.0);
    sprintf(v2str," %.6f %.6f %.6f 0.0 0.0 0.0 0.0 0.0 ", 122.87*M_PI/180.0,  58.55*M_PI/180.0, 305.84*M_PI/180.0);
    assert(v1.loadFromString(v1str,true)); 
    assert(v2.loadFromString(v2str,true)); 

    std::pair<double,tf::Quaternion> r = QDisorientation(v1.q,v2.q);
    printf("Q1: %f %f %f %f\n",v1.q.x(),v1.q.y(),v1.q.z(),v1.q.w());
    printf("Q2: %f %f %f %f\n",v2.q.x(),v2.q.y(),v2.q.z(),v2.q.w());
    tf::Quaternion q1inv = v1.q.inverse();
    printf("QI: %f %f %f %f\n",q1inv.x(),q1inv.y(),q1inv.z(),q1inv.w());
    tf::Quaternion qd = v2.q * v1.q.inverse();
    printf("QD1: %f %f %f %f\n",qd.x(),qd.y(),qd.z(),qd.w());
    qd = v1.q.inverse() * v2.q;
    printf("QD2: %f %f %f %f\n",qd.x(),qd.y(),qd.z(),qd.w());
    qd = v2.q.inverse() * v1.q;
    printf("QD3: %f %f %f %f\n",qd.x(),qd.y(),qd.z(),qd.w());
    qd = v1.q * v2.q.inverse();
    printf("QD4: %f %f %f %f\n",qd.x(),qd.y(),qd.z(),qd.w());
    printf("Disorientation: %f deg\n",r.first*180./M_PI);
    printf("Q: %f %f %f %f\n",r.second.x(),r.second.y(),r.second.z(),r.second.w());
    DisorientationClass dc = classify_disorientation(r.second,true);
    printf("Disorientation variant: %d\n",dc.variant);
    printf("%s",dc.toString().c_str());
#else


    tf::Quaternion q232(-0.6034864,0.1482138,-0.1917939,0.7596394);
    tf::Quaternion q242(-0.09353159,-0.04110352,0.2649561,0.958833);
    tf::Quaternion q246(-0.09157745,0.009882568,-0.251399,0.9634909);

    q232.normalize();
    q242.normalize();
    q246.normalize();
    tf::Quaternion q1 = q232, q2 = q242, q3 = q246;

    printf("Q1: %f %f %f %f\n",q1.x(),q1.y(),q1.z(),q1.w());
    printf("Q2: %f %f %f %f\n",q2.x(),q2.y(),q2.z(),q2.w());
    std::pair<double,tf::Quaternion> r1 = QDisorientation(q2,q1);
    printf("Disorientation1: %f\n%f %f %f %f\n",r1.first*180./M_PI,
            r1.second.x(),r1.second.y(),r1.second.z(),r1.second.w());
    DisorientationClass dc1 = classify_disorientation(r1.second,true);
    printf("%s\n",dc1.toString().c_str());
    printf("Q1: %f %f %f %f\n",q1.x(),q1.y(),q1.z(),q1.w());
    printf("Q3: %f %f %f %f\n",q3.x(),q3.y(),q3.z(),q3.w());
    std::pair<double,tf::Quaternion> r2 = QDisorientation(q3,q1);
    printf("Disorientation2: %f\n%f %f %f %f\n",r2.first*180./M_PI,
            r2.second.x(),r2.second.y(),r2.second.z(),r2.second.w());
    DisorientationClass dc2 = classify_disorientation(r2.second,true);
    printf("%s\n",dc2.toString().c_str());
#endif

    return 0;
}

