#include <cmath>

#include "crystals/ConnectedNeighbour.h"

using namespace crystals;


ConnectedNeighbour::ConnectedNeighbour() : EBSDNeighbour(), 
    embedded(false),active(false),
    manual(false),twinning_type(NO_TWINNING) 
{
    ignore_for_phases = false;
    twinning_weight.clear();
    twinning_weight[NO_TWINNING] = 0;
    twinning_weight[TWINNING_ZERO] = 0;
    switch (MaterialType) {
        case MAT_MAGNESIUM:
            twinning_weight[Mg_TWINNING_TENSILE] = 0;
            twinning_weight[Mg_TWINNING_COMPRESSIVE] = 0;
            break;
        case MAT_ZIRCONIUM:
            twinning_weight[Zr_TWINNING_TENSILE_1] = 0;
            twinning_weight[Zr_TWINNING_TENSILE_2] = 0;
            twinning_weight[Zr_TWINNING_COMPRESSIVE_1] = 0;
            twinning_weight[Zr_TWINNING_COMPRESSIVE_2] = 0;
            break;
        case MAT_MARTENSITE:
            twinning_weight[Mr_TWINNING_TYPE] = 0;
            break;
        default:
            break;
    }
}

ConnectedNeighbour::ConnectedNeighbour(int _i, int _j, bool sg) :
    EBSDNeighbour(_i,_j), embedded(sg),active(false),
    manual(false),twinning_type(NO_TWINNING) 
{
    ignore_for_phases = false;
    twinning_weight.clear();
    twinning_weight[NO_TWINNING] = 0;
    twinning_weight[TWINNING_ZERO] = 0;
    switch (MaterialType) {
        case MAT_MAGNESIUM:
            twinning_weight[Mg_TWINNING_TENSILE] = 0;
            twinning_weight[Mg_TWINNING_COMPRESSIVE] = 0;
            break;
        case MAT_ZIRCONIUM:
            twinning_weight[Zr_TWINNING_TENSILE_1] = 0;
            twinning_weight[Zr_TWINNING_TENSILE_2] = 0;
            twinning_weight[Zr_TWINNING_COMPRESSIVE_1] = 0;
            twinning_weight[Zr_TWINNING_COMPRESSIVE_2] = 0;
            break;
        case MAT_MARTENSITE:
            twinning_weight[Mr_TWINNING_TYPE] = 0;
            break;
        default:
            break;
    }
}

void ConnectedNeighbour::addEdge(size_t edge_id, TwinningType type, size_t w) {
    edges.push_back((unsigned int)edge_id);
    weight += (uint32_t)w;
    twinning_weight[type] += (unsigned int)w;
}

void ConnectedNeighbour::updateTwinning() {
    assert(!std::isnan(q_disorientation.getAngle()));
    DisorientationClass diso = classify_disorientation(q_disorientation);
    twinning_error = diso.error;
    twinning_error_q = diso.error_q;
    twinning_type = diso.type;
    twinning_variant = diso.variant;
    alpha = diso.alpha;
    beta = diso.beta;
}

void ConnectedNeighbour::updateTwinning(const tf::Quaternion & qmean) {
    assert(!std::isnan(q_disorientation.getAngle()));
    DisorientationClass diso = classify_disorientation(q_disorientation);
    twinning_error = diso.error;
    twinning_error_q = diso.error_q;
    twinning_type = diso.type;
    twinning_variant = diso.variant;
    alpha = diso.alpha;
    beta = diso.beta;

    switch (twinning_type) {
        case TWINNING_ZERO:
            active = (diso.error < TwinningTypeTolerance);
            twinning = false;
            return;
        case NO_TWINNING:
            active = false;
            twinning = false;
            return;
        case Mg_TWINNING_TENSILE: 
        case Zr_TWINNING_TENSILE_1: 
        case Mg_TWINNING_COMPRESSIVE: 
        case Zr_TWINNING_TENSILE_2: 
        case Zr_TWINNING_COMPRESSIVE_1:
            active = twinning = (diso.error < TwinningTypeTolerance);
            break;
        case Zr_TWINNING_COMPRESSIVE_2: 
            active = false;
            twinning = true;
            break;
        case Mr_TWINNING_TYPE:
            active = true;
            twinning = true;
        default:
            break;
    }
}

void ConnectedNeighbour::updateTwinning(const EBSDSurface<ConnectedNeighbour> & parent) {
    updateTwinning(parent.qmean);
}

