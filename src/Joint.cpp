#include <assert.h>
#include <functional>

#include <QRect>
#include <QPoint>
#include <QSize>
#include "crystals/Joint.h"

using namespace crystals;

Joint::Joint() {
    grain_joint = false;
    connected_joint = true;
    grain_1 = grain_2 = -1;
    connected_1 = connected_2 = -1;
}

uint32_t Joint::first_face() const {
    return face_ids[0];
}

uint32_t Joint::last_face() const {
    assert(face_ids.size()>0);
    return face_ids[face_ids.size()-1];
}

void Joint::updateGrains(const std::vector<Vertex> & vertices, 
        const std::vector<Edge> & edges) {
    const Edge & E = edges[ref_edge];
    grain_1 = vertices[E.i].grain;
    grain_2 = vertices[E.j].grain;
    grain_joint = (grain_1 != grain_2);
}

// #define DEBUG_BUILD_JOINT
bool Joint::buildFromBranch(std::vector<Face> & faces, 
        const std::vector<Vertex> & vertices, 
        const std::vector<Edge> & edges, 
        uint32_t id, uint8_t direction)
{
    uint32_t current,prev;
    assert(direction < Face::FaceSize);
#ifdef DEBUG_BUILD_JOINT
    printf("Building Joint from %d[%d]\n",id,direction);
#endif
    face_ids.clear();
    face_ids.push_back(id);
    faces[id].visited = true;
    ref_edge = faces[id].edges[direction];
    const Edge & E = edges[ref_edge];
    if (E.isConnected(vertices)) {
        return false;
    }
    grain_1 = vertices[E.i].grain;
    grain_2 = vertices[E.j].grain;
    connected_1 = vertices[E.i].membership;
    connected_2 = vertices[E.j].membership;
    grain_joint = (grain_1 != grain_2);
    connected_joint = (connected_1 != connected_2);
#ifdef DEBUG_BUILD_JOINT
    printf("%d %d :",grain_joint,connected_joint);fflush(stdout);
#endif
    prev = id;
    current = faces[id].neighbours[direction];
    while (1) {
#ifdef DEBUG_BUILD_JOINT
        printf("%d ",current); fflush(stdout);
#endif
        Face & f = faces[current];
        face_ids.push_back(current);
        assert(f.type!=UnclassifiedFace);
        assert(f.type!=InteriorFace);
        if ((f.type==EndOfJointFace)||(f.type==JointBranchFace)) {
            break;
        }
        if (f.visited) {
            // Closed a topologically circular joint 
            break;
        }
        f.updateBorders(vertices);
        f.visited = true;
        assert(f.type == JointFace);
        for (size_t i=0;i<Face::FaceSize;i++) {
            if (f.neighbours[i]<0) continue;
            // if (faces[f.neighbours[i]].visited) continue;
            if (f.neighbours[i]==(signed)prev) continue;
            if (edges[f.edges[i]].isConnected(vertices)) continue;
            switch (faces[f.neighbours[i]].type) {
                case EndOfJointFace:
                case JointBranchFace:
                case JointFace:
                    prev = current;
                    current = f.neighbours[i];
                    break; // out of the switch
                default:
                    continue;
            }
            break; // out of the loop
        }
    };
#ifdef DEBUG_BUILD_JOINT
    printf("\n");
#endif
    return true;
}

//#define DEBUG_ORDER_CONTOUR
bool Joint::orderJointVector(std::vector<Contour> & contours,
        const std::vector<int32_t> & joint_ids,
        const std::vector<Joint> & joints,
        const std::vector<Face> & faces)
{
    int32_t face_it, start_face = -1;
    typedef std::multimap<uint32_t,int32_t,std::greater<uint32_t> > StartMap;
    StartMap start;
    contours.clear();
    if (joint_ids.size()==0) {
        return true;
    }
    if (joint_ids.size()==1) {
        std::vector<int32_t> res(1,joint_ids[0]+1);
        contours.push_back(Contour(false,res,joints,faces));
        return true;
    }
#ifdef DEBUG_ORDER_CONTOUR
    printf("Starting order joint vector.\nInitial State:\n");
#endif
    for (size_t i=0;i<joint_ids.size();i++) {
        const Joint & J = joints[joint_ids[i]];
        start.insert(StartMap::value_type(J.first_face(),(joint_ids[i]+1)));
        start.insert(StartMap::value_type(J.last_face(),-(joint_ids[i]+1)));
#ifdef DEBUG_ORDER_CONTOUR
        printf("%6d %6d: %d -> %d\n",(int)i,(int)joint_ids[i],
                J.first_face(),J.last_face());
#endif
    }
    while (start.size()>0) {
        start_face = -1;
        // First where to start from. We look for map_edge elements first
        for (StartMap::const_iterator it=start.begin();it!=start.end();it++) {
            size_t idx = abs(it->second)-1;
            const Joint & J = joints[idx];
            if (start_face<0) {
                if (faces[J.first_face()].map_edge) {
                    start_face = J.first_face();
                    break;
                } else if (faces[J.last_face()].map_edge) {
                    start_face = J.last_face();
                    break;
                }
            }
        }
        if (start_face < 0) {
            start_face = joints[abs(start.begin()->second)-1].first_face();
        }
#ifdef DEBUG_ORDER_CONTOUR
        printf("Start face: %d\n",(int)start_face);
#endif
        std::vector<int32_t> res;
        face_it = start_face;
        while (1) {
            // protection against stupid code
            assert(res.size() <= joint_ids.size());
            // First, find joint starting in face_it
            StartMap::iterator it = start.find(face_it);
            assert(it != start.end());
            size_t idx = abs(it->second)-1;
            res.push_back(it->second);
#ifdef DEBUG_ORDER_CONTOUR
            printf("%d -> %d -> ",(int)it->first,(int)it->second);
#endif
            // Use it to find the next face_it
            if (it->second<=0) {
                face_it = joints[idx].first_face();
            } else {
                face_it = joints[idx].last_face();
            }
#ifdef DEBUG_ORDER_CONTOUR
            printf("%d\n",(int)face_it);
#endif

            // Now remove any reference to this joint in start
            start.erase(it);
            it = start.find(face_it);
            while ((signed)it->first == face_it) {
                if (abs(it->second)-1 == (signed)idx) {
                    start.erase(it);
                    break;
                }
                it++;
            }

            // End conditions
            if (face_it == start_face) {
                break;
            }
            if (faces[face_it].map_edge) {
                break;
            }
        }
        contours.push_back(Contour(false,res,joints,faces));
    }
    if (contours.size()>1) {
        // Now we just need to identify the holes. Because we know that
        // topologically we can only have a single countour with small hole inside it, we
        // can simplify this by naming exterior the largest contour in terms of
        // bounding box
        float best_area = 0;
        size_t ext = 0;
        for (size_t i=0;i<contours.size();i++) {
            QRectF bbox;
            Contour & C = contours[i];
            C.hole = true;
            for (size_t j=0;j<C.face_ids.size();j++) {
                bbox = bbox.united(QRectF(QPointF(faces[C.face_ids[j]].x,faces[C.face_ids[j]].y),QSizeF()));
            }
            float area = bbox.width()*bbox.height();
            if (area > best_area) {
                best_area = area;
                ext = i;
            }
        }
        std::swap(contours[ext],contours[0]);
        contours[0].hole = false;
    }
#ifdef DEBUG_ORDER_CONTOUR
    printf("Completed\n");
#endif
    return true;
}

Contour::Contour(bool is_hole, const std::vector<int32_t> & joint_ids,
        const std::vector<Joint> & joints,const std::vector<Face> & faces)
{
    hole = is_hole;
    orientation.clear();
    face_ids.clear();
    face_idx.clear();
    for (size_t i=0;i<joint_ids.size();i++) {
        const Joint & J = joints[abs(joint_ids[i])-1];
        if (joint_ids[i]>0) {
            face_ids.insert(face_ids.end(),J.face_ids.begin(),J.face_ids.end());
        } else {
            face_ids.insert(face_ids.end(),J.face_ids.rbegin(),J.face_ids.rend());
        }
    }
//#define DEBUG_LINREG
    int32_t i,N;
    N = (int32_t)face_ids.size();
    orientation.resize(N);
    bool closed = (face_ids[0] == face_ids[N-1]);
    for (i=0;i<N;i++) {
        face_idx[face_ids[i]] = i;
        double sxy = 0; 
        double sx = 0;  
        double sy = 0;  
        double sx2 = 0; 
        double sy2 = 0; 
        size_t n = 0;
#ifdef DEBUG_LINREG
        printf("D = [");
#endif
        double xc = faces[face_ids[i]].x;
        double yc = faces[face_ids[i]].y;
        for (int32_t j=-(signed)JointNeighbourhood;j<=(signed)JointNeighbourhood;j++) {
            double x=0;
            double y=0;
            if (closed) {
                x = faces[face_ids[(i+N+j)%N]].x-xc;
                y = faces[face_ids[(i+N+j)%N]].y-yc;
            } else if ((i+j<0) || ((int)(i+j)>=(int)N)) {
                continue;
            } else {
                x = faces[face_ids[i+j]].x-xc;
                y = faces[face_ids[i+j]].y-yc;
            }
#ifdef DEBUG_LINREG
            printf("%e %e\n",x,y);
#endif
            sxy += x*y;
            sx  += x;
            sy  += y;
            sx2 += x*x;
            sy2 += y*y;
            n += 1;
        }
        // TODO: solve the real homogenous system ax + by + c = 0
        // but this requires an SVD and more dependencies
        // Fit a x + b y + 1 = 0 
        // A X = B, with A = [ sumX2 sumXY ] = [ a c ] and B = [ sumX ]
        //                   [ sumXY sumY2 ]   [ c b ]         [ sumY ]
        //  X = [ a ] = [(sumY2 * sumX - sumXY*sumY) / (sumX2*sumY2-sumXY*sumXY) ]
        //      [ b ]   [(sumX2 * sumY - sumXY*sumX) / (sumX2*sumY2-sumXY*sumXY) ] 
        double a,b;
        a = -(sy2 * sx - sxy*sy) / (sx2*sy2-sxy*sxy);
        b = -(sx2 * sy - sxy*sx) / (sx2*sy2-sxy*sxy);
#ifdef DEBUG_LINREG
        printf("]\n;[%e %e]\n",a,b);
#endif
        // [a,b] is the normal
        orientation[i] = atan2(a,-b);
#ifdef DEBUG_LINREG
        getchar();
#endif
    }
}


