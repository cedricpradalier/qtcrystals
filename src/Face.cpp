#include <assert.h>
#include <functional>

#include "crystals/Face.h"

using namespace crystals;

uint8_t Face::FaceSize = 0;

Face::Face(int32_t a, int32_t b, bool point_up,
        const std::vector<Vertex> & v, const std::vector<Edge> & e, 
        uint32_t e1, uint32_t e2, uint32_t e3)
{
    int k;
    if (FaceSize==0) {
        FaceSize=3;
    }
    assert(FaceSize==3);
    i = a; j = b; up = point_up;
    map_edge = border = grain_border = false;
    type = UnclassifiedFace;
    edges[0] = e1; edges[1] = e2; edges[2] = e3; 
    std::set<uint32_t> vx;
    x = 0; y = 0;
    for (k=0;k<FaceSize;k++) {
        neighbours[k] = -1;
        vx.insert(e[edges[k]].i); vx.insert(e[edges[k]].j);
        x += v[e[edges[k]].i].x; y += v[e[edges[k]].i].y;
        x += v[e[edges[k]].j].x; y += v[e[edges[k]].j].y;
    }
    assert(vx.size() == FaceSize);
    x /= 2*FaceSize; y /= 2*FaceSize;
    k=0;
    for (std::set<uint32_t>::const_iterator it=vx.begin();it!=vx.end();it++) {
        vertices[k] = *it;
        k += 1;
    }
}

Face::Face(int32_t a, int32_t b, 
        const std::vector<Vertex> & v, const std::vector<Edge> & e, 
        uint32_t e1, uint32_t e2, uint32_t e3, uint32_t e4)
{
    unsigned int k;
    if (FaceSize==0) {
        FaceSize=4;
    }
    assert(FaceSize==4);
    i = a; j = b; up = true;
    map_edge = border = grain_border = false;
    type = UnclassifiedFace;
    edges[0] = e1; edges[1] = e2; edges[2] = e3; edges[3] = e4; 
    std::set<uint32_t> vx;
    x = 0; y = 0;
    for (k=0;k<FaceSize;k++) {
        neighbours[k] = -1;
        vx.insert(e[edges[k]].i); vx.insert(e[edges[k]].j);
        x += v[e[edges[k]].i].x; y += v[e[edges[k]].i].y;
        x += v[e[edges[k]].j].x; y += v[e[edges[k]].j].y;
    }
    assert(vx.size() == FaceSize);
    x /= 2*FaceSize; y /= 2*FaceSize;
    k=0;
    for (std::set<uint32_t>::const_iterator it=vx.begin();it!=vx.end();it++) {
        vertices[k] = *it;
        k += 1;
    }
}

void Face::updateBorders(const std::vector<Vertex> & v)
{
    uint8_t countMapEdge = 0;
    std::set<uint32_t> countGrains;
    std::set<uint32_t> countConnected;
    for (unsigned int i=0;i<FaceSize;i++) {
        countMapEdge += v[vertices[i]].map_edge?1:0;
        countGrains.insert(v[vertices[i]].grain);
        countConnected.insert(v[vertices[i]].membership);
    }
    border = (countConnected.size()>1);
    grain_border = (countGrains.size()>1);
    map_edge = (countMapEdge>1);
}

void Face::updateFaceType(const std::vector<Vertex> & v, const std::vector<Edge> & e) 
{
    uint8_t countSolid = 0;
    for (unsigned int i=0;i<FaceSize;i++) {
        countSolid += (v[e[edges[i]].i].membership == v[e[edges[i]].j].membership)?1:0;
        //countSolid += (e[edges[i]].isSolid())?1:0;
    }
    visited = false;
    updateBorders(v);
    switch (FaceSize) {
        case 3: switch (countSolid) {
                    case 0:
                        type = JointBranchFace;
                        break;
                    case 1:
                        type = map_edge?EndOfJointFace:JointFace;
                        break;
                    case 2:
                        type = EndOfJointFace;
                        break;
                    case 3:
                    default:
                        type = InteriorFace;
                        break;
                }
                break;
        case 4: switch (countSolid) {
                    case 0:
                    case 1:
                        type = JointBranchFace;
                        break;
                    case 2:
                        type = map_edge?EndOfJointFace:JointFace;
                        break;
                    case 3:
                        type = EndOfJointFace;
                        break;
                    case 4:
                    default:
                        type = InteriorFace;
                        break;
                }
                break;
        default:
                assert((FaceSize == 3) || (FaceSize == 4));
                break;
    }
}


