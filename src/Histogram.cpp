
#include "crystals/Histogram.h"

using namespace crystals;

FreqHistogram countToFrequency(const CountHistogram & h) {
    FreqHistogram f;
    double count = 0;
    for (size_t i=0;i<h.size();i++) {
        count += h[i].second;
    }
    for (size_t i=0;i<h.size();i++) {
        f.push_back(FreqSample(h[i].first,h[i].second/count));
    }
    return f;
}

