#define _USE_MATH_DEFINES
#include <cmath>
#include "crystals/quaternion_tools.h"
#include "crystals/constants.h"
#include "crystals/log.h"

namespace crystals {

    double MeshSize = 0.5;
    double MeshArea = 3 * SQR(MeshSize/2)*sin(M_PI/3);
    double XOffset = 0.0;
    double YOffset = 0.0;
    double DeltaX = MeshSize/2;
    double DeltaY = sqrt(SQR(MeshSize) - SQR(MeshSize/2));
    double EdgeTolerance = 5*M_PI/180.; 
    double SigmaSqr = SQR(EdgeTolerance);
    double MinWeight = 0.3;
    size_t MinJointWeight = 0;
    size_t JointNeighbourhood = 10;
    size_t OrientationResolutionDeg = 10;
    double MinComponentSize = 0.5;
    double SubGrainMinEnclosure = 0.75; // 50% of the border must be inside
    double TwinningTypeTolerance = 7*M_PI/180.; 
    double TwinningGammaMg = 1.632; // For Mg
    double TwinningGammaZr = 1.5926; // For Zr
    double TwinningGammaMr = 1.5926; // For Mr
    double TwinningGamma = TwinningGammaMg;
    bool TrustExternalPhaseData = true;

    MaterialEnum MaterialType = MAT_UNDEF;

    TwinningDisorientationsMap twinning_disorientations;

    void initialise_twinning_disorientations() {
        twinning_disorientations.clear();
        tf::Quaternion q;
        tf::Vector3 z(0,0,1); 
        q.setRPY(0,0,0);
        twinning_disorientations[TWINNING_ZERO*MAX_VARIANT_PER_TYPE] = q;
        //////////////////////////////////////////
        //
        // Remarks: all the reference orientations below are inverted. This
        // comes from the fact that the initial values were given from the twin
        // to the parent. Changing the quaternion directly may make more sense
        switch (MaterialType) {
            case MAT_MAGNESIUM:
                {
                    TwinningGamma = TwinningGammaMg;
                    static const double beta_t = atan(TwinningGamma/sqrt(3.0));
                    static const double beta_c = atan(2*TwinningGamma/sqrt(3.0));
                    LOG_INFO("Disorientation T: %.2f",(2*beta_t)*180./M_PI);
                    LOG_INFO("Disorientation T: %.2f",(M_PI-2*beta_c)*180./M_PI);
                    for (unsigned i=0;i<6;i++) {
                        tf::Vector3 v3(cos(i*M_PI/3),sin(i*M_PI/3),0.0);
                        tf::Vector3 v6(cos(M_PI/6+i*M_PI/3),sin(M_PI/6+i*M_PI/3),0.0);
                        q = tf::Quaternion(v3,2*beta_t).inverse();
                        // LOG_INFO("T %d Quaternion: w=%.4f v=[%.4f %.4f %.4f]", i, q.w(),q.x(),q.y(),q.z());
                        twinning_disorientations[Mg_TWINNING_TENSILE*MAX_VARIANT_PER_TYPE+i] = q; 
                        q = tf::Quaternion(v3,M_PI-2*beta_c).inverse();
                        // LOG_INFO("C %d Quaternion: w=%.4f v=[%.4f %.4f %.4f]", i, q.w(),q.x(),q.y(),q.z());
                        twinning_disorientations[Mg_TWINNING_COMPRESSIVE*MAX_VARIANT_PER_TYPE+i] = q; 
                    }
                }
                break;
            case MAT_ZIRCONIUM:
                {
                    TwinningGamma = TwinningGammaZr;
                    static const double beta_t1 = atan(TwinningGamma/sqrt(3.0));
                    static const double beta_t2 = atan(2*TwinningGamma);
                    static const double beta_c1 = atan(TwinningGamma);
                    static const double beta_c2 = atan(2*TwinningGamma/sqrt(3.0));
                    LOG_INFO("Disorientation T1: %.2f",(2*beta_t1)*180./M_PI);
                    LOG_INFO("Disorientation T2: %.2f",(M_PI-2*beta_t2)*180./M_PI);
                    LOG_INFO("Disorientation C1: %.2f",(M_PI-2*beta_c1)*180./M_PI);
                    LOG_INFO("Disorientation C2: %.2f",(M_PI-2*beta_c2)*180./M_PI);

                    // Replacing variants above with reference values from L.
                    // Capolungo, P.A. Juan
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+0] = tf::Quaternion(-0.63760758,-0.368122912,0.676714185, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+1] = tf::Quaternion(0.,-0.736245823,0.676714185, 0.0);          
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+2] = tf::Quaternion(0.637607586,-0.368122912,0.676714185, 0.0); 
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+3] = tf::Quaternion(0.637607586,0.368122912,0.676714185, 0.0);  
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+4] = tf::Quaternion(0.,0.736245823,0.676714185, 0.0);           
                    twinning_disorientations[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+5] = tf::Quaternion(-0.637607586,0.368122912,0.676714185, 0.0); 

                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+0] = tf::Quaternion(-0.149819797,-0.259495501,0.954052469, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+1] = tf::Quaternion(0.149819797,-0.259495501,0.954052469, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+2] = tf::Quaternion(0.299639595,0.,0.954052469, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+3] = tf::Quaternion(0.149819797,0.259495501,0.954052469, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+4] = tf::Quaternion(-0.149819797,0.259495501,0.954052469, 0.0);
                    twinning_disorientations[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+5] = tf::Quaternion(-0.299639595,0.,	0.954052469, 0.0);

                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+0] = tf::Quaternion(0.265955031,0.460647672,-0.846800864, 0.0);
                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+1] = tf::Quaternion(-0.265955031,0.460647672,-0.846800864, 0.0);
                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+2] = tf::Quaternion(-0.531910062,0.0,-0.846800864, 0.0);
                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+3] = tf::Quaternion(-0.265955031,-0.460647672,-0.846800864, 0.0);
                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+4] = tf::Quaternion(0.265955031,-0.460647672,-0.846800864, 0.0);
                    twinning_disorientations[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+5] = tf::Quaternion(0.531910062,0.0,-0.846800864, 0.0);

                }
                break;
            case MAT_MARTENSITE:
                {
                    TwinningGamma = TwinningGammaMr;
                    // Replacing variants above with reference values from L.
                    // Capolungo, P.A. Juan
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 0] = tf::Quaternion(0.522904,-0.0813887,0.276639,0.529027);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 1] = tf::Quaternion(0.694206,-0.0613053,0.208375,0.153263);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 2] = tf::Quaternion(0.65559,0.303434,0.123275,-0.129833);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 3] = tf::Quaternion(0.570572,-0.199469,0.141643,0.149178);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 4] = tf::Quaternion(0.549657,-0.129632,0.263174,-0.193568);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 5] = tf::Quaternion(0.444833,0.542871,0.32519,-0.621874);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 6] = tf::Quaternion(0.694206,0.0613053,0.208375,-0.153263);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 7] = tf::Quaternion(0.522904,0.0813887,0.276639,-0.529027);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 8] = tf::Quaternion(0.570572,0.199469,0.141643,-0.149178);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+ 9] = tf::Quaternion(0.65559,-0.303434,0.123275,0.129833);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+10] = tf::Quaternion(0.444833,-0.542871,0.32519,0.621874);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+11] = tf::Quaternion(0.549657,0.129632,0.263174,0.193568);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+12] = tf::Quaternion(0.522904,0.0813887,0.276639,-0.529027);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+13] = tf::Quaternion(0.694206,0.0613053,0.208375,-0.153263);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+14] = tf::Quaternion(0.549657,0.439341,0.495456,-0.193568);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+15] = tf::Quaternion(0.444833,-0.160179,0.612209,-0.621874);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+16] = tf::Quaternion(0.417669,-0.272492,0.804868,-0.20379);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+17] = tf::Quaternion(0.264955,0.750801,1.26878,0.321251);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+18] = tf::Quaternion(0.64426,0.14013,0.297261,0.693609);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+19] = tf::Quaternion(0.771772,-0.116978,0.248148,-0.0827159);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+20] = tf::Quaternion(0.65559,-0.303434,-0.123275,-0.129833);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+21] = tf::Quaternion(0.570572,0.199469,-0.141643,0.149178);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+22] = tf::Quaternion(0.475995,-0.0894096,-0.57213,-0.223524);
                    twinning_disorientations[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+23] = tf::Quaternion(0.134454,-0.316529,-2.02546,-2.05744);

                }
                break;
            default:
                LOG_ERROR("Trying to update twinning when no material is defined. Aborting");
                abort();
                break;
        }
        LOG_INFO("Known disorientations:");
        TwinningDisorientationsMap::const_iterator it;
        for (it=twinning_disorientations.begin();it!=twinning_disorientations.end();it++) {
            TwinningType nt = (TwinningType)(it->first/MAX_VARIANT_PER_TYPE);
            int variant = it->first % MAX_VARIANT_PER_TYPE;
            const tf::Quaternion & q = it->second; 
            LOG_INFO("%s V %d: Quaternion: w=%.4f v=[%.4f %.4f %.4f]", twinning_type_str(nt), variant, 
                    q.w(),q.x(),q.y(),q.z());
        }
    }

    const char * material_type_str(MaterialEnum type) {
        switch (type) {
            case MAT_MARTENSITE: return "Martensite";
            case MAT_AUSTENITE: return "Austenite";
            case MAT_MAGNESIUM: return "Magnesium";
            case MAT_ZIRCONIUM: return "Zirconium";
            default: return "Undefined Material";
        }
    }

    const char * twinning_type_str(TwinningType type) {
        switch (type) {
            case NO_TWINNING: return "No Twinning";
            case TWINNING_ZERO: return "Zero Twinning";
            case Mg_TWINNING_TENSILE: return "Twinning Tensile Mg";
            case Mg_TWINNING_COMPRESSIVE: return "Twinning Compressive Mg";
            case Zr_TWINNING_TENSILE_1: return "Twinning Tensile 1 Zr";
            case Zr_TWINNING_TENSILE_2: return "Twinning Tensile 2 Zr";
            case Zr_TWINNING_COMPRESSIVE_1: return "Twinning Compressive 1 Zr";
            case Zr_TWINNING_COMPRESSIVE_2: return "Twinning Compressive 2 Zr";
            case Mr_TWINNING_TYPE: return "Twinning Martensite";
            default: return "Incorrect twinning value";
        }
    }

};
