
#include "crystals/Grain.h"
#include "crystals/log.h"

using namespace crystals;
// this is for show

Grain::Grain() : disp_x(0),disp_y(0),size(0),
    border_size(0), parent_cluster(0),
    active(true),refined(false),n_in(0),n_out(0),convexity_ratio(0.0) 
{
}

Grain::Grain(size_t id) : disp_x(0),disp_y(0),size(0),
    border_size(0), parent_cluster(0),
    active(true),refined(false),n_in(0),n_out(0),convexity_ratio(0.0)  
{
    i = (int)id; // From Surface
}

void Grain::add_neighbour(int id, size_t weight,const tf::Quaternion & q) {
    NeighbourMap::iterator nit = Surface::add_neighbour(id);
    nit->second.q_disorientation = q;
    nit->second.weight += (uint32_t)weight;
}

void Grain::add_joint(size_t grain_id, size_t joint_id)
{
    NeighbourMap::iterator nit;
    nit = neighbours.find((unsigned int)grain_id);
#if 0
    assert(nit != neighbours.end());
#else
    if (nit == neighbours.end()) {
        LOG_ERROR("Trying to add joint %d between unconnected G %d and G %d",
                (int)joint_id,(int)i,(int)grain_id);
        return;
    }
#endif
    nit->second.joints.push_back((unsigned int)joint_id);
}
