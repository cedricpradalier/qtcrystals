
#include <crystals/Graph.h>
#include <crystals/Phase.h>
#include <crystals/log.h>

using namespace crystals;

Phase::Phase(const Grain & G,const std::vector<Connected> & connected,
        const std::vector<Joint> & joints, std::vector<Phase> & phases) : 
    qmean(0,0,0,1), qdiso(0,0,0,1)
{
    updated = false;
    parent = -1;
    twinning_order = 0;
    grain = G.i;
    parts = G.parts;
    is_parent.resize(parts.size());
    for (size_t i=0;i<parts.size();i++) {
        is_parent[i] = connected[parts[i]].parent;
    }
    refine(connected,joints,phases);
}

bool Phase::refine(const std::vector<Connected> & connected,
        const std::vector<Joint> & conn_joints, std::vector<Phase> & phases) {

    // printf("%d %d Refining: ",grain,twinning_order);
    // for(size_t i=0;i<parts.size();i++) {
    //     printf("%d ",(int)parts[i]);
    // }
    // printf("\n");
    updated = false;

    if (parts.size()==1) {
        const Connected & C = connected[parts[0]];
        joints = C.joints;
        return true;
    }

    std::vector<bool> is_subparent(parts.size());
    int main_cluster = -1;
    std::set<uint32_t> parent;
    std::map<uint32_t,uint32_t> id_to_part;
    std::map<uint32_t,uint32_t>::const_iterator idit;
    // First build the parent set.
    for (size_t i=0;i<parts.size();i++) {
        is_parent[i] = /*is_parent[i] ||*/ 
            (twinning_order == connected[parts[i]].twinning_order);
        if (is_parent[i]) {
            if (main_cluster>=0) {
                if (main_cluster != connected[parts[i]].cluster) {
                    LOG_ERROR("Weird main cluster: T %d Grain %d, Main cluster %d Connected %d Conn. Cluster %d Parent %d",
                            twinning_order, grain,main_cluster,(int)parts[i],
                            (int)connected[parts[i]].cluster,is_parent[i]);
                }
            } else {
                main_cluster = connected[parts[i]].cluster;
            }
        }
        is_subparent[i] = false;
        if (is_parent[i]) {
            parent.insert((unsigned int)i);
        }
        id_to_part[parts[i]]=(unsigned int)i;
    }
    if (main_cluster>=0) {
        cluster = main_cluster;
    } else {
        LOG_ERROR("No main cluster: T %d Grain %d Connected %d",
                twinning_order, grain, (int)parts[0]);
    }


    joints.clear();
    for (size_t i=0;i<parts.size();i++) {
        const Connected & C = connected[parts[i]];
        for (size_t j=0;j<C.joints.size();j++) {
            const Joint & J = conn_joints[C.joints[j]];
            bool c1 = id_to_part.find(J.connected_1)!=id_to_part.end();
            bool c2 = id_to_part.find(J.connected_2)!=id_to_part.end();
            assert(c1 || c2);
            // printf("Joint %d: %d %d<> %d %d\n",C.joints[j],J.connected_1,c1,J.connected_2,c2);
            if (c1 && c2) continue; // internal joint
            joints.push_back(C.joints[j]);
        }
    }
    // then build a graph of parts but remove anything connected to the parent.
    Graph graph((unsigned int)parts.size());
    for (size_t i=0;i<parts.size();i++) {
        if (is_parent[i]) continue;
        Connected::NeighbourMap::const_iterator it;
        for (it=connected[parts[i]].neighbours.begin();
                it != connected[parts[i]].neighbours.end();it++) {
            // printf("C %d %d %d\n ",(int)i,it->first,it->second.active);
            if (!it->second.active) continue; 
            if (it->second.ignore_for_phases) continue; 
            idit = id_to_part.find(it->first);
            if (idit == id_to_part.end()) continue;
            if (is_parent[idit->second]) {
                // Future parent
                is_subparent[i] = true;
                continue;
            }
            graph.add_connection(i,idit->second);
            // printf(" >%d %d\n",(int)i,idit->second);
        }
    }
    // int ncomp = 
        graph.find_connected_components();
    // printf("Found %d connected components out of %d(%d)\n",ncomp,(int)parts.size(),(int)(parts.size()-parent.size()));
    subphases.clear();
    std::map<size_t,Phase> sub;
    std::map<size_t,Phase>::iterator phit;
    for (size_t i=0;i<parts.size();i++) {
        if (is_parent[i]) continue;
        sub[graph.membership(i)].parts.push_back(parts[i]);
        sub[graph.membership(i)].is_parent.push_back(is_subparent[i]);
    }
    for (phit=sub.begin();phit!=sub.end();phit++) {
        // printf("%d subphase\n",twinning_order);
        phit->second.grain = grain;
        phit->second.twinning_order = twinning_order + 1;
        // Take the first link between a phase component and the parent as the
        // neighbouring type
        TwinningType twinning_type = NO_TWINNING;
        double best_error = M_PI;
        for (size_t i=0;i<phit->second.parts.size();i++) {
            const Connected & C = connected[phit->second.parts[i]];
            Connected::NeighbourMap::const_iterator it;
            for (it=C.neighbours.begin();it != C.neighbours.end();it++) {
                if (it->second.ignore_for_phases) continue; 
                if (!it->second.active) continue; // implicitly, same grain
                idit = id_to_part.find(it->first);
                if (idit == id_to_part.end()) continue;
                if (is_parent[idit->second] && (it->second.twinning_type>TWINNING_ZERO) 
                        && (it->second.twinning_error < best_error)) {
                    twinning_type = it->second.twinning_type;
                }
            }
        }
        phit->second.refine(connected,conn_joints,phases);
        subphases.insert(std::pair<TwinningType,size_t>(twinning_type,phases.size()));
        // printf("%d Inserted as %d %s\n",twinning_order,(int)phases.size(),
        //         twinning_type_str(twinning_type));
        phases.push_back(phit->second);
    }

    
    std::vector<uint32_t> filtered_parts;
    for (std::set<uint32_t>::const_iterator it=parent.begin();it!=parent.end();it++) {
        filtered_parts.push_back(parts[*it]);
    }
    // assert(filtered_parts.size()>0);
    parts = filtered_parts;

    return true;
}


bool Phase::update_statistics(const std::vector<Connected> & connected,
        std::vector<Phase> & phases) {
    if (updated) {
        return true;
    }
    // printf("Updating statistics: %d parts in parent\n",(int)parts.size());
    // Now compute the new barycenters
    std::vector<tf::Quaternion> qmeans;
    std::vector<double> weights;
    size = 0;
    x = y = 0;
    qmean = tf::Quaternion(0,0,0,1);
    qdiso = tf::Quaternion(0,0,0,1);

    if (parts.size() == 0) {
        // No point updating statistics, this is a buggy case
        return false;
    }
    std::vector<uint32_t>::const_iterator sit = parts.begin();
    for (;sit!=parts.end();sit++) {
        size_t j = *sit;
        const Connected & C = connected[j];
        qmeans.push_back(C.qmean);
        weights.push_back(C.size());
        size += (uint32_t)C.size();
        x += C.size()*connected[j].x;
        y += C.size()*connected[j].y;
    }
    qmean = QDisorientation(QRef,QMeanWithSymmetries(qmeans, weights)).second;
    assert(!std::isnan(qmean.getAngle()));
#if 1
    std::multimap<TwinningType,size_t>::const_iterator phit;
    for (phit=subphases.begin();phit!=subphases.end();phit++) {
        // Subphases don't contribute to quaternion mean
        phases[phit->second].id = phit->second;
        phases[phit->second].update_statistics(connected,phases);
        phases[phit->second].parent = (int)id;
        phases[phit->second].qdiso = QDisorientation(qmean,phases[phit->second].qmean).second;
        assert(!std::isnan(phases[phit->second].qdiso.getAngle()));
        DisorientationClass dc = classify_disorientation(phases[phit->second].qdiso);
        phases[phit->second].twinning_type = dc.type;
        phases[phit->second].twinning_variant = dc.variant;
        size += phases[phit->second].size;
        x += phases[phit->second].size*phases[phit->second].x;
        y += phases[phit->second].size*phases[phit->second].y;
    }
#endif
    x /= size;
    y /= size;

    updated = true;
    

    return true;
}

