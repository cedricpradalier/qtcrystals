#define _USE_MATH_DEFINES
#include <cmath>

#include "crystals/Connected.h"
#include "crystals/log.h"

using namespace crystals;

Connected::Connected() {
    i=-1;valid = false; phase = -1; cluster = -1; group=-1; 
    embedded = false; num_connections = 0;
    twinstrip = 0; manual_twin = manual_parent = false; parent = false; map_edge = false;
    connected_twin = false;
    x = y = 0.0;
    cov_xx = 0; cov_xy = 0; cov_yy = 0;
    length = 0; thickness = 0;
    area = 0; border_length = 0; schmidt_factor = 0;
    subgrain_type = 0;
    twinning_order = -1;
    has_weird_parents = false;
};

void Connected::add_neighbour(int id, size_t edge_id, TwinningType type, size_t weight) {
    NeighbourMap::iterator nit = Surface::add_neighbour(id);
    num_connections += weight;
    nit->second.addEdge(edge_id, type, weight);
}

void Connected::add_joint(size_t conn_id, size_t joint_id)
{
    NeighbourMap::iterator nit;
    nit = neighbours.find((unsigned int)conn_id);
#if 0
    assert(nit != neighbours.end());
#else
    if (nit == neighbours.end()) {
        LOG_ERROR("Trying to add joint %d between unconnected C %d and C %d",
                (int)joint_id,(int)i,(int)conn_id);
        return;
    }
#endif
    nit->second.joints.push_back((unsigned int)joint_id);
}

size_t Connected::count_twinning_type(TwinningType t) const {
    size_t count = 0;
    NeighbourMap::const_iterator nit = neighbours.begin();
    // printf("Counting twinning %d from %d\n",(int)t,(int)i);
    for (;nit != neighbours.end(); nit++) {
        if (nit->second.twinning_type == t) {
            count++;
        }
        // printf("  %d to %d: twinning %d -- count %d\n",(int)nit->second.i,(int)nit->second.j,nit->second.twinning_type,(int)count);
    }
    // printf("Total: %d\n",(int)count);
    return count;
}

void Connected::updateStatistics(const std::vector<Vertex> & v) {
    x = y = 0.0;
    cov_xx = cov_xy = cov_yy = 0.0;
    for (size_t k=0;k<vertices.size();k++) {
        x += v[vertices[k]].x;
        y += v[vertices[k]].y;
    }
    x /= vertices.size();
    y /= vertices.size();
    for (size_t k=0;k<vertices.size();k++) {
        cov_xx += (v[vertices[k]].x-x)*(v[vertices[k]].x-x);
        cov_xy += (v[vertices[k]].y-y)*(v[vertices[k]].x-x);
        cov_yy += (v[vertices[k]].y-y)*(v[vertices[k]].y-y);
    }
    cov_xx /= vertices.size();
    cov_xy /= vertices.size();
    cov_yy /= vertices.size();

    area = size() * MeshArea; // Hexagonal area for one measurement
    border_length = border_vertices.size() * MeshSize; // Approximation
#if 1
    qmean = QDisorientation(QRef,QMeanWithSymmetries(v, vertices)).second;
#else
    qmean = v[vertices[0]].q;
#endif
    // TODO: Compute grain schmidt factor, subgrain type
    schmidt_factor = 0;
    subgrain_type = 0;

    // Evtl, grain thickness, grain length
    // http://www.math.harvard.edu/archive/21b_fall_04/exhibits/2dmatrices/index.html
    double lambda1=0, lambda2=0;
    double T = cov_xx + cov_yy;
    double D = cov_xx*cov_yy - cov_xy*cov_xy;
    double delta = T*T/4 - D;
    assert (delta > -1e-8);
    if (fabs(delta) <= 1e-8) {
        delta = 0;
    }
    lambda1 = T/2 + sqrt(delta);
    lambda2 = T/2 - sqrt(delta);
    if ((lambda1>1e-12) && (lambda2>1e-12)) {
        // Added mesh-size to account for the EBSD footprint
        length = 4*sqrt(lambda1);
        thickness = 4*sqrt(lambda2);
        angle = 0.5 * atan2(2*cov_xy,(cov_xx-cov_yy));

        double ellipse_area = M_PI*length*thickness/4;
        ellipsicity = 1 - area/ellipse_area;
        ellipsicity = exp(-0.5*SQR(ellipsicity/0.3));
    } else {
        // default value for pathological cases
        length = 0;
        thickness = 0;
        angle = 0;
        ellipsicity = 0;
    }
}

TwinStrip::TwinStrip() : Connected() {
    i = 0;
    twinstrip = -1;
}

void TwinStrip::assemble(std::vector<Connected> & connected, 
        const std::vector<Vertex> & full_vertices) {
    if (parts.empty()) return;
    // Some properties can be copied directly from the first one
    valid = connected[parts[0]].valid;
    color = connected[parts[0]].color;
    group = connected[parts[0]].group;
    cluster = connected[parts[0]].cluster;
    phase = connected[parts[0]].phase;
    twinning_order = connected[parts[0]].twinning_order;

    // This is something that does not make sense for a TwinStrip
    manual_parent = false;
    parent = false;
    embedded = true;
    // The following must be accumulated.
    connected_twin = false;
    map_edge = false;
    num_connections = 0;
    vertices.clear();
    expanded_vertices.clear();
    border_vertices.clear();
    grain_border_vertices.clear();
    neighbours.clear();
    joints.clear();
    contours.clear();
    for (size_t c=0;c<parts.size();++c) {
        Connected & C = connected[parts[c]];
        C.twinstrip = this->i;
        num_connections += C.num_connections;
        map_edge = map_edge || C.map_edge;
        connected_twin = connected_twin || C.connected_twin;
        vertices.insert(vertices.end(),
                C.vertices.begin(),C.vertices.end());
        expanded_vertices.insert(expanded_vertices.end(),
                C.expanded_vertices.begin(),C.expanded_vertices.end());
        border_vertices.insert(border_vertices.end(),C.border_vertices.begin(),
                C.border_vertices.end());
        grain_border_vertices.insert(grain_border_vertices.end(),
                C.grain_border_vertices.begin(),C.grain_border_vertices.end());
        joints.insert(joints.end(),C.joints.begin(),C.joints.end());
        contours.insert(contours.end(),C.contours.begin(),C.contours.end());
        NeighbourMap::const_iterator it;
        NeighbourMap::iterator fit;
        for (it=C.neighbours.begin();it!=C.neighbours.end();it++) {
            fit = neighbours.find(it->first);
            if (fit == neighbours.end()) {
                neighbours[it->first] = it->second;
                neighbours[it->first].i = i;
            } else {
                fit->second.weight += it->second.weight;
                fit->second.manual = fit->second.manual | it->second.manual;
                fit->second.edges.insert(fit->second.edges.end(),
                        it->second.edges.begin(),it->second.edges.end());
                fit->second.joints.insert(fit->second.joints.end(),
                        it->second.joints.begin(),it->second.joints.end());
                std::map<TwinningType,uint32_t>::iterator tit;
                for (tit=fit->second.twinning_weight.begin();tit!=fit->second.twinning_weight.end();tit++) {
                    std::map<TwinningType,uint32_t>::const_iterator titfrom = 
                        it->second.twinning_weight.find(tit->first);
                    if (titfrom != it->second.twinning_weight.end()) {
                        tit->second += titfrom->second;
                    }
                }
            }
        }
        // Removed as duplicated. True?
        // neighbours.insert(C.neighbours.begin(),C.neighbours.end());
    }
    updateStatistics(full_vertices);
    for (NeighbourMap::iterator it=neighbours.begin();
            it != neighbours.end(); it ++) {
        it->second.q_disorientation = QDisorientation(qmean,connected[it->first].qmean).second;
        it->second.embedded = it->second.weight > (SubGrainMinEnclosure * num_connections);
        it->second.updateTwinning(connected[it->first]);
    }
    rev_neighbours.clear();
    for (NeighbourMap::iterator it=neighbours.begin();
            it != neighbours.end(); it ++) {
        ConnectedNeighbour cn = it->second;
        cn.i = it->first;
        cn.j = this->i;
        cn.q_disorientation = QDisorientation(connected[it->first].qmean,qmean).second;
        cn.updateTwinning(*this);
        rev_neighbours.insert(NeighbourMap::value_type(cn.i,cn));
    }
    // printf("Twinstrip %d: %d neighbours, %d rev_neighbours\n",(int)this->i,(int)neighbours.size(),(int)rev_neighbours.size());

}

void TwinStrip::buildOrientationHistograms(const std::vector<Joint> & joints)
{
    Connected::buildOrientationHistograms(joints);
    NeighbourMap::iterator it;
    for (it=rev_neighbours.begin();it != rev_neighbours.end();it++) {
        it->second.orientation_histogram.clear();
        for (int alpha = 0; alpha * OrientationResolutionDeg <= 180 ; alpha ++) {
            it->second.orientation_histogram[(short)(alpha*OrientationResolutionDeg)] = 0;
            it->second.orientation_histogram[(short)(-alpha*OrientationResolutionDeg)] = 0;
        }
        for (size_t j=0;j<it->second.joints.size();j++) {
            const Joint & J = joints[it->second.joints[j]];
            for (size_t k=0;k<J.face_ids.size();k++) {
                float angle = get_orientation_at_face(J.face_ids[k])*180/M_PI;
                if (std::isnan(angle)) continue;
                float angle_discretized = round(round(angle/OrientationResolutionDeg)*OrientationResolutionDeg);
                // Relying on the default value of integers to be zero
                it->second.orientation_histogram[angle_discretized] += 1;
            }
        }
    }
}



