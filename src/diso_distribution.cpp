
#include <crystals/quaternion_tools.h>
#include <crystals/Vertex.h>

#include <stdio.h>
#include <QImage>

using namespace crystals;


int main() {
    initialise_quaternion_symmetries();
    MaterialType = MAT_MAGNESIUM;
    initialise_twinning_disorientations();

    QImage mesh(10*120,6*120,QImage::Format_RGB32);
    for (int ixl=0;ixl<6;ixl+=1) {
        for (int ixc=0;ixc<10;ixc+=1) {
            double x = 3. * (ixl * 10 + ixc);
            for (int iz=0;iz<120;iz+=1) {
                double z1 = iz * 3.;
                for (int jz=0;jz<120;jz+=1) {
                    double z2 = jz * 3.;
                    if ((iz==0)||(jz==0)) {
                        mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(255,255,255).rgb());
                        continue;
                    }
                    char line[1024];
                    sprintf(line," %e %e %e 0 0 0 0 0 0 0 phase",z1*M_PI/180.,x*M_PI/180.,z2*M_PI/180.);
                    // printf("%.2f %.2f %.2f\n",z1*M_PI/180.,x*M_PI/180.,z2*M_PI/180.);
                    Vertex V;
                    V.loadFromString(line,true);
                    DisorientationClass dc = classify_disorientation(V.q);
                    if (dc.error > TwinningTypeTolerance) {
                        mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128,128,128).rgb());
                        continue;
                    } 
                    switch (dc.type) {
                        case TWINNING_ZERO:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,0,0).rgb());
                            break;
                        case Mg_TWINNING_TENSILE:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+dc.variant*10,0,0).rgb());
                            break;
                        case Mg_TWINNING_COMPRESSIVE:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,128+dc.variant*10,0).rgb());
                            break;
                        case Mr_TWINNING_TYPE:
                            // printf("%d %d %d\n",dc.variant/4,dc.variant%4,128+15*(dc.variant%4));
                            switch (dc.variant/4) {
                                case 0:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+(dc.variant%4)*15,0,0).rgb());
                                    break;
                                case 1:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+(dc.variant%4)*15,128+(dc.variant%4)*15,0).rgb());
                                    break;
                                case 2:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,128+(dc.variant%4)*15,0).rgb());
                                    break;
                                case 3:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,128+(dc.variant%4)*15,128+(dc.variant%4)*15).rgb());
                                    break;
                                case 4:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,0,128+(dc.variant%4)*15).rgb());
                                    break;
                                case 5:
                                    mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+(dc.variant%4)*15,0,128+(dc.variant%4)*15).rgb());
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case Zr_TWINNING_TENSILE_1:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+dc.variant*10,0,0).rgb());
                            break;
                        case Zr_TWINNING_TENSILE_2:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,128+dc.variant*10,0).rgb());
                            break;
                        case Zr_TWINNING_COMPRESSIVE_1:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(0,0,128+dc.variant*10).rgb());
                            break;
                        case Zr_TWINNING_COMPRESSIVE_2:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128+dc.variant*10,128+dc.variant*10,0).rgb());
                            break;
                        default:
                            mesh.setPixel(ixc*120+iz,ixl*120+jz,QColor(128,128,128).rgb());
                            break;
                    }
                    
                }
            }
            mesh.save("Distribution.png","PNG");
        }
    }
    return 0;
}

