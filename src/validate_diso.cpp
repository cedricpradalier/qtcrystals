#include <LinearMath/Quaternion.h>
#include <LinearMath/Vector3.h>
#include <LinearMath/Matrix3x3.h>
#include <math.h>

#include "crystals/constants.h"
#include "crystals/Vertex.h"
#include "crystals/quaternion_tools.h"

using namespace crystals;

int main(int argc, char * argv[])
{
    initialise_quaternion_symmetries();
    MaterialType = MAT_ZIRCONIUM;
    initialise_twinning_disorientations();
    std::map<std::string,TwinningType> tmap;
    tmap["T1"] = Zr_TWINNING_TENSILE_1;
    tmap["T2"] = Zr_TWINNING_TENSILE_2;
    tmap["C1"] = Zr_TWINNING_COMPRESSIVE_1;
    tmap["C2"] = Zr_TWINNING_COMPRESSIVE_2;

    assert(argc>1);
    FILE *fp = fopen(argv[1],"r");
    unsigned int linecount = 0;
    while (!feof(fp)) {
        char line[1024]="";
        char parent[1024]="", type[6]="";
        if (fgets(line,1024,fp)==NULL) {
            break;
        }
        if (line[0] == '#') {
            continue;
        }
        float p_phi1=0,p_PHI=0,p_phi2=0;
        float t_phi1=0,t_PHI=0,t_phi2=0;
        int variant;
        if (sscanf(line," %s %e %e %e %s %e %e %e %d ",parent, &p_phi1,&p_PHI,&p_phi2, type, &t_phi1, &t_PHI, &t_phi2, &variant)!=9) {
            continue;
        }
        linecount += 1;
        TwinningType nt = tmap[type];
    
        Vertex v1, v2;
        char v1str[1024], v2str[1024];
        sprintf(v1str," %.6f %.6f %.6f 0.0 0.0 0.0 0.0 0.0 ", p_phi1*M_PI/180.0, p_PHI*M_PI/180.0,  p_phi2*M_PI/180.0);
        sprintf(v2str," %.6f %.6f %.6f 0.0 0.0 0.0 0.0 0.0 ", t_phi1*M_PI/180.0, t_PHI*M_PI/180.0, t_phi2*M_PI/180.0);
        assert(v1.loadFromString(v1str,true)); 
        assert(v2.loadFromString(v2str,true)); 

        std::pair<double,tf::Quaternion> r = QDisorientation(v1.q,v2.q);
        printf("Line: %d\n",(int)linecount);
        printf("Parent: %.2f %.2f %.2f Q1: %f %f %f %f\n",p_phi1,p_PHI,p_phi2,v1.q.x(),v1.q.y(),v1.q.z(),v1.q.w());
        printf("  Twin: %.2f %.2f %.2f Q2: %f %f %f %f\n",t_phi1,t_PHI,t_phi2,v2.q.x(),v2.q.y(),v2.q.z(),v2.q.w());
        printf("Disorientation: %f deg\n",r.first*180./M_PI);
        printf("Q: %f %f %f %f\n",r.second.x(),r.second.y(),r.second.z(),r.second.w());
        DisorientationClass dc = classify_disorientation(r.second,true);
        printf("Meaured type: %s variant: %d\n",twinning_type_str(dc.type), dc.variant+1);
        printf("Expected type: %s variant %d\n",twinning_type_str(nt),variant);
        if (nt != dc.type) {
            printf("\nINCORRECT TWINNING TYPE\n\n");
        } else if (dc.variant+1 != (unsigned)variant) {
            printf("\nINCORRECT TWINNING VARIANT\n\n");
        } else {
            printf("%s",dc.toString().c_str());
        }
        printf("\n\n");
    }

    return 0;
}

