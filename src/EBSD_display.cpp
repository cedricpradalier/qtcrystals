
#include <crystals/EBSD.h>
#include <crystals/log.h>
#ifndef WIN64
#include <unistd.h>
#endif
#include <boost/format.hpp>

#include <QRect>
#include <QSize>
#include <QPoint>
#include <QPen>
#include <QBrush>
#include <QPainter>

#define QUIET_QHULL

using boost::format;
using namespace crystals;


void EBSDAnalyzer::force_repaint() {
    change_zoom = true;
}

void EBSDAnalyzer::set_mesh_size(size_t w,size_t h,bool maximize) {
    size_t new_width=w,new_height=h;
    if (maximize) {
        new_width = w;
        new_height = ((y_max-y_min)*new_width)/(x_max-x_min);
        if (new_height > h) {
            new_height = h;
            new_width = ((x_max-x_min)*new_height)/(y_max-y_min);
        }
    }
    if ((width!=new_width)||(height!=new_height)) {
        width = (uint32_t)new_width;
        height = (uint32_t)new_height;
        mesh = QImage(width,height,QImage::Format_RGB32);
        display = QImage(width,height,QImage::Format_RGB32);
        mesh.fill(QColor(0,0,0));
        display.fill(QColor(0,0,0));
        change_zoom = true;
        // reset_zoom();
        LOG_INFO("Using grid aspect ratio: image size is %dx%d",(int)width,(int)height);
    }
}

void EBSDAnalyzer::reset_zoom() {
    change_zoom = true;
    x_org=x_min;
    y_org=y_min;
    x_scale=(x_max-x_min)/width;
    y_scale=(y_max-y_min)/height;
    if (x_scale < y_scale) {
        x_scale=y_scale;
    } else {
        y_scale=x_scale;
    }
    x_org -= (width - (x_max-x_min)/x_scale) * x_scale/2;
    y_org -= (height - (y_max-y_min)/y_scale) * y_scale/2;
}

void EBSDAnalyzer::zoom(double ratio) {
    change_zoom = true;
    x_scale /= ratio;
    y_scale /= ratio;
    x_org -= (width - width*ratio) * x_scale/2;
    y_org -= (height - height*ratio) * y_scale/2;
}

void EBSDAnalyzer::translate(int x, int y) {
    dx += x;
    dy += y;
    x_org += x * x_scale;
    y_org += y * y_scale;
}

void EBSDAnalyzer::pan_to(int x, int y) {
    x_org = x*(x_max-x_min)/width - width*x_scale/2;
    y_org = y*(y_max-y_min)/height - height*y_scale/2;
}

bool EBSDAnalyzer::pan_to_object() {
    int id = -1;
    char line[128]="";
    fflush(stdin);
    switch (graph_mode) {
        case NO_GRAPH:
            break;
        case GRAPH_TWINNING: 
        case GRAPH_CONNECTED: 
        case GRAPH_CONNECTED_TWINS: 
        case GRAPH_ELLIPSES: 
            printf("\nEnter ID of connected part to pan to: ");
            fflush(stdout);
            if (!fgets(line,128,stdin) || sscanf(line," %d",&id)<1) {
                break;
            }
            if ((id < 0) || (id > (int)connected.size())) {
                break;
            }
            x_org = connected[id].x - width*x_scale/2;
            y_org = connected[id].y - height*y_scale/2;
            return true;
        case GRAPH_HULLS: 
        case GRAPH_GRAINS:
            printf("\nEnter ID of Grain to pan to: ");
            fflush(stdout);
            if (!fgets(line,128,stdin) || sscanf(line," %d",&id)<1) {
                break;
            }
            if ((id < 0) || (id > (int)grains.size())) {
                break;
            }
            x_org = grains[id].x - width*x_scale/2;
            y_org = grains[id].y - height*y_scale/2;
            return true;
        case GRAPH_CLUSTERS:
            printf("\nEnter ID of Cluster to pan to: ");
            fflush(stdout);
            if (!fgets(line,128,stdin) || sscanf(line," %d",&id)<1) {
                break;
            }
            if ((id < 0) || (id > (int)clusters.size())) {
                break;
            }
            x_org = clusters[id].x - width*x_scale/2;
            y_org = clusters[id].y - height*y_scale/2;
            return true;
        default:
            break;
    }
    printf("\nInvalid ID or mode\n");
    return false;
}


bool EBSDAnalyzer::pan_to_object(int id) {
    switch (graph_mode) {
        case NO_GRAPH:
            break;
        case GRAPH_TWINNING: 
        case GRAPH_CONNECTED: 
        case GRAPH_CONNECTED_TWINS: 
        case GRAPH_ELLIPSES: 
            if ((id < 0) || (id > (int)connected.size())) {
                break;
            }
            x_org = connected[id].x - width*x_scale/2;
            y_org = connected[id].y - height*y_scale/2;
            return true;
        case GRAPH_HULLS: 
        case GRAPH_GRAINS:
            if ((id < 0) || (id > (int)grains.size())) {
                break;
            }
            x_org = grains[id].x - width*x_scale/2;
            y_org = grains[id].y - height*y_scale/2;
            return true;
        case GRAPH_CLUSTERS:
            if ((id < 0) || (id > (int)clusters.size())) {
                break;
            }
            x_org = clusters[id].x - width*x_scale/2;
            y_org = clusters[id].y - height*y_scale/2;
            return true;
        default:
            break;
    }
    return false;
}

void EBSDAnalyzer::zoom_to(int x, int y, double ratio) {
    double x_m = x_org + x * x_scale; 
    double y_m = y_org + y * y_scale; 
    x_scale /= ratio;
    y_scale /= ratio;
    x_org = x_m - x_scale * width/2;
    y_org = y_m - y_scale * height/2;
    change_zoom = true;
}

bool EBSDAnalyzer::handle_edge_click(int x, int y) {
    std::string out;
    bool res = handle_edge_click(x,y,out);
    printf("%s",out.c_str());
    return res;
}

bool EBSDAnalyzer::handle_edge_click(int x, int y,std::string & output) {
    QPoint Pm(x,y);
    format fql;
    switch (graph_mode) {
        case NO_GRAPH:
            {
                float xm = x_org+x*x_scale;
                float ym = y_org+y*y_scale;
                if (xm < x_min) break;
                if (xm > x_max) break;
                if (ym < y_min) break;
                if (ym > y_max) break;
                printf("No Graph: Mouse click on %d,%d -> %.3f,%.3f\n",x,y,xm,ym);
                int i_mesh, j_mesh;
                j_mesh = round((ym-y_min)/DeltaY);
                if (square_mesh) {
                    i_mesh = round((xm-x_min)/DeltaX);
                } else {
                    if (j_mesh % 2) {
                        i_mesh = 2*round((xm+DeltaX-x_min)/(2*DeltaX))-1;
                    } else {
                        i_mesh = 2*round((xm-x_min)/(2*DeltaX));
                    }
                }
                CoordMap::const_iterator it;
                it = coords.find(ICoord(i_mesh,j_mesh));
                // printf("%d %d -> %d %d  ",(int)i,(int)j,(int)i_mesh,(int)j_mesh);
                if (it != coords.end()) {
                    const Vertex & u = vertices[it->second];
                    output = str(format("Closest vertex: %d (%d %d)-> (%.03f %.03f)\nphi1 %.2f PHI %.2f phi2 %.2f\nQ w=%.2f v=[%.2f %.2f %.2f] \n%s %s Membership %d Grain %d\n%s") 
                            % it->second % i_mesh % j_mesh % u.x % u.y 
                            % (u.phi1*180/M_PI) % (u.PHI*180/M_PI) % (u.phi2*180/M_PI)
                            % u.q.w() % u.q.x() % u.q.y() % u.q.z()  
                            % (u.good?"Good":"Bad ") 
                            % (u.grain_border?"G. Border":(u.border?"C. Border":"  Inside")) 
                            % u.membership % u.grain % u.phase_name());
                }
            }
            break;
        case GRAPH_CLUSTERS:
            {
                float xm = x_org+x*x_scale;
                float ym = y_org+y*y_scale;
                if (xm < x_min) break;
                if (xm > x_max) break;
                if (ym < y_min) break;
                if (ym > y_max) break;
                printf("Twinning: Mouse click on %d,%d - %.3f,%.3f\n",x,y,xm,ym);
                int i_mesh, j_mesh;
                j_mesh = round((ym-y_min)/DeltaY);
                if (square_mesh) {
                    i_mesh = round((xm-x_min)/DeltaX);
                } else {
                    if (j_mesh % 2) {
                        i_mesh = 2*round((xm+DeltaX-x_min)/(2*DeltaX))-1;
                    } else {
                        i_mesh = 2*round((xm-x_min)/(2*DeltaX));
                    }
                }
                CoordMap::const_iterator it;
                it = coords.find(ICoord(i_mesh,j_mesh));
                if (it != coords.end()) {
                    const Vertex & u = vertices[it->second];
                    if (u.membership == 0) return false;
                    Connected & c = connected[u.membership];
                    Phase & f = phases[c.phase];
                    double phi1,PHI,phi2;
                    quaternionToBungeEuler(f.qmean,phi1,PHI,phi2,true);
                    output = str(format("Phase %d: %.2f %.2f statistics\n") % (int)f.id % f.x %f.y);
                    output += str(format("\tTwinning order %d Size: %d\n")%f.twinning_order%(int)f.size);
                    if (f.parent>=0) {
                        output += str(format("\tGrain %d Cluster %d Parent %d\n")%(int)f.grain%(int)f.cluster%f.parent);
                    } else {
                        output += str(format("\tGrain %d Cluster %d Is Parent\n")%(int)f.grain%(int)f.cluster);
                    }
                    output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% f.qmean.w()%f.qmean.x()%f.qmean.y()%f.qmean.z());
                    output += str(format("\tEuler: phi1=%.2f PHI=%.2f phi2=%.2f\n")% phi1%PHI%phi2);
                    output += str(format("\tNumber of parts %d, subphases %d\n")%(int)f.parts.size()%(int)f.subphases.size());
                }
                return false;
            }
            break;
        case GRAPH_ELLIPSES:
        case GRAPH_GRAINS:
            {
                float xm = x_org+x*x_scale;
                float ym = y_org+y*y_scale;
                if (xm < x_min) break;
                if (xm > x_max) break;
                if (ym < y_min) break;
                if (ym > y_max) break;
                printf("Twinning: Mouse click on %d,%d - %.3f,%.3f\n",x,y,xm,ym);
                int i_mesh, j_mesh;
                j_mesh = round((ym-y_min)/DeltaY);
                if (square_mesh) {
                    i_mesh = round((xm-x_min)/DeltaX);
                } else {
                    if (j_mesh % 2) {
                        i_mesh = 2*round((xm+DeltaX-x_min)/(2*DeltaX))-1;
                    } else {
                        i_mesh = 2*round((xm-x_min)/(2*DeltaX));
                    }
                }
                CoordMap::const_iterator it;
                it = coords.find(ICoord(i_mesh,j_mesh));
                if (it != coords.end()) {
                    const Vertex & u = vertices[it->second];
                    if (u.membership == 0) return false;
                    Connected & c = connected[u.membership];
                    Grain & g = grains[c.group];
                    if (graph_mode==GRAPH_GRAINS) {
                        if (g.parts.size()==1) return false;
                        if (c.manual_twin) {
                            c.manual_twin = false;
                        }
                        if (c.parent) {
                            c.manual_parent = false;
                            c.manual_twin = true;
                            return true;
                        }
                        // Now set the manually entered parent
                        for (size_t i=0;i<g.parts.size();++i) {
                            connected[g.parts[i]].manual_parent = false;
                        }
                        c.manual_parent = true;
                        c.parent = true;
                        LOG_INFO("Marked grain %d as manual parent (connected %d cluster %d)",
                                (int)g.i,(int)u.membership,(int)c.cluster);
                        return true;
                    } else {
                        double phi1,PHI,phi2;
                        tf::Matrix3x3 R;
                        R.setRotation(g.qmean);
                        quaternionToBungeEuler(g.qmean,phi1,PHI,phi2,true);
                        output = str(format("Grain %d: %.2f %.2f statistics\n") % (int)g.i % g.x %g.y);
                        output += str(format("\tArea %.2fum2 Size: %d\n")%g.area%(int)g.size);
                        output += str(format("\tBorder: length: %.2fum Count: %d\n")%g.border_length%(int)g.border_vertices.size());
                        output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% g.qmean.w()%g.qmean.x()%g.qmean.y()%g.qmean.z());
                        output += str(format("\tEuler: phi1=%.2f PHI=%.2f phi2=%.2f\n")% phi1%PHI%phi2);
                        output += str(format("\tMatrix: %6.2f %6.2f %6.2f\n")%R[0][0]%R[0][1]%R[0][2]);
                        output += str(format("\t        %6.2f %6.2f %6.2f\n")%R[1][0]%R[1][1]%R[1][2]);
                        output += str(format("\t        %6.2f %6.2f %6.2f\n")%R[2][0]%R[2][1]%R[2][2]);
                        output += str(format("\tConvexity %.2f\n")%g.convexity_ratio);
                        int n = 0;
                        for (size_t i=0;i<g.parts.size();++i) {
                            if (!connected[g.parts[i]].parent) {
                                ++n;
                            }
                        }
                        output += str(format("\tNumber of twins %d neighbours %d\n")%n%(int)g.neighbours.size());
                        if (!c.parent) {
                            if (replace_twinstrips && (c.twinstrip>0)) {
                                const TwinStrip & t = twinstrips[c.twinstrip-connected.size()]; 
                                quaternionToBungeEuler(t.qmean,phi1,PHI,phi2,true);
                                output += str(format("Twin %d: %.2f %.2f T.order %d statistics\n")%(int)c.i%c.x%c.y%c.twinning_order);
                                output += str(format("TwinStrip %d: %.2f %.2f T.order %d statistics\n")%(int)t.i%t.x%t.y%t.twinning_order);
                                output += str(format("\tLength: %.2fum Thickness: %.2fum Angle: %.2fdeg\n")%t.length%t.thickness%t.angle); 
                                output += str(format("\tArea: %.2fum2 Size: %d Border: %.2fum\n")%t.area%t.size()%t.border_length);
                                output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% t.qmean.w()%t.qmean.x()%t.qmean.y()%t.qmean.z());
                                output += str(format("\tEuler: phi1=%.2f PHI=%.2f phi2=%.2f\n")% phi1%PHI%phi2);
                                output += str(format("\tEllipsicity: %.2f\n")%t.ellipsicity);
                            } else {
                                quaternionToBungeEuler(c.qmean,phi1,PHI,phi2,true);
                                output += str(format("Twin %d: %.2f %.2f T.order %d statistics\n")%(int)c.i%c.x%c.y%c.twinning_order);
                                output += str(format("\tLength: %.2fum Thickness: %.2fum Angle: %.2fdeg\n")%c.length%c.thickness%c.angle); 
                                output += str(format("\tArea: %.2fum2 Size: %d Border: %.2fum\n")%c.area%c.size()%c.border_length);
                                output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% c.qmean.w()%c.qmean.x()%c.qmean.y()%c.qmean.z());
                                output += str(format("\tEuler: phi1=%.2f PHI=%.2f phi2=%.2f\n")% phi1%PHI%phi2);
                                output += str(format("\tEllipsicity: %.2f %s\n")%c.ellipsicity%(c.has_weird_parents?"Has weird parents":""));
                            }
                        } else {
                                quaternionToBungeEuler(c.qmean,phi1,PHI,phi2,true);
                                output += str(format("Connected %d: %.2f %.2f T.order %d statistics\n")%(int)c.i%c.x%c.y%c.twinning_order);
                                output += str(format("\tLength: %.2fum Thickness: %.2fum Angle: %.2fdeg\n")%c.length%c.thickness%c.angle); 
                                output += str(format("\tArea: %.2fum2 Size: %d Border: %.2fum\n")%c.area%c.size()%c.border_length);
                                output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% c.qmean.w()%c.qmean.x()%c.qmean.y()%c.qmean.z());
                                output += str(format("\tEuler: phi1=%.2f PHI=%.2f phi2=%.2f\n")% phi1%PHI%phi2);
                                output += str(format("\tEllipsicity: %.2f\n")%c.ellipsicity);
                        }
#if 0
                        // Now print the border points DEBUG
                        FILE *fp_g, *fp_c;
                        fp_g = fopen("/tmp/grain.txt","w");
                        for (size_t i=0;i<g.border_vertices.size();++i) {
                            const Vertex & v = vertices[g.border_vertices[i]];
                            fprintf(fp_g,"%e %e\n",v.x,v.y);
                        }
                        fclose(fp_g);
                        fp_c = fopen("/tmp/connected.txt","w");
                        for (size_t i=0;i<c.border_vertices.size();++i) {
                            const Vertex & v = vertices[c.border_vertices[i]];
                            fprintf(fp_c,"%e %e\n",v.x,v.y);
                        }
                        fclose(fp_c);
                        fp_c = fopen("/tmp/grain_joint.txt","w");
                        for (size_t i=0;i<g.contours.size();i++) {
                            const Contour & C = g.contours[i];
                            for (size_t j=0;j<C.face_ids.size();j++) {
                                    fprintf(fp_c,"%d %d %e %e %d\n",(int)i,(int)j,
                                            faces[C.face_ids[j]].x,faces[C.face_ids[j]].y,C.hole);
                            }
                            fprintf(fp_c,"\n");
                        }
                        fclose(fp_c);
                        fp_c = fopen("/tmp/conn_joint.txt","w");
                        for (size_t i=0;i<c.contours.size();i++) {
                            const Contour & C = c.contours[i];
                            for (size_t j=0;j<C.face_ids.size();j++) {
                                    fprintf(fp_c,"%d %d %e %e %d\n",(int)i,(int)j,
                                            faces[C.face_ids[j]].x,faces[C.face_ids[j]].y,C.hole);
                            }
                            fprintf(fp_c,"\n");
                        }
                        fclose(fp_c);
                        printf("Saved grain.txt and connected.txt in /tmp\n");
#endif
                    }
                }
            }
            break;
        case GRAPH_CONNECTED:
        case GRAPH_TWINNING:
            {
                bool best_ts = false;
                double closest_d = 10.0;
                Connected::NeighbourMap::iterator best_it;
                for (size_t i=0;i<connected.size();++i) {
                    if (!connected[i].valid) continue;
                    QPoint Pg((connected[i].x - x_org) / x_scale, (connected[i].y - y_org) / y_scale);
                    double dx = Pm.x() - Pg.x(),dy = Pm.y() - Pg.y();
                    Connected::NeighbourMap::iterator nit;
                    for (nit=connected[i].neighbours.begin();
                            nit != connected[i].neighbours.end();
                            nit++) {
                        if (!connected[nit->first].valid) continue;
                        if (connected[i].twinning_order > connected[nit->first].twinning_order) continue;
                        if (replace_twinstrips && (graph_mode==GRAPH_TWINNING)) {
                            if (connected[i].twinstrip>0) continue;
                            if (connected[nit->first].twinstrip>0) continue;
                        }
                        // if (nit->first < (signed)i) continue;
                        QPoint Pn((connected[nit->first].x - x_org) / x_scale, (connected[nit->first].y - y_org) / y_scale);
                        double ux = Pn.x() - Pg.x(),uy = Pn.y() - Pg.y();
                        double un = hypot(ux,uy);
                        double d = (ux*dx+uy*dy)/un;
                        if ((d < 0) || (d>un)) continue; // not on the segment
                        d = fabs((-uy*dx+ux*dy)/un);
                        if (d > 5) continue;
                        if (d < closest_d) {
                            closest_d = d;
                            best_it = nit;
                        }
                    }
                }
                if (replace_twinstrips && (graph_mode==GRAPH_TWINNING)) {
                    for (size_t i=0;i<twinstrips.size();++i) {
                        if (!twinstrips[i].valid) continue;
                        QPoint Pg((twinstrips[i].x - x_org) / x_scale, (twinstrips[i].y - y_org) / y_scale);
                        double dx = Pm.x() - Pg.x(),dy = Pm.y() - Pg.y();
                        Connected::NeighbourMap::iterator nit;
                        for (nit=twinstrips[i].rev_neighbours.begin();
                                nit != twinstrips[i].rev_neighbours.end();
                                nit++) {
                            QPoint Pn((connected[nit->second.i].x - x_org) / x_scale, (connected[nit->second.i].y - y_org) / y_scale);
                            double ux = Pn.x() - Pg.x(),uy = Pn.y() - Pg.y();
                            double un = hypot(ux,uy);
                            double d = (ux*dx+uy*dy)/un;
                            if ((d < 0) || (d>un)) continue; // not on the segment
                            d = fabs((-uy*dx+ux*dy)/un);
                            if (d > 5) continue;
                            if (d < closest_d) {
                                closest_d = d;
                                best_it = nit;
                                best_ts = true;
                            }
                        }
                    }
                }
                if (closest_d < 5.0) {
                    if (graph_mode == GRAPH_CONNECTED) {
                        // Mark as hand-labelled, in both directions
                        connected[best_it->second.i].neighbours[best_it->second.j].active = 
                            !connected[best_it->second.i].neighbours[best_it->second.j].active;
                        connected[best_it->second.i].neighbours[best_it->second.j].manual = true;

                        connected[best_it->second.j].neighbours[best_it->second.i].active = 
                            !connected[best_it->second.j].neighbours[best_it->second.i].active;
                        connected[best_it->second.j].neighbours[best_it->second.i].manual = true;
                        return true;
                    } else {
                            tf::Quaternion q = best_it->second.q_disorientation;
                            DisorientationClass diso = classify_disorientation(q,true);
                            double theta = q.getAngle();
                            tf::Vector3 v = q.getAxis();
                            printf("Disorientation2: \n%f %f %f %f\n",
                                    q.x(),q.y(),q.z(),q.w());
                        if (best_ts) {
                            // If the best info is a twinstrip, we need to
                            // reverse the link to make sure we put in relation
                            // to the parent
                            output += str(format("\nSelected edge from %d to %d (twinstrip)\n")%
                                    best_it->second.i%best_it->second.j);
                            output += str(format("\tDegrees at %d: %d at %d: %d Weight: %d\n")%
                                    best_it->second.i%
                                    (int)connected[best_it->second.i].neighbours.size()%
                                    best_it->second.j%
                                    (int)(twinstrips[best_it->second.j-connected.size()].neighbours.size())%
                                    (int)best_it->second.weight);
                        } else {
#if 0
                            tf::Quaternion q1 = connected[best_it->second.i].qmean;
                            tf::Quaternion q2 = connected[best_it->second.j].qmean;
                            printf("Q1: %f %f %f %f\n",q1.x(),q1.y(),q1.z(),q1.w());
                            printf("Q2: %f %f %f %f\n",q2.x(),q2.y(),q2.z(),q2.w());
                            std::pair<double,tf::Quaternion> r1 = QDisorientation(q1,q2);
                            printf("Disorientation1: %f\n%f %f %f %f\n",r1.first*180./M_PI,
                                    r1.second.x(),r1.second.y(),r1.second.z(),r1.second.w());
#endif
                            output += str(format("\nSelected edge from %d to %d\n")%
                                    best_it->second.i%best_it->second.j);
                            output += str(format("\tDegrees at %d: %d at %d: %d Weight: %d\n")%
                                    best_it->second.i%(int)connected[best_it->second.i].neighbours.size()%
                                    best_it->second.j%(int)connected[best_it->second.j].neighbours.size()%(int)best_it->second.weight);
                        }
                        output += str(format("\tQuaternion: w=%.2f v=[%.2f %.2f %.2f]\n")% q.w()%q.x()%q.y()%q.z());
                        output += str(format("\tRodrigues: theta=%.2f v=[%.2f %.2f %.2f]\n")% theta%v.x()%v.y()%v.z());
                        output += str(format("\tTwinning: %s error %.2f alpha %.2f deg beta %.2f deg\n")%
                                (best_it->second.twinning?"yes":"no")%(best_it->second.twinning_error*180/M_PI)%
                                (best_it->second.alpha*180/M_PI)% 
                                (best_it->second.beta*180/M_PI));
                        output += str(format("\tTwinning type: %s variant %d active %s\n")%
                                twinning_type_str(best_it->second.twinning_type)%
                                (best_it->second.twinning_variant)% (best_it->second.active?"yes":"no"));
                        output += "\tTwinning distribution:\n";
                        std::map<TwinningType,uint32_t>::const_iterator tit;
                        for (tit=best_it->second.twinning_weight.begin();tit!=best_it->second.twinning_weight.end();tit++) {
                            output += str(format("\t\t%7.2f%%: %s\n")%(100.*tit->second/(float)best_it->second.weight)%
                                    twinning_type_str(tit->first));
                        }
                        output += diso.toString();
                        output += "-----------------------------\n";
#if 0
                        FILE *fp = fopen("/tmp/orientations","w");
                        for (size_t i=0;i<best_it->second.joints.size();i++) {
                            const Joint & J = joints[best_it->second.joints[i]];
                            std::map<uint32_t,uint32_t>::const_iterator it;
                            // Check if we're looking at a connected part
                            // touching a hole.
                            size_t contour_number = 0;
                            while (contour_number < connected[best_it->second.j].contours.size()) {
                                const Contour & c = connected[best_it->second.j].contours[contour_number];
                                it = c.face_idx.find(J.face_ids[0]);
                                if (it != c.face_idx.end()) {
                                    break;
                                }
                                contour_number += 1;
                            }
                            assert(contour_number < connected[best_it->second.j].contours.size());
                            const Contour & C = connected[best_it->second.j].contours[contour_number];
                            for (size_t j=0;j<J.face_ids.size();j++) {
                                const Face & F = faces[J.face_ids[j]];
                                it = C.face_idx.find(J.face_ids[j]);
                                assert(it != C.face_idx.end());
                                fprintf(fp,"%d %d %e %e %e %e\n",(int)i,(int)j,F.x,F.y,
                                        C.orientation[it->second]*M_PI/180., best_it->second.alpha);
                            }
                            fprintf(fp,"\n");
                        }
                        fclose(fp);
#endif
                        return false;
                    }
                }
            }
            break;
        case GRAPH_CONNECTED_TWINS:
            {
                float xm = x_org+x*x_scale;
                float ym = y_org+y*y_scale;
                if (xm < x_min) break;
                if (xm > x_max) break;
                if (ym < y_min) break;
                if (ym > y_max) break;
                printf("Twinning: Mouse click on %d,%d -> %.3f,%.3f\n",x,y,xm,ym);
                int i_mesh, j_mesh;
                j_mesh = round((ym-y_min)/DeltaY);
                if (square_mesh) {
                    i_mesh = round((xm-x_min)/DeltaX);
                } else {
                    if (j_mesh % 2) {
                        i_mesh = 2*round((xm+DeltaX-x_min)/(2*DeltaX))-1;
                    } else {
                        i_mesh = 2*round((xm-x_min)/(2*DeltaX));
                    }
                }
                CoordMap::const_iterator it;
                it = coords.find(ICoord(i_mesh,j_mesh));
                if (it != coords.end()) {
                    const Vertex & u = vertices[it->second];
                    if (u.membership == 0) return false;
                    Connected & c = connected[u.membership];
                    if (!c.connected_twin) {
                        switch (ctwin_in_progress.type) {
                            case ConnectedTwins::UNINITIALIZED:
                                ctwin_in_progress.type = ConnectedTwins::UNCONNECTED;
                                ctwin_in_progress.i = u.membership;
                                break;
                            case ConnectedTwins::UNCONNECTED:
                                ctwin_in_progress.type = ConnectedTwins::CONNECTED;
                                ctwin_in_progress.j = u.membership;
                                if (ctwin_in_progress.j == ctwin_in_progress.i) {
                                    ctwin_in_progress.type = ConnectedTwins::SINGLE;
                                }
                                connected[ctwin_in_progress.i].connected_twin = true;
                                connected[ctwin_in_progress.j].connected_twin = true;
                                ctwins.push_back(ctwin_in_progress);
                                ctwin_in_progress.type = ConnectedTwins::UNINITIALIZED;
                                break;
                            default:
                                LOG_ERROR("Invalid state for connected twins in progress");
                                break;
                        }
                    } else {
                        switch (ctwin_in_progress.type) {
                            case ConnectedTwins::UNINITIALIZED:
                                {
                                    // Breaking a link
                                    std::vector< ConnectedTwins > new_ctwins;
                                    for (size_t i=0;i<ctwins.size();++i) {
                                        if (((int)ctwins[i].i!=c.i) && ((int)ctwins[i].j!=c.i)) {
                                            new_ctwins.push_back(ctwins[i]);
                                        }
                                    }
                                    ctwins = new_ctwins;
                                    c.connected_twin = false;
                                }
                                break;
                            case ConnectedTwins::UNCONNECTED:
                                ctwin_in_progress.type = ConnectedTwins::CONNECTED;
                                ctwin_in_progress.j = u.membership;
                                if (ctwin_in_progress.j == ctwin_in_progress.i) {
                                    ctwin_in_progress.type = ConnectedTwins::SINGLE;
                                }
                                connected[ctwin_in_progress.i].connected_twin = true;
                                connected[ctwin_in_progress.j].connected_twin = true;
                                ctwins.push_back(ctwin_in_progress);
                                ctwin_in_progress.type = ConnectedTwins::UNINITIALIZED;
                                break;
                            default:
                                LOG_ERROR("Invalid state for connected twins in progress");
                                break;
                        }

                    }
                    return true;
                }
            }
            break;
        default:
            break;
    }
    return false;
}

bool EBSDAnalyzer::isInFrame(const QPoint & P,bool strict) const {
    if (strict) {
        if (P.x() < 0) return false;
        if (P.x() >= (int)width) return false;
        if (P.y() < 0) return false;
        if (P.y() >= (int)height) return false;
    } else {
        if (P.x() < -((int)width/2)) return false;
        if (P.x() >= (3*(int)width)/2) return false;
        if (P.y() < -((int)height/2)) return false;
        if (P.y() >= (3*(int)height/2)) return false;
    }
    return true;
}

void EBSDAnalyzer::drawId(QPainter & painter,const QPoint & P, int id,bool shift) {
    QPoint Pg = P;
    char tmp[32]="";
    sprintf(tmp,"%d",id);
    if (shift) {
        Pg -= QPoint((int)(3 + 5*strlen(tmp)/2), 5);
    } 
    QPen bg(QColor(0xC0,0xC0,0xC0));
    QPen fg(QColor(0x00,0x00,0x00));
    painter.setPen(bg);
    painter.setFont(idFontFg);
    painter.drawText(Pg-QPoint(-1,-1),QString(tmp));
    painter.drawText(Pg-QPoint(-1,1),QString(tmp));
    painter.drawText(Pg-QPoint(1,-1),QString(tmp));
    painter.drawText(Pg-QPoint(1,1),QString(tmp));
    painter.setPen(fg);
    painter.drawText(Pg,QString(tmp));
}

void EBSDAnalyzer::drawLine(QPainter & painter,const QPoint & P1, const QPoint & P2, const QColor & color, int width, bool dashed) {
    QPen pen(color,width,(dashed?Qt::DashLine:Qt::SolidLine));
    painter.setPen(pen);
    painter.drawLine(P1,P2);
}

void EBSDAnalyzer::drawCircle(QPainter & painter,const QPoint & P1, int radius, const QColor & color, int width) {
    if (width <= 0) {
        QPen pen(color);
        pen.setWidth(1);
        QBrush brush(color);
        painter.setPen(pen);
        painter.setBrush(brush);
        painter.drawEllipse(P1,radius,radius);
    } else {
        QPen pen(color);
        pen.setWidth(width);
        painter.setPen(pen);
        painter.drawEllipse(P1,radius,radius);
    }
}

void EBSDAnalyzer::drawEllipse(QPainter & painter,const QPoint & C, const QSize & S, 
        int angle_deg, const QColor & color, int width) {
    QPen pen(color);
    pen.setWidth(width);
    painter.setBrush(QBrush());
    painter.setPen(pen);
    painter.save();
    painter.translate(C);
    painter.rotate(angle_deg);
    painter.drawEllipse(QPoint(),S.width(),S.height());
    painter.restore();
}

bool EBSDAnalyzer::update_image(GraphModes with_graph,bool ignore_map_edge,bool draw_ids) {
    graph_mode = with_graph;
    float fradius = square_mesh?(0.3*MeshSize/x_scale):(0.3*MeshSize/x_scale);
    int radius = ceil(fradius);
    size_t x_start = 0, x_end = width;
    size_t y_start = 0, y_end = height;
#if 1
    if (!change_zoom && (dx || dy)) {
        // There was only a translation
        QRect dest(0,0,width,height);
        QRect src(0,0,width,height);
        if (dx < 0) {
            src=QRect(0,src.y(),width + dx, src.height());
            dest=QRect(-dx,dest.y(),src.width(), dest.height());
            x_start = 0; x_end = -dx;
        } else if (dx > 0) {
            src=QRect(dx,src.y(),width - dx, src.height());
            dest=QRect(0,dest.y(),src.width(), dest.height());
            x_start = width-dx; x_end = width;
        }
        if (dy < 0) {
            src=QRect(src.x(),0,src.width(),height+dy);
            dest=QRect(dest.x(),-dy,dest.width(), src.height());
            y_start = 0; y_end = -dy;
        } else if (dy > 0) {
            src=QRect(src.x(),dy,src.width(),height-dy);
            dest=QRect(dest.x(),0,dest.width(), src.height());
            y_start = height-dy; y_end = height;
        }
        QImage new_mesh(width,height,QImage::Format_RGB32);
        new_mesh.fill(QColor(0,0,0));
        QPainter painter(&new_mesh);
        painter.drawImage(dest.topLeft(), mesh.copy(src));
        painter.end();
        mesh = new_mesh;
    } else {
        mesh.fill(QColor(0,0,0));
    }
#else
    mesh = ColorV(0,0,0);
#endif
    QPainter painter(&mesh);
    if (graph_mode == GRAPH_GRAIN_JOINTS) {
        for (size_t i=0;i<edges.size();++i) {
            const Edge & e = edges[i];
            // if (e.weight < 1e-2) continue;
            const Vertex & v1 = vertices[e.i];
            const Vertex & v2 = vertices[e.j];
            QPoint P1((v1.x - x_org) / x_scale, (v1.y - y_org) / y_scale);
            if (!isInFrame(P1)) continue;
            QPoint P2((v2.x - x_org) / x_scale, (v2.y - y_org) / y_scale);
            if (!isInFrame(P2)) continue;
            TwinningType ttype = e.twinning_type;
            if ((ttype == TWINNING_ZERO) && (e.twinning_error >= EdgeTolerance)) {
                ttype = NO_TWINNING;
            } else if (e.twinning_error >= TwinningTypeTolerance) {
                ttype = NO_TWINNING;
            }
            if (!v1.good || !v2.good) {
                if (v1.map_edge && v2.map_edge) {
                    drawLine(painter,P1,P2,Color(255,255,255),2);
                } else if (v1.grain_border && v2.grain_border) {
                    if (v1.membership!=v2.membership) {
                        drawLine(painter,P1,P2,Color(0,255,255),2);
                    // } else {
                    //     drawLine(painter,P1,P2,Color(0,128,128),2);
                    }
                } else if (v1.border && v2.border) {
                    if (v1.membership!=v2.membership) {
                        drawLine(painter,P1,P2,Color(128,128,128),2);
                    }
                }
            } else switch (ttype) {
                case Zr_TWINNING_TENSILE_2:
                    drawLine(painter,P1,P2,Color(255,0,255),2);
                    break;
                case Zr_TWINNING_COMPRESSIVE_2:
                    drawLine(painter,P1,P2,Color(255,0,0),2);
                    break;
                case Zr_TWINNING_COMPRESSIVE_1:
                case Mg_TWINNING_COMPRESSIVE:
                    drawLine(painter,P1,P2,Color(0,0,255),2);
                    break;
                case Zr_TWINNING_TENSILE_1:
                case Mg_TWINNING_TENSILE:
                    drawLine(painter,P1,P2,Color(0,255,0),2);
                    break;
                case NO_TWINNING:
                    if (v1.map_edge && v2.map_edge) {
                        drawLine(painter,P1,P2,Color(255,255,255),2);
                    } else if (v1.grain_border && v2.grain_border) {
                        if (v1.membership!=v2.membership) {
                            drawLine(painter,P1,P2,Color(0,255,255),2);
                        // } else {
                        //     drawLine(painter,P1,P2,Color(0,128,128),2);
                        }
                    } else if (v1.border && v2.border) {
                        if (v1.membership!=v2.membership) {
                            drawLine(painter,P1,P2,Color(128,128,128),2);
                        } 
                    }
                    break;
                case TWINNING_ZERO:
                default:
                    if (v1.map_edge && v2.map_edge) {
                        drawLine(painter,P1,P2,Color(255,255,255),2);
                    }
                    break;
            }
        }
    } else if ((MeshSize / x_scale)<10) {
        // When the distance inter measurement is smaller than 10 pixels we just
        // render the map as an image
        for (size_t j=y_start;j<y_end;++j) {
            for (size_t i=x_start;i<x_end;++i) {
                double x=x_org+i*x_scale, y=y_org+j*y_scale;
                if (x < x_min) continue;
                if (x > x_max) continue;
                if (y < y_min) continue;
                if (y > y_max) continue;
                int i_mesh, j_mesh;
                j_mesh = round((y-y_min)/DeltaY);
                if (square_mesh) {
                    i_mesh = round((x-x_min)/DeltaX);
                } else {
                    if (j_mesh % 2) {
                        i_mesh = 2*round((x+DeltaX-x_min)/(2*DeltaX))-1;
                    } else {
                        i_mesh = 2*round((x-x_min)/(2*DeltaX));
                    }
                }
                CoordMap::const_iterator it;
                it = coords.find(ICoord(i_mesh,j_mesh));
                // printf("%d %d -> %d %d  ",(int)i,(int)j,(int)i_mesh,(int)j_mesh);
                if (it != coords.end()) {
                    bool too_small = false;
                    bool no_phase = false;
                    if (!ignore_map_edge && vertices[it->second].membership>=0) {
                        const Connected & G = connected[vertices[it->second].membership];
                        if (G.group >= 0) {
                            too_small = sqrt(grains[G.group].area/M_PI) < min_grain_size;
                        }
                        if (G.phase < 0) {
                            no_phase = true;
                        }
                    }
                    if (too_small) {
                        mesh.setPixel((int)i,(int)j,QColor(0,0,0).rgb());
                    } else if (no_phase) {
                        mesh.setPixel((int)i,(int)j,QColor(255,255,255).rgb());
                    } else if (vertices[it->second].membership>=0 && with_graph) {
                        if (connected[vertices[it->second].membership].parent && ((j>>1)%2) && ((i>>1)%2) ) {
                            mesh.setPixel((int)i,(int)j,QColor(0,0,0).rgb());
                        } else {
                            mesh.setPixel((int)i,(int)j,QColor(connected[vertices[it->second].membership].color).rgb());
                        }
                    } else if (vertices[it->second].good) {
                        mesh.setPixel((int)i,(int)j,QColor(vertices[it->second].color).rgb());
                    }
                }
            }
        }
    } else {
        // When we've zoomed enough, display the vertices and their relation to
        // their neighbours
        for (size_t i=0;i<edges.size();++i) {
            const Edge & e = edges[i];
            // if (e.weight < 1e-2) continue;
            const Vertex & v1 = vertices[e.i];
            const Vertex & v2 = vertices[e.j];
            if (!v1.good || !v2.good) continue;
            QPoint P1((v1.x - x_org) / x_scale, (v1.y - y_org) / y_scale);
            if (!isInFrame(P1)) continue;
            QPoint P2((v2.x - x_org) / x_scale, (v2.y - y_org) / y_scale);
            if (!isInFrame(P2)) continue;
            int thickness = round(fradius*e.weight/3);
            if (graph_mode!=NO_GRAPH) {
                TwinningType ttype = e.twinning_type;
                if ((ttype == TWINNING_ZERO) && (e.twinning_error >= EdgeTolerance)) {
                    ttype = NO_TWINNING;
                } else if (e.twinning_error >= TwinningTypeTolerance) {
                    ttype = NO_TWINNING;
                }
                switch (ttype) {
                    case TWINNING_ZERO:
                        drawLine(painter,P1,P2,Color(255,255,255),thickness);
                        break;
                    case Zr_TWINNING_TENSILE_2:
                        drawLine(painter,P1,P2,Color(255,0,255),2);
                        break;
                    case Zr_TWINNING_COMPRESSIVE_2:
                        drawLine(painter,P1,P2,Color(255,0,0),2);
                        break;
                    case Zr_TWINNING_COMPRESSIVE_1:
                    case Mg_TWINNING_COMPRESSIVE:
                        drawLine(painter,P1,P2,Color(0,0,255),2);
                        break;
                    case Zr_TWINNING_TENSILE_1:
                    case Mg_TWINNING_TENSILE:
                        drawLine(painter,P1,P2,Color(0,255,0),2);
                        break;
                    case NO_TWINNING:
                    default:
                        break;
                }
            } else if (e.isSolid(vertices)) {
                drawLine(painter,P1,P2,Color(255,255,255),thickness);
            }
        }
        for (size_t i=0;i<vertices.size();++i) {
            QPoint Pg((vertices[i].x - x_org) / x_scale, (vertices[i].y - y_org) / y_scale);
            if (!isInFrame(Pg)) continue;
            if (vertices[i].good) {
                drawCircle(painter, Pg, radius, vertices[i].color, -1);
            } else {
                drawCircle(painter, Pg, radius, Color(0,0,0), -1);
                drawCircle(painter, Pg, radius, vertices[i].color, 1);
            }
        }
    }
    for (size_t i=0;i<joints.size();i++) {
        const Joint & J = joints[i];
        for (size_t j=1;j<J.face_ids.size();j++) {
            QPoint P1 = toImage(faces[J.face_ids[j-1]]);
            if (!isInFrame(P1)) continue;
            QPoint P2 = toImage(faces[J.face_ids[j]]);
            if (!isInFrame(P2)) continue;
            bool map_edge = !ignore_map_edge && grains[J.grain_1].map_edge && grains[J.grain_2].map_edge;
            if (J.grain_joint && !map_edge && displayGrainJoint) {
                drawLine(painter,P1,P2,Color(0,255,255),((MeshSize / x_scale)>=10)?2:1);
            } else if (J.connected_joint) {
                if (displayTwinJoint) {
                    drawLine(painter,P1,P2,Color(0,0,127),((MeshSize / x_scale)>=10)?2:1);
                }
            } else {
                if (displayTwinJoint) {
                    drawLine(painter,P1,P2,Color(255,255,255),((MeshSize / x_scale)>=10)?2:1);
                }
            }
        }
        if (displayTwinJoint || displayGrainJoint) {
            QPoint Pf = toImage(faces[J.face_ids[0]]);
            if (!isInFrame(Pf)) continue;
            drawCircle(painter, Pf, std::max(radius/2,1), Color(0,255,0), -1);
            if (J.face_ids.size()>1) {
                QPoint Pt = toImage(faces[J.face_ids[J.face_ids.size()-1]]);
                drawCircle(painter, Pt, std::max(radius/2,1), Color(0,255,0), -1);
            }
        }
    }
    switch (with_graph) {
        case NO_GRAPH:
            break;
        case GRAPH_ELLIPSES:
            for (int bad=0;bad<2;++bad) {
                int thickness[2] = {1,2};
                QColor color[2] = {QColor(0,255,0),QColor(255,0,0)};
                for (size_t c=0;c<connected.size();c++) {
                    const Connected & C = connected[c];
                    if (C.parent) continue;
                    if (replace_twinstrips && (C.twinstrip>0)) continue;
                    if (!C.valid) continue;
                    if (bad ^ (fabs(C.ellipsicity) < 0.5)) {
                        continue;
                    }
                    QPoint Pg = toImage(C);
                    if (!isInFrame(Pg)) continue;
                    QSize Sz(C.length/x_scale/2, C.thickness/x_scale/2);
                    drawEllipse(painter, Pg, Sz, C.angle * 180./M_PI, color[bad] , thickness[bad]);
                    if (draw_ids) {
                        drawId(painter,Pg,(int)c);
                    }
                }
            }
            if (replace_twinstrips) {
                for (size_t t=0;t<twinstrips.size();++t) {
                    const TwinStrip & T = twinstrips[t];
                    QPoint Pg = toImage(T);
                    if (!isInFrame(Pg)) continue;
                    QSize Sz(T.length/x_scale/2, T.thickness/x_scale/2);
                    drawEllipse(painter, Pg, Sz, T.angle * 180./M_PI, Color(255,0,255),2);
                    if (draw_ids) {
                        drawId(painter,Pg,T.i);
                    }
                }
            } else {
                for (size_t t=0;t<twinstrips.size();++t) {
                    const TwinStrip & T = twinstrips[t];
                    if (T.parts.size()<=1) continue;
                    for (size_t j=1;j<T.parts.size();++j) {
                        const Connected & C1 = connected[T.parts[j-1]];
                        const Connected & C2 = connected[T.parts[j]];
                        QPoint P1 = toImage(C1);
                        if (!isInFrame(P1)) continue;
                        QPoint P2 = toImage(C2);
                        if (!isInFrame(P2)) continue;
                        drawLine(painter,P1,P2,Color(255,0,255),2);
                    }
                }
            }
#if 0
            // debug to visualize closest points
            // works but does not look nice
            for (size_t c=0;c<connected.size();c++) {
                const Connected & C = connected[c];
                if (C.parent) continue;
                if (!C.valid) continue;
                std::map<uint32_t,Connected::ClosestPoint>::const_iterator diti,ditj;
                for (diti=C.ingrain_distance.begin();diti!=C.ingrain_distance.end();diti++) {
                    if (diti->first >= c) continue;
                    ditj = connected[diti->first].ingrain_distance.find(c);
                    assert(ditj != connected[diti->first].ingrain_distance.end());
                    QPoint Pi,Pj;
                    Pi = toImage(diti->second);
                    Pj = toImage(ditj->second);
                    if (!isInFrame(Pi)) continue;
                    if (!isInFrame(Pj)) continue;
                    drawLine(painter,Pi,Pj,Color(255,0,0),2);
                }
            }
#endif
            break;
        case GRAPH_HULLS:
            for (int bad=0;bad<2;++bad) {
                QColor color[2] = {QColor(0,255,0),QColor(255,0,0)};
                for (size_t c=0;c<grains.size();c++) {
                    if (bad ^ (grains[c].convexity_ratio < 0.9)) {
                        continue;
                    }
                    QPoint Pg = toImage(grains[c]);
                    drawCircle(painter, Pg, 5, Color(0,0,0),-1);
                    if (draw_ids) {
                        drawId(painter,Pg,(int)c);
                    }
                    for (size_t i=0;i<grains[c].border_hull.size();++i) {
#ifdef USE_PCL
                        QPoint P1 = toImage(grains[c].border_hull[i]);
                        QPoint P2 = toImage(grains[c].border_hull[(i+1)%grains[c].border_hull.size()]);
#endif
#ifdef USE_CGAL
                        QPoint P1 = toImageF(to_inexact(grains[c].border_hull[i]));
                        QPoint P2 = toImageF(to_inexact(grains[c].border_hull[(i+1)%grains[c].border_hull.size()]));
#endif
#ifdef CRYSTAL_POLYGON_DEFINED
                        if (!isInFrame(P1,false)) continue;
                        if (!isInFrame(P2,false)) continue;
                        drawLine(painter,P1,P2,color[bad],2);
#endif
                    }
                }
            }
            break;
        case GRAPH_CONNECTED_TWINS:
            // First draw detected point of contact between non-parent
            // connected components.
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                if (connected[i].parent) continue;
                if (replace_twinstrips && (connected[i].twinstrip>0)) continue;
                QPoint Pg = toImage(connected[i]);
                if (!isInFrame(Pg,false)) continue;
                Connected::NeighbourMap::iterator nit;
                for (nit=connected[i].neighbours.begin();
                        nit != connected[i].neighbours.end();
                        nit++) {
                    if (!connected[nit->first].valid) continue;
                    if (connected[nit->first].parent) continue;
                    if (replace_twinstrips && (connected[nit->first].twinstrip>0)) continue;
                    if (connected[nit->first].group != connected[i].group) continue;
                    if (nit->first <= i) continue;
                    QPoint Pn = toImage(connected[nit->first]);
                    if (!isInFrame(Pn,false)) continue;
                    drawCircle(painter, Pg, 4, Color(255,0,255),-1);
                    drawCircle(painter, Pn, 4, Color(255,0,255),-1);
                    drawLine(painter,Pg,Pn,Color(255,0,255),2);
                }
            }
            if (replace_twinstrips) {
                for (size_t i=0;i<twinstrips.size();++i) {
                    QPoint Pg = toImage(twinstrips[i]);
                    if (!isInFrame(Pg,false)) continue;
                    Connected::NeighbourMap::iterator nit;
                    for (nit=twinstrips[i].neighbours.begin();
                            nit != twinstrips[i].neighbours.end();
                            nit++) {
                        if (connected[nit->first].parent) continue;
                        if (connected[nit->first].group != twinstrips[i].group) continue;
                        QPoint Pn = toImage(connected[nit->first]);
                        if (!isInFrame(Pn,false)) continue;
                        drawCircle(painter, Pg, 4, Color(255,0,255),-1);
                        drawCircle(painter, Pn, 4, Color(255,0,255),-1);
                        drawLine(painter,Pg,Pn,Color(255,0,255),2);
                    }
                }
            }
            // Then draw the manually marked contact relations. 
            for (size_t i=0;i<ctwins.size();++i) {
                if (ctwins[i].type == ConnectedTwins::SINGLE) {
                    QPoint Pi = toImage(connected[ctwins[i].i]);
                    if (!isInFrame(Pi,false)) continue;
                    if (replace_twinstrips && (connected[ctwins[i].i].twinstrip>0)) {
                        int ts = connected[ctwins[i].i].twinstrip;
                        Pi = toImage(twinstrips[ts]);
                    }
                    drawCircle(painter, Pi, 4, Color(255,0,0),-1);
                    drawCircle(painter, Pi, 8, Color(255,0,0),1);
                } else {
                    QPoint Pi = toImage(connected[ctwins[i].i]);
                    if (!isInFrame(Pi,false)) continue;
                    QPoint Pj = toImage(connected[ctwins[i].j]);
                    if (!isInFrame(Pj,false)) continue;
                    if (replace_twinstrips && (connected[ctwins[i].i].twinstrip>0)) {
                        int ts = connected[ctwins[i].i].twinstrip;
                        Pi = toImage(twinstrips[ts]);
                    }
                    if (replace_twinstrips && (connected[ctwins[i].j].twinstrip>0)) {
                        int ts = connected[ctwins[i].j].twinstrip;
                        Pj = toImage(twinstrips[ts]);
                    }
                    drawCircle(painter, Pi, 4, Color(255,0,0),-1);
                    drawCircle(painter, Pj, 4, Color(255,0,0),-1);
                    drawLine(painter,Pi,Pj,Color(255,0,0),2);
                }
            }
            // Finally draw the connection in progress for visualization.
            if (ctwin_in_progress.type == ConnectedTwins::UNCONNECTED) {
                QPoint Pi = toImage(connected[ctwin_in_progress.i]);
                if (replace_twinstrips && (connected[ctwin_in_progress.i].twinstrip>0)) {
                    int ts = connected[ctwin_in_progress.i].twinstrip;
                    Pi = toImage(twinstrips[ts]);
                }
                drawCircle(painter, Pi, 4, Color(0,255,0),-1);
                drawCircle(painter, Pi, 8, Color(0,255,0),1);
            }
            if (draw_ids) {
                for (size_t c=0;c<connected.size();c++) {
                    const Connected & C = connected[c];
                    if (C.parent) continue;
                    if (!C.valid) continue;
                    if (replace_twinstrips && (C.twinstrip>0))  continue;
                    QPoint Pg = toImage(C);
                    drawId(painter,Pg,(int)c);
                }
                if (replace_twinstrips) {
                    for (size_t c=0;c<twinstrips.size();c++) {
                        const TwinStrip & C = twinstrips[c];
                        QPoint Pg = toImage(C);
                        drawId(painter,Pg,C.i);
                    }
                }
            }
            break;
        case GRAPH_GRAINS:
            for (size_t c=0;c<grains.size();c++) {
                QPoint Pg = toImage(grains[c]);
                if (!isInFrame(Pg,false)) continue;
                Grain::NeighbourMap::iterator nit;
                for (nit=grains[c].neighbours.begin();
                        nit != grains[c].neighbours.end();
                        nit++) {
                    if (nit->first < c) continue;
                    QPoint Pn = toImage(grains[nit->first]);
                    if (!isInFrame(Pn,false)) continue;
                    drawLine(painter,Pg,Pn,Color(255,0,0),1);
                }
                if (grains[c].map_edge) {
                    drawCircle(painter, Pg, 4, Color(92,92,92),-1);
                } else {
                    drawCircle(painter, Pg, 4, Color(0,0,0),-1);
                }
                if (draw_ids) {
                    drawId(painter,Pg,(int)c);
                }
            }
            break;
        case GRAPH_CONNECTED:
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                QPoint Pg = toImage(connected[i]);
                if (!isInFrame(Pg,false)) continue;
                Connected::NeighbourMap::iterator nit;
                for (nit=connected[i].neighbours.begin();
                        nit != connected[i].neighbours.end();
                        nit++) {
                    if (!connected[nit->first].valid) continue;
                    if (nit->first < i) continue;
                    QPoint Pn = toImage(connected[nit->first]);
                    if (!isInFrame(Pn,false)) continue;
                    int sg_type = (int)(nit->second.twinning_type);
                    int w = (nit->second.active)?3:1;
                    bool dashed = nit->second.ignore_for_phases;
                    bool C = colorTwinningLinks || nit->second.active;
                    QColor gray = Color(127,127,127);
                    switch (sg_type) {
                        case TWINNING_ZERO:
                            drawLine(painter,Pg,Pn,C?Color(0,0,0):gray,w,dashed);
                            break;
                        case Zr_TWINNING_TENSILE_2:
                            drawLine(painter,Pg,Pn,C?Color(255,0,255):gray,w,dashed);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_2:
                            drawLine(painter,Pg,Pn,C?Color(255,0,0):gray,w,dashed);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_1:
                        case Mg_TWINNING_COMPRESSIVE:
                            drawLine(painter,Pg,Pn,C?Color(0,0,255):gray,w,dashed);
                            break;
                        case Zr_TWINNING_TENSILE_1:
                        case Mg_TWINNING_TENSILE:
                            drawLine(painter,Pg,Pn,C?Color(0,255,0):gray,w,dashed);
                            break;
                        default:
                            if (nit->second.active) {
                                drawLine(painter,Pg,Pn,Color(255,255,255),w,dashed);
                            } else {
                                drawLine(painter,Pg,Pn,gray,w);
                            }
                            break;
                    }
                }
            }
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                QPoint Pg = toImage(connected[i]);
                switch (connected[i].twinning_order) {
                    case 0:
                        drawCircle(painter, Pg, 2, Color(0,255,255),-1);
                        break;
                    case 1:
                        drawCircle(painter, Pg, 2, Color(255,255,0),-1);
                        break;
                    case 2:
                        drawCircle(painter, Pg, 5, Color(255,0,0),-1);
                        break;
                    case 3:
                    default:
                        drawCircle(painter, Pg, 2*connected[i].twinning_order, Color(0,0,255),-1);
                        break;
                }
                if (draw_ids) {
                    drawId(painter,Pg,(int)i);
                }
            } 
            break;
        case GRAPH_GRAIN_JOINTS:
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                QPoint Pg = toImage(connected[i]);
                if (!isInFrame(Pg,false)) continue;
                Connected::NeighbourMap::iterator nit;
                for (nit=connected[i].neighbours.begin();
                        nit != connected[i].neighbours.end();
                        nit++) {
                    if (!connected[nit->first].valid) continue;
                    if (nit->first < i) continue;
                    QPoint Pn = toImage(connected[nit->first]);
                    if (!isInFrame(Pn,false)) continue;
                    int sg_type = (int)(nit->second.twinning_type);
                    int w = 1;
                    switch (sg_type) {
                        case TWINNING_ZERO:
                            drawLine(painter,Pg,Pn,Color(0,0,0),w);
                            break;
                        case Zr_TWINNING_TENSILE_2:
                            drawLine(painter,Pg,Pn,Color(255,0,255),w);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_2:
                            drawLine(painter,Pg,Pn,Color(255,0,0),w);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_1:
                        case Mg_TWINNING_COMPRESSIVE:
                            drawLine(painter,Pg,Pn,Color(0,0,255),w);
                            break;
                        case Zr_TWINNING_TENSILE_1:
                        case Mg_TWINNING_TENSILE:
                            drawLine(painter,Pg,Pn,Color(0,255,0),w);
                            break;
                        default:
                            break;
                    }
                }
            }
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                QPoint Pg = toImage(connected[i]);
                switch (connected[i].twinning_order) {
                    case 0:
                        drawCircle(painter, Pg, 4, Color(0,255,255),-1);
                        break;
                    case 1:
                        drawCircle(painter, Pg, 4, Color(255,255,0),-1);
                        break;
                    case 2:
                        drawCircle(painter, Pg, 4, Color(255,0,0),-1);
                        break;
                    case 3:
                    default:
                        drawCircle(painter, Pg, 4, Color(0,0,255),-1);
                        break;
                }
                if (draw_ids) {
                    drawId(painter,Pg,(int)i);
                }
            } 
            break;
        case GRAPH_TWINNING:
            for (int draw_all=0;draw_all<2;draw_all++) {
                for (size_t i=0;i<connected.size();++i) {
                    const Connected & C = connected[i];
                    if (!C.valid) continue;
                    if (replace_twinstrips && (C.twinstrip>0)) continue;
                    QPoint Pg = toImage(C);
                    if (!isInFrame(Pg,false)) continue;
                    Connected::NeighbourMap::const_iterator nit;
                    for (nit=C.neighbours.begin(); nit != C.neighbours.end(); nit++) {
                        const Connected & D = connected[nit->first];
                        if (!D.valid) continue;
                        // if (nit->first < i) continue;
                        if (replace_twinstrips && (D.twinstrip>0)) continue;
                        QPoint Pn = toImage(D);
                        if (!isInFrame(Pn,false)) continue;
                        int sg_type = (int)(nit->second.twinning_type);
                        if (D.group != C.group)  {
                            sg_type=NO_TWINNING;
                        }
                        int w = 2;
                        //QPoint u(20*cos(nit->second.alpha),20*sin(nit->second.alpha));
                        QPoint Pc = (Pg + Pn) * 0.5;
                        bool dashed = nit->second.ignore_for_phases;
                        if (draw_all) {
                            switch (sg_type) {
                                case TWINNING_ZERO:
                                    drawLine(painter,Pg,Pn,Color(0,0,0),w,dashed);
                                    break;
                                case Zr_TWINNING_TENSILE_2:
                                    if (abs(C.twinning_order-D.twinning_order)!=1) break; // do it later
                                    drawLine(painter,Pg,Pn,Color(255,0,255),w,dashed);
                                    if (D.twinning_order-C.twinning_order==1) {
                                        drawId(painter,Pc,nit->second.twinning_variant,false);
                                    }
                                    break;
                                case Zr_TWINNING_COMPRESSIVE_2:
                                    if (abs(C.twinning_order-D.twinning_order)!=1) break; // do it later
                                    drawLine(painter,Pg,Pn,Color(255,0,0),w,dashed);
                                    if (D.twinning_order-C.twinning_order==1) {
                                        drawId(painter,Pc,nit->second.twinning_variant,false);
                                    }
                                    break;
                                case Zr_TWINNING_COMPRESSIVE_1:
                                case Mg_TWINNING_COMPRESSIVE:
                                    if (abs(C.twinning_order-D.twinning_order)!=1) break; // do it later
                                    drawLine(painter,Pg,Pn,Color(0,0,255),w,dashed);
                                    if (D.twinning_order-C.twinning_order==1) {
                                        drawId(painter,Pc,nit->second.twinning_variant,false);
                                    }
                                    break;
                                case Zr_TWINNING_TENSILE_1:
                                case Mg_TWINNING_TENSILE:
                                    if (abs(C.twinning_order-D.twinning_order)!=1) break; // do it later
                                    drawLine(painter,Pg,Pn,Color(0,255,0),w,dashed);
                                    if (D.twinning_order-C.twinning_order==1) {
                                        // printf("Drawing %d -> %d variant %d\n",C.i,D.i,nit->second.twinning_variant);
                                        drawId(painter,Pc,nit->second.twinning_variant,false);
                                    }
                                    break;
                                default:
                                    if (nit->second.active) {
                                        drawLine(painter,Pg,Pn,Color(255,255,255),w,dashed);
                                    } else {
                                        drawLine(painter,Pg,Pn,Color(64,64,64),1,false);
                                    }
                                    break;
                            }
                        } else if ((C.twinning_order||D.twinning_order)&&abs(C.twinning_order-D.twinning_order)!=1) {
                            drawLine(painter,Pg,Pn,Color(127,127,127),1);
                        }
                    }
                }
            }
            if (replace_twinstrips) {
                for (size_t t=0;t<twinstrips.size();++t) {
                    const TwinStrip & T = twinstrips[t];
                    QPoint Pg = toImage(T);
                    if (!isInFrame(Pg,false)) continue;
                    Connected::NeighbourMap::const_iterator nit;
                    for (nit=T.rev_neighbours.begin(); nit != T.rev_neighbours.end(); nit++) {
                        const Connected & D = connected[nit->second.i];
                        QPoint Pn = toImage(D);
                        if (!isInFrame(Pn,false)) continue;
                        int sg_type = (int)(nit->second.twinning_type);
                        if (D.group != T.group)  {
                            //sg_type=NO_TWINNING;
                            continue;
                        }
                        int w = 2;
                        bool dashed = nit->second.ignore_for_phases;
                        switch (sg_type) {
                            case TWINNING_ZERO:
                                drawLine(painter,Pg,Pn,Color(0,0,0),w,dashed);
                                break;
                            case Zr_TWINNING_TENSILE_2:
                                drawLine(painter,Pg,Pn,Color(255,0,255),w,dashed);
                                if (T.twinning_order-D.twinning_order==1) {
                                    drawId(painter,0.5*(Pg+Pn),nit->second.twinning_variant,false);
                                }
                                break;
                            case Zr_TWINNING_COMPRESSIVE_2:
                                drawLine(painter,Pg,Pn,Color(255,0,0),w,dashed);
                                if (T.twinning_order-D.twinning_order==1) {
                                    drawId(painter,0.5*(Pg+Pn),nit->second.twinning_variant,false);
                                }
                                break;
                            case Zr_TWINNING_COMPRESSIVE_1:
                            case Mg_TWINNING_COMPRESSIVE:
                                drawLine(painter,Pg,Pn,Color(0,0,255),w,dashed);
                                if (T.twinning_order-D.twinning_order==1) {
                                    drawId(painter,0.5*(Pg+Pn),nit->second.twinning_variant,false);
                                }
                                break;
                            case Zr_TWINNING_TENSILE_1:
                            case Mg_TWINNING_TENSILE:
                                drawLine(painter,Pg,Pn,Color(0,255,0),w,dashed);
                                if (T.twinning_order-D.twinning_order==1) {
                                    drawId(painter,0.5*(Pg+Pn),nit->second.twinning_variant,false);
                                }
                                break;
                            default:
                                drawLine(painter,Pg,Pn,Color(127,127,127),1);
                                break;
                        }
                    }
                    drawCircle(painter, Pg, 4, Color(255,0,255),-1);
                    if (draw_ids) {
                        drawId(painter,Pg,T.i);
                    }
                }
            } 
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                if (replace_twinstrips && (connected[i].twinstrip>0)) continue;
                QPoint Pg = toImage(connected[i]);
                if (connected[i].has_weird_parents) {
                    drawCircle(painter, Pg, 8, Color(255,255,255),-1);
                    drawCircle(painter, Pg, 6, Color(0,0,0),-1);
                }
                switch (connected[i].twinning_order) {
                    case 0:
                        drawCircle(painter, Pg, 2, Color(0,255,255),-1);
                        break;
                    case 1:
                        drawCircle(painter, Pg, 2, Color(255,255,0),-1);
                        break;
                    case 2:
                        drawCircle(painter, Pg, 2, Color(255,0,0),-1);
                        break;
                    case 3:
                    default:
                        drawCircle(painter, Pg, 2, Color(0,0,255),-1);
                        break;
                }
                if (draw_ids) {
                    drawId(painter,Pg,(int)i);
                }
            } 
            // Display tangents at this scale
            if ((MeshSize / x_scale)>=15) {
                for (size_t i=0;i<connected.size();i++) {
                    const Contour & C = connected[i].contours[0];
                    for (size_t j=0;j<C.face_ids.size();j++) {
                        if ((j%5)==0) {
                            QPoint P = toImage(faces[C.face_ids[j]]);
                            if (!isInFrame(P)) continue;
                            double theta = C.orientation[j];
                            QPoint u(20*cos(theta),20*sin(theta));
                            QPoint P1 = P + u;
                            QPoint P2 = P - u;
                            drawLine(painter,P1,P2,Color(255,255,0),1);
                        }
                    }
                }
            }
            break;
        case GRAPH_CLUSTERS:
            {
                std::multimap< int,size_t,std::less<int> > sorted_phases;
                for (size_t i=0;i<phases.size();i++) {
                    sorted_phases.insert(std::pair<int,size_t>(phases[i].twinning_order,i));
                }
                std::multimap< int,size_t,std::less<int> >::const_iterator it;
                for (it=sorted_phases.begin();it!=sorted_phases.end();it++) {
                    QPointF C = toImage(phases[it->second]);
                    for (size_t i=0;i<phases[it->second].joints.size();i++) {
                        const Joint & J = joints[phases[it->second].joints[i]];
                        for (size_t j=1;j<J.face_ids.size();j++) {
                            QPointF P1 = toImage(faces[J.face_ids[j-1]]);
                            if (!isInFrame(P1.toPoint())) continue;
                            QPointF P2 = toImage(faces[J.face_ids[j]]);
                            if (!isInFrame(P2.toPoint())) continue;
                            QPointF u1 = (P1-C), u2 = (P2-C);
                            double l1 = hypot(u1.x(),u1.y()), l2 = hypot(u2.x(),u2.y());
                            if (fabs(l1)>1.0) {
                                u1 /= l1; 
                                P1 = C + (l1-it->first*2)*u1;
                            }
                            if (fabs(l2)>1.0) {
                                u2 /= l2;
                                P2 = C + (l2-it->first*2)*u2;
                            }
                            switch (it->first) {
                                case 0:
                                    drawLine(painter,P1.toPoint(),P2.toPoint(),Color(0,255,255),2);
                                    break;
                                case 1:
                                    drawLine(painter,P1.toPoint(),P2.toPoint(),Color(255,255,0),2);
                                    break;
                                case 2:
                                    drawLine(painter,P1.toPoint(),P2.toPoint(),Color(255,0,0),2);
                                    break;
                                case 3:
                                default:
                                    drawLine(painter,P1.toPoint(),P2.toPoint(),Color(0,0,255),2);
                                    break;
                            }
                        }
                    }
                }
                for (size_t i=0;i<phases.size();i++) {
                    if (phases[i].twinning_order == 0) continue;
                    QPoint Pg = toImage(phases[i]);
                    if (!isInFrame(Pg,false)) continue;
                    QPoint Pn = toImage(phases[phases[i].parent]);
                    if (!isInFrame(Pn,false)) continue;
                    int sg_type = (int)(phases[i].twinning_type);
                    int w = 2;
                    // printf("Drawing %d [%d %d] -> %d [%d %d] %s\n",(int)i,Pg.x(),Pg.y(),
                    //         (int)it->second,Pn.x(),Pn.y(), twinning_type_str(it->first));
                    switch (sg_type) {
                        case TWINNING_ZERO:
                            drawLine(painter,Pg,Pn,Color(0,0,0),w);
                            break;
                        case Zr_TWINNING_TENSILE_2:
                            drawLine(painter,Pg,Pn,Color(255,0,255),w);
                            drawId(painter,0.5*(Pg+Pn),phases[i].twinning_variant,false);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_2:
                            drawLine(painter,Pg,Pn,Color(255,0,0),w);
                            drawId(painter,0.5*(Pg+Pn),phases[i].twinning_variant,false);
                            break;
                        case Zr_TWINNING_COMPRESSIVE_1:
                        case Mg_TWINNING_COMPRESSIVE:
                            drawLine(painter,Pg,Pn,Color(0,0,255),w);
                            drawId(painter,0.5*(Pg+Pn),phases[i].twinning_variant,false);
                            break;
                        case Zr_TWINNING_TENSILE_1:
                        case Mg_TWINNING_TENSILE:
                            drawLine(painter,Pg,Pn,Color(0,255,0),w);
                            drawId(painter,0.5*(Pg+Pn),phases[i].twinning_variant,false);
                            break;
                        default:
                            drawLine(painter,Pg,Pn,Color(255,255,255),w);
                            break;
                    }
                }
            }
#if 0
            for (size_t i=0;i<connected.size();++i) {
                if (!connected[i].valid) continue;
                QPoint Pc = toImage(clusters[connected[i].cluster]);
                if (!isInFrame(Pc,false)) continue;
                QPoint Pg = toImage(connected[i]);
                if (!isInFrame(Pg,false)) continue;
                drawLine(painter,Pg,Pc,Color(0,255,255),1);
            }
            for (size_t c=0;c<clusters.size();c++) {
                QPoint Pg = toImage(clusters[c]);
                if (clusters[c].parent) {
                    drawCircle(painter, Pg, 5, Color(255,255,255),-1);
                } else {
                    drawCircle(painter, Pg, 5, Color(0,0,0),-1);
                }
            }
            if (draw_ids) {
                for (size_t c=0;c<clusters.size();c++) {
                    QPoint Pg = toImage(clusters[c]);
                    drawId(painter,Pg,c);
                }
            }
#else
            for (size_t i=0;i<phases.size();++i) {
                QPoint Pg = toImage(phases[i]);
                switch (phases[i].twinning_order) {
                    case 0:
                        drawCircle(painter, Pg, 4, Color(0,255,255),-1);
                        break;
                    case 1:
                        drawCircle(painter, Pg, 4, Color(255,255,0),-1);
                        break;
                    case 2:
                        drawCircle(painter, Pg, 4, Color(255,0,0),-1);
                        break;
                    case 3:
                    default:
                        drawCircle(painter, Pg, 4, Color(0,0,255),-1);
                        break;
                }
            } 
            if (draw_ids) {
                for (size_t c=0;c<phases.size();c++) {
                    QPoint Pg = toImage(phases[c]);
                    drawId(painter,Pg,(int)c);
                }
            }
#endif
            break;
        default:
            break;
    }
    painter.end();


    display = mesh.copy();
    QPainter dpainter(&display);
    dpainter.fillRect(QRect(QPoint(width-125,height-32),QPoint(width-15,height-15)),QBrush(QColor(192,192,192)));
    drawLine(dpainter,QPoint(width-120,height-20),QPoint(width-20,height-20), Color(0,0,0),1);
    drawLine(dpainter,QPoint(width-120,height-23),QPoint(width-120,height-17), Color(0,0,0),1);
    drawLine(dpainter,QPoint(width-20,height-23),QPoint(width-20,height-17), Color(0,0,0),1);
    {
        char tmp[32]="";
        sprintf(tmp,"%.2f um",100*x_scale);
        dpainter.setFont(idFontFg);
        dpainter.setPen(QColor(0,0,0));
        dpainter.drawText(QPoint(width-100,height-22),tmp);
    }
    dpainter.end();

    change_zoom = false;
    dx = dy = 0;
    return true;
}

