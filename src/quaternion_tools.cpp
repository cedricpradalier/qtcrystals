
#include <math.h>
#include <cmath>
#include "crystals/quaternion_tools.h"
#include "crystals/constants.h"
#include <boost/format.hpp>

using boost::format;
namespace crystals {

    typedef std::vector<tf::Quaternion> QuaternionSymmetries;
    typedef std::map<MaterialEnum,QuaternionSymmetries> QuaternionSymmetryMap;
    static QuaternionSymmetryMap QSymLeft,QSymRight;

    tf::Quaternion QRef(0,0,0,1);


    static void initialise_quaternion_symmetries_MgZr() {
        QuaternionSymmetries qsym;
        std::vector<tf::Quaternion> qx, qz, qzeros;
        tf::Quaternion qzero;
        qzero.setRPY(0,0,0);
        qzeros.push_back(qzero);

        // First deal with magnesium and zirconium
        for (size_t i=0;i<2;i++) {
            tf::Quaternion q;
            q.setRPY(i*M_PI,0,0);
            qx.push_back(q);
        }
        for (size_t i=0;i<6;i++) {
            tf::Quaternion q;
            q.setRPY(0,0,i*M_PI/3);
            qz.push_back(q);
        }
        qsym.clear();
        for (size_t i=0;i<qx.size();i++) {
            for (size_t j=0;j<qz.size();j++) {
                tf::Quaternion q = qz[j] * qx[i];
                // printf("Q %d %d: %f %f %f %f\n",(int)i,(int)j,q.x(),q.y(),q.z(),q.w());
                qsym.push_back(q);
            }
        }
        // For now, they are all the same, but don't assume anything
        QSymLeft[MAT_MAGNESIUM] = qsym;
        QSymRight[MAT_MAGNESIUM] = qzeros;
        QSymLeft[MAT_ZIRCONIUM] = qsym;
        QSymRight[MAT_ZIRCONIUM] = qzeros;
    }

    static void initialise_quaternion_symmetries_Mr() {
        QuaternionSymmetries qsym_cubic,qsym_tetra;
        tf::Quaternion qm(0,0,0,1);
        tf::Vector3 vi[4] = {
            tf::Vector3(1,1,1),
            tf::Vector3(-1,1,1),
            tf::Vector3(-1,-1,1),
            tf::Vector3(1,-1,1)
        };
        for (size_t j=0;j<2;j++) {
            for (size_t i=0;i<4;i++) {
                for (size_t k=0;k<3;k++) {
                    tf::Quaternion qi(vi[i],2*k*M_PI/3);
                    for (size_t l=0;l<3;l++) {
                        tf::Quaternion qz(tf::Vector3(0,0,1),l*M_PI/2);
                        tf::Quaternion q = qi * qz * qm;
                        // printf("Q %d %d: %f %f %f %f\n",(int)i,(int)j,q.x(),q.y(),q.z(),q.w());
                        qsym_cubic.push_back(q);
                    }
                }
            }
            qm = tf::Quaternion(tf::Vector3(-1,1,0),M_PI);
        }
#if 0
        printf("Cubic symetries:\n");
        for (size_t i=0;i<qsym_cubic.size();i++) {
            printf("%3d: %5.2f %5.2f %5.2f | %5.2f\n",(int)i,
                    qsym_cubic[i].x(),qsym_cubic[i].y(),qsym_cubic[i].z(),qsym_cubic[i].w());
        }
#endif
        std::vector<bool> dup(qsym_cubic.size(),false);
        for (size_t i=0;i<qsym_cubic.size();i++) {
            const tf::Quaternion & q1 = qsym_cubic[i];
            for (size_t j=0;j<i;j++) {
                const tf::Quaternion & q2 = qsym_cubic[j];
                tf::Quaternion qm = q1*q2.inverse();
                if (qm.w()>(1-1e-4)) {
                    //printf("%d = %d\n",(int)i,(int)j);
                    dup[i] = true;
                    break;
                }
            }
        }
        QuaternionSymmetries qsym_cubicf;
        for (size_t i=0;i<qsym_cubic.size();i++) {
            if (!dup[i]) {
                qsym_cubicf.push_back(qsym_cubic[i]);
            }
        }
        //printf("After filtering: %d quaternions\n",(int)qsym_cubicf.size());

        tf::Quaternion qx(0,0,0,1);
        for (size_t j=0;j<2;j++) {
            for (size_t l=0;l<3;l++) {
                tf::Quaternion qz(tf::Vector3(0,0,1),l*M_PI/2);
                tf::Quaternion q = qx * qz;
                // printf("Q %d %d: %f %f %f %f\n",(int)i,(int)j,q.x(),q.y(),q.z(),q.w());
                qsym_tetra.push_back(q);
            }
            qx = tf::Quaternion(tf::Vector3(1,0,0),M_PI);
        }
#if 0
        printf("Tetra symetries:\n");
        for (size_t i=0;i<qsym_tetra.size();i++) {
            printf("%3d: %5.2f %5.2f %5.2f | %5.2f\n",(int)i,
                    qsym_tetra[i].x(),qsym_tetra[i].y(),qsym_tetra[i].z(),qsym_tetra[i].w());
        }
#endif


        QSymLeft[MAT_MARTENSITE] = qsym_cubicf;
        QSymRight[MAT_MARTENSITE] = qsym_tetra;
        QSymLeft[MAT_AUSTENITE] = qsym_tetra;
        QSymRight[MAT_AUSTENITE] = qsym_cubicf;
    }

    void initialise_quaternion_symmetries() {
        initialise_quaternion_symmetries_MgZr();
        initialise_quaternion_symmetries_Mr();
    }

    tf::Quaternion QFondamental(const tf::Quaternion & q, MaterialEnum mat_type) {
        double cmax = -1;
        const QuaternionSymmetries & left = QSymLeft[mat_type];
        tf::Quaternion qbest = q;
        for (size_t k1=0;k1<left.size();k1++) {
            tf::Quaternion qs = left[k1] * q;
            qs.normalize();
            if (qs.w() < 0) {
                qs = tf::Quaternion(-qs.x(),-qs.y(),-qs.z(),-qs.w());;
            }
            if (qs.w() > cmax) {
                cmax = qs.w();
                qbest = qs;
            }
        }
        return qbest;
    }

    DisorientationClass classify_disorientation(const tf::Quaternion & q,bool with_proba) {
        // const float norm_factor = 1. / (sqrt(2*M_PI)*TwinningTypeTolerance);
        assert(!twinning_disorientations.empty());
        DisorientationClass diso;
        diso.type = NO_TWINNING;
        diso.alpha = 0.0;
        diso.beta = 0.0;
        diso.variant = 0;
        diso.error = 2*M_PI;
        TwinningDisorientationsMap::const_iterator it = twinning_disorientations.begin();
        double proba_no_twinning = 1;
        for (;it != twinning_disorientations.end();it++) {
            if (it->first == NO_TWINNING*MAX_VARIANT_PER_TYPE) {
                continue;
            }
            std::pair<double,tf::Quaternion> ps = QDisorientation(it->second,q);
            const tf::Quaternion & qs = ps.second;
            double angle = ps.first;
#if 0
            const tf::Quaternion & qr = it->second;
            printf("Qref %d: %f %f %f %f\n",(int)it->first,qr.x(),qr.y(),qr.z(),qr.w());
            printf("\tDiso angle: %d %.3f %.3fdeg\n",it->first, qs.w(),angle*180./M_PI);
#endif
            if (with_proba) {
                // Push P(Z | Twinning type and variant)
                double proba = /* norm_factor * */ exp(-0.5 * SQR(angle / TwinningTypeTolerance));
                proba_no_twinning *= (1-proba);
                diso.probas[it->first] = proba;
            }
            if (angle < diso.error) {
                diso.error = angle;
                diso.error_q = qs;
                diso.type = (TwinningType)(it->first / MAX_VARIANT_PER_TYPE);
                diso.variant = it->first  % MAX_VARIANT_PER_TYPE; 
            }
        }
        if (with_proba) {
            diso.probas[NO_TWINNING*MAX_VARIANT_PER_TYPE]=proba_no_twinning;
        }
        if (diso.type != NO_TWINNING) {
            double theta = q.getAngle();
            tf::Vector3 v = q.getAxis();
            diso.beta = theta;
            diso.alpha = atan2(v.y(),v.x());
        }
        return diso;
    }

    std::string DisorientationClass::toString() {
        std::string output =  "\tTwinning likelihood: ";
        output += str(format("Error:%6.2f deg\n")%(error*180./M_PI));
        output += str(format("\t\tNO:%6.2f%%\n")%(100*probas[NO_TWINNING*MAX_VARIANT_PER_TYPE]));
        output += str(format("\t\t 0: %6.2f%%\n")%(100*probas[TWINNING_ZERO*MAX_VARIANT_PER_TYPE]));
        switch (MaterialType) {
            case MAT_MAGNESIUM:
                output += "\t\tT :";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Mg_TWINNING_TENSILE*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n\t\tC :";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Mg_TWINNING_COMPRESSIVE*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n";
                break;
            case MAT_ZIRCONIUM:
                output += "\t\tT1:";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Zr_TWINNING_TENSILE_1*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n\t\tT2:";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Zr_TWINNING_TENSILE_2*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n\t\tC1:";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Zr_TWINNING_COMPRESSIVE_1*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n\t\tC2:";
                for (int i=0;i<6;++i) {
                    output += str(format("%5.2f%% ")%(100*probas[Zr_TWINNING_COMPRESSIVE_2*MAX_VARIANT_PER_TYPE+i]));
                }
                output += "\n";
                break;
            case MAT_MARTENSITE:
                output += "\t\tMr:";
                for (int j=0;j<4;j++) {
                    for (int i=0;i<6;++i) {
                        output += str(format("%5.2f%% ")%(100*probas[Mr_TWINNING_TYPE*MAX_VARIANT_PER_TYPE+j*6+i]));
                    }
                    output += "\n";
                }
                break;
            default:
                break;
        }
        return output;
    }


    std::pair<double,tf::Quaternion> QDisorientation(const tf::Quaternion & q1, const tf::Quaternion & q2) {
        tf::Quaternion invm = q2 * q1.inverse();
        double cmax = -1;
        tf::Quaternion qbest;
        // Store the Material type hypotheses
        std::vector<MaterialEnum> MaterialTypes;
        MaterialTypes.push_back(MaterialType);
        switch (MaterialType) {
            case MAT_MARTENSITE:
                MaterialTypes.push_back(MAT_AUSTENITE);
                break;
            case MAT_ZIRCONIUM:
            case MAT_MAGNESIUM:
            default:
                break;
        }
#if 0
        tf::Quaternion q1i = q1.inverse();
        printf("1 : %f %f %f %f\n",q1.x(),q1.y(),q1.z(),q1.w());
        printf("1I: %f %f %f %f\n",q1i.x(),q1i.y(),q1i.z(),q1i.w());
        printf("2 : %f %f %f %f\n",q2.x(),q2.y(),q2.z(),q2.w());
        printf("D: %f %f %f %f\n",invm.x(),invm.y(),invm.z(),invm.w());
#endif
        for (size_t m=0;m<MaterialTypes.size();m++) {
            const QuaternionSymmetries & left = QSymLeft[MaterialTypes[m]];
            const QuaternionSymmetries & right = QSymRight[MaterialTypes[m]];
            for (size_t k2=0;k2<right.size();k2++) {
                tf::Quaternion qr = invm * right[k2];
                for (size_t k1=0;k1<left.size();k1++) {
                    tf::Quaternion qs = left[k1] * qr;
                    qs.normalize();
                    if (qs.w() < 0) {
                        qs = tf::Quaternion(-qs.x(),-qs.y(),-qs.z(),-qs.w());;
                    }
#if 0
                    double angle = qs.getAngle();
                    //printf("Q %d: %f %f %f %f %f\n",(int)k,angle,qs.x(),qs.y(),qs.z(),qs.w());
                    assert(angle>=0.0);
#endif
                    if (qs.w() > cmax) {
                        cmax = qs.w();
                        qbest = qs;
                    }
                }
            }
        }
        //printf("B %f: %f %f %f %f\n",cmax,qbest.x(),qbest.y(),qbest.z(),qbest.w());
        return std::pair<double,tf::Quaternion>(qbest.getAngle(),qbest);
    }


    tf::Vector3 QLog(const tf::Quaternion & q) {
        return q.getAngle() * q.getAxis();
    }

    tf::Quaternion QExp(tf::Vector3 v) {
        double angle = v.length();
        if (fabs(angle)<1e-4) {
            return tf::Quaternion::getIdentity();
        } else {
            v /= angle;
            return tf::Quaternion(v,angle);
        }
    }

    tf::Quaternion QMeanWithSymmetries(const std::vector<tf::Quaternion> & v, double tol, size_t max_iter) {
        assert(v.size() > 0);
        tf::Quaternion qmean = v[0];
        for (size_t i=0;i<max_iter;i++) {
            tf::Quaternion qmean_prev(qmean);
            tf::Vector3 e(0,0,0);
            for (size_t j=0;j<v.size();j++) {
                // Compute the difference
                std::pair<double,tf::Quaternion> r = QDisorientation(qmean,v[j]);
                // Add the Log of the difference
                e += QLog(r.second);
            }
            e /= v.size();
            qmean = QExp(e) * qmean;
            // printf("%d %.2e %.2e %.2e %.2e\n",(int)i,qmean.x(),qmean.y(),qmean.z(),qmean.w());
            if ((qmean - qmean_prev).length() < tol) {
                break;
            }
        }
        return qmean;
    }

    tf::Quaternion QMeanWithSymmetries(const std::vector<tf::Quaternion> & v, const std::vector<double> & weights, double tol, size_t max_iter) {
        assert(v.size() > 0);
        assert(v.size() == weights.size());
        double sum_weights = 0;
        for (size_t j=0;j<v.size();j++) {
            sum_weights += weights[j];
        }
        tf::Quaternion qmean = v[0];
        for (size_t i=0;i<max_iter;i++) {
            tf::Quaternion qmean_prev(qmean);
            tf::Vector3 e(0,0,0);
            for (size_t j=0;j<v.size();j++) {
                // Compute the difference
                std::pair<double,tf::Quaternion> r = QDisorientation(qmean,v[j]);
                // Add the Log of the difference
                e += weights[j] * QLog(r.second);
            }
            e /= sum_weights;
            qmean = QExp(e) * qmean;
            // printf("%d %.2e %.2e %.2e %.2e\n",(int)i,qmean.x(),qmean.y(),qmean.z(),qmean.w());
            if ((qmean - qmean_prev).length() < tol) {
                break;
            }
        }
        return qmean;
    }

    tf::Quaternion QMeanWithSymmetries(const std::vector<Vertex> & v, double tol, size_t max_iter) {
        std::vector<tf::Quaternion> qs(v.size());
        for (size_t j=0;j<v.size();j++) {
            qs[j] = v[j].q;
        }
        return QMeanWithSymmetries(qs,tol,max_iter);
    }

    bool QIsNan(const tf::Quaternion & q) {
        return std::isnan(q.x()) || std::isnan(q.y()) || std::isnan(q.z()) || std::isnan(q.w());
    }

    tf::Quaternion QMeanWithSymmetries(const std::vector<Vertex> & v, 
            const std::vector<uint32_t> & cluster, 
            double tol, size_t max_iter) {
        assert(cluster.size()>0);
        assert(cluster[0]>=0);
        assert(cluster[0]<v.size());
        std::vector<tf::Quaternion> qs(cluster.size());
        for (size_t j=0;j<cluster.size();j++) {
            qs[j] = v[cluster[j]].q;
        }
        return QMeanWithSymmetries(qs,tol,max_iter);
    }

    // From Rod McCabe
    tf::Matrix3x3 matrixFromBungeEuler(double phi1,double PHI,double phi2) {
        double cp1,sp1,cp2,sp2,cP,sP;
        cp1 = cos(phi1); sp1 = sin(phi1);
        cp2 = cos(phi2); sp2 = sin(phi2);
        cP = cos(PHI); sP = sin(PHI);
        return tf::Matrix3x3( cp1*cp2 - sp1*sp2*cP, sp1*cp2 + cp1*sp2*cP, sp2*sP,
                -cp1*sp2 - sp1*cp2*cP, -sp1*sp2 + cp1*cp2*cP, cp2*sP,
                sp1*sP, -cp1*sP, cP);
    }

    tf::Quaternion quaternionFromBungeEuler(double phi1,double PHI,double phi2) {
        tf::Quaternion q;
        tf::Matrix3x3 R = matrixFromBungeEuler(phi1,PHI,phi2);
#if 0
        printf("------------------\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n------------------\n",
                R[0][0], R[0][1], R[0][2],
                R[1][0], R[1][1], R[1][2],
                R[2][0], R[2][1], R[2][2]);
#endif
        R.getRotation(q);
        q.normalize();
        if (q.w() < 0) {
            q = tf::Quaternion(-q.x(),-q.y(),-q.z(),-q.w());;
        }
        return q;
    }

    void quaternionToBungeEuler(const tf::Quaternion & q,double &phi1,double &PHI,double &phi2,bool degrees) {
        tf::Matrix3x3 R;
        R.setRotation(q);
        matrixToBungeEuler(R,phi1,PHI,phi2,degrees);
    }

    // From Rod McCabe
    void matrixToBungeEuler(const tf::Matrix3x3 & R,double &phi1,double &PHI,double &phi2,bool degrees) {
        if (R[2][2]>0.9999999) {
            PHI = 0.0;
            phi2 = 0.0;
            phi1 = 0.5*acos(R[0][0]);
            if (R[0][1]<0.0) phi1 = 2*M_PI-phi1;
        } else if (R[2][2]<-0.9999999) {
            PHI = M_PI;
            phi2 = 0.0;
            phi1 = 0.5*acos(R[0][0]);
            if (R[0][1]<0.0) phi1 = 2*M_PI-phi1;
        } else {
            PHI = acos(R[2][2]);
            phi1 = atan2(R[2][0],-R[2][1]);
            phi2 = atan2(R[0][2], R[1][2]);
        }
        if (degrees) {
            phi1 *= 180./M_PI;
            PHI  *= 180./M_PI;
            phi2 *= 180./M_PI;
        }
        return;
    }

};

