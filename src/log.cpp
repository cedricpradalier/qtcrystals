#include <string>
#include <stdio.h>
#include <QTime>
#include "crystals/log.h"
namespace crystals {


    static LoggerFunction logger_fun = NULL;

    static QTime start_clock() {
        QTime t;
        t.start();
        return t;
    }

    static QTime clock = start_clock();

    void setLogger(LoggerFunction f) {
        logger_fun = f;
    }

    std::string time_string() {
        char outstr[200];
#if 0
        QTime t = QTime::currentTime();
        sprintf(outstr,"%9ld.%03ld",(long)t.second(),(long)t.msec());
#else
        long msec = clock.elapsed();
        sprintf(outstr,"%9ld.%03ld",(long)(msec/1000),(long)(msec%1000));
#endif
        return std::string(outstr);
    }

    void LOG_INFO(const char * format, ...) {
        va_list ap;
        va_start(ap, format);
        if (logger_fun) {
            logger_fun(LogInfo,(time_string()+": "+format).c_str(),ap);
        } else {
            vfprintf(stdout,(time_string()+": "+format).c_str(),ap);
            fprintf(stdout,"\n");
        }
        va_end(ap);
    }

    void LOG_ERROR(const char * format, ...) {
        va_list ap;
        va_start(ap, format);
        if (logger_fun) {
            logger_fun(LogError,(time_string()+": "+format).c_str(),ap);
        } else {
            vfprintf(stderr,(time_string()+": "+format).c_str(),ap);
            fprintf(stderr,"\n");
        }
        va_end(ap);
    }

}
