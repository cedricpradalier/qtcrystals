
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include "crystals/log.h"
#include "crystals/color_tools.h"
#include "crystals/quaternion_tools.h"

#define MOD_PI (M_PI/4)

namespace crystals {

    bool bgr_mode = true;

    QColor Color(int a, int b, int c) {
        if (bgr_mode) {
            return QColor::fromRgb(a,b,c);
        } else {
            return QColor::fromRgb(c,b,a);
        }
    }

    float EBSDColorSet::iq_min = -1;
    float EBSDColorSet::iq_max = 0;
    float EBSDColorSet::fit_max = 0;
    void EBSDColorSet::resetEBSDStats() {
        iq_min = -1;
        iq_max = 0;
        fit_max = 0;
    }

    void EBSDColorSet::updateEBSDStats(float iq, float fit) {
        if (fit>fit_max) fit_max = fit;
        if ((iq_min<0) || (iq < iq_min)) iq_min = iq;
        if (iq>iq_max) iq_max = iq;
    }



    EBSDColorMode EBSDColorSet::colorMode = EBSDCM_FULL;
    void EBSDColorSet::setMode(EBSDColorMode m) {
        colorMode = m;
        switch(colorMode) {
            case EBSDCM_STEREO_001: 
                LOG_INFO("Now displaying with colors in stereographic 001 mode");
                break;
            case EBSDCM_STEREO_010: 
                LOG_INFO("Now displaying with colors in stereographic 010 mode");
                break;
            case EBSDCM_STEREO_100: 
                LOG_INFO("Now displaying with colors in stereographic 100 mode");
                break;
            case EBSDCM_FULL: 
                LOG_INFO("Now displaying with colors in full gamet mode");
                break;
            case EBSDCM_EULER: 
                LOG_INFO("Now displaying with colors in Euler RPY mode");
                break;
            case EBSDCM_FIT: 
                LOG_INFO("Now displaying EBSD fitness as gray values");
                break;
            case EBSDCM_CONFIDENCE: 
                LOG_INFO("Now displaying EBSD confidence index as gray values");
                break;
            case EBSDCM_IMAGE_QUALITY: 
                LOG_INFO("Now displaying EBSD image_quality as gray values");
                break;
            default:
                break;
        }
    }

    void EBSDColorSet::nextColorMode() {
        colorMode = (EBSDColorMode)((colorMode + 1) % EBSDCM_NUM_MODES);
        setMode(colorMode);
    }

    EBSDColor::EBSDColor() : scalar(0,0,0) {
#ifdef STORE_COLOR_NAME
        name = scalar.name()
#endif
    }
    EBSDColor::EBSDColor(const QColor & s) : scalar(s) {
#ifdef STORE_COLOR_NAME
        name = scalar.name();
#endif
    }

    EBSDColor::EBSDColor(uint8_t red, uint8_t green, uint8_t blue) : scalar(red,green,blue) {
#ifdef STORE_COLOR_NAME
        name = scalar.name();
#endif
    }

    EBSDColor::EBSDColor(uint8_t gray) : scalar(gray,gray,gray) {
#ifdef STORE_COLOR_NAME
        name = scalar.name();
#endif
    }

    EBSDColorSet::EBSDColorSet(const tf::Quaternion & q,float data_iq,float data_ci,float data_fit) {
        tf::Matrix3x3 R; 
#if 0
        tf::Quaternion qn = QDisorientation(crystals::QRef,q).second;
#else
        const tf::Quaternion & qn = q; // alias
#endif
        R.setRotation(qn);
        float theta = qn.getAngle();
        tf::Vector3 vec = qn.getAxis();

        // First compute the mapping betwewn euler and RGB
        tfScalar roll,pitch,yaw;
        R.getEulerYPR(yaw,pitch,roll);
        yaw = (M_PI+remainder(yaw,2*M_PI))*255/(2*M_PI);
        pitch = (M_PI/2 + remainder(pitch,2*M_PI))*255/M_PI;
        roll = (M_PI + remainder(roll,2*M_PI))*255/(2*M_PI);
        // printf("%.1f %.1f %.1f\n",roll,pitch,yaw);
        yaw = std::max(0.0f, std::min(255.0f, yaw));
        pitch = std::max(0.0f, std::min(255.0f, pitch));
        roll = std::max(0.0f, std::min(255.0f, roll));
        euler = EBSDColor(yaw,pitch,roll);

        float phi = atan2(vec.y(),vec.x());
        float psi = 0;
        if (fabs(theta)>1e-4) psi = atan2(vec.z(),hypot(vec.y(),vec.x()));
        float h,s,v;
        h = phi * 180./M_PI;
        if (h<0) h+=360;
        // h /= 2; //??
        v = (psi+M_PI/2)/M_PI;
        s = theta / (M_PI/2);
        // printf("%.3f %.3f %.3f\n",std::max(0.0,std::min(h/360.,1.0)),std::min(s,1.0f),std::min(v,1.0f));
        full = EBSDColor(QColor::fromHsvF(std::max(0.0,std::min(h/360.,1.0)),
                    std::max(0.0f,std::min(s,1.0f)),std::max(0.0f,std::min(v,1.0f))));
        // full = EBSDColor(h*255/360.,s*255,v*255);

        // Now try the stereographic projection TODO
        // a x + b x - b + c x - c = 0 = x - b - c 
        // a y + b y + cy - c = 0 = y - c
        // a + b + c = 1
        // 
        //     b + c = x 
        //         c = y 
        // a + b + c = 1
        // 
        // b = x - y 
        // c = y 
        // a = 1 - x + y - y = 1 - x
        // 
        double x,y,z, xp,yp, rp, alpha;
        double fred, fgreen, fblue;
        // First along axis X ([0 0 1]?)
        x = R[0][0]; y = R[1][0]; z = R[2][0];
        if (x < 0) {x=-x;y=-y;z=-z;}
        xp = y / ( x + 1); yp = z / (x + 1); 
        rp = hypot(yp,xp);
        alpha = fabs(remainder(atan2(yp,xp),2*MOD_PI))/MOD_PI; 
        // Now set Red in (0,0), Green in (1,0) and Blue in (1,1)
        // And find the barycentric coefficient of (rp, alphap)
        fred = 1 - rp;
        fgreen = rp - alpha;
        fblue = alpha;
        stereo001 = EBSDColor(fred*255, fgreen*255, fblue*255);

        // Second along axis Y ([0 1 0]?)
        x = R[0][1]; y = R[1][1]; z = R[2][1];
        if (y < 0) {x=-x;y=-y;z=-z;}
        xp = z / ( y + 1); yp = x / (y + 1); 
        rp = hypot(yp,xp);
        alpha = fabs(remainder(atan2(yp,xp),2*MOD_PI))/MOD_PI; 
        // Now set Red in (0,0), Green in (1,0) and Blue in (1,1)
        // And find the barycentric coefficient of (rp, alphap)
        fred = 1 - rp;
        fgreen = rp - alpha;
        fblue = alpha;
        stereo010 = EBSDColor(fred*255, fgreen*255, fblue*255);

        // Finally along axis Z ([1 0 0]?)
        x = R[0][2]; y = R[1][2]; z = R[2][2];
        if (z < 0) {x=-x;y=-y;z=-z;}
        xp = x / ( z + 1); yp = y / (z + 1); 
        rp = hypot(yp,xp);
        alpha = fabs(remainder(atan2(yp,xp),2*MOD_PI))/MOD_PI; 
        // Now set Red in (0,0), Green in (1,0) and Blue in (1,1)
        // And find the barycentric coefficient of (rp, alphap)
        fred = 1 - rp;
        fgreen = rp - alpha;
        fblue = alpha;
        stereo100 = EBSDColor(fred*255, fgreen*255, fblue*255);

        if (fit_max>0.0) {
            double expfit = exp(-0.5*(data_fit*data_fit)/(1.5*1.5));
            fit = EBSDColor(expfit*255);
            //fit = EBSDColor((data_fit*255)/fit_max);
        }
        if ((iq_min>=0.0)&&((iq_max-iq_min)>0.0)) {
            iq = EBSDColor((data_iq-iq_min)*255/(iq_max-iq_min));
        }
        ci = EBSDColor(data_ci*255);
    }


#ifdef STORE_COLOR_NAME
    EBSDColorSet::operator const std::string&() const 
#else
    EBSDColorSet::operator std::string() const 
#endif
    {
#ifdef STORE_COLOR_NAME
#define OP name
#else
#define OP name().toLatin1().constData()
#endif
        switch (colorMode) {
            case EBSDCM_STEREO_001: 
                return stereo001.OP;
            case EBSDCM_STEREO_010: 
                return stereo010.OP;
            case EBSDCM_STEREO_100: 
                return stereo100.OP;
            case EBSDCM_CONFIDENCE: 
                return ci.OP;
            case EBSDCM_FIT: 
                return fit.OP;
            case EBSDCM_IMAGE_QUALITY: 
                return iq.OP;
            case EBSDCM_EULER: 
            default:
                return euler.OP;
        }
#undef OP
    }

    EBSDColorSet::operator const QColor&() const {
        switch (colorMode) {
            case EBSDCM_STEREO_001: 
                return stereo001.scalar;
            case EBSDCM_STEREO_010: 
                return stereo010.scalar;
            case EBSDCM_STEREO_100: 
                return stereo100.scalar;
            case EBSDCM_FULL: 
                return full.scalar;
            case EBSDCM_CONFIDENCE: 
                return ci.scalar;
            case EBSDCM_FIT: 
                return fit.scalar;
            case EBSDCM_IMAGE_QUALITY: 
                return iq.scalar;
            case EBSDCM_EULER: 
            default:
                return euler.scalar;
        }
    }


    QColor cvColor(const tf::Quaternion & q) {
        tf::Matrix3x3 R; 
        R.setRotation(q);
        tfScalar roll,pitch,yaw;
        R.getEulerYPR(yaw,pitch,roll);
        yaw = (M_PI+remainder(yaw,2*M_PI))*255/(2*M_PI);
        pitch = (M_PI/2 + remainder(pitch,2*M_PI))*255/M_PI;
        roll = (M_PI + remainder(roll,2*M_PI))*255/(2*M_PI);
        // printf("%.1f %.1f %.1f\n",roll,pitch,yaw);
        yaw = std::max(0.0f, std::min(255.0f, yaw));
        pitch = std::max(0.0f, std::min(255.0f, pitch));
        roll = std::max(0.0f, std::min(255.0f, roll));
        return Color(yaw,pitch,roll);
    }

    std::string QuatColor(const tf::Quaternion & q) {
#if 0
        tf::Matrix3x3 R; 
        R.setRotation(q);
        tfScalar roll,pitch,yaw;
        R.getEulerYPR(yaw,pitch,roll);
        yaw = (M_PI+remainder(yaw,2*M_PI))*255/(2*M_PI);
        yaw = std::max(0.0, std::min(255.0, yaw));
        pitch = (M_PI/2 + remainder(pitch,2*M_PI))*255/M_PI;
        pitch = std::max(0.0, std::min(255.0, pitch));
        roll = (M_PI + remainder(roll,2*M_PI))*255/(2*M_PI);
        roll = std::max(0.0, std::min(255.0, roll));
        char buffer[8];
        snprintf(buffer,8,"#%02X%02X%02X",
                (int)roll,(int)pitch,(int)yaw);
        return std::string(buffer);
#else
        return cvColor(q).name().toLatin1().constData();
#endif
    }

};

