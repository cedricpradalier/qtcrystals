
#include <math.h>
#include <string>
#include <stdio.h>

#include "crystals/constants.h"
#include "crystals/quaternion_tools.h"
#include "crystals/Vertex.h"

using namespace crystals;

Vertex::Vertex() {
    good=false; map_edge = grain_border = border=false; membership = -1;
    for (int i=0;i<6;i++) {
        edges[i] = 0;
    }
}


std::string Vertex::phase_name() const {
    return material_type_str(mat_type);
}

bool Vertex::loadFromString(const std::string & s, bool radian) {
    char c = 0;
    char name[1024];
    int grainid=0, edge=1; // not used
    sscanf(s.c_str()," %c",&c);
    if ((c==0) || (c=='#') || (c=='%')) {
        return false;
    }
    if (sscanf(s.c_str()," %e %e %e %e %e %e %e %e %d %d %s",
                &phi1, &PHI, &phi2,
                &x,&y,&iq,&ci,&fit,&grainid,&edge,name) != 11) {
        return false;
    }
    if (TrustExternalPhaseData) {
        std::string sname(name);
        if (sname == "Magnesium") {
            mat_type = MAT_MAGNESIUM;
        } else if (sname == "Zirconium") {
            mat_type = MAT_ZIRCONIUM;
        } else if (sname == "Austenite") {
            mat_type = MAT_AUSTENITE;
        } else if ((sname == "Ferrite") || (sname == "Martensite")) {
            mat_type = MAT_MARTENSITE;
        } else {
            mat_type = MaterialType;
        }
    } else {
        mat_type = MaterialType;
    }

    i = round((x-XOffset)/DeltaX);
    j = round((y-YOffset)/DeltaY);

    if (!radian) {
        phi1 *= (float)(M_PI/180.);
        PHI *= (float)(M_PI/180.);
        phi2 *=(float)(M_PI/180.);
    }

    // if (q.w() < 0) {
    //     q = tf::Quaternion(-q.x(),-q.y(),-q.z(),-q.w());;
    // }
    // Here we could flag the point as good depending on iq or ci
    good = true;
    EBSDColorSet::updateEBSDStats(iq,fit);
    update_quaternion(crystals::QRef);
    return true;
}

bool Vertex::loadFromCPR(const std::string & s, bool radian) {
    char c = 0;
    int phase, band,bc,bs;
    float mad,error,reliability;
    sscanf(s.c_str()," %c",&c);
    if ((c==0) || (c=='#') || (c=='%')) {
        return false;
    }
    if (sscanf(s.c_str()," %e %e %e %d %d %d %d %e %e %e %e %e ",
                &phi1, &PHI, &phi2,
                &phase,&band,&bc,&bs,&error,&mad,&reliability,&x,&y) != 12) {
        // printf("Not enough data field in '%s'\n",s.c_str());
        return false;
    }
    mat_type = MaterialType;

    i = round((x-XOffset)/DeltaX);
    j = round((y-YOffset)/DeltaY);

    if (radian) {
        phi2 += M_PI/2;
    } else {
        // ????
        phi2 += 90;
        phi1 *= (float)(M_PI/180.);
        PHI *= (float)(M_PI/180.);
        phi2 *=(float)(M_PI/180.);
    }

    good = phase>0;
    EBSDColorSet::updateEBSDStats(bc,bs);
    update_quaternion(crystals::QRef);
    return true;
}

bool Vertex::loadFromCTF(const std::string & s, bool radian) {
    char c = 0;
    int gd,bands,error;
    sscanf(s.c_str()," %c",&c);
    if ((c==0) || (c=='#') || (c=='%')) {
        return false;
    }
    iq = ci = fit = 0;
    if (sscanf(s.c_str()," %d %e %e %d %d %e %e %e %e %e %e",
                &gd, &x,&y, &bands, &error, 
                &phi1, &PHI, &phi2,&iq,&ci,&fit) != 11) {
        return false;
    }

    i = round((x-XOffset)/DeltaX);
    j = round((y-YOffset)/DeltaY);
    mat_type = MaterialType;
    
    if (radian) {
        phi2 += M_PI/2;
    } else {
        // ????
        phi2 += 90;

        phi1 *= (float)(M_PI/180.);
        PHI *= (float)(M_PI/180.);
        phi2 *=(float)(M_PI/180.);
    }

    // if (q.w() < 0) {
    //     q = tf::Quaternion(-q.x(),-q.y(),-q.z(),-q.w());;
    // }
    // Here we could flag the point as good depending on iq or ci
    good = gd && !error;
    EBSDColorSet::updateEBSDStats(iq,fit);
    update_quaternion(crystals::QRef);
    return true;
}

// To be called once DeltaX,DeltaY have been computed
void Vertex::update_indices() {
    i = round((x-XOffset)/DeltaX);
    j = round((y-YOffset)/DeltaY);
}

void Vertex::update_quaternion(const tf::Quaternion & qref) {
#if 0
    // There seems to be a 90 deg offset in the coordinate frame here. 
    tf::Quaternion q1;q1.setRPY(0,0,phi1*M_PI/180.);
    tf::Quaternion q2;q2.setRPY(PHI*M_PI/180.,0,0);
    tf::Quaternion q3;q3.setRPY(0,0,(phi2+90)*M_PI/180.);
    q = q1*q2*q3;
    // Convert q from a transform from the grain frame to world, to the inverse
    q = q.inverse();
    q.normalize();
#else
    q = quaternionFromBungeEuler(phi1,PHI,phi2);
#endif
    // Express quaternion in its fundamental zone. Could be useful to compute
    // Schmidt factor... To be verified -> create wrong variants
    q = QFondamental(q,mat_type);
}
