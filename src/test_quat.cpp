#include <LinearMath/Quaternion.h>
#include <LinearMath/Vector3.h>
#include <LinearMath/Matrix3x3.h>
#include <math.h>

#include "crystals/constants.h"
#include "crystals/Vertex.h"
#include "crystals/quaternion_tools.h"

using namespace crystals;

int main(int argc, char * argv[])
{

    // double gamma = 1.592;
    tf::Vector3 vx(1,0,0);
    tf::Vector3 vy(0,1,0);
    tf::Vector3 vz(0,0,1);
    tf::Vector3 axis;

    tf::Quaternion q1 = tf::Quaternion(vz,0);
    tf::Quaternion q2 = tf::Quaternion(vx,M_PI/3);
    tf::Quaternion q3 = tf::Quaternion(vz,0);
    tf::Quaternion q = q3 * q2 * q1;

    tf::Matrix3x3 R;
    R.setRotation(q);
    axis = q.getAxis();
    double alpha = q.getAngle();

    printf("Quaternion: w=%.4f v=[%.4f %.4f %.4f]\n", q.w(),q.x(),q.y(),q.z());
    printf("Rotation\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n",
            R[0][0], R[0][1], R[0][2],
            R[1][0], R[1][1], R[1][2],
            R[2][0], R[2][1], R[2][2]);
    printf("Angle: %.4f deg Vector: %.4f %.4f %.4f\n",alpha*180./M_PI,axis.x(),axis.y(),axis.z());

    return 0;
}

