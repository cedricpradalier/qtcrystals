
#include "crystals/Edge.h"

using namespace crystals;

Edge::Edge(const std::vector<Vertex> & v, int a, int slot_a, int b, int slot_b) {
    i = a; j = b; 
    slot_i = slot_a; slot_j = slot_b;
    marked = false;
    std::pair<double,tf::Quaternion> r = QDisorientation(v[a].q,v[b].q);
    q_disorientation = r.second;
    disorientation = r.first;
    different_phase = (v[a].mat_type != v[b].mat_type); 
    weight = exp(-SQR(disorientation)/SigmaSqr);
    twinning_type = (weight<MinWeight)?NO_TWINNING:TWINNING_ZERO;
}

bool Edge::isSolid(const std::vector<Vertex> & v) const {
    if (different_phase) return false;
    if (!v[i].good) return false;
    if (!v[j].good) return false;
    return weight > MinWeight;
}

bool Edge::isConnected(const std::vector<Vertex> & v) const {
    return v[i].membership == v[j].membership;
}

bool Edge::isBorder(const std::vector<Vertex> & v) const {
    if (!v[i].border) return false;
    if (!v[j].border) return false;
    return true;
}

bool Edge::isGrainBorder(const std::vector<Vertex> & v) const {
    if (!v[i].grain_border) return false;
    if (!v[j].grain_border) return false;
    return true;
}

void Edge::updateType() {
    DisorientationClass diso = classify_disorientation(q_disorientation);
    twinning_type = diso.type;
    twinning_error = diso.error;
}


std::vector<uint32_t> Edge::getNeighbours(const std::vector<Vertex> & v, bool square_mesh) const {
    std::vector<uint32_t> results;
    const Vertex & vi = v[i];
    const Vertex & vj = v[j];
    uint8_t t,N;
    N = (square_mesh?4:6);
    t = (slot_i+1)%N;
    if (vi.edges[t]) {
        results.push_back(abs(vi.edges[t])-1);
    }
    t = (slot_i+N-1)%N;
    if (vi.edges[t]) {
        results.push_back(abs(vi.edges[t])-1);
    }
    t = (slot_j+1)%N;
    if (vj.edges[t]) {
        results.push_back(abs(vj.edges[t])-1);
    }
    t = (slot_j+N-1)%N;
    if (vj.edges[t]) {
        results.push_back(abs(vj.edges[t])-1);
    }
    return results;
}
