#include <string.h>
#include <crystals/EBSD.h>
#include <crystals/log.h>
#include <sqlite3.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp> //no i/o just types

#define QUIET_QHULL

using boost::format;
using namespace crystals;

#if defined(_WIN32) || defined(_WIN64)
  #define snprintf _snprintf
  #define vsnprintf _vsnprintf
  #define strcasecmp _stricmp
  #define strncasecmp _strnicmp
#endif

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
   int i;
   for(i=0; i<argc; ++i){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}


static bool exec_db(sqlite3 *db, const format & sql) {
    char *zErrMsg = 0;
    int  rc;
    rc = sqlite3_exec(db, sql.str().c_str(), callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("Request: '%s'\nSQL error: %s\n", sql.str().c_str(), zErrMsg);
        sqlite3_free(zErrMsg);
        assert(rc == SQLITE_OK);
        return false;
    }
    return true;
}

static bool exec_db(sqlite3 *db, const std::string & sql) {
    char *zErrMsg = 0;
    int  rc;
    rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("Request: '%s'\nSQL error: %s\n", sql.c_str(), zErrMsg);
        sqlite3_free(zErrMsg);
        assert(rc == SQLITE_OK);
        return false;
    }
    return true;
}

bool EBSDNeighbour::storeOrientationHistogram(sqlite3 *db,size_t hist_id) const {
    std::map<int16_t,uint32_t>::const_iterator it;
    format fql;

    for (it = orientation_histogram.begin(); it != orientation_histogram.end(); it++) {
        fql = format("INSERT INTO JointOrientationHistogram (HIST_ID,ANGLE_DEG,OCCURENCE) "
                "VALUES (%d,%d, %d);")
            % hist_id % it->first % it->second; 
        if (!exec_db(db,fql)) { return false; }
    }
    return true;
}

bool EBSDAnalyzer::close_db() {
    assert(sqlite3_close(db)==SQLITE_OK);
    db = NULL;
    db_filename.clear();
    return true;
}

bool EBSDAnalyzer::connect_db(const std::string & filename) {
    int rc = 0;
    if (db) {
        if (filename == db_filename) {
            // already open
            return true;
        } else {
            close_db();
        }
    }
    rc = sqlite3_open(filename.c_str(), &db);
    if( rc ){
        LOG_ERROR("Can't open database: %s", sqlite3_errmsg(db));
        return false;
    }
    db_filename = filename;
    LOG_INFO("Opened database successfully");
    return true;
}


static int callback_has_fid(void *data, int argc, char **argv, char **azColName){
   bool *that = (bool*)data;
   // *that = false;
   if (argc < 2) return -1;
   if (strcasecmp(argv[1],"fid")==0) {
       *that = true;
   }
   return 0;
}

static int callback_has_file(void *data, int argc, char **argv, char **azColName){
   int *that = (int*)data;
   // *that = false;
   if (argc < 1) return -1;
   *that = atoi(argv[0]);
   return 0;
}

static bool db_has_fid(sqlite3 *db) {
    char *zErrMsg = 0;
    int rc = 0;
    bool db_has_fid = false;
    std::string sql = "pragma table_info(Files);";
    rc = sqlite3_exec(db, sql.c_str(), callback_has_fid, &db_has_fid, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO(db_has_fid?"Database has FID field":"Older database without the field field");
    }
    return db_has_fid;
}

static int db_has_file(sqlite3 *db, const std::string & filename, int & next_fid) {
    char *zErrMsg = 0;
    int rc = 0;
    int fid = -1, max_fid = -1;
    std::string sql = "select fid from Files where path=\""+filename+"\";";
    rc = sqlite3_exec(db, sql.c_str(), callback_has_file, &fid, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }
    sql = "select max(fid) from Files;";
    rc = sqlite3_exec(db, sql.c_str(), callback_has_file, &max_fid, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return -1;
    }
    next_fid = max_fid + 1;
    return fid;
}

bool EBSDAnalyzer::save_to_db(const std::string & sourcename, bool measurements,const std::string & dbname, bool update) {
    std::string sql;
    format fql;
    bool first;
    //char datestr[128]="";
    //struct tm now;
    //time_t t = time(NULL);
    boost::posix_time::ptime pnow;
    std::string nowstr;
    unsigned int histogram_id = 0;
    // Connect to DB
    if (boost::filesystem::exists( dbname ) ) {
        if (!update) {
            boost::filesystem::copy_file( dbname, dbname+".bak",
                    boost::filesystem::copy_option::overwrite_if_exists );
        }
    } else {
        update = false;
    }
    
    
    if( !connect_db(dbname) ){
        return false;
    }
    exec_db(db,"PRAGMA synchronous = OFF;");

    int fid = 0;
    if (update) {
        if (!db_has_fid(db)) {
            LOG_ERROR("Can't update this file because it does not have the FID field");
            return false;
        } 
        int next_fid = 0;
        fid = db_has_file(db, sourcename, next_fid);
        if (fid < 0) {
            fid = next_fid;
        } else {
            // Clean-up
            exec_db(db,str(format("DELETE FROM Files WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM Strings WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM Constants WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM Connected WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM ConnectedStats WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM ConnectedEdges WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM IngrainDistances WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM JointOrientationHistogram WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM Phases WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM Grains WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM GrainEdges WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM ConnectedClusters WHERE FID=%d;") % fid));
            exec_db(db,str(format("DELETE FROM ConnectedTwins WHERE FID=%d;") % fid));
            LOG_INFO("Removed existing data from the table");
        }
    } else {
        // Create tables
        exec_db(db,"DROP TABLE IF EXISTS Files;");
        exec_db(db,"DROP TABLE IF EXISTS Strings;");
        exec_db(db,"DROP TABLE IF EXISTS Constants;");
        exec_db(db,"DROP TABLE IF EXISTS TwinningTypes;");
        exec_db(db,"DROP TABLE IF EXISTS Connected;");
        exec_db(db,"DROP TABLE IF EXISTS ConnectedStats;");
        exec_db(db,"DROP TABLE IF EXISTS ConnectedEdges;");
        exec_db(db,"DROP TABLE IF EXISTS IngrainDistances;");
        exec_db(db,"DROP TABLE IF EXISTS JointOrientationHistogram;");
        exec_db(db,"DROP TABLE IF EXISTS Phases;");
        exec_db(db,"DROP TABLE IF EXISTS Grains;");
        exec_db(db,"DROP TABLE IF EXISTS GrainEdges;");
        exec_db(db,"DROP TABLE IF EXISTS ConnectedClusters;");
        exec_db(db,"DROP TABLE IF EXISTS ConnectedTwins;");
        LOG_INFO("Removed existing tables");
        sql = "CREATE TABLE Files("  
            "FID           INT PRIMARY KEY     NOT NULL,"
            "PATH          TEXT);";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE Strings("  
            "FID            INT,"
            "NAME           CHAR(64) NOT NULL,"
            "VALUE          TEXT,"
            "PRIMARY KEY (FID,NAME));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE Constants("  
            "FID            INT,"
            "NAME           CHAR(64) NOT NULL,"
            "VALUE          REAL,"
            "PRIMARY KEY (FID,NAME));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE TwinningTypes("  
            "ID           INT PRIMARY KEY     NOT NULL,"
            "NAME         CHAR(64));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        if (measurements) {
            exec_db(db,"DROP TABLE IF EXISTS Vertices;");
            exec_db(db,"DROP TABLE IF EXISTS Edges;");
            sql = "CREATE TABLE Vertices("  
                "FID            INT,"
                "ID             INT NOT NULL," 
                "PRIMARY KEY (FID,ID),"
                "X              REAL," 
                "Y              REAL," 
                "IQ             REAL," 
                "CI             REAL," 
                "FIT            REAL," 
                "QX             REAL," 
                "QY             REAL," 
                "QZ             REAL," 
                "QW             REAL," 
                "IS_GOOD        INT," 
                "IS_BORDER      INT," 
                "MEMBERSHIP     INT);";
            /* Execute SQL statement */
            if (!exec_db(db,sql)) { goto clean_exit; }

            sql = "CREATE TABLE Edges("  
                "FID            INT,"
                "I              INT," 
                "J              INT," 
                "PRIMARY KEY (FID,I,J),"
                "WEIGHT         REAL," 
                "DISO           REAL," 
                "QX             REAL," 
                "QY             REAL," 
                "QZ             REAL," 
                "QW             REAL," 
                "IS_SOLID          INT," 
                "IS_BORDER         INT);";
            if (!exec_db(db,sql)) { goto clean_exit; }
        }

        sql = "CREATE TABLE Connected("  
            "FID            INT,"
            "ID             INT NOT NULL," 
            "X              REAL," 
            "Y              REAL," 
            "IS_EMBEDDED    INT,"  // bool
            "IS_PARENT      INT,"  // bool
            "IS_MANUAL_PARENT INT,"  // bool
            "IS_MANUAL_TWIN INT,"  // bool
            "GRAIN          INT,"
            "CLUSTER        INT,"
            "PHASE          INT,"
            "TWINSTRIP      INT,"
            "IS_VALID       INT," 
            "DEGREE         INT," 
            "TWINNING_ORDER INT," 
            "NUM_CONN       INT," 
            "SIZE_GOOD      INT," 
            "SIZE           INT," 
            "BORDER_SIZE    INT," 
            "JOINT_BORDER_SIZE    INT,"  // size of the contact with the grain joint
            "MAP_EDGE       INT," // bool, does this grain touch the map's edge
            "PRIMARY KEY (FID,ID));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE ConnectedTwins("  
            "FID        INT,"
            "I          INT,"
            "J          INT,"
            "IS_MERGED  INT,"
            "PRIMARY KEY (FID,I,J));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE ConnectedStats("  
            "FID            INT,"
            "ID             INT NOT NULL," 
            "QX             REAL," 
            "QY             REAL," 
            "QZ             REAL," 
            "QW             REAL," 
            "COV_XX         REAL," 
            "COV_XY         REAL," 
            "COV_YY         REAL," 
            "LENGTH         REAL," 
            "THICKNESS      REAL," 
            "ANGLE          REAL," 
            "AREA           REAL," 
            "BORDER_LENGTH  REAL," 
            "ELLIPSICITY    REAL," 
            "SUBGRAIN_TYPE  INT,"
            "PRIMARY KEY (FID,ID));";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE IngrainDistances("  
            "FID       INT,"
            "I         INT," 
            "J         INT," 
            "DIST      REAL," 
            "Xi        REAL," 
            "Yi        REAL," 
            "Xj        REAL," 
            "Yj        REAL," 
            "PRIMARY KEY (FID,I,J)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE ConnectedEdges("  
            "FID         INT,"
            "I           INT," 
            "J           INT," 
            "QX          REAL," 
            "QY          REAL," 
            "QZ          REAL," 
            "QW          REAL," 
            "ACTIVE      INT,"  // bool
            "IS_MANUAL   INT,"  // bool
            "IS_EMBEDDED    INT,"  // J is embedded in I
            "NUM_CONN    INT,"
            "TWINNING    INT,"
            "VARIANT     INT,"
            "ERROR       REAL,"
            "ALPHA       REAL,"
            "BETA        REAL,"
            "HIST_ID     INT," // REFERS to JointOrientationHistogram
            "PRIMARY KEY (FID,I,J)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE JointOrientationHistogram("  
            "FID         INT,"
            "HIST_ID     INT," 
            "ANGLE_DEG   INT," 
            "OCCURENCE   INT" 
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE Grains("  
            "FID         INT,"
            "ID          INT NOT NULL," 
            "X           REAL," 
            "Y           REAL," 
            "SIZE        INT," 
            "QX          REAL," 
            "QY          REAL," 
            "QZ          REAL," 
            "QW          REAL," 
            "TWIN_JOINT_RATIO  REAL," // The proportion of the border belonging to twins 
            "NUM_CONN       INT," 
            "AREA           REAL," 
            "BORDER_LENGTH  REAL," 
            "CONVEXITY_RATIO REAL,"
            "PARENT_CLUSTER    INT," // Refers to a grain cluster
            "MAP_EDGE   INT," // Bool, based on the grain touching the map edge
            "ACTIVE     INT," // Bool, could be set by hand
            "PRIMARY KEY (FID,ID)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE Phases("  
            "FID              INT,"
            "ID               INT NOT NULL," 
            "X                REAL," 
            "Y                REAL," 
            "SIZE             INT," 
            "QX               REAL," 
            "QY               REAL," 
            "QZ               REAL," 
            "QW               REAL," 
            "DISOX            REAL," 
            "DISOY            REAL," 
            "DISOZ            REAL," 
            "DISOW            REAL," 
            "TWINNING_VARIANT INT," 
            "TWINNING_TYPE    INT," 
            "TWINNING_ORDER   INT," 
            "PARENT_PHASE     INT," 
            "CLUSTER          INT," 
            "GRAIN            INT," 
            "PRIMARY KEY (FID,ID)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }

        sql = "CREATE TABLE GrainEdges("  
            "FID         INT,"
            "I           INT," 
            "J           INT," 
            "QX          REAL," 
            "QY          REAL," 
            "QZ          REAL," 
            "QW          REAL," 
            "NUM_CONN    INT,"
            "BORDER_LENGTH  REAL,"
            "HIST_ID     INT," // Refers to JointOrientationHistogram
            "PRIMARY KEY (FID,I,J)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }


        sql = "CREATE TABLE ConnectedClusters("  
            "FID                INT,"
            "ID                 INT NOT NULL," 
            "GRAIN              INT," 
            "IS_PARENT          INT," // bool
            "TWINNING_ORDER     INT," 
            "QX                 REAL," 
            "QY                 REAL," 
            "QZ                 REAL," 
            "QW                 REAL," 
            "SIZE               INT," 
            "PRIMARY KEY (FID,ID)"
            ");";
        if (!exec_db(db,sql)) { goto clean_exit; }
    }
    LOG_INFO("Table created tables");


    sql = str(format("INSERT INTO Files (FID,PATH) VALUES (%1%, \"%2%\");") % fid % sourcename);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'MeshSize',%2%);") % fid % MeshSize);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'MeshArea',%2%);") % fid % MeshArea);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'DeltaX',%2%);") % fid % DeltaX);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'DeltaY',%2%);") % fid % DeltaY);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'SigmaSqr',%2%);") % fid % SigmaSqr);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'MinWeight',%2%);") % fid % MinWeight);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'MinComponentSize',%2%);") % fid % MinComponentSize);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'SubGrainMinEnclosure',%2%);") % fid % SubGrainMinEnclosure);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'MinJointWeight',%2%);") % fid % MinJointWeight);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'TwinningTypeTolerance',%2%);") % fid % TwinningTypeTolerance);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'TwinningGamma',%2%);") % fid % TwinningGamma);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'JointNeighbourhood',%2%);") % fid % JointNeighbourhood);
    sql += str(format("INSERT INTO Constants (FID,NAME,VALUE) VALUES (%1%, 'OrientationResolutionDeg',%2%);") % fid % OrientationResolutionDeg);
    sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'FileName','%2%');") % fid % sourcename);
    switch (MaterialType) { 
        case MAT_MAGNESIUM:
            sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'Material','%2%');") % fid % "Magnesium");
            break;
        case MAT_ZIRCONIUM:
            sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'Material','%2%');") % fid % "Zirconium");
            break;
        case MAT_MARTENSITE:
            sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'Material','%2%');") % fid % "Martensite");
            break;
        default:
            sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'Material','%2%');") % fid % "Undef");
            break;
    }

    //localtime_r(&t,&now);
    //strftime(datestr,128,"%Y-%m-%1% %H:%M:%S",&now);
    pnow = boost::posix_time::second_clock::local_time();
    nowstr = boost::posix_time::to_simple_string(pnow);

    sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'WriteTime','%2%');") % fid % nowstr);
    sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'CompileDate','%2%');") % fid % __DATE__);
    sql += str(format("INSERT INTO Strings (FID,NAME,VALUE) VALUES (%1%, 'CompileTime','%2%');") % fid % __TIME__);
    if (!exec_db(db,sql)) { goto clean_exit; }

    if (!update) {
        sql = str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'No Twinning');") % (int)NO_TWINNING);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Zero Twinning');") % (int)TWINNING_ZERO);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Tensile Mg');") % (int)Mg_TWINNING_TENSILE);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Compressive Mg');") % (int)Mg_TWINNING_COMPRESSIVE);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Tensile 1 Zr');") % (int)Zr_TWINNING_TENSILE_1);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Tensile 2 Zr');") % (int)Zr_TWINNING_TENSILE_2);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Compressive 1 Zr');") % (int)Zr_TWINNING_COMPRESSIVE_1);
        sql += str(format("INSERT INTO TwinningTypes (ID,NAME) VALUES (%1%,'Twinning Compressive 2 Zr');") % (int)Zr_TWINNING_COMPRESSIVE_2);
        if (!exec_db(db,sql)) { goto clean_exit; }
    }

    // Insert constants
    if (measurements) {
        /* Create SQL statement */
        exec_db(db, "BEGIN TRANSACTION");
        for (size_t i=0;i<vertices.size();++i) {
            const Vertex & u = vertices[i];
            fql = format("INSERT INTO Vertices (FID, ID, X, Y, IQ, CI, FIT, QX, QY, QZ, QW, "
                    " IS_GOOD, IS_BORDER, MEMBERSHIP) VALUES (%d,%d,%e,%e,%e,%e,%e, %e,%e,%e,%e, %d,%d,%d);")
                % fid % i % u.x % u.y % u.iq % u.ci % u.fit 
                % u.q.x() % u.q.y() % u.q.z() % u.q.w()
                % (int)u.good % (int)u.border % u.membership;
            if (i==0) printf("%s\n",fql.str().c_str());
            if (!exec_db(db,fql)) { goto clean_exit; }
        }
        exec_db(db, "END TRANSACTION");
        LOG_INFO("Inserted vertices");

        exec_db(db, "BEGIN TRANSACTION");
        for (size_t i=0;i<edges.size();++i) {
            const Edge & f = edges[i];
            const tf::Quaternion & q = f.q_disorientation;
            fql = format("INSERT INTO Edges (FID, I,J,WEIGHT,DISO,QX,QY,QZ,QW,IS_SOLID,IS_BORDER)"
                    " VALUES (%d,%d,%d,%e,%e, %e,%e,%e,%e, %d,%d);")
                % fid % f.i % f.j % f.weight % f.disorientation
                % q.x() % q.y() % q.z() % q.w() % (int)f.isSolid(vertices) % (int)f.isBorder(vertices);
            if (i==0) printf("%s\n",fql.str().c_str());
            if (!exec_db(db,fql)) { goto clean_exit; }
        }
        exec_db(db, "END TRANSACTION");
        LOG_INFO("Inserted edges");
    }

    exec_db(db, "BEGIN TRANSACTION");
    for(std::vector<Grain >::const_iterator it = grains.begin();it != grains.end(); it++) {
        const Grain & r = *it;
        const tf::Quaternion & q = r.qmean;
        double twin_joint_ratio = r.twin_border_vertices.size() / (double)r.border_vertices.size();
        fql = format("INSERT INTO Grains (FID,ID,X,Y, QX,QY,QZ,QW, SIZE,NUM_CONN,TWIN_JOINT_RATIO, PARENT_CLUSTER, MAP_EDGE, ACTIVE, AREA, BORDER_LENGTH,CONVEXITY_RATIO) "
                "VALUES (%d, %d,%e,%e, %e,%e,%e,%e, %d,%d,%e, %d,%d,%d, %e, %e, %e);")
            % fid % r.i % r.x % r.y % q.x() % q.y() % q.z() % q.w() 
            % r.size  % r.border_size % twin_joint_ratio
            % r.parent_cluster % (int)r.map_edge % (int)r.active
            % r.area % r.border_length % r.convexity_ratio;
        if (it==grains.begin()) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
        Grain::NeighbourMap::const_iterator nit;
        for (nit=r.neighbours.begin(); nit != r.neighbours.end(); nit++) {
            const GrainNeighbour & e = nit->second;
            const tf::Quaternion & q = e.q_disorientation;
            if (!e.storeOrientationHistogram(db,histogram_id)) {goto clean_exit;}
            fql = format("INSERT INTO GrainEdges (FID, I,J, QX,QY,QZ,QW, NUM_CONN,BORDER_LENGTH,HIST_ID) "
                    "VALUES (%d, %d,%d, %e,%e,%e,%e, %d,%e,%d);")
                % fid % e.i % e.j % q.x() % q.y() % q.z() % q.w() % e.weight % (e.weight*MeshSize) % histogram_id ;
            if (!exec_db(db,fql)) { goto clean_exit; }
            histogram_id += 1;
        }
    }
    for (size_t i=0;i<clusters.size();++i) {
        const tf::Quaternion & q = clusters[i].q_mean;
        fql = format("INSERT INTO ConnectedClusters (FID, ID,GRAIN,SIZE, IS_PARENT, TWINNING_ORDER, QX,QY,QZ,QW) "
                "VALUES (%d, %d,%d,%d,%d,%d, %e,%e,%e,%e);")
            % fid % i % clusters[i].group % clusters[i].size % (int)clusters[i].parent  % (int)clusters[i].twinning_order 
            % q.x() % q.y() % q.z() % q.w();
        if (i==0) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
    }
    for (size_t i=0;i<phases.size();++i) {
        const tf::Quaternion & q = phases[i].qmean;
        const tf::Quaternion & d = phases[i].qdiso;
        fql = format("INSERT INTO Phases (FID, ID,GRAIN,CLUSTER,SIZE, PARENT_PHASE, TWINNING_TYPE, TWINNING_ORDER, X,Y,"
                "QX,QY,QZ,QW, DISOX, DISOY, DISOZ, DISOW, TWINNING_VARIANT) "
                "VALUES (%d, %d,%d,%d,%d,%d,%d,%d, %e,%e, %e,%e,%e,%e, %e,%e,%e,%e, %d);")
            % fid % i % phases[i].grain % phases[i].cluster % phases[i].size 
            % (int)phases[i].parent  % (int)phases[i].twinning_order % (int)phases[i].twinning_type
            % phases[i].x % phases[i].y % q.x() % q.y() % q.z() % q.w()
            % d.x() % d.y() % d.z() % d.w() % (phases[i].twinning_variant);
        if (i==0) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
    }
    for (size_t i=0;i<ctwins.size();++i) {
        fql = format("INSERT INTO ConnectedTwins (FID, I,J,IS_MERGED) "
                "VALUES (%d, %d,%d,%d);")
            % fid % ctwins[i].i % ctwins[i].j % (int)(ctwins[i].type == ConnectedTwins::SINGLE);
        if (i==0) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
    }
    first = true;
    for (size_t i=0;i<connected.size();++i) {
        // We need to remove these otherwise, we'll just have to many of them
        const Connected & r = connected[i];
        if (!r.valid) continue;
        const tf::Quaternion & q = r.qmean;
        fql = format("INSERT INTO Connected (FID, ID,X,Y, IS_VALID,IS_EMBEDDED,IS_PARENT,IS_MANUAL_PARENT,IS_MANUAL_TWIN,TWINNING_ORDER,GRAIN,CLUSTER,PHASE,TWINSTRIP,DEGREE,NUM_CONN,SIZE,SIZE_GOOD,BORDER_SIZE,JOINT_BORDER_SIZE, MAP_EDGE) "
                "VALUES (%d, %d,%e,%e, %d,%d,%d,%d,%d,%d,%d, %d,%d,%d,%d, %d,%d,%d,%d,%d,%d);")
                % fid % (int)r.i % r.x % r.y 
                % (int)r.valid % (int)r.embedded % (int)r.parent % (int)r.manual_parent % (int) r.manual_twin % (int)r.twinning_order 
                % r.group % r.cluster % r.phase % r.twinstrip % r.neighbours.size() 
                % (int)r.num_connections % (int)r.size() %(int)r.vertices.size() % (int)r.border_vertices.size() % (int)r.grain_border_vertices.size()
                % (int)r.map_edge; 
        if (first) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
        fql = format("INSERT INTO ConnectedStats (FID,ID, QX,QY,QZ,QW, COV_XX,COV_XY,COV_YY, LENGTH,THICKNESS,ANGLE, AREA,BORDER_LENGTH,ELLIPSICITY,SUBGRAIN_TYPE) "
                "VALUES (%d,%d, %e,%e,%e,%e, %e,%e,%e, %e,%e,%e, %e,%e,%e,%d);")
                % fid % (int)r.i % q.x() % q.y() % q.z() % q.w() 
                % r.cov_xx % r.cov_xy % r.cov_yy % r.length % r.thickness % r.angle 
                % r.area % r.border_length % r.ellipsicity % r.subgrain_type; 
        if (first) printf("%s\n",fql.str().c_str());
        // printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
        Connected::NeighbourMap::const_iterator nit;
        assert((int)i == r.i);
        for (nit=r.neighbours.begin(); nit != r.neighbours.end(); nit++) {
            const tf::Quaternion & q = nit->second.q_disorientation;
            assert((int)i == nit->second.i);
            assert((int)nit->first == nit->second.j);
            if (!nit->second.storeOrientationHistogram(db,histogram_id)) {goto clean_exit;}
            fql = format("INSERT INTO ConnectedEdges (FID, I,J, QX,QY,QZ,QW, "
                    "NUM_CONN,ACTIVE,IS_MANUAL,IS_EMBEDDED,TWINNING,"
                    "VARIANT,ALPHA,BETA,ERROR,HIST_ID) "
                    "VALUES (%d, %d,%d, %e,%e,%e,%e, %d,%d,%d,%d, %d,%d, %e,%e,%e,%d);")
                % fid % nit->second.i % nit->second.j 
                % q.x() % q.y() % q.z() % q.w()
                % nit->second.weight % (int)nit->second.active % (int)nit->second.manual % (int)nit->second.embedded
                % (nit->second.twinning_type) % (nit->second.twinning_variant) % nit->second.alpha % nit->second.beta 
                % nit->second.twinning_error % histogram_id; 
            if (!exec_db(db,fql)) { goto clean_exit; }
            histogram_id += 1;
        }
        std::map<uint32_t,Connected::ClosestPoint>::const_iterator diti,ditj;
        for (diti=r.ingrain_distance.begin();diti!=r.ingrain_distance.end();diti++) {
            if (diti->first >= i) continue;
            ditj = connected[diti->first].ingrain_distance.find(i);
            assert(ditj != connected[diti->first].ingrain_distance.end());
            fql = format("INSERT INTO IngrainDistances (FID, I,J, DIST, Xi, Yi, Xj, Yj) "
                    "VALUES (%d, %d,%d, %e, %e, %e, %e, %e);")
                % fid % i % diti->first % diti->second.distance 
                % ditj->second.x % ditj->second.y
                % diti->second.x % diti->second.y;

            if (!exec_db(db,fql)) { goto clean_exit; }
        }
        first = false;
    }
    // Now also insert twinstrips as if they were normal connected parts
    for (size_t i=0;i<twinstrips.size();++i) {
        // We need to remove these otherwise, we'll just have to many of them
        const TwinStrip & r = twinstrips[i];
        const tf::Quaternion & q = r.qmean;
        fql = format("INSERT INTO Connected (FID, ID,X,Y, IS_VALID,IS_EMBEDDED,IS_PARENT,IS_MANUAL_PARENT,IS_MANUAL_TWIN,TWINNING_ORDER,GRAIN,CLUSTER,PHASE,TWINSTRIP,DEGREE,NUM_CONN,SIZE,BORDER_SIZE,JOINT_BORDER_SIZE, MAP_EDGE) "
                "VALUES (%d, %d,%e,%e, %d,%d,%d,%d,%d,%d, %d,%d,%d,%d,%d, %d,%d,%d,%d,%d);")
                % fid % (int)r.i % r.x % r.y 
                % (int)r.valid % (int)r.embedded % (int)r.parent % (int)r.manual_parent % (int)r.manual_twin % (int)r.twinning_order 
                % r.group % r.cluster % r.phase % r.twinstrip % r.neighbours.size() 
                % (int)r.num_connections % (int)r.vertices.size() % (int)r.border_vertices.size() % (int)r.grain_border_vertices.size()
                % (int)r.map_edge; 
        if (first) printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
        fql = format("INSERT INTO ConnectedStats (FID, ID, QX,QY,QZ,QW, COV_XX,COV_XY,COV_YY, LENGTH,THICKNESS,ANGLE, AREA,BORDER_LENGTH,ELLIPSICITY,SUBGRAIN_TYPE) "
                "VALUES (%d, %d, %e,%e,%e,%e, %e,%e,%e, %e,%e,%e, %e,%e,%e,%d);")
                % fid % (int)r.i % q.x() % q.y() % q.z() % q.w() 
                % r.cov_xx % r.cov_xy % r.cov_yy % r.length % r.thickness % r.angle 
                % r.area % r.border_length % r.ellipsicity % r.subgrain_type; 
        if (first) printf("%s\n",fql.str().c_str());
        // printf("%s\n",fql.str().c_str());
        if (!exec_db(db,fql)) { goto clean_exit; }
        Connected::NeighbourMap::const_iterator nit;
        for (nit=r.neighbours.begin(); nit != r.neighbours.end(); nit++) {
            const tf::Quaternion & q = nit->second.q_disorientation;
            if (!nit->second.storeOrientationHistogram(db,histogram_id)) {goto clean_exit;}
            fql = format("INSERT INTO ConnectedEdges (FID, I,J, QX,QY,QZ,QW, NUM_CONN,ACTIVE,IS_MANUAL,IS_EMBEDDED,TWINNING,VARIANT,ALPHA,BETA,ERROR,HIST_ID) "
                    "VALUES (%d, %d,%d, %e,%e,%e,%e, %d,%d,%d,%d, %d,%d, %e,%e,%e, %d);")
                % fid % nit->second.i % nit->second.j 
                % q.x() % q.y() % q.z() % q.w()
                % nit->second.weight % (int)nit->second.active % (int)nit->second.manual % (int)nit->second.embedded
                % (nit->second.twinning_type) % (nit->second.twinning_variant) % nit->second.alpha % nit->second.beta 
                % nit->second.twinning_error % histogram_id; 
            if (!exec_db(db,fql)) { goto clean_exit; }
            histogram_id += 1;
        }

        for (nit=r.rev_neighbours.begin(); nit != r.rev_neighbours.end(); nit++) {
            const tf::Quaternion & q = nit->second.q_disorientation;
            if (!nit->second.storeOrientationHistogram(db,histogram_id)) {goto clean_exit;}
            fql = format("INSERT INTO ConnectedEdges (FID, I,J, QX,QY,QZ,QW, NUM_CONN,ACTIVE,IS_MANUAL,IS_EMBEDDED,TWINNING,VARIANT,ALPHA,BETA,ERROR,HIST_ID) "
                    "VALUES (%d, %d,%d, %e,%e,%e,%e, %d,%d,%d,%d, %d,%d, %e,%e,%e, %d);")
                % fid % nit->second.i % nit->second.j 
                % q.x() % q.y() % q.z() % q.w()
                % nit->second.weight % (int)nit->second.active % (int)nit->second.manual % (int)nit->second.embedded
                % (nit->second.twinning_type) % (nit->second.twinning_variant) 
                % nit->second.alpha % nit->second.beta % nit->second.twinning_error % histogram_id; 
            if (!exec_db(db,fql)) { goto clean_exit; }
            histogram_id += 1;
        }
        first = false;
    }
    exec_db(db, "END TRANSACTION");

    // Close DB
    return true;
clean_exit:
    return false;
}

static int callback_settings(void *data, int argc, char **argv, char **azColName){
   EBSDAnalyzer *that = (EBSDAnalyzer*)data;
   if (argc < 2) return -1;
   that->update_settings(argv[0],argv[1]);
   return 0;
}
bool EBSDAnalyzer::update_settings(const std::string & name, const std::string & value) {
    if (name == "SigmaSqr") { 
        SigmaSqr = atof(value.c_str()); 
        EdgeTolerance = sqrt(SigmaSqr);
        LOG_INFO("DB: SigmaSqr %.5f",SigmaSqr);
    } else if (name == "MinWeight") { 
        MinWeight = atof(value.c_str()); 
        LOG_INFO("DB: MinWeight %.5f",MinWeight);
    } else if (name == "MinComponentSize") { 
        MinComponentSize = atof(value.c_str()); 
        LOG_INFO("DB: MinComponentSize %.5f",MinComponentSize);
    } else if (name == "SubGrainMinEnclosure") {
        SubGrainMinEnclosure = atof(value.c_str()); 
        LOG_INFO("DB: SubGrainMinEnclosure %.5f",SubGrainMinEnclosure);
    } else if (name == "MinJointWeight") {
        MinJointWeight = atoi(value.c_str()); 
        LOG_INFO("DB: MinJointWeight %.5f",MinJointWeight);
    } else if (name == "TwinningTypeTolerance") {
        TwinningTypeTolerance = atof(value.c_str()); 
        LOG_INFO("DB: TwinningTypeTolerance %.5f",TwinningTypeTolerance);
    } else if (name == "TwinningGamma") {
        TwinningGamma = atof(value.c_str()); 
        LOG_INFO("DB: TwinningGamma %.5f",TwinningGamma);
    } else if ((name == "Material") && (value == "Magnesium")) {
        MaterialType = MAT_MAGNESIUM;
        TwinningGammaMg = TwinningGamma;
    } else if ((name == "Material") && (value == "Zirconium")) {
        MaterialType = MAT_ZIRCONIUM;
        TwinningGammaZr = TwinningGamma;
    } else if ((name == "Material") && (value == "Martensite")) {
        MaterialType = MAT_MARTENSITE;
        TwinningGammaMr = TwinningGamma;
    } 
    return true;
}

static int callback_connected(void *data, int argc, char **argv, char **azColName){
   EBSDAnalyzer *that = (EBSDAnalyzer*)data;
   if (argc < 3) return -1;
   if (argc==4) {
       that->update_connected_settings(atoi(argv[0]),atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
   } else {
       that->update_connected_settings(atoi(argv[0]),atoi(argv[1]),atoi(argv[2]),0);
   }
   return 0;
}
bool EBSDAnalyzer::update_connected_settings(size_t id, bool is_parent, bool is_manual_parent, bool is_manual_twin) {
    if (id >= connected.size()) {
        LOG_ERROR("Trying to update inexistent connected part from DB: %d",(int)id);
        return false;
    }
    connected[id].manual_parent = is_manual_parent;
    connected[id].manual_twin = is_manual_twin;
    if (is_manual_parent) {
        connected[id].parent = true;
    }
    if (is_manual_twin) {
        connected[id].parent = false;
    }
    return true;
}

static int callback_edges(void *data, int argc, char **argv, char **azColName){
   EBSDAnalyzer *that = (EBSDAnalyzer*)data;
   if (argc < 4) return -1;
   that->update_connected_edge_settings(atoi(argv[0]),atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
   return 0;
}

static int callback_twins(void *data, int argc, char **argv, char **azColName){
   EBSDAnalyzer *that = (EBSDAnalyzer*)data;
   if (argc < 3) return -1;
   that->add_connected_twins(atoi(argv[0]),atoi(argv[1]),atoi(argv[2]));
   return 0;
}

bool EBSDAnalyzer::add_connected_twins(size_t i, size_t j, bool is_merged) {
    if(i >= connected.size()) {
        LOG_ERROR("Trying to insert inconsistent connected twins from DB: %d",(int)i);
        return false;
    }
    if(j >= connected.size()) {
        LOG_ERROR("Trying to insert inconsistent connected twins from DB: %d",(int)j);
        return false;
    }
    ConnectedTwins ct;
    ct.i = i;
    ct.j = j;
    ct.type = is_merged?ConnectedTwins::SINGLE:ConnectedTwins::CONNECTED;
    ctwins.push_back(ct);
    connected[ct.i].connected_twin = true;
    connected[ct.j].connected_twin = true;
    return true;
}

bool EBSDAnalyzer::update_connected_edge_settings(size_t i,size_t j, bool is_active, bool is_manual) {
    if (i >= connected.size()) {
        LOG_ERROR("Trying to update inexistent connected part from DB: %d",(int)i);
        return false;
    }
    Connected & r = connected[i];
    Connected & s = connected[j];
    Connected::NeighbourMap::iterator nit = r.neighbours.find((unsigned int)j);
    if (nit == r.neighbours.end()) {
        LOG_ERROR("Trying to update inexistent edge from DB: %d - %d",(int)i,(int)j);
        return false;
    }
    nit->second.manual = is_manual;
    if (is_manual) {
        nit->second.active = is_active;
    }

    nit  = s.neighbours.find((unsigned int)i);
    if (nit == s.neighbours.end()) {
        LOG_ERROR("Trying to update inexistent edge from DB: %d - %d",(int)i,(int)j);
        return false;
    }
    nit->second.manual = is_manual;
    if (is_manual) {
        nit->second.active = is_active;
    }
    return true;
}

bool EBSDAnalyzer::load_settings_from_db(const std::string & sourcename, 
        const std::string & dbname) {
    char *zErrMsg = 0;
    int  rc;
    std::string sql;
    format fql;
    // Connect to DB
    if( !connect_db(dbname) ){
        return false;
    }

    TwinningGamma = -1;
    if (!db_has_fid(db)) {
        rc = sqlite3_exec(db, "select Name,Value from Constants;", callback_settings, (void*)this, &zErrMsg);
    } else {
        int next_fid = 0;
        int fid = db_has_file(db, sourcename, next_fid);
        if (fid < 0) {
            return false;
            LOG_ERROR("This DB does not contains information about this file");
        }
        rc = sqlite3_exec(db, 
                str(format("select Name,Value from Constants where FID=%d;") % fid).c_str(),
                callback_settings, (void*)this, &zErrMsg);
    }
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO("Constants updated successfully");
    }

    return true;
}

static int callback_table_info(void *data, int argc, char **argv, char **azColName){
   bool *that = (bool*)data;
   // *that = false;
   if (argc < 2) return -1;
   if (strcasecmp(argv[1],"twinstrip")==0) {
       that[0] = true;
   }
   if (strcasecmp(argv[1],"is_manual_parent")==0) {
       that[1] = true;
   }
   if (strcasecmp(argv[1],"is_manual_twin")==0) {
       that[2] = true;
   }
   return 0;
}

bool EBSDAnalyzer::load_connected_settings_from_db(const std::string & sourcename, 
        const std::string & dbname) {
    char *zErrMsg = 0;
    int  rc;
    std::string sql;
    format fql;
    // Connect to DB
    if( !connect_db(dbname) ){
        return false;
    }

    bool db_has[3]={false,false,false};
    sql = "pragma table_info(Connected);";
    rc = sqlite3_exec(db, sql.c_str(), callback_table_info, db_has, &zErrMsg);
    bool db_has_twinstrip = db_has[0];
    bool db_has_manual_parent = db_has[1];
    bool db_has_manual_twin = db_has[2];
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO(db_has_twinstrip?"Database has the twinstrip flag":"Older database without the twinstrip flag");
        LOG_INFO(db_has_manual_parent?"Database has the is_manual_parent flag":"Older database without the is_manual_parent flag");
        LOG_INFO(db_has_manual_twin?"Database has the is_manual_twin flag":"Older database without the is_manual_twin flag");
    }


    sql = "select Id,IS_PARENT,IS_MANUAL";
    if (db_has_manual_parent) {
        sql += "_PARENT";
    }
    if (db_has_manual_twin) {
        sql += ",IS_MANUAL_TWIN";
    }
    sql += " from Connected where (is_manual";
    if (db_has_manual_parent) {
        sql += "_parent";
    }
    if (db_has_manual_twin) {
        sql += " or is_manual_twin";
    }
    sql += ")";
    if (db_has_twinstrip) {
        sql += " and twinstrip>=0";
    }
    sql += ";";
    rc = sqlite3_exec(db, sql.c_str(), callback_connected, (void*)this, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO("Connected flags updated successfully");
    }

    sql = "select e.i,e.j,e.ACTIVE,e.IS_MANUAL "
            "from connectededges as e "
            "inner join connected as c1 on c1.id=e.i "
            "inner join connected as c2 on c2.id=e.j "
            "where e.i != e.j and e.IS_MANUAL ";
    if (db_has_twinstrip) {
            sql += "and c1.twinstrip>=0 and c2.twinstrip>=0";
    }
    sql += ";";
    rc = sqlite3_exec(db, sql.c_str(), callback_edges, (void*)this, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO("Connected flags updated successfully");
    }

    sql = "select i,j,is_merged from ConnectedTwins;";
    rc = sqlite3_exec(db, sql.c_str(), callback_twins, (void*)this, &zErrMsg);
    if( rc != SQLITE_OK ){
        LOG_ERROR("SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }else{
        LOG_INFO("Connected twins updated successfully");
    }
    if (TwinningGamma>0) {
        switch (MaterialType) {
            case MAT_MAGNESIUM:
                TwinningGammaMg = TwinningGamma;
                LOG_INFO("Twinning gamma Mg: %.2f",TwinningGammaMg);
                break;
            case MAT_ZIRCONIUM:
                TwinningGammaZr = TwinningGamma;
                LOG_INFO("Twinning gamma Zr: %.2f",TwinningGammaZr);
                break;
            case MAT_MARTENSITE:
                TwinningGammaMr = TwinningGamma;
                LOG_INFO("Twinning gamma Mr: %.2f",TwinningGammaMr);
                break;
            default:
                break;
        }
    }
    return true;
}


