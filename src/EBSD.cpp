
#include <crystals/convex_hull.h>
#include <crystals/Graph.h>
#include <crystals/EBSD.h>
#include <crystals/log.h>
#include <boost/format.hpp>
#include <queue>


#define QUIET_QHULL

using boost::format;
using namespace crystals;

EBSDAnalyzer::~EBSDAnalyzer() {
    close_db();
}

const char * crystals::EBSDFileTypeToString(EBSDFileType ft) {
    switch (ft) {
        case FileTypeStandard: return "Standard";
        case FileTypeCTF: return "CTF";
        case FileTypeCPR: return "CPR";
        default: return "Undefined";
    }
}

class PECData : public QThread {
    public :
        PECData(EBSDAnalyzer*analyzer, size_t from, size_t to) : that(analyzer),v_start(from), v_end(to) {}
        void run() {
            edges.clear();
            that->prepare_edge_and_color(edges, v_start,v_end);
        }
    public:
        EBSDAnalyzer *that;
        size_t v_start, v_end;
        std::vector<Edge> edges;
};



bool EBSDAnalyzer::prepare_edge_and_color(std::vector<Edge> & edges, size_t v_start, size_t v_end) {
    assert(v_start < vertices.size());
    assert(v_end <= vertices.size());
    for (size_t i=v_start;i<v_end;++i) {
        CoordMap::const_iterator it;
        Vertex & v = vertices[i];
        v.update_quaternion(QRef);
    }
    edges.reserve((v_end-v_start)*3);
    for (size_t i=v_start;i<v_end;++i) {
        CoordMap::const_iterator it;
        Vertex & v = vertices[i];
#if 1
        const tf::Quaternion & qs = v.q;
#else
        tf::Quaternion qs = QDisorientation(QRef,v.q).second;
#endif
        v.color = EBSDColorSet(qs,v.iq,v.ci,v.fit);
        if (square_mesh) {
            if (coords.find(ICoord(v.i-1,v.j)) == coords.end()) { v.map_edge = true; }
            if (coords.find(ICoord(v.i,v.j-1)) == coords.end()) { v.map_edge = true; }
            it = coords.find(ICoord(v.i+1,v.j));
            if (it != coords.end()) {
                edges.push_back(Edge(vertices, (int)i, 0, it->second, 2));
                vertices[i].edges[0] = -(int32_t)(edges.size());
                vertices[it->second].edges[2] = (int32_t)(edges.size());
            } else {
                v.map_edge = true;
            }
            it = coords.find(ICoord(v.i,v.j+1));
            if (it != coords.end()) {
                edges.push_back(Edge(vertices, (int)i, 3, it->second, 1));
                vertices[i].edges[3] = -(int32_t)(edges.size());
                vertices[it->second].edges[1] = (int32_t)(edges.size());
            } else {
                v.map_edge = true;
            }
        } else {
            if (coords.find(ICoord(v.i-2,v.j)) == coords.end()) { v.map_edge = true; }
            if (coords.find(ICoord(v.i-1,v.j-1)) == coords.end()) { v.map_edge = true; }
            if (coords.find(ICoord(v.i+1,v.j-1)) == coords.end()) { v.map_edge = true; }
            it = coords.find(ICoord(v.i+2,v.j));
            if (it != coords.end()) {
                edges.push_back(Edge(vertices, (int)i, 0, it->second, 3));
                vertices[i].edges[0] = -(int32_t)(edges.size());
                vertices[it->second].edges[3] = (int32_t)(edges.size());
            } else {
                v.map_edge = true;
            }
            it = coords.find(ICoord(v.i-1,v.j+1));
            if (it != coords.end()) {
                edges.push_back(Edge(vertices, (int)i, 4, it->second, 1));
                vertices[i].edges[4] = -(int32_t)(edges.size());
                vertices[it->second].edges[1] = (int32_t)(edges.size());
            } else {
                v.map_edge = true;
            }
            it = coords.find(ICoord(v.i+1,v.j+1));
            if (it != coords.end()) {
                edges.push_back(Edge(vertices, (int)i, 5, it->second, 2));
                vertices[i].edges[5] = -(int32_t)(edges.size());
                vertices[it->second].edges[2] = (int32_t)(edges.size());
            } else {
                v.map_edge = true;
            }
        }
    }
    return true;
}

bool EBSDAnalyzer::build_faces()
{
    CoordMap face_map;
    Face::resetFaceSize();
    faces.clear();
    faces.reserve(vertices.size()*3);
    // Assumption, the edges have been connected
    for (size_t i=0;i<vertices.size();++i) {
        const Vertex & v = vertices[i];
        if (square_mesh) {
            if (v.edges[0] && v.edges[3]) {
                int32_t e1, e2, e3, e4;
                e1 = abs(v.edges[0])-1;
                e2 = abs(v.edges[3])-1;
                const Vertex & w = vertices[edges[e1].j];
                const Vertex & u = vertices[edges[e2].j];
                if (w.edges[3]) {
                    assert(u.edges[0]); // that must be true by construction
                    e3 = abs(w.edges[3])-1;
                    e4 = abs(u.edges[0])-1;
                    face_map.insert(CoordMap::value_type(ICoord(v.i,v.j),(uint32_t)faces.size()));
                    faces.push_back(Face(v.i,v.j, vertices,edges, e1,e2,e3,e4));
                }
            }
        } else {
            if (v.edges[4] && v.edges[5]) {
                int32_t e1, e2, e3;
                e1 = abs(v.edges[4])-1;
                e2 = abs(v.edges[5])-1;
                const Vertex & w = vertices[edges[e2].j];
                e3 = abs(w.edges[3])-1;
                face_map.insert(CoordMap::value_type(ICoord(v.i,v.j),(uint32_t)faces.size()));
                faces.push_back(Face(v.i,v.j,true,vertices,edges, e2,e3,e1));
            }
            if (v.edges[0] && v.edges[5]) {
                int32_t e1, e2, e3;
                e1 = abs(v.edges[0])-1;
                e2 = abs(v.edges[5])-1;
                const Vertex & w = vertices[edges[e1].j];
                e3 = abs(w.edges[4])-1;
                face_map.insert(CoordMap::value_type(ICoord(v.i+1,v.j),(uint32_t)faces.size()));
                faces.push_back(Face(v.i+1,v.j,false,vertices,edges, e3,e1,e2));
            }
        }
    }
    // Now build neighbourhood
    for (size_t i=0;i<faces.size();i++) {
        Face & f = faces[i];
        CoordMap::const_iterator it;
        if (square_mesh) {
            // Order of neighbours to match traversed edges, by design
            it = face_map.find(ICoord(f.i,f.j-1));
            if (it != face_map.end()) {
                f.neighbours[0] = it->second;
            }
            it = face_map.find(ICoord(f.i-1,f.j));
            if (it != face_map.end()) {
                f.neighbours[1] = it->second;
            }
            it = face_map.find(ICoord(f.i+1,f.j));
            if (it != face_map.end()) {
                f.neighbours[2] = it->second;
            }
            it = face_map.find(ICoord(f.i,f.j+1));
            if (it != face_map.end()) {
                f.neighbours[3] = it->second;
            }
        } else {
            it = face_map.find(ICoord(f.i+1,f.j));
            if (it != face_map.end()) {
                f.neighbours[0] = it->second;
            }
            if (f.up) {
                it = face_map.find(ICoord(f.i,f.j+1));
            } else {
                it = face_map.find(ICoord(f.i,f.j-1));
            }
            if (it != face_map.end()) {
                f.neighbours[1] = it->second;
            }
            it = face_map.find(ICoord(f.i-1,f.j));
            if (it != face_map.end()) {
                f.neighbours[2] = it->second;
            }
        }
    }
    return true;
}

bool EBSDAnalyzer::parse_file(const std::string & sourcename,EBSDFileType filetype,bool radian)
{
    LOG_INFO("Parsing file (%s, angles in %s) and building vertex list",
            EBSDFileTypeToString(filetype),radian?"radian":"degree");
    switch (filetype) {
        case FileTypeCTF:
            square_mesh = true;
            break;
        case FileTypeCPR:
            square_mesh = true;
            break;
        case FileTypeStandard:
        default:
            square_mesh = false;
            break;
    }
    EBSDColorSet::resetEBSDStats();
    FILE * fp = fopen(sourcename.c_str(),"r");
    if (!fp) {
        perror("EBSDAnalyzer::parse_file:");
        return false;
    }
    
    vertices.clear();
    edges.clear();
    while (!feof(fp)) {
        char buffer[1024];
        if (fgets(buffer,1024,fp)!=NULL) {
            Vertex v;
            switch (filetype) {
                case FileTypeCTF:
                    if (v.loadFromCTF(buffer,radian) /* && (v.x < 25.0) && (v.y < 25.0) */) {
                        vertices.push_back(v);
                    }
                    break;
                case FileTypeCPR:
                    if (v.loadFromCPR(buffer,radian) /* && (v.x < 25.0) && (v.y < 25.0) */) {
                        vertices.push_back(v);
                    }
                    break;
                case FileTypeStandard:
                default:
                    if (v.loadFromString(buffer,radian) /* && (v.x < 50.0) && (v.y < 50.0) */ ) {
                        vertices.push_back(v);
                    }
                    break;
            }
        }
    }
    fclose(fp);
    if (vertices.size()<2) {
        LOG_ERROR("No data point in file, aborting");
        return false;
    }
    edges.reserve(vertices.size()*3);
    // Assuming the two first points are on the same line
    if (square_mesh) {
        assert(fabs(vertices[1].y - vertices[0].y)<1e-2);
        MeshSize = (vertices[1].x - vertices[0].x);
        MeshArea = SQR(MeshSize);
        DeltaX = MeshSize;
        DeltaY = MeshSize;
#ifdef USE_CGAL
        cgal_point structure[4] = {
            cgal_point(MeshSize/2,MeshSize/2),
            cgal_point(-MeshSize/2,MeshSize/2),
            cgal_point(-MeshSize/2,-MeshSize/2),
            cgal_point(MeshSize/2,-MeshSize/2)
        };
        ebsd_footprint = CrystalPolygon(structure,structure+4);
#endif
        MinComponentSize = std::max(MinComponentSize,4.0*MeshArea);
    } else {
        assert(fabs(vertices[1].y - vertices[0].y)<1e-2);
        MeshSize = (vertices[1].x - vertices[0].x);
        MeshArea = 6 * SQR(MeshSize/2)/sqrt(3);
        DeltaX = MeshSize/2;
        DeltaY = sqrt(SQR(MeshSize) - SQR(MeshSize/2));
#ifdef USE_CGAL
        cgal_point structure[6] = {
            cgal_point(MeshSize/2,tan(M_PI/6)*MeshSize/2),
            cgal_point(0,(MeshSize/2)/cos(M_PI/6)),
            cgal_point(-MeshSize/2,tan(M_PI/6)*MeshSize/2),
            cgal_point(-MeshSize/2,-tan(M_PI/6)*MeshSize/2),
            cgal_point(0,-(MeshSize/2)/cos(M_PI/6)),
            cgal_point(MeshSize/2,-tan(M_PI/6)*MeshSize/2),
        };
        ebsd_footprint = CrystalPolygon(structure,structure+6);
#endif
        MinComponentSize = std::max(MinComponentSize,2.5*MeshArea);
    }
    XOffset = vertices[0].x;
    YOffset = vertices[0].y;
    LOG_INFO("Grid offset: %f %f steps: %f %f area: %.f",XOffset,YOffset,DeltaX,DeltaY, MeshArea);
    if (MeshArea < 1e-8) {
        LOG_ERROR("Invalid grid step and mesh area, aborting");
        return false;
    }
    for (size_t i=0;i<vertices.size();++i) {
        vertices[i].update_indices();
        // if (i < 10) {
        //     printf("Vertice %d : %d %d\n",(int)i,vertices[i].i,vertices[i].j);
        // }
        coords.insert(CoordMap::value_type(ICoord(vertices[i].i,vertices[i].j),(uint32_t)i));
    }
    x_min=vertices[0].x;
    y_min=vertices[0].y;
    x_max=vertices[0].x;
    y_max=vertices[0].y;
    for (size_t i=0;i<vertices.size();++i) {
        if (vertices[i].x > x_max) x_max = vertices[i].x;
        if (vertices[i].y > y_max) y_max = vertices[i].y;
        if (vertices[i].x < x_min) x_min = vertices[i].x;
        if (vertices[i].y < y_min) y_min = vertices[i].y;
    }
    height = ((y_max-y_min)*width)/(x_max-x_min);
    mesh = QImage(width,height,QImage::Format_RGB32);
    display = QImage(width,height,QImage::Format_RGB32);
    mesh.fill(QColor(0,0,0));
    display.fill(QColor(0,0,0));
    idFontFg.setFamily(QString("Courier"));
    idFontFg.setWeight(QFont::Normal);
    idFontFg.setPointSize(12);
    idFontFg.setStyleHint(QFont::Courier);
    idFontFg.setFixedPitch(true);
    idFontFg.setWeight(QFont::Bold);
    reset_zoom();
    LOG_INFO("Using grid aspect ratio: image size is %dx%d",(int)width,(int)height);


    LOG_INFO("Computing colors");
    LOG_INFO("All color computed w.r.t %.2f %.2f %.2f %.2f",
            QRef.w(),QRef.x(),QRef.y(),QRef.z());
    if (num_threads <= 1) {
        prepare_edge_and_color(edges,0,vertices.size());
    } else {
        size_t v_blocks = (size_t)ceil(vertices.size()/(double)num_threads);
        std::vector<PECData *> worker(num_threads);
        for (size_t i=0;i<num_threads;++i) {
            worker[i] = new PECData(this,i*v_blocks,std::min((i+1)*v_blocks,vertices.size()));
            QObject::connect(worker[i], SIGNAL(finished()), worker[i], SLOT(deleteLater()));
            worker[i]->start();
        }
        for (size_t i=0;i<num_threads;++i) {
            worker[i]->wait();
        }
        for (size_t i=0;i<num_threads;++i) {
            if (edges.size()>0) {
                for (size_t j=0;j<worker[i]->edges.size();j++) {
                    const Edge & e = worker[i]->edges[j];
                    if (vertices[e.i].edges[e.slot_i]>0) {
                        vertices[e.i].edges[e.slot_i] += (int32_t)edges.size();
                    } else {
                        vertices[e.i].edges[e.slot_i] -= (int32_t)edges.size();
                    }
                    if (vertices[e.j].edges[e.slot_j]>0) {
                        vertices[e.j].edges[e.slot_j] += (int32_t)edges.size();
                    } else {
                        vertices[e.j].edges[e.slot_j] -= (int32_t)edges.size();
                    }
                }
                edges.insert(edges.end(),worker[i]->edges.begin(),worker[i]->edges.end());
            } else {
                edges = worker[i]->edges;
            }
        }
    }
    LOG_INFO("Building faces");

    build_faces();

    LOG_INFO("Completely parsed input file");
    reset_zoom();
    return true;
}



struct RGEdge {
    size_t g;
    size_t i,j;
    bool cut_it;
    Grain gsplit[2];
    RGEdge() : cut_it(false) {}
    RGEdge(size_t g, size_t i, size_t j) : g(g), i(i), j(j), cut_it(false) {}
};

class RGData: public QThread {
    public:
        EBSDAnalyzer *that;
        std::vector<RGEdge> groups;
        void run() {
            // LOG_INFO("Starting RG thread %lu",(long unsigned int)data->tid);
            for (size_t i = 0; i <groups.size(); ++i) {
                RGEdge & rge = groups[i];
                rge.cut_it = that->test_split_group(rge.g,rge.i,rge.j,rge.gsplit);
                // LOG_INFO("Group %d: %d - %d : %d",(int)rge.g, (int)rge.i, (int)rge.j,rge.cut_it);
            }
            // LOG_INFO("Completed RG thread %lu",(long unsigned int)data->tid);
        }
};

class GPData : public QThread{
    public:
        EBSDAnalyzer *that;
        size_t g_start, g_end;
        void run() {
            // LOG_INFO("Starting GP thread %lu for i in [%d,%d[",(long unsigned int)data->tid,(int)data->start,(int)data->end);
            for (size_t i = g_start; i < g_end; ++i) {
                // LOG_INFO("Updating group %d",(int)i);
                that->update_group_properties(i);
            }
            // LOG_INFO("Completed GP thread %lu",(long unsigned int)data->tid);
        }
};



void EBSDAnalyzer::update_grain_groups() {
    assert(num_threads > 0);
    std::vector<GPData*> data(num_threads);
    for (size_t i=0;i<num_threads;++i) {
        data[i] = new GPData;
        data[i]->that = this;
        data[i]->g_start = i*grains.size()/num_threads;
        data[i]->g_end = std::min(grains.size(),(i+1)*grains.size()/num_threads);
        QObject::connect(data[i], SIGNAL(finished()), data[i], SLOT(deleteLater()));
        data[i]->start();
    }
    // Now all the threads are running
    for (size_t i=0;i<num_threads;++i) {
        data[i]->wait();
    }

    update_face_and_joints();
}

bool EBSDAnalyzer::build_face_and_joints() 
{
    // TODO: the joint structure will not change depending on how we update
    // the grain structure. If this take too much time, move it to
    // "build_connected_parts" and only update the grains on each side here. 
    // First update face type
// #define DEBUG_FACES
#ifdef DEBUG_FACES
    FILE * fp = fopen("faces.txt","w");
#endif
    for (size_t i=0;i<faces.size();i++) {
        Face & f = faces[i];
        f.updateFaceType(vertices,edges);
        if (f.type > InteriorFace) {
#ifdef DEBUG_FACES
            fprintf(fp,"%d %d %d |%d %d %d| %d %d %d | %d %d %d\n",
                    (int)i,f.i,f.j,f.type,f.grain_border,f.border,
                    f.vertices[0], f.vertices[1], f.vertices[2],
                    f.neighbours[0], f.neighbours[1], f.neighbours[2]);
#endif
        }
    }
#ifdef DEBUG_FACES
    fclose(fp);
#endif
    // First Clean-Up
    for (size_t i=0;i<connected.size();i++) {
        connected[i].joints.clear();
        Connected::NeighbourMap::iterator it;
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            it->second.joints.clear();
        }
    }
    // First search for contours, starting from branch points
    for (size_t i=0;i<faces.size();i++) {
        const Face & f = faces[i];
        if (!f.border) continue;
        if ((f.type!=EndOfJointFace)&&(f.type!=JointBranchFace)) {
            continue;
        }
        // if (f.visited) continue;
        for (size_t k=0;k<Face::FaceSize;k++) {
            if (f.neighbours[k]<0) continue;
            if (faces[f.neighbours[k]].visited) continue;
            Joint J;
            if (J.buildFromBranch(faces,vertices,edges,(uint32_t)i,(uint8_t)k)) {
                // LOG_INFO("Built joint %d: %d faces",joints.size(),J.face_ids.size());
                if (J.connected_joint) {
                    connected[J.connected_1].joints.push_back((int)joints.size());
                    connected[J.connected_2].joints.push_back((int)joints.size());
                    connected[J.connected_1].add_joint(J.connected_2,joints.size());
                    connected[J.connected_2].add_joint(J.connected_1,joints.size());
                }
                joints.push_back(J);
            }
        }
    }
    // Then do it again for closed contours we haven't seen
    for (size_t i=0;i<faces.size();i++) {
        const Face & f = faces[i];
        if (!f.border) continue;
        if (f.visited) continue;
        for (size_t k=0;k<Face::FaceSize;k++) {
            if (f.neighbours[k]<0) continue;
            if (faces[f.neighbours[k]].visited) continue;
            Joint J;
            if (J.buildFromBranch(faces,vertices,edges,(uint32_t)i,(uint8_t)k)) {
                // LOG_INFO("Built joint %d: %d faces",joints.size(),J.face_ids.size());
                if (J.connected_joint) {
                    connected[J.connected_1].joints.push_back((int)joints.size());
                    connected[J.connected_2].joints.push_back((int)joints.size());
                    connected[J.connected_1].add_joint(J.connected_2,joints.size());
                    connected[J.connected_2].add_joint(J.connected_1,joints.size());
                }
                joints.push_back(J);
                break;
            }
        }
    }
    LOG_INFO("Built %d joints",joints.size());
    for (size_t i=0;i<connected.size();i++) {
        // printf("Ordering Connected Joints %d\n",(int)i);
        Joint::orderJointVector(connected[i].contours, connected[i].joints,joints,faces);
        // Finally build the orientation histograms along the neighbourhoods
        connected[i].buildOrientationHistograms(joints);
    }
    return true;
}

bool EBSDAnalyzer::update_face_and_joints() 
{
    for (size_t i=0;i<faces.size();i++) {
        Face & f = faces[i];
        f.updateFaceType(vertices,edges);
    }
    for (size_t i=0;i<grains.size();i++) {
        grains[i].joints.clear();
        Grain::NeighbourMap::iterator it;
        for (it=grains[i].neighbours.begin();it != grains[i].neighbours.end();it++) {
            it->second.joints.clear();
        }
    }
    for (size_t i=0;i<joints.size();i++) {
        Joint & J = joints[i];
        J.updateGrains(vertices,edges);
        if (J.grain_joint) {
            grains[J.grain_1].joints.push_back((int)i);
            grains[J.grain_2].joints.push_back((int)i);
            grains[J.grain_1].add_joint(J.grain_2,i);
            grains[J.grain_2].add_joint(J.grain_1,i);
        }
    }
    for (size_t i=0;i<grains.size();i++) {
        // printf("Ordering Grain Joints %d\n",(int)i);
        Joint::orderJointVector(grains[i].contours, grains[i].joints,joints,faces);
        // Finally build the orientation histograms along the neighbourhoods
        grains[i].buildOrientationHistograms(joints);
    }
    return true;
}

bool EBSDAnalyzer::build_grain_groups(bool refine) {
#if defined(QUIET_QHULL) && defined(USE_PCL)
    int sout = dup(fileno(stdout));
    int serr = dup(fileno(stderr));
    if (!freopen("/dev/null","a",stderr)) perror("reopen stderr");
    dup2(sout,fileno(stdout));
    close(sout);
#endif
    while (1) {
        LOG_INFO("Building grain groups");
        grains.clear();
        Graph graph((unsigned int)connected.size());
        for (size_t i=0;i<connected.size();++i) {
            if (!connected[i].valid) continue;
            // if (connected[i].embedded) continue;
            Connected::NeighbourMap::iterator it;
            for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
                size_t j = it->first;
                if (!connected[j].valid) continue;
                if (j < i) continue;
                if (it->second.active) { // == TWINNING_TENSILE
                    graph.add_connection(i,j);
                }
            }
        }
        size_t ngroups = graph.find_connected_components();
        // LOG_INFO("Extracted %d grain groups",ngroups);

        grains.resize(ngroups);
        for (size_t i=0;i<connected.size();++i) {
            size_t group = graph.membership(i);
            connected[i].group = (int)group;
            grains[group].i = (int)group;
            grains[group].parts.push_back((unsigned int)i);
            // just in case
            grains[group].border_hull.clear();
        }
        
        update_grain_groups();
        LOG_INFO("Created %d grain groups",(int)grains.size());

        if (refine) {
            // Now find, if we need to break links
            // If we have multiple thread we first need to distribute the
            // grains, but we want to balance that based on the number of edges
            std::vector<RGData*> data(num_threads);
            // Reset the refined status
            // TODO: remove that and only mark a grain to be refined if one of the
            // links inside changed.
            // for (size_t g=0;g<grains.size();++g) {
            //     grains[g].refined = false;
            // }
            std::vector<RGEdge> rg_edges;
            for (size_t g=0;g<grains.size();++g) {
                // if (grains[g].refined) continue;
                grains[g].refined = true;
                for (size_t c=0;c<grains[g].parts.size();++c) {
                    Connected::NeighbourMap::iterator it;
                    // This is a fully embedded object, no need to try to
                    // disconnect it.
                    if (connected[grains[g].parts[c]].neighbours.size()<=1) continue;
                    for (it=connected[grains[g].parts[c]].neighbours.begin();
                            it != connected[grains[g].parts[c]].neighbours.end();it++) {
                        // printf("Edge %d %d - %d\n",(int)it->first,it->second.i,it->second.j);
                        size_t j = it->first;
                        if (j < grains[g].parts[c]) continue;
                        if (connected[j].group != (signed)g) continue;
                        // This is a fully embedded object, no need to try to
                        // disconnect it.
                        if (connected[j].neighbours.size()<=1) continue;
                        if (it->second.manual) continue;
                        // if (connected[it->second.j].embedded) continue;
                        if (it->second.active) { 
                            rg_edges.push_back(RGEdge(g,grains[g].parts[c],j));
                        }
                    }
                }
            }
            // Something to do?
            if (rg_edges.size()==0) {
                break;
            }

            size_t rgi = 0;
            for (size_t i=0;i<num_threads;++i) {
                data[i] = new RGData;
                data[i]->that = this;
                data[i]->groups.clear();
                for (;rgi < std::min(rg_edges.size(),(i+1)*rg_edges.size()/num_threads);++rgi) {
                    data[i]->groups.push_back(rg_edges[rgi]);
                }
                QObject::connect(data[i], SIGNAL(finished()), data[i], SLOT(deleteLater()));
                data[i]->start();
            }
            // Now all the threads are running
            for (size_t i=0;i<num_threads;++i) {
                data[i]->wait();
            }
            // Back to single thread here
            bool done_something = false;
            for (size_t i=0;i<num_threads;++i) {
                for (size_t j=0;j<data[i]->groups.size();++j) {
                    RGEdge & rge = data[i]->groups[j];
                    if (rge.cut_it) {
                        LOG_INFO("Splitting group %d, edge %d - %d",(int)rge.g,(int)rge.i,(int)rge.j);
                        bool done_one = do_split_group(rge.g,rge.i,rge.j,rge.gsplit);
                        done_something = done_something || done_one;
                    }
                }
            }
            // Nothing worked, let's leave
            if (!done_something) {
                refine = false;
                LOG_INFO("Finished grain refinement");
            } else {
                continue;
            }
        }
        if (!refine) {
            break;
        }
    }
#if defined(QUIET_QHULL) && defined(USE_PCL)
    fflush(stderr);
    dup2(serr,fileno(stderr));
    close(serr);
#endif
    if (!find_grain_clusters()) return false;
    if (!update_twinning_order()) return false;
    return true;
}

bool EBSDAnalyzer::do_split_group(size_t g, size_t edge_i, size_t edge_j, Grain gsplit[2]) {
    Grain & G = grains[g];
    for (size_t p=0;p<G.parts.size();++p) {
        size_t i = G.parts[p];
        if (i != edge_i) continue;
        Connected::NeighbourMap::iterator it;
        it = connected[i].neighbours.find((unsigned int)edge_j);
        assert(it != connected[i].neighbours.end());
        it->second.active = false;
    }
#if 0
    // No need to do that in the current implementation since we 
    // will rebuild the grains anyway
    grains[g] = gsplit[0];
    grains[g].i = g;
    size_t n = grains.size();
    gsplit[1].i = n;
    grains.push_back(gsplit[1]);
    update_group_ids(grains[g]);
    update_group_ids(grains[n]);
#endif
    return true;
}



bool EBSDAnalyzer::test_split_group(size_t g, size_t edge_i, size_t edge_j, Grain gsplit[2]) const {
    const Grain & G = grains[g];
    std::map<size_t,size_t> dict_parts;
    Graph graph((unsigned int)G.parts.size());
    for (size_t p=0;p<G.parts.size();++p) {
        dict_parts[G.parts[p]] = p;
    }
    for (size_t p=0;p<G.parts.size();++p) {
        size_t i = G.parts[p];
        if (!connected[i].valid) continue;
        Connected::NeighbourMap::const_iterator it;
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            size_t j = it->first;
            if (!connected[j].valid) continue;
            // Ignore the edge we want to delete
            if ((i == edge_i) && (j == edge_j)) continue;
            if (j < i) continue;
            if (connected[j].group != connected[i].group) continue;
            if (it->second.active) { // == TWINNING_TENSILE
                // Wrong indices i and j
                graph.add_connection(p,dict_parts[j]);
            }
        }
    }
    size_t ngroups = graph.find_connected_components();
    // LOG_INFO("Extracted %d grain groups",ngroups);

    if (ngroups < 2) {
        // This edge cannot be used to separate the grains. We're done.
        return false;
    }
    // By construction, this has to be true
    assert(ngroups == 2);
            

    // Create two additional groups
    std::map<size_t,size_t> cgroup;
    // We cannot use G after resizing
    // WARNING: the loop below modifies data everywhere in the class. We'll need
    // to re-build the grain properties after it
    for (size_t p=0;p<grains[g].parts.size();++p) {
        size_t group = graph.membership(p);
        cgroup[grains[g].parts[p]] = group;
        // if (g <= 1) {
        //     printf("G %d P %d -> %d\n",(int)g,(int)grains[g].parts[p],(int)group);
        // }
        gsplit[group].i = (int)group;
        gsplit[group].parts.push_back(grains[g].parts[p]);
    }

    update_group_properties(gsplit[0],cgroup);
    update_group_properties(gsplit[1],cgroup);
    // Split convexity measure
    double conv_ratio = grains[g].area / (gsplit[0].convex_area + gsplit[1].convex_area);
    // LOG_INFO("Grain %d %.2f split %d %d %f",(int)g,grains[g].convexity_ratio,(int)edge_i,(int)edge_j,conv_ratio);
    return (conv_ratio > grains[g].convexity_ratio+0.03);
}


void EBSDAnalyzer::update_group_ids(Grain & gg) {
    std::vector<uint32_t>::const_iterator sit = gg.parts.begin();
    for (;sit!=gg.parts.end();sit++) {
        uint32_t j = *sit;
        Connected & G = connected[j];
        G.group = gg.i;
    }
}

#ifdef USE_PCL
struct QHullXYZ : public pcl::ConvexHull<pcl::PointXYZ> {
    public:
        QHullXYZ() {
            reset_flags();
        }
        void reset_flags() {
            // qhull_flags += " TO /dev/null ";
        }
};
#endif

void EBSDAnalyzer::update_group_properties(Grain & gout) {
    std::map<size_t,size_t> empty;
    update_group_properties(gout,empty);
    std::vector<uint32_t>::const_iterator sit = gout.parts.begin();
    for (;sit!=gout.parts.end();sit++) {
        size_t j = *sit;
        Connected & G = connected[j];
        G.grain_border_vertices.clear();
        // Update grain borderness
        std::vector<uint32_t> all_vertices = G.vertices;
        all_vertices.insert(all_vertices.end(),G.expanded_vertices.begin(),G.expanded_vertices.end());
        for (size_t i=0;i<all_vertices.size();++i) {
            Vertex & v = vertices[all_vertices[i]];
            v.grain = gout.i;
            // To be a grain border, one needs to be a connected part border first
            if (!v.border) continue;
            v.grain_border = false;
            ICoord neighbours_6[6] = {
                ICoord(v.i+2,v.j),
                ICoord(v.i+1,v.j+1),
                ICoord(v.i-1,v.j+1),
                ICoord(v.i-2,v.j),
                ICoord(v.i-1,v.j-1),
                ICoord(v.i+1,v.j-1)
            };
            ICoord neighbours_4[4] = {
                ICoord(v.i+1,v.j),
                ICoord(v.i-1,v.j),
                ICoord(v.i,v.j+1),
                ICoord(v.i,v.j-1),
            };
            ICoord *neighbours=NULL;
            if (square_mesh) {
                neighbours = neighbours_4;
            } else {
                neighbours = neighbours_6;
            }

            for (size_t k=0;k<(square_mesh?4:6);++k) {
                CoordMap::iterator it;
                it = coords.find(neighbours[k]);
                // Is it a point on the border of the measured area?
                if (it == coords.end()) {
                    G.map_edge = true;
                    v.grain_border = true;
                    continue;
                }
                const Vertex & w = vertices[it->second];
                Connected & Gw = connected[w.membership];
                if (Gw.group != G.group) {
                    v.grain_border = true;
                }
            }
            if (v.grain_border) {
                G.grain_border_vertices.push_back(all_vertices[i]);
            }
        }
    }
}

void EBSDAnalyzer::update_group_properties(Grain & gout, const std::map<size_t,size_t> & cgroup) const {
    gout.disp_x = gout.disp_y = gout.x = gout.y = 0.0;
    gout.border_size = gout.size = 0;
    gout.neighbours.clear();
    std::vector<uint32_t>::iterator sit = gout.parts.begin();
    gout.border_vertices.clear();
    for (;sit!=gout.parts.end();sit++) {
        size_t j = *sit;
        const Connected & G = connected[j];
        // Compute the new barycenters
        gout.size += G.size();
        gout.x += G.x*G.size();
        gout.y += G.y*G.size();
        gout.disp_x += G.x;
        gout.disp_y += G.y;
        // Update grain borderness
        std::vector<uint32_t> all_vertices = G.vertices;
        all_vertices.insert(all_vertices.end(),G.expanded_vertices.begin(),G.expanded_vertices.end());
        for (size_t i=0;i<all_vertices.size();++i) {
            const Vertex & v = vertices[all_vertices[i]];
            // To be a grain border, one needs to be a connected part border first
            if (!v.border) continue;

            ICoord neighbours_6[6] = {
                ICoord(v.i+2,v.j),
                ICoord(v.i+1,v.j+1),
                ICoord(v.i-1,v.j+1),
                ICoord(v.i-2,v.j),
                ICoord(v.i-1,v.j-1),
                ICoord(v.i+1,v.j-1)
            };
            ICoord neighbours_4[4] = {
                ICoord(v.i+1,v.j),
                ICoord(v.i-1,v.j),
                ICoord(v.i,v.j+1),
                ICoord(v.i,v.j-1),
            };
            ICoord *neighbours=NULL;
            if (square_mesh) {
                neighbours = neighbours_4;
            } else {
                neighbours = neighbours_6;
            }

            bool v_grain_border = false; // Can't update the vertex one, it is read-only here
            for (size_t k=0;k<(square_mesh?4:6);++k) {
                CoordMap::const_iterator it;
                it = coords.find(neighbours[k]);
                // Is it a point on the border of the measured area?
                // NOTE: we can't move this part to the update_group_properties
                // part because we need it when evaluating links to break.
                if (it == coords.end()) {
                    v_grain_border = true;
                    gout.map_edge = true;
                    continue;
                }
                const Vertex & w = vertices[it->second];
                const Connected & Gw = connected[w.membership];
                if (Gw.group != G.group) {
                    v_grain_border = true;
                    break;
                } else if (!cgroup.empty()) {
                    std::map<size_t,size_t>::const_iterator vit = cgroup.find(v.membership);
                    std::map<size_t,size_t>::const_iterator wit = cgroup.find(w.membership);
                    assert(vit != cgroup.end());
                    assert(wit != cgroup.end());
                    if (vit != wit) {
                        v_grain_border = true;
                        break;
                    }
                }
            }
            if (v_grain_border) {
                gout.border_vertices.push_back(all_vertices[i]);
            }
        }
        // Now update grain neighbours by going through the connected component
        // neighbours
        Connected::NeighbourMap::const_iterator it;
        for (it=G.neighbours.begin();it != G.neighbours.end();it++) {
            size_t j = it->first;
            if (!connected[j].valid) continue;
            bool outside_neighbour = false;
            if (cgroup.empty()) {
                outside_neighbour = (G.group != connected[j].group);
            } else {
                std::map<size_t,size_t>::const_iterator cgit = cgroup.find(j);
                outside_neighbour = ((cgit == cgroup.end()) || (G.group != (int)cgit->second));
            }
            if (outside_neighbour) {
                // This neighbour is not in the grain
                tf::Quaternion q = QDisorientation(G.qmean,connected[j].qmean).second;
                gout.add_neighbour(connected[j].group, it->second.weight,q);
                gout.border_size += it->second.weight;
            }
        }
    }
    gout.x /= gout.size;
    gout.y /= gout.size;
    gout.disp_x /= gout.parts.size();
    gout.disp_y /= gout.parts.size();
    gout.area = gout.size * MeshArea;
    gout.border_length = gout.border_size * MeshSize;


    if (gout.border_hull.size()==0) {
#ifdef USE_PCL
        // Now evaluate convexity, but this cannot change after an update
        pcl::PointCloud<pcl::PointXYZ>::Ptr border(new pcl::PointCloud<pcl::PointXYZ>);
        gout.border_hull.clear();
        border->resize(gout.border_vertices.size());
        for (size_t i=0;i<gout.border_vertices.size();++i) {
            const Vertex & v = vertices[gout.border_vertices[i]];
            (*border)[i].x = v.x+1e-4*cos(2*i*M_PI/gout.border_vertices.size());
            (*border)[i].y = v.y+1e-4*sin(2*i*M_PI/gout.border_vertices.size());
            (*border)[i].z = 0;
        }
        QHullXYZ chull;
        chull.setInputCloud(border);
        chull.setDimension(2);
        chull.setComputeAreaVolume(true);
        chull.reset_flags();
        // TODO: The requirement for mutex here is a waste and a bug in
        // libqhull. To be verified with a good internet connection.
        lock_connected();
        chull.reconstruct(gout.border_hull);
        unlock_connected();
        gout.convex_area = chull.getTotalArea();
#endif
#ifdef USE_CGAL
        // Now evaluate convexity, but this cannot change after an update
        std::vector<cgal_point> border,result,result_exp;
        gout.border_hull.clear();
        for (size_t i=0;i<gout.border_vertices.size();++i) {
            const Vertex & v = vertices[gout.border_vertices[i]];
            // border.push_back(cgal_point(v.x+1e-4*cos(2*i*M_PI/gout.border_vertices.size()),
            //             v.y+1e-4*sin(2*i*M_PI/gout.border_vertices.size())));
            border.push_back(cgal_point(v.x, v.y));
        }
        // This mutex is only required because CGAL is no re-entrant
        lock_connected();
        CGAL::convex_hull_2( border.begin(), border.end(), 
                std::back_inserter(result) );
        unlock_connected();
        gout.border_hull = CrystalPolygon(result.begin(),result.end());
#if 0
        FILE *fp_b, *fp_fp;
        fp_b = fopen("/tmp/border.txt","w");
        for (size_t i=0;i<gout.border_hull.size();++i) {
            Kf::Point_2 Pf = to_inexact(gout.border_hull[i]);
            fprintf(fp_b,"%e %e\n",Pf.x(),Pf.y());
        }
        fclose(fp_b);
        fp_fp = fopen("/tmp/footprint.txt","w");
        for (size_t i=0;i<ebsd_footprint.size();++i) {
            Kf::Point_2 Pf = to_inexact(ebsd_footprint[i]);
            fprintf(fp_fp,"%e %e\n",Pf.x(),Pf.y());
        }
        fclose(fp_fp);
#endif
        // This mutex is only required because CGAL is no re-entrant
        CrystalPolygonWithHoles borderPM;
        lock_connected();
        try {
            borderPM = CGAL::minkowski_sum_2(gout.border_hull,ebsd_footprint);
        } catch (CGAL::Assertion_exception e) {
            LOG_ERROR("Cgal assumption violation in minkowski_sum_2 for grain %d. Border hull size is %d",int(gout.i),int(gout.border_hull.size()));
        } catch (CGAL::Precondition_exception e) {
            LOG_ERROR("Cgal precondition exception in minkowski_sum_2 for grain %d. Border hull size is %d",int(gout.i),int(gout.border_hull.size()));
        }
        unlock_connected();
        gout.border_hull = borderPM.outer_boundary();
        gout.convex_area = to_inexact(gout.border_hull.area());
#endif
        if (gout.border_hull.size()>0) {
            gout.convexity_ratio = gout.area / gout.convex_area;
        } else {
            gout.convexity_ratio = 0;
        }
        // printf("Group %d: %d A %.2f cA %.2f %.1f\n",(int)gout.i,(int)gout.size,gout.area,gout.convex_area,100*gout.convexity_ratio);
    }
    fflush(stdout); fflush(stderr);
}

ConnectedCluster& EBSDAnalyzer::get_vertex_cluster(int vertex_id) {
    assert(vertex_id < (int)vertices.size());
    const Vertex & v = vertices[vertex_id];
    const Connected & G = connected[v.membership];
    assert(G.cluster >= 0);
    return clusters[G.cluster];
}

ConnectedCluster& EBSDAnalyzer::get_grain_cluster(int grain_id) {
    assert(grain_id < (int)connected.size());
    const Connected & G = connected[grain_id];
    assert(G.cluster >= 0);
    return clusters[G.cluster];
}

Grain& EBSDAnalyzer::get_vertex_group(int vertex_id) {
    assert(vertex_id < (int)vertices.size());
    const Vertex & v = vertices[vertex_id];
    const Connected & G = connected[v.membership];
    assert(G.group >= 0);
    return grains[G.group];
}

Grain& EBSDAnalyzer::get_grain_group(int grain_id) {
    assert(grain_id < (int)connected.size());
    const Connected & G = connected[grain_id];
    assert(G.group >= 0);
    return grains[G.group];
}

class ConditionSetTwinningOrder {
    protected:
        const std::vector<Connected> & connected;
        int curr_order;
    public:
        ConditionSetTwinningOrder(const std::vector<Connected> & conn, int current_order):connected(conn),curr_order(current_order) {
        }
        bool operator()(unsigned int i) const {
            return (connected[i].twinning_order > curr_order)
                || (connected[i].twinning_order < 0);
        }
};

class ActionSetTwinningOrder {
    protected:
        std::vector<Connected> & connected;
        int cluster;
        int twinning_order;
    public:
        ActionSetTwinningOrder(std::vector<Connected> & conn, size_t c_id, int order):connected(conn),cluster((int)c_id), twinning_order(order) {
        }
        bool operator()(unsigned int i) const {
            if (connected[i].cluster == cluster) {
                connected[i].twinning_order = twinning_order;
            }
            return true;
        }
};

bool EBSDAnalyzer::update_twinning_order() {
    // This assumes find_grain_clusters has run before
    std::vector<size_t> connQ;
    size_t count = 0;
    for (size_t i=0;i<connected.size();++i) {
        if (!connected[i].valid) continue;
        if ((connected[i].parent || connected[i].manual_parent) 
                && !connected[i].manual_twin) {
            connected[i].twinning_order = 0;
            count += 1;
        } else {
            connected[i].twinning_order = -1;
            connQ.push_back(i);
        }
    }
    if (MaterialType == MAT_MARTENSITE) {
        LOG_INFO("Martensite: in development, not updating twinning order");
        for (size_t i=0;i<connected.size();++i) {
            connected[i].twinning_order = 0;
        }
    } else {
        int current_order = 0;
        LOG_INFO("%d fragment with twinning order 0",(int)count);
        Graph cgraph((unsigned int)connected.size());
        for (size_t i=0;i<connected.size();i++) {
            Connected::NeighbourMap::const_iterator it;
            for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
                if (!it->second.active) continue;
                cgraph.add_connection(i,it->first);
            }
        }
        while (1) {
            count = 0;
            std::vector<size_t> newQ;
            for (size_t j=0;j<connQ.size();j++) {
                size_t i = connQ[j];
                //printf("T %d C %d O %d V %d M %d%d\n",(int)current_order,(int)i,connected[i].twinning_order,connected[i].valid,connected[i].manual_parent,connected[i].manual_twin);
                if (connected[i].twinning_order != -1) continue;
                Connected::NeighbourMap::const_iterator it;
                for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
                    // printf("-> %d A %d V %d T %d \n",(int)it->first,
                    //          (int)it->second.active,(int)connected[it->first].valid,
                    //          (int)connected[it->first].twinning_order);
                    if (!it->second.active) continue; // implicitly, same grain
                    if (!connected[it->first].valid) continue;
                    if (connected[it->first].twinning_order == -1) continue;
                    if (connected[it->first].twinning_order != current_order) continue;
                    // at this point connected[it->first].twinning_order == current_order
                    ConnectedCluster & C = clusters[connected[i].cluster];
                    C.twinning_order = current_order + 1;
#if 0
                    for (size_t j=0;j<C.parts.size();j++) {
                        if (!connected[C.parts[j]].parent) {
                            // the test takes care of secondary twins with the same
                            // orientation as the parent, so in the same cluster
                            connected[C.parts[j]].twinning_order = current_order + 1;
                        }
                        // printf("%d: %d\n",current_order+1,(int)C.parts[j]);
                        count += 1;
                    }
#else
                    ConditionSetTwinningOrder csto(connected,current_order); 
                    ActionSetTwinningOrder asto(connected, connected[i].cluster,current_order+1);
                    count += cgraph.floodfill(i,csto,asto);
#endif
                    // printf("   -> OK %d marked\n",(int)count);
                    break;
                }
                if (connected[i].valid && (connected[i].twinning_order == -1)) {
                    assert(newQ.size()<connected.size());
                    newQ.push_back(i);
                }
            }
            LOG_INFO("%d fragment with twinning order %d, remaining %d",(int)count,current_order+1,(int)newQ.size());
            assert((count>0)||(newQ.size()==0));
            if (newQ.empty()) {
                break;
            }
            connQ = newQ;
            current_order += 1;
        }
    }
    // Now mark inactive any link that are probably inactive...
    for (size_t i=0;i<connected.size();i++) {
        if (!connected[i].valid) continue;
        Connected::NeighbourMap::iterator it;
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            it->second.ignore_for_phases = false;
            if (it->second.manual) continue;
            if (!it->second.active) continue;
            if (connected[i].twinning_order == connected[it->first].twinning_order) {
                it->second.ignore_for_phases = true;
            }
        }
    }

    phases.clear();
    for (size_t i=0;i<grains.size();i++) { 
        phases.push_back(Phase(grains[i],connected,joints,phases));
        // printf("Grain %d inserted as phase %d\n",(int)i,(int)phases.size()-1);
    }
    for (size_t i=0;i<phases.size();i++) { 
        // printf("Updating phase %d\n",(int)i);
        phases[i].id = i;
        phases[i].update_statistics(connected,phases);
        for (size_t j=0;j<phases[i].parts.size();j++) {
            connected[phases[i].parts[j]].phase = (int)i;
        }
    }

#if 1
    // Now identify twins with weird parents
    for (size_t i=0;i<connected.size();i++) {
        if (!connected[i].valid) continue;
        Connected & C = connected[i];
        Connected::NeighbourMap::iterator it;
        std::set<int> variants;
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            if (!it->second.active) continue;
            if (it->second.ignore_for_phases) continue;
            Connected & D = connected[it->first];
            if ((C.twinning_order-D.twinning_order)!=1) continue;
            variants.insert(D.neighbours[C.i].twinning_variant);
        }
        C.has_weird_parents = variants.size()>1;
    }
#endif


    for (size_t i=0;i<twinstrips.size();i++) {
        twinstrips[i].twinning_order = connected[twinstrips[i].parts[0]].twinning_order;
    }

    return true;
}

bool EBSDAnalyzer::find_grain_clusters() {
    Graph graph((unsigned int)connected.size());
    clusters.clear();
    for (size_t i=0;i<connected.size();++i) {
        if (!connected[i].valid) continue;
        for (size_t j=0;j<i;++j) {
            if (!connected[j].valid) continue;
            if (connected[i].group != connected[j].group) continue;
            double disorientation = QDisorientation(connected[i].qmean,connected[j].qmean).first;
            double weight = exp(-SQR(disorientation)/(2*SigmaSqr));
            if (weight > MinWeight) {
                graph.add_connection(i,j);
            }
        }
    }
    size_t ngroups = graph.find_connected_components();
    LOG_INFO("Extracted %d grain clusters",(int)ngroups);

    clusters.resize(ngroups);
    for (size_t i=0;i<connected.size();++i) {
        if (!connected[i].valid) continue;
        size_t cluster = graph.membership(i);
        // printf("Grain %d in group %d size %d\n",(int)i,(int)group,(int)size);
        connected[i].cluster = (int)cluster;
        clusters[cluster].group = connected[i].group;
        clusters[cluster].parts.push_back((unsigned int)i);
    }
    // Now compute the new barycenters
    for (size_t i=0;i<clusters.size();++i) {
        ConnectedCluster & gg=clusters[i];
        std::vector<tf::Quaternion> qmeans;
        std::vector<double> weights;
        gg.size = 0;
        gg.x = gg.y = 0;
        std::vector<uint32_t>::const_iterator sit = gg.parts.begin();
        for (;sit!=gg.parts.end();sit++) {
            size_t j = *sit;
            const Connected & C = connected[j];
            qmeans.push_back(C.qmean);
            weights.push_back(C.size());
            gg.size += C.size();
            gg.x += C.x;
            gg.y += C.y;
        }
        gg.q_mean = QDisorientation(QRef,QMeanWithSymmetries(qmeans, weights)).second;
        gg.x /= gg.parts.size();
        gg.y /= gg.parts.size();
    }
    LOG_INFO("Created %d grain clusters",(int)clusters.size());

    // Now find parent cluster
    for (size_t i=0;i<connected.size();++i) {
        if (connected[i].manual_twin) {
            connected[i].parent = false;
        }
        if (!connected[i].manual_parent) {
            connected[i].parent = false;
        }
    }
    for(std::vector<Grain >::iterator it = grains.begin();it != grains.end(); it++) {
        size_t biggest_cluster = 0;
        int biggest_size = -1; // exists by construction
        std::vector<uint32_t>::const_iterator sit = it->parts.begin();
        for (;sit!=it->parts.end();sit++) {
            size_t j = *sit;
            if (connected[j].manual_twin) {
                continue;
            }
            int c = connected[j].cluster;
            assert(c>=0);
            if (connected[j].manual_parent) {
                biggest_cluster = c;
                biggest_size = (int)clusters[c].size;
                break;
            }
            if ((int)clusters[c].size > biggest_size) {
                biggest_cluster = c;
                biggest_size = (int)clusters[c].size;
            }
        }
        it->parent_cluster = biggest_cluster;
        clusters[biggest_cluster].parent = true;
        std::vector<uint32_t>::iterator git = clusters[biggest_cluster].parts.begin();
        for (;git!=clusters[biggest_cluster].parts.end();git++) {
            connected[*git].parent = !connected[*git].manual_twin;  
        }
        // Now update twinned border
        it->twin_border_vertices.clear();
        std::vector<tf::Quaternion> qparts;
        std::vector<double> qweight;
        for (sit=it->parts.begin();sit!=it->parts.end();sit++) {
            const Connected & C = connected[*sit];
            if (C.parent) {
                qparts.push_back(C.qmean);
                qweight.push_back(C.area);
            } else for (size_t j=0;j<C.grain_border_vertices.size();++j) {
                it->twin_border_vertices.push_back(C.grain_border_vertices[j]);
            }
        }
        if (qparts.size()>0) {
            it->qmean = QDisorientation(QRef,QMeanWithSymmetries(qparts,qweight)).second;
        }
    };
    // Now it's time to identify twin strips
    for (size_t i=0;i<connected.size();++i) {
        connected[i].ingrain_distance.clear();
    }
#ifdef USE_CGAL
    for (size_t i=0;i<connected.size();++i) {
        if (!connected[i].valid) continue;
        if (connected[i].parent) continue;
        try{
            PointSet psi;
            for (size_t pi=0;pi<connected[i].border_vertices.size();pi++) {
                const Vertex & V = vertices[connected[i].border_vertices[pi]];
                psi.insert(cgal_pointf(V.x,V.y));
            }
            for (size_t j=0;j<i;++j) {
                if (!connected[j].valid) continue;
                if (connected[j].parent) continue;
                if (connected[i].group != connected[j].group) continue;
                double closest = -1;
                cgal_pointf Pi_closest(0,0),Pj_closest(0,0);
                for (size_t pj=0;pj<connected[j].border_vertices.size();pj++) {
                    const Vertex & V = vertices[connected[j].border_vertices[pj]];
                    cgal_pointf Pj(V.x,V.y);
                    PointSet::Vertex_handle  vhandle = psi.nearest_neighbor(Pj);
                    double dist2 = CGAL::squared_distance(vhandle->point(),Pj);
                    if (!pj || (dist2 < closest)) {
                        closest = dist2;
                        Pj_closest = Pj;
                        Pi_closest = vhandle->point();
                    }
                }
                // printf("%5d -> %5d : %.3f\n",(int)i,(int)j,sqrt(closest));
                connected[i].ingrain_distance[j] = Connected::ClosestPoint(sqrt(closest),Pj_closest.x(),Pj_closest.y());
                connected[j].ingrain_distance[i] = Connected::ClosestPoint(sqrt(closest),Pi_closest.x(),Pi_closest.y());
            }
        } catch (CGAL::Assertion_exception e) {
            LOG_ERROR("Cgal assumption violation in point_set");
        }
    }
#endif

    // Now it's time to identify twin strips
    Graph stripgraph((unsigned int)connected.size());
    for (size_t i=0;i<connected.size();++i) {
        if (!connected[i].valid) continue;
        if (connected[i].parent) continue;
        for (size_t j=0;j<i;++j) {
            if (!connected[j].valid) continue;
            if (connected[j].parent) continue;
            if (connected[i].group != connected[j].group) continue;
            // This computation is a waste, we've done it before
            double disorientation = QDisorientation(connected[i].qmean,connected[j].qmean).first;
            double weight = exp(-SQR(disorientation)/(2*SigmaSqr));
            if (weight > MinWeight) {
                // Now see if these two are part of an artificially cut twin
                double angle_diff = remainder(connected[i].angle-connected[j].angle,M_PI);
                double distance = hypot(connected[i].y-connected[j].y,connected[i].x-connected[j].x);
                double inter_angle = atan2(connected[i].y-connected[j].y,connected[i].x-connected[j].x);
                double inter_length = (connected[i].length+connected[j].length)/2;
                if ((fabs(angle_diff) < 0.1) && (fabs((distance - inter_length)/distance) < 0.2)
                        && (fabs(remainder(inter_angle-connected[i].angle,M_PI)) < 0.05)) {
                    // printf("Potential twinstrip between %d and %d\n",(int)i,(int)j);
                    stripgraph.add_connection(i,j);
                }
            }
        }
    }
    ngroups = stripgraph.find_connected_components();
    std::map<size_t,TwinStrip> tstrip_map;
    std::map<size_t,TwinStrip>::iterator it;
    for (size_t i=0;i<connected.size();++i) {
        // Reset the twinstrip flag
        connected[i].twinstrip = 0;
        size_t tsid = stripgraph.membership(i);
        if (stripgraph.csize(tsid)<2) continue;
        it = tstrip_map.find(tsid);
        if (it == tstrip_map.end()) {
            TwinStrip ts;
            ts.i = (int)tsid;
            ts.parts.push_back((unsigned int)i);
            tstrip_map[tsid] = ts;
        } else {
            it->second.parts.push_back((unsigned int)i);
        }
    }
    twinstrips.clear();
    for (it=tstrip_map.begin();it!=tstrip_map.end();it++) {
        // Ids of twinstrips can be consistent with connected ids.
        it->second.i = (int)(connected.size() + twinstrips.size());
        it->second.assemble(connected,vertices);
        it->second.buildOrientationHistograms(joints);
        twinstrips.push_back(it->second);
    }
    LOG_INFO("Detected %d twinstrip",(int)twinstrips.size());
    return true;
}

class EdgeUpdate : public QThread {
    public:
        EBSDAnalyzer *that;
        size_t e_start, e_end;
        size_t n_solid;
    public:
        void run() {
            n_solid = that->update_edge_type(e_start,e_end);
        }
};

size_t EBSDAnalyzer::update_edge_type(size_t start, size_t end) {
    size_t n_solid = 0;
    for (size_t i=start;i<end;i++) {
        edges[i].updateType();
        if (edges[i].isSolid(vertices)) {
            n_solid ++;
        } 
    }
    return n_solid;
}

bool EBSDAnalyzer::build_connected_parts() {
    Graph graph((unsigned int)vertices.size());
    LOG_INFO("Now creating graph: %ld nodes, %ld edges",(long)vertices.size(),(long)edges.size());
    // Count the number of solid edges and update types
    size_t n_solid = 0;
    if (num_threads<=1) {
        n_solid = update_edge_type(0,edges.size());
    } else {
        std::vector<EdgeUpdate*> data(num_threads);
        size_t e_blocks = (size_t)ceil(edges.size()/(double)num_threads);
        for (size_t i=0;i<num_threads;++i) {
            data[i] = new EdgeUpdate;
            data[i]->that = this;
            data[i]->e_start = i*e_blocks;
            data[i]->e_end = std::min((i+1)*e_blocks,edges.size());
            QObject::connect(data[i], SIGNAL(finished()), data[i], SLOT(deleteLater()));
            data[i]->start();
        }
        for (size_t i=0;i<num_threads;++i) {
            data[i]->wait();
            n_solid += data[i]->n_solid;
        }
    }
    LOG_INFO("%d solid edges",(int)n_solid);
    // And create an edge vector of the correct size
    for (size_t i=0;i<edges.size();++i) {
        if (edges[i].isSolid(vertices)) {
            graph.add_connection(edges[i].i,edges[i].j);
        }
    }
    LOG_INFO("Graph created, kept %d edges",(int)n_solid);
    size_t ngrains = graph.find_connected_components();
    LOG_INFO("Found %d connected compontents",(int)ngrains);
    for (size_t i=0;i<vertices.size();++i) {
        if (vertices[i].good) {
            vertices[i].membership = graph.membership(i);
        } else {
            vertices[i].membership = -1;
        }
    }
    std::vector< Connected > pre_grains;
    pre_grains.resize(ngrains);
    for (size_t i=0;i<vertices.size();++i) {
        if (vertices[i].good) {
            pre_grains[vertices[i].membership].vertices.push_back((unsigned int)i);
        }
    }
    LOG_INFO("Extracted %d connected",(int)ngrains);
    size_t n_grains = 0;
    for (size_t i=0;i<pre_grains.size();++i) {
        pre_grains[i].i = (int)i;
        pre_grains[i].valid = pre_grains[i].vertices.size()*MeshArea >= MinComponentSize;
        if (pre_grains[i].valid) {
            n_grains ++;
        }
    }

    // Finally, mark bad all vertices belonging to invalid connected
    size_t good_vertices = 0;
    for (size_t i=0;i<vertices.size();++i) {
        if (!vertices[i].good)  continue;
        good_vertices += 1;
        if (pre_grains[vertices[i].membership].vertices.size()*MeshArea < MinComponentSize) {
            vertices[i].good = false;
            vertices[i].membership = -1;
        }
    }
    LOG_INFO("Extracted %d connected components, %d good ones",(int)ngrains,(int)n_grains);
    LOG_INFO("Graph destroyed");


    // Reset membership
    for (size_t i=0;i<vertices.size();++i) {
        vertices[i].membership = -1;
    }

    // Re-index connected parts by removing invalid pre_grains
    connected.clear();
    for (size_t i=0;i<pre_grains.size();++i) {
        if (!pre_grains[i].valid) continue;
        size_t j = connected.size();
        connected.push_back(pre_grains[i]);
        connected[j].i = (int)j;
        for (size_t k=0;k<connected[j].vertices.size();++k) {
            vertices[connected[j].vertices[k]].membership = (int32_t)j;
        }
    }
    LOG_INFO("Re-indexed connected parts");

    if ((100.*good_vertices)/vertices.size() < 40) {
        LOG_ERROR("Less than 40%% of good vertices. There is a problem with this map. Aborting");
        return false;
    }

    // Expand connected parts:
    std::deque<size_t> queue;
    for (size_t i=0;i<edges.size();++i) {
        if ((vertices[edges[i].i].membership<0) || (vertices[edges[i].j].membership<0)) {
            queue.push_back(i);
        }
    }
    if ((100.*queue.size())/edges.size() > 50) {
        LOG_ERROR("More than 50%% of bad edges. There is a problem with this map. Aborting");
        return false;
    }

    while (!queue.empty()) {
        std::deque<size_t> new_queue;
        std::map<size_t,size_t> new_membership;
        while (!queue.empty()) {
            size_t i = queue.front();
            queue.pop_front();
            if ((vertices[edges[i].i].membership>=0) 
                    && (vertices[edges[i].j].membership>=0)) {
                // Already processed
                continue;
            }
            if ((vertices[edges[i].i].membership<0) 
                    && (vertices[edges[i].j].membership<0)) {
                // Not yet connected
                new_queue.push_back(i);
                continue;
            }
            if (vertices[edges[i].i].membership>=0) {
                new_membership[edges[i].j] = vertices[edges[i].i].membership;
            }
            if (vertices[edges[i].j].membership>=0) {
                new_membership[edges[i].i] = vertices[edges[i].j].membership;
            }
        }
        std::map<size_t,size_t>::iterator it = new_membership.begin();
        for (;it != new_membership.end();it++) {
            vertices[it->first].membership = (int32_t)it->second;
            connected[it->second].expanded_vertices.push_back((unsigned int)it->first);
        }
        queue = new_queue;
    }
    LOG_INFO("Expanded connected parts");

    return true;
}


bool EBSDAnalyzer::update_grain_properties(QWaitCondition * cond,
        QMutex * sync1, QMutex * sync2,
        size_t v_start, size_t v_end, 
        size_t e_start, size_t e_end, 
        size_t c_start, size_t c_end) {
    size_t n_border = 0;
    assert(v_end <= vertices.size());
    assert(v_start <= v_end);
    assert(e_end <= edges.size());
    assert(e_start <= e_end);
    assert(c_end <= connected.size());
    assert(c_start <= c_end);

    for (size_t i=v_start;i<v_end;++i) {
        Vertex & v = vertices[i];
        v.border = false;
        ICoord neighbours_6[6] = {
            ICoord(v.i+2,v.j),
            ICoord(v.i+1,v.j+1),
            ICoord(v.i-1,v.j+1),
            ICoord(v.i-2,v.j),
            ICoord(v.i-1,v.j-1),
            ICoord(v.i+1,v.j-1)
        };
        ICoord neighbours_4[4] = {
            ICoord(v.i+1,v.j),
            ICoord(v.i-1,v.j),
            ICoord(v.i,v.j+1),
            ICoord(v.i,v.j-1),
        };
        ICoord *neighbours=NULL;
        if (square_mesh) {
            neighbours = neighbours_4;
        } else {
            neighbours = neighbours_6;
        }
        // A point is inside if all its neighbour have the same Grain membership
        for (size_t k=0;k<(square_mesh?4:6);++k) {
            CoordMap::const_iterator it;
            it = coords.find(neighbours[k]);
            // Is it a point on the border of the measured area?
            if (it == coords.end()) {
                v.border = true;
                continue;
            }
            const Vertex & w = vertices[it->second];
            if (w.membership != v.membership) {
                v.border = true;
#if 0
                lock_connected();
                assert(v.membership >= 0);
                assert(w.membership >= 0);
                assert(v.membership == connected[v.membership].i);
                assert(w.membership == connected[w.membership].i);
                assert(v.membership != w.membership);
                assert(connected[v.membership].i != connected[w.membership].i);
                // update Grain neighbours
                connected[v.membership].add_neighbour(w.membership);
                if (v.membership == 0) {
                    printf("After insertion of %d - %d : %d - %d\n",v.membership,w.membership,
                            connected[v.membership].neighbours[w.membership].i,
                            connected[v.membership].neighbours[w.membership].j);
                }
                assert(connected[v.membership].neighbours[w.membership].i == v.membership);
                assert(connected[v.membership].neighbours[w.membership].j == w.membership);
                unlock_connected();
#endif
            }
        }
        if (v.border) {
            lock_connected();
            connected[v.membership].border_vertices.push_back((unsigned int)i);
            unlock_connected();
            n_border ++;
        }
    }
    LOG_INFO("%ld: Computed borderness and Grain neighbourhood: %d nodes on borders",
            (long int)QThread::currentThreadId(),(int)n_border);

    // Now update neighbourhood and count the twinning types associated with the edges
    for (size_t i=e_start;i<e_end;++i) { 
        const Edge & e = edges[i];
        // if (!vertices[e.i].good) continue;
        if (vertices[e.i].membership<0) continue;
        // if (!vertices[e.j].good) continue;
        if (vertices[e.j].membership<0) continue;
        size_t c1 = vertices[e.i].membership;
        size_t c2 = vertices[e.j].membership;
        if (c1 != c2) {
            TwinningType ttype = NO_TWINNING;
            if (e.twinning_error < TwinningTypeTolerance) {
                ttype = e.twinning_type;
            }
            lock_connected();
            connected[c1].add_neighbour((int)c2,(int)i,ttype);
            connected[c2].add_neighbour((int)c1,(int)i,ttype);
            unlock_connected();
        }
    }
    LOG_INFO("%ld: Finished vertice and edge loop",(long int)QThread::currentThreadId());


    if (cond && sync1) {
        cond->wait(sync1);
        //printf("%ld: Starting update statistics\n",(long int)QThread::currentThreadId());
    }
    
    for (size_t i=c_start;i<c_end;++i) {
        // printf("Grain %d, %d nodes\n",(int)i,(int)connected[i].vertices.size());
        if (!connected[i].valid) continue;
        connected[i].updateStatistics(vertices);
        // if ((i%10)==0) {
        //     printf(".");fflush(stdout);
        // }
        tf::Quaternion qs = QDisorientation(QRef,connected[i].qmean).second;
        connected[i].color = EBSDColorSet(qs);
    }

    // printf("\n");fflush(stdout);
    LOG_INFO("%ld: Updated statistics",(long int)QThread::currentThreadId());
    if (cond && sync2) {
        cond->wait(sync2);
        //printf("%ld: Starting neighbours update\n",(long int)QThread::currentThreadId());
    }
    
    for (size_t i=c_start;i<c_end;++i) {
        assert(connected[i].i == (int)i);
        if (!connected[i].valid) continue;
        Connected::NeighbourMap::iterator it;
        //printf("Connected %d: %d neighbours\n",(int)i,(int)connected[i].neighbours.size());
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            assert (it->second.i>=0);
            assert (it->second.j>=0);
            assert (it->second.j==(int)it->first);
            assert (it->second.i==connected[i].i);
            assert (connected[it->first].i==it->second.j);
            assert (connected[it->first].valid);
            it->second.q_disorientation = QDisorientation(connected[i].qmean,connected[it->first].qmean).second;
            assert(!std::isinf(it->second.q_disorientation.w()));
            it->second.embedded = it->second.weight > (SubGrainMinEnclosure * connected[i].num_connections);
            // printf("%d - %d: %d %d %d\n",(int)it->second.i, (int)it->second.j,
            //         (int)it->second.weight,(int)connected[i].num_connections,(int)it->second.embedded);
            it->second.updateTwinning(connected[i]);
            if (!connected[i].embedded && it->second.embedded) {
                // We need the test to make sure we will not unmark the
                // embeddedness depending on the order in which we consider the
                // neighbours.
                connected[i].embedded = it->second.embedded;
            }
        }
    }
    // Yet another pass, we need the previous pass to be completed
    for (size_t i=c_start;i<c_end;++i) {
        if (!connected[i].valid) continue;
        Connected::NeighbourMap::iterator it;
        for (it=connected[i].neighbours.begin();it != connected[i].neighbours.end();it++) {
            assert (connected[it->first].valid);
            // Mark active any link to a degree one connected item.
            if (it->second.weight < MinJointWeight) {
                it->second.active = false;
            }
            if ((connected[it->second.i].neighbours.size()==1)
                    ||(connected[it->second.j].neighbours.size()==1)) {
                it->second.active = true;
                continue;
            }
#if 0
            if (MaterialType == MAT_MAGNESIUM) {
                // We mark any compressing twinning as active for clustering if one
                // of the grain is only connected to its neighbour using
                // compressive twinning.
                if (it->second.twinning_type == Mg_TWINNING_COMPRESSIVE) {
                    size_t i_tensile = connected[it->second.i].count_twinning_type(Mg_TWINNING_TENSILE);
                    size_t j_tensile = connected[it->second.j].count_twinning_type(Mg_TWINNING_TENSILE);
                    // printf("C Edge %d: %d (T %d) to %d (T %d)\n",it->first,it->second.i,(int)i_tensile, it->second.j, (int)j_tensile);
                    if (i_tensile * j_tensile == 0) {
                        it->second.active = true;
                        continue;
                    }
                }
            }
#endif
        }
    }
    if (sync1 && sync2) {
        sync1->unlock();
        sync2->unlock();
    }
    LOG_INFO("%ld: Updated embedded-ness",0xFFF & (long int)QThread::currentThreadId());


    return true;
}

class UGPData : public QThread {
    public:
        EBSDAnalyzer * that;
        size_t v_start, v_end;
        size_t e_start, e_end;
        size_t c_start, c_end;
        QMutex sync1;
        QMutex sync2;
        QWaitCondition cond;
    public:
        void run() {
            that->update_grain_properties(&cond,&sync1,&sync2,
                    v_start,v_end, e_start,e_end, c_start,c_end);
        }
};


bool EBSDAnalyzer::update_grain_properties() {
    for (size_t i=0;i<connected.size();++i) {
        connected[i].border_vertices.clear();
        connected[i].grain_border_vertices.clear();
    }

    if (num_threads <= 1) {
        if (!update_grain_properties(NULL,NULL,NULL,
                0,vertices.size(),
                0,edges.size(),
                0,connected.size())) {
            return false;
        }
        build_face_and_joints();
        return true;
    } 
    size_t v_blocks = (size_t)ceil(vertices.size()/(double)num_threads);
    size_t e_blocks = (size_t)ceil(edges.size()/(double)num_threads);
    size_t c_blocks = (size_t)ceil(connected.size()/(double)num_threads);
    // printf("C %d: v %d e %d c %d\n",(int)connected.size(),(int)v_blocks,(int)e_blocks,(int)c_blocks);
    std::vector<UGPData*> data(num_threads);
    for (size_t i=0;i<num_threads;++i) {
        data[i] = new UGPData;
        data[i]->that = this;
        data[i]->v_start = std::min(i*v_blocks,vertices.size());
        data[i]->v_end = std::min((i+1)*v_blocks,vertices.size());
        data[i]->e_start = std::min(i*e_blocks,edges.size());
        data[i]->e_end = std::min((i+1)*e_blocks,edges.size());
        data[i]->c_start = std::min(i*c_blocks,connected.size());
        data[i]->c_end = std::min((i+1)*c_blocks,connected.size());
        // printf("i%d v %d %d e %d %d c %d %d\n",(int)i,(int)data[i]->v_start,(int)data[i]->v_end,
        //         (int)data[i]->e_start,(int)data[i]->e_end,
        //         (int)data[i]->c_start,(int)data[i]->c_end);
        data[i]->sync1.lock();
        data[i]->sync2.lock();
        QObject::connect(data[i], SIGNAL(finished()), data[i], SLOT(deleteLater()));
        data[i]->start();
    }
    for (size_t i=0;i<num_threads;++i) {
        data[i]->sync1.lock();
    }
    LOG_INFO("update_grain_properties: completed first part");
    for (size_t i=0;i<num_threads;++i) {
        data[i]->cond.wakeAll();
        data[i]->sync1.unlock();
    }
    for (size_t i=0;i<num_threads;++i) {
        data[i]->sync2.lock();
    }
    LOG_INFO("update_grain_properties: completed second part");
    for (size_t i=0;i<num_threads;++i) {
        data[i]->cond.wakeAll();
        data[i]->sync2.unlock();
    }
    for (size_t i=0;i<num_threads;++i) {
        data[i]->wait();
    }

    build_face_and_joints();

    return true;
}

CountHistogram EBSDAnalyzer::getGrainAreaHistogram() const {
    std::vector<double> grainArea;
    for (size_t i=0;i<grains.size();i++) {
        grainArea.push_back(grains[i].area);
    }
    return buildHistogram(grainArea.begin(),grainArea.end());
}




