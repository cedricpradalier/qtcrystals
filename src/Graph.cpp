
#include <stdio.h>
#include <list>
#include "crystals/Graph.h"

using namespace crystals;

size_t Graph::find_connected_components() {
    size_t csize = 0;
    connected_components = 0;
    component_size.clear();
    for (size_t i=0;i<vertices.size();++i) {
        vertices[i].membership = -1;
    }
    for (size_t i=0;i<vertices.size();++i) {
        if (vertices[i].membership>=0) continue;
        // printf("starting at vertex %d\n",(int)i);
        std::list<unsigned int> to_visit;
        to_visit.push_back((unsigned int)i);
        while (!to_visit.empty()) {
            size_t v = to_visit.front();
            to_visit.pop_front();
            if (vertices[v].membership>=0) {
                assert(vertices[v].membership == (int)connected_components);
                continue;
            }
            ++csize;
            vertices[v].membership = (int)connected_components;
            for (GraphVertexNeighbours::const_iterator j=vertices[v].neighbours.begin();
                    j!=vertices[v].neighbours.end();j++) {
                if (vertices[*j].membership<0) {
                    to_visit.push_back(*j);
                } else {
                    assert(vertices[*j].membership == (int)connected_components);
                }
            }
        }
        ++connected_components;
        component_size.push_back((unsigned int)csize);
        csize = 0;
    }
    return connected_components;
}

void Graph::print() const {
    printf("Graph: %d nodes\n",(int)vertices.size());
    for (size_t i=0;i<vertices.size(); ++i) {
        printf("%8d (%4d) -> ",(int)i,vertices[i].membership);
        for (GraphVertexNeighbours::const_iterator j=vertices[i].neighbours.begin();
                j!=vertices[i].neighbours.end();j++) {
            printf("%d ",(int)*j);
        }
        printf("\n");
    }
    printf("Components: %d\n",(int)component_size.size());
    for (size_t i=0;i<component_size.size(); ++i) {
        printf("%4d: %8d nodes",(int)i,(int)component_size[i]);
    }
    printf("----- EOG -----\n");
}



