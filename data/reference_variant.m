  cas= 1
%--------------------------------------------------------------------------
% Euler Angles parent and twin phases - from Cedric s software

 q44=angle2quat(-104.48*degree,21.37*degree,94.19*degree,'ZXZ')
 q441=angle2quat(174.76*degree,32.26*degree,171.22*degree,'ZXZ')
 q442=angle2quat(-7.95*degree,90.79*degree,21.37*degree,'ZXZ')
 q444=angle2quat(-13.0*degree,84.72*degree,22.24*degree,'ZXZ')

 q26=angle2quat(-174.5*degree,17.55*degree,-175.07*degree,'ZXZ')
 q261=angle2quat(123.09*degree,40.51*degree,-120.17*degree,'ZXZ')
 q262=angle2quat(167.32*degree,49.66*degree,-160.33*degree,'ZXZ')
 q263=angle2quat(-151.55*degree,49.91*degree,161.56*degree,'ZXZ')
 q265=angle2quat(9.04*degree,69.84*degree,-0.19*degree,'ZXZ')

 q65=angle2quat(-43.15*degree,27.75*degree,47.31*degree,'ZXZ')
 q651=angle2quat(-130.44*degree,61.99*degree,119.34*degree,'ZXZ')
 q652=angle2quat(23.62*degree,73.28*degree,-1.09*degree,'ZXZ')
 q653=angle2quat(122.25*degree,58.22*degree,-113.27*degree,'ZXZ')
 q654=angle2quat(-173.63*degree,76.73*degree,159.32*degree,'ZXZ')
 q655=angle2quat(124.2*degree,68.06*degree,-112.44*degree,'ZXZ')
 
 q1=q26
 q2=q265

 nn=zeros(6,3)
 bur=zeros(6,3)
 stress=zeros(3,3)
 stress(2,2)=-1.

% Goal is to obtain variant------------------------------------------------  
 
if cas==1

qref_1=[0.,-0.63760758,-0.368122912,0.676714185];
qref_2=[0.,0.,-0.736245823,0.676714185];
qref_3=[0.,0.637607586,-0.368122912,0.676714185];
qref_4=[0.,0.637607586,0.368122912,0.676714185];
qref_5=[0.,0.,0.736245823,0.676714185];
qref_6=[0.,-0.637607586,0.368122912,0.676714185];

bur(1,:)=[-0.63760758,-0.368122912,0.676714185]
bur(2,:)=[0.,-0.736245823,0.676714185]
bur(3,:)=[0.637607586,-0.368122912,0.676714185]
bur(4,:)=[0.637607586,0.368122912,0.676714185]
bur(5,:)=[0.,0.736245823,0.676714185]
bur(6,:)=[-0.637607586,0.368122912,0.676714185]

nn(1,:)=[0.58605,0.33836,0.73625]
nn(2,:)=[0.,0.6767,0.73625]
nn(3,:)=[-0.58605,0.33836,0.73625]
nn(4,:)=[-0.58605,-0.33836,0.73625]
nn(5,:)=[0.,-0.6767,0.73625]
nn(6,:)=[0.58605,-0.33836,0.73625]

elseif cas == 2
    
qref_1=[0.,-0.149819797,-0.259495501,0.954052469]
qref_2=[0.,0.149819797,-0.259495501,0.954052469]
qref_3=[0.,0.299639595,0.,0.954052469]
qref_4=[0.,0.149819797,0.259495501,0.954052469]
qref_5=[0.,-0.149819797,0.259495501,0.954052469]
qref_6=[0.,-0.299639595,0.,	0.954052469]

bur(1,:)=[-0.149819806,-0.259495497,0.954052448]
bur(2,:)=[0.149819806,-0.259495497,0.954052448]
bur(3,:)=[0.299639583,0.0,0.954052448]
bur(4,:)=[0.149819806,0.259495497,0.954052448]
bur(5,:)=[-0.149819806,0.259495497,0.954052448]
bur(6,:)=[-0.299639583,0.0,0.954052448]

nn(1,:)=[0.477026224,0.826233625,0.299639583]
nn(2,:)=[-0.477026224,0.826233625,0.299639583]
nn(3,:)=[-0.954052448,0.0,0.299639583]
nn(4,:)=[-0.477026224,-0.826233625,0.299639583]
nn(5,:)=[0.477026224,-0.826233625,0.299639583]
nn(6,:)=[0.954052448,0.0,0.299639583]

elseif cas ==3
    
qref_1=[0.0,0.265955031,0.460647672,-0.846800864]
qref_2=[0.0,-0.265955031,0.460647672,-0.846800864]
qref_3=[0.0,-0.531910062,0.0,-0.846800864]
qref_4=[0.0,-0.265955031,-0.460647672,-0.846800864]
qref_5=[0.0,0.265955031,-0.460647672,-0.846800864]
qref_6=[0.0,0.531910062,0.0,-0.846800864]

bur(1,:)=[0.265955031,0.460647672,-0.846800864]
bur(2,:)=[-0.265955031,0.460647672,-0.846800864]
bur(3,:)=[-0.531910062,0.0,-0.846800864]
bur(4,:)=[-0.265955031,-0.460647672,-0.846800864]
bur(5,:)=[0.265955031,-0.460647672,-0.846800864]
bur(6,:)=[0.531910062,0.0,-0.846800864]

nn(1,:)=[0.423400283,0.733351171,0.531910121]
nn(2,:)=[-0.423400283,0.733351171,0.531910121]
nn(3,:)=[-0.846800864,0.0,0.531910121]
nn(4,:)=[-0.423400283,-0.733351171,0.531910121]
nn(5,:)=[0.423400283,-0.733351171,0.531910121]
nn(6,:)=[0.846800864,0.0,0.531910121]


end

 qidentity=[1 0 0 0];
 q1inv=quatinv(q1);
 RotMatrix = quat2dcm(q1inv);
 qdeso=quatmultiply(q1inv,q2);

 
 
s.a=0.;
s.b=0.;
s.c=0.;

Q_REFE=repmat(s,6,1);

[Q_REFE(1).a,Q_REFE(1).b,Q_REFE(1).c]=quat2angle(qref_1,'ZXZ');
[Q_REFE(2).a,Q_REFE(2).b,Q_REFE(2).c]=quat2angle(qref_2,'ZXZ');
[Q_REFE(3).a,Q_REFE(3).b,Q_REFE(3).c]=quat2angle(qref_3,'ZXZ');
[Q_REFE(4).a,Q_REFE(4).b,Q_REFE(4).c]=quat2angle(qref_4,'ZXZ');
[Q_REFE(5).a,Q_REFE(5).b,Q_REFE(5).c]=quat2angle(qref_5,'ZXZ');
[Q_REFE(6).a,Q_REFE(6).b,Q_REFE(6).c]=quat2angle(qref_6,'ZXZ');

Q_REFE(1).b=real(Q_REFE(1).b)
Q_REFE(2).b=real(Q_REFE(2).b)
Q_REFE(3).b=real(Q_REFE(3).b)
Q_REFE(4).b=real(Q_REFE(4).b)
Q_REFE(5).b=real(Q_REFE(5).b)
Q_REFE(6).b=real(Q_REFE(6).b)


Q_SYM_Z=repmat(s,6,1);

for j=1:6
Q_SYM_Z(j).a=pi*(j-1)/3.
Q_SYM_Z(j).b=0.
Q_SYM_Z(j).c =0.
end

Q_SYM_X=repmat(s,2,1);

for j=1:2
[Q_SYM_X(j).a,Q_SYM_X(j).b,Q_SYM_X(j).c] =quat2angle(angle2quat(0., 0., pi*(j-1),'ZYX'),'ZXZ');
end

l=0;

qangle=zeros(864,1);
qalltrial=zeros(72,4);
qalldeso=zeros(12,4);
count=0;
count2=0;

   for j=1:6
       
        qz=angle2quat(Q_SYM_Z(j).a,Q_SYM_Z(j).b,Q_SYM_Z(j).c,'ZXZ');
       
    for k=1:2
         count=count+1;
        qx=angle2quat(Q_SYM_X(k).a,Q_SYM_X(k).b,Q_SYM_X(k).c,'ZXZ');
        qtemp_sym1=quatmultiply(qz,qx);
  
           qtemp_deso=quatmultiply(qdeso,qtemp_sym1);
           
        %   qtemp_deso=quatmultiply(qidentity,qdeso);
           
           qalldeso(count,:)=qtemp_deso;

    end 
   end
   
   
   for i=1:6
 
    q_trial=angle2quat(Q_REFE(i).a,Q_REFE(i).b,Q_REFE(i).c,'ZXZ');
    
   for l=1:6        
            qz2=angle2quat(Q_SYM_Z(l).a,Q_SYM_Z(l).b,Q_SYM_Z(l).c,'ZXZ');
            
             for n=1:2
          count2=count2+1;
          qx2=angle2quat(Q_SYM_X(n).a,Q_SYM_X(n).b,Q_SYM_X(n).c,'ZXZ');
          qtemp_sym2=quatmultiply(qz2,qx2);
          

          
   %       qtesttemp=quatmultiply(qtemp_sym2,q_trial);
          
                    qtesttemp=quatmultiply(qtemp_sym2,q_trial);
          
        %   qtesttemp=quatmultiply(qidentity,q_trial);
           qalltrial(count2,:)=qtesttemp;

      
        end
   end      
   end 
   
count3=0;
AA=-10;
BB=zeros(6,1);

     count4=0;
     
     for j=1:6
         for k=1:12
             
      count4=count4+1;
      qtwo=qalltrial(count4,:);

        for i=1:12 
     
      qone=[qalldeso(i,1) qalldeso(i,2) qalldeso(i,3) qalldeso(i,4)];
             
       count3=count3+1;
       
         qtest=quatmultiply(qone,qtwo);  
         qangle(count3,1)=qtest(1);
        
       if qtest(1)>BB(j,1) 
       BB(j,1)=qtest(1);
       end
         
         end
     end    
  end

[AA,I]=max(BB)
2.*acos(AA)*180/pi
BB

RR=quat2dcm(q1inv);
results=zeros(6,1);
for I=1:6
vvec=zeros(3,1);

vvec(1,1)=nn(I,1);
vvec(2,1)=nn(I,2);
vvec(3,1)=nn(I,3);

n_prim=mtimes(RR,vvec);

vvec(1,1)=bur(I,1);
vvec(2,1)=bur(I,2);
vvec(3,1)=bur(I,3);

b_prime=mtimes(RR,vvec);

Schmid=zeros(3,3);

 for j=1:3
     for k=1:3
 Schmid(j,k)=0.5*(n_prim(j)*b_prime(k)+n_prim(k)*b_prime(j));
     end
 end
 
 results(I)=0.;
 for j=1:3
     for k=1:3
 
 results(I)=results(I)+Schmid(j,k)*stress(j,k);
 
     end
 end
end